if(${LFR_X86_USE_SSE2})
  set(rust_x86_arch "i686")
else()
  set(rust_x86_arch "i586")
endif()

if(ANDROID)
  if(SR_ARCH STREQUAL "x86")
    set(RUST_TARGET "${rust_x86_arch}-linux-android")
  elseif(SR_ARCH STREQUAL "x86_64")
    set(PY_ARCH "x86_64")
    set(RUST_TARGET "x86_64-linux-android")
  elseif(SR_ARCH STREQUAL "arm")
    set(PY_ARCH "arm")
    set(RUST_TARGET "arm-linux-androideabi")
  elseif(SR_ARCH STREQUAL "arm64")
    set(PY_ARCH "aarch64")
    set(RUST_TARGET "aarch64-linux-android")
  endif()
else()
  if(SR_ARCH STREQUAL "x86")
    set(PY_ARCH "${rust_x86_arch}")
    set(RUST_TARGET "${rust_x86_arch}-unknown-linux-gnu")
  elseif(SR_ARCH STREQUAL "x86_64")
    set(PY_ARCH "x86_64")
    set(RUST_TARGET "x86_64-unknown-linux-gnu")
  elseif(SR_ARCH STREQUAL "arm")
    if(GCC_ARCH STREQUAL "armv5e")
      set(PY_ARCH $GCC_ARCH)  # PEP 599 doesn't support armv5 platform tags. Investigate supporting PEP 600.
      set(RUST_TARGET "armv5te-unknown-linux-gnueabi")
    else()
      set(PY_ARCH "armv7l")
      set(RUST_TARGET "armv7-unknown-linux-gnueabihf")
    endif()
  elseif(SR_ARCH STREQUAL "arm64")
    set(PY_ARCH "aarch64")
    set(RUST_TARGET "aarch64-unknown-linux-gnu")
  endif()
endif()

set(RUST_TARGET ${RUST_TARGET} CACHE STRING "Rust target")

function(list_transform_prepend var prefix)
    set(temp "")
    foreach(f ${${var}})
        list(APPEND temp "${prefix}${f}")
    endforeach()
    set(${var} "${temp}" PARENT_SCOPE)
  endfunction()

# This is a replacement for cmakes "list(JOIN...)".  That is not supported
#  before cmake 3.12.  I know the yocto 3.4 work uses and older cmake so this
#  is required.
function(JOIN VALUES GLUE OUTPUT)
  string (REGEX REPLACE "([^\\]|^);" "\\1${GLUE}" _TMP_STR "${VALUES}")
  string (REGEX REPLACE "[\\](.)" "\\1" _TMP_STR "${_TMP_STR}") #fixes escaping
  set (${OUTPUT} "${_TMP_STR}" PARENT_SCOPE)
endfunction()

function(cargo_test)
      cmake_parse_arguments(CARGO_TEST "" "NAME" "RUSTFLAGS;COMMAND;CONFIGURATIONS;WORKING_DIRECTORY;" ${ARGN})

      if(RUST_TARGET MATCHES "-musl$")
           list(APPEND CARGO_TEST_RUSTFLAGS -C target-feature=-crt-static)
      endif()  

      JOIN("${CARGO_TEST_RUSTFLAGS}" " " CARGO_TEST_FINAL_RUSTFLAGS)
  
      add_test(
        NAME ${CARGO_TEST_NAME}
        COMMAND ${CMAKE_COMMAND} -E env "RUSTFLAGS=${CARGO_TEST_FINAL_RUSTFLAGS}" ${CARGO_TEST_COMMAND}
        CONFIGURATIONS ${CARGO_TEST_CONFIGURATIONS}
        WORKING_DIRECTORY ${CARGO_TEST_WORKING_DIRECTORY}
      )     
endfunction()

function(cargo_build name)
  cmake_parse_arguments(CARGO "STATIC_LIB;SHARED_LIB;PYTHON;NO_LTO;NO_STD" "" "LIB_NAME;ENV;FLAGS;BUILD_RUSTFLAGS;BUILD_FLAGS;TEST_FLAGS;BINS;WORKDIR;OUT;DEPENDS;TOOLCHAIN" ${ARGN})

  if (NOT (CARGO_PYTHON OR CARGO_STATIC_LIB OR CARGO_SHARED_LIB OR CARGO_BINS))
    message(FATAL_ERROR "cargo_build for ${name} requires at least one of: STATIC_LIB, SHARED_LIB, BINS")
  endif()

  string(TOLOWER "${CMAKE_BUILD_TYPE}" CMAKE_BUILD_TYPE_lower)
  if(CMAKE_BUILD_TYPE_lower STREQUAL "debug")
    set(PROFILE "dev")
  else()
    set(PROFILE release)
  endif()

  if (CARGO_NO_STD)
     set(PROFILE ${PROFILE}-nostd)
  endif()

  if (CARGO_STATIC_LIB OR CARGO_NO_LTO)
     set(PROFILE ${PROFILE}-nolto)
  endif()

  if (PROFILE STREQUAL "dev")
     set(RUST_TARGET_DIR "${RUST_TARGET}/debug")
  elseif(CMAKE_BUILD_TYPE_lower STREQUAL relwithdebinfo)
    set(CARGO_ENV ${CARGO_ENV} CARGO_PROFILE_RELEASE_STRIP=none)
    set(CARGO_ENV ${CARGO_ENV} CARGO_PROFILE_RELEASE_DEBUG=true)
    set(RUST_TARGET_DIR "${RUST_TARGET}/${PROFILE}")  
  else()
     set(RUST_TARGET_DIR "${RUST_TARGET}/${PROFILE}")  
  endif()

  if (CMAKE_SYSROOT)
    SET(CARGO_ENV ${CARGO_ENV} "BINDGEN_EXTRA_CLANG_ARGS=--gcc-toolchain=${CMAKE_SYSROOT} --sysroot=${CMAKE_SYSROOT}")
    SET(CARGO_ENV ${CARGO_ENV} TOOLCHAIN=${CMAKE_SYSROOT})
  endif()

  SET(CARGO_ENV ${CARGO_ENV} CC=${CMAKE_C_COMPILER})
  SET(CARGO_ENV ${CARGO_ENV} CFLAGS=${CMAKE_C_FLAGS})
  SET(CARGO_ENV ${CARGO_ENV} CXX=${CMAKE_CXX_COMPILER})
  SET(CARGO_ENV ${CARGO_ENV} CXXFLAGS=${CMAKE_CXX_FLAGS})

  if(${LFR_YOCTO})
      SET(CARGO_ENV ${CARGO_ENV} CARGO_PROFILE_RELEASE_STRIP=none)
  endif()

  SET(CARGO_ENV ${CARGO_ENV} SR_ARCH=${SR_ARCH})
  
  set(RUSTC_LINKER ${CMAKE_C_COMPILER})
  
  if (ANDROID)
    set(RUSTC_LINKER ${ANDROID_TOOLCHAIN_ROOT}/bin/${RUST_TARGET}${ANDROID_NATIVE_API_LEVEL}-clang)
    set(CARGO_ENV ${CARGO_ENV} PATH=${ANDROID_TOOLCHAIN_ROOT}/bin/:$ENV{PATH})
  endif()


  list(APPEND RUSTFLAGS -C "linker=${CMAKE_SOURCE_DIR}/scripts/arm-reorder.pl")  
  list(APPEND RUSTFLAGS -C "link-arg=-Wl,--lfr-real-linker=${RUSTC_LINKER}")

  if(CMAKE_COMPILE_WARNING_AS_ERROR)
    list(APPEND RUSTFLAGS -D warnings)
  endif()
    
  if(CMAKE_SYSROOT)
    list(APPEND RUSTFLAGS -C link-arg=--sysroot=${CMAKE_SYSROOT})
  endif()

  if(RUST_HASH_STYLE)
    list(APPEND RUSTFLAGS -C link-arg=-Wl,--hash-style=${RUST_HASH_STYLE})
  endif()

  if(RUST_DYN_LINKER)
    list(APPEND RUSTFLAGS -C link-arg=-Wl,--dynamic-linker=${RUST_DYN_LINKER})
  endif()

  # This is going to disable Rust from statically linking musl libc. Without
  # this, at runtime, calls to C library functions will break in weird ways.
  if(RUST_TARGET MATCHES "-musl$")
    list(APPEND RUSTFLAGS_TARGET -C target-feature=-crt-static)
  endif()

  #Fun Fact: RUSTFLAGS are not used for build.rs when --target is specified.
  #https://github.com/rust-lang/cargo/issues/4423
  # We are using a rustc wrapper to get around this issue.
  JOIN("${RUSTFLAGS}" " " FINAL_ENV_RUSTFLAGS)
  JOIN("${RUSTFLAGS_TARGET}" " " FINAL_ENV_RUSTFLAGS_TARGET)

  set(env_cmd ${CMAKE_COMMAND} -E env
    "RUSTFLAGS_TARGET=${FINAL_ENV_RUSTFLAGS_TARGET}"
    "RUSTC_WRAPPER=${CMAKE_SOURCE_DIR}/dev/helpers/rustc_wrapper.sh"
    "RUSTFLAGS=${FINAL_ENV_RUSTFLAGS}" "${CARGO_ENV}")

  list(APPEND FINAL_BUILD_RUSTFLAGS_LIST ${CARGO_BUILD_RUSTFLAGS} ${RUSTFLAGS})
  JOIN("${FINAL_BUILD_RUSTFLAGS_LIST}" " " FINAL_BUILD_RUSTFLAGS)

  set(build_env_cmd ${CMAKE_COMMAND} -E env
    "RUSTFLAGS_TARGET=${FINAL_ENV_RUSTFLAGS_TARGET}"
    "RUSTC_WRAPPER=${CMAKE_SOURCE_DIR}/dev/helpers/rustc_wrapper.sh"
    "RUSTFLAGS=${FINAL_BUILD_RUSTFLAGS}" "${CARGO_ENV}")

  if (CARGO_WORKDIR)
  else()
    set(CARGO_WORKDIR ${CMAKE_CURRENT_SOURCE_DIR})
  endif()

  # The build/test directories should be seperate for Python,
  # since it compiles with a different set of features. Doing this prevents
  # a lot of recompilation.
  set(RUST_BASEDIR ${CMAKE_BINARY_DIR}/rust)
  
  list(APPEND CARGO_TEST_FLAGS ${CARGO_FLAGS})

  list(APPEND CARGO_FLAGS ${CARGO_BUILD_FLAGS})
  list(APPEND CARGO_FLAGS "--profile" ${PROFILE})
  list(APPEND CARGO_FLAGS "--target" ${RUST_TARGET})
  list(APPEND CARGO_FLAGS "--target-dir" ${RUST_BASEDIR})
  list(APPEND CARGO_TEST_FLAGS "--target" ${RUST_TARGET})
  list(APPEND CARGO_TEST_FLAGS "--target-dir" ${RUST_TESTDIR})

  if(CARGO_PYTHON)
    set(bin ${name})
    if(CMAKE_BUILD_TYPE_lower STREQUAL "debug")
      set(RUST_PYTHON_BUILD_TYPE "debug")
    else()
      set(RUST_PYTHON_BUILD_TYPE "release")
    endif()

    set(PY_LIBS)
    set(output_file "${CMAKE_CURRENT_BINARY_DIR}/${bin}")

    set(MATURIN_ARGS --no-sdist --skip-auditwheel)
    if(CMAKE_BUILD_TYPE_lower STREQUAL "release")
      set(MATURIN_ARGS ${MATURIN_ARGS} --release --strip)
    endif()
    set(py_ver 3.6)
    set(py_output_file "${output_file}_${py_ver}")
    #This builds a python version independent binary for >= 3.6
    find_program(MATURIN maturin REQUIRED)

    list(APPEND PY_TARGET ${bin}_py${py_ver})

    set(MATURIN_CARGO_ARGS "${CARGO_FLAGS}")
    list(FILTER MATURIN_CARGO_ARGS EXCLUDE REGEX "--release")

     list(REMOVE_ITEM MATURIN_CARGO_ARGS --target ${RUST_TARGET})
     set(MATURIN_ARGS ${MATURIN_ARGS} --target ${RUST_TARGET})

    list_transform_prepend(MATURIN_CARGO_ARGS "--cargo-extra-args=")

    set(MATURIN_RUST_ARGS ${RUSTFLAGS})
    list_transform_prepend(MATURIN_RUST_ARGS "--rustc-extra-args=")

    add_custom_target(
      ${bin}_py${py_ver}
      ALL
      BYPRODUCTS ${CARGO_OUT} ${PY_LIBS}
      COMMENT "Build ${name}"
      COMMAND
      ${build_env_cmd}
      PYO3_PYTHON=unused
      PYO3_NO_PYTHON=true
      ${MATURIN} build ${MATURIN_ARGS} -o ${output_file}/dist ${MATURIN_CARGO_ARGS}
      ${MATURIN_RUST_TARGET}
      WORKING_DIRECTORY ${CARGO_WORKDIR}
      DEPENDS ${CARGO_DEPENDS}
      VERBATIM)
  endif()

  if(CARGO_BINS)
    foreach(bin IN LISTS CARGO_BINS)
      set(output_file "${RUST_BASEDIR}/${RUST_TARGET_DIR}/${bin}")

      add_custom_target(
        ${bin}_bin
        ALL
        BYPRODUCTS ${output_file} ${CARGO_OUT}
        COMMENT "Build ${name} with cargo"
        COMMAND ${build_env_cmd} cargo ${CARGO_TOOLCHAIN} build ${CARGO_FLAGS} --bin ${bin}
        WORKING_DIRECTORY ${CARGO_WORKDIR}
        DEPENDS ${CARGO_DEPENDS}
        VERBATIM)
      add_executable(${bin} IMPORTED GLOBAL)
      set_target_properties(${bin}
        PROPERTIES
        IMPORTED_LOCATION ${output_file}
        target_dir "${RUST_BASEDIR}/${RUST_TARGET_DIR}"
        source_dir "${CARGO_WORKDIR}"
        )
    endforeach()
  endif()

  if(CARGO_STATIC_LIB OR CARGO_SHARED_LIB)
    if (CARGO_LIB_NAME)
       set(lib_name ${CARGO_LIB_NAME})
    else()
       set(lib_name ${name})
    endif()
    if(CARGO_STATIC_LIB)
      set(static_lib_name ${CMAKE_STATIC_LIBRARY_PREFIX}${lib_name}${CMAKE_STATIC_LIBRARY_SUFFIX})
      set(static_output_file "${RUST_BASEDIR}/${RUST_TARGET_DIR}/${static_lib_name}")
      list(APPEND output_file ${static_output_file})
    endif()
    if (CARGO_SHARED_LIB)
      set(shared_lib_name ${CMAKE_SHARED_LIBRARY_PREFIX}${lib_name}${CMAKE_SHARED_LIBRARY_SUFFIX})
      set(shared_output_file "${RUST_BASEDIR}/${RUST_TARGET_DIR}/${shared_lib_name}")
      list(APPEND output_file ${shared_output_file})
    endif()

    if (CARGO_STATIC_LIB)
       list(APPEND crate_type --crate-type staticlib)
    endif()
    if (CARGO_SHARED_LIB)
       list(APPEND crate_type --crate-type cdylib)
    endif()

    add_custom_target(
      ${lib_name}_lib
      ALL
      BYPRODUCTS ${output_file} ${CARGO_OUT}
      COMMENT "Build ${name} with cargo"
      COMMAND ${build_env_cmd} cargo ${CARGO_TOOLCHAIN} build ${CARGO_FLAGS} -p ${name} --lib
      COMMAND ${build_env_cmd} cargo ${CARGO_TOOLCHAIN} clippy ${CARGO_FLAGS} -p ${name} --lib
      WORKING_DIRECTORY "${CARGO_WORKDIR}"
      DEPENDS ${CARGO_DEPENDS}
      VERBATIM)

    if(CARGO_STATIC_LIB)
      add_library(${lib_name}_static STATIC IMPORTED GLOBAL)
      add_dependencies(${lib_name}_static ${lib_name}_lib)
      set_target_properties(${lib_name}_static
        PROPERTIES
        IMPORTED_LOCATION ${static_output_file}
        target_dir "${RUST_BASEDIR}/${RUST_TARGET_DIR}"
        source_dir "${CARGO_WORKDIR}"
        )
    endif()
    if (CARGO_SHARED_LIB)
      add_library(${lib_name}_shared SHARED IMPORTED GLOBAL)
      add_dependencies(${lib_name}_shared ${lib_name}_lib)
      set_target_properties(${lib_name}_shared
        PROPERTIES
        IMPORTED_LOCATION ${shared_output_file}
        target_dir "${RUST_BASEDIR}/${RUST_TARGET_DIR}"
        source_dir "${CARGO_WORKDIR}"
        )
    endif()

  endif()

endfunction()
