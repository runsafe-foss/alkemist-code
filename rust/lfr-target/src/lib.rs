/* Copyright (c) 2022 RunSafe Security Inc. */

#[derive(Eq, PartialEq, Copy, Clone)]
pub enum Os {
    Android,
    Linux,
}

impl Os {
    pub fn build_script() -> Self {
        build::env("CARGO_CFG_TARGET_OS").unwrap().parse().unwrap()
    }
}

impl std::str::FromStr for Os {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "android" => Ok(Self::Android),
            "linux" => Ok(Self::Linux),
            arch => Err(format!("invalid target os {}", arch)),
        }
    }
}

impl std::fmt::Display for Os {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        let s = match self {
            Self::Android => "android",
            Self::Linux => "linux",
        };
        write!(f, "{}", s)
    }
}

#[derive(Eq, PartialEq, Copy, Clone)]
pub enum Arch {
    X86,
    X86_64,
    Arm,
    Arm64,
}

impl Arch {
    pub fn size(&self) -> u32 {
        match self {
            Arch::X86 | Arch::Arm => 32,
            Arch::X86_64 | Arch::Arm64 => 64,
        }
    }
}

impl std::str::FromStr for Arch {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "x86" => Ok(Arch::X86),
            "x86_64" => Ok(Arch::X86_64),
            "arm" => Ok(Arch::Arm),
            "aarch64" | "arm64" => Ok(Arch::Arm64),
            arch => Err(format!("invalid architecture {}", arch)),
        }
    }
}

impl std::fmt::Display for Arch {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        let s = match self {
            Arch::X86 => "x86",
            Arch::Arm => "arm",
            Arch::Arm64 => "arm64",
            Arch::X86_64 => "x86_64",
        };
        write!(f, "{}", s)
    }
}

impl Arch {
    pub fn build_script() -> Self {
        build::env("CARGO_CFG_TARGET_ARCH")
            .unwrap()
            .parse()
            .unwrap()
    }
}

pub trait BuildTarget {
    fn new_target(cpp: bool) -> cc::Build;
}

impl BuildTarget for cc::Build {
    fn new_target(cpp: bool) -> cc::Build {
        let arch = Arch::build_script();
        let mut build = cc::Build::new();

        build
            .flag_if_supported("-gz=none")
            .flag_if_supported("-fno-integrated-as")
            .flag_if_supported("-Wa,-mrelax-relocations=no")
            .flag_if_supported("-Wa,--noexecstack")
            .asm_flag("-D__ASSEMBLY__=1")
            .flag("-nodefaultlibs")
            .flag("-ffreestanding")
            .flag("-fno-exceptions")
            .flag("-fno-unwind-tables")
            .flag("-nostdlib")
            .flag("-fno-stack-protector")
            .cpp_link_stdlib(None)
            .pic(true)
            .static_flag(true);

        if cpp {
            build.flag("-static-libstdc++");
        }

        if arch == Arch::X86 {
            // This could check CARGO_CFG_TARGET_FEATURE for sse flags
            build.flag(if build::feature("x86_sse") {
                "-msse2"
            } else {
                "-mno-sse2"
            });
        }

        let os = Os::build_script();
        match os {
            Os::Android => {
                build.flag("-DRANDOLIB_IS_ANDROID=1");
            }
            Os::Linux => {
                build.flag("-DRANDOLIB_IS_LINUX=1");
            }
        }

        match arch {
            Arch::X86 | Arch::X86_64 => {
                build.flag("-minline-all-stringops");
            }
            _ => {}
        }

        build
    }
}

pub mod build {
    use std::ffi::OsString;
    use std::path::Path;
    pub fn feature<S>(f: S) -> bool
    where
        S: AsRef<str>,
    {
        let f = f.as_ref().to_uppercase();
        let f = f.replace('-', "_");
        let f = format!("CARGO_FEATURE_{}", f);
        env_os(f).is_some()
    }
    pub fn env<S>(e: S) -> Result<String, std::env::VarError>
    where
        S: AsRef<str>,
    {
        println!("cargo:rerun-if-env-changed={}", e.as_ref());
        std::env::var(e.as_ref())
    }
    pub fn env_os<S>(e: S) -> Option<OsString>
    where
        S: AsRef<str>,
    {
        println!("cargo:rerun-if-env-changed={}", e.as_ref());
        std::env::var_os(e.as_ref())
    }
    pub fn depend<P>(path: P) -> P
    where
        P: AsRef<Path>,
    {
        println!("cargo:rerun-if-changed={}", path.as_ref().display());
        path
    }
}
