// Copyright (c) 2022 RunSafe Security Inc.

#![cfg_attr(not(test), no_std)]
extern crate alloc;

use alloc::{vec, vec::Vec};
use core::ops::Range;

const BTREE_LINKS: usize = 6;
const BRANCH_FACTOR: usize = BTREE_LINKS + 2;

#[derive(Debug, PartialEq, Copy, Clone)]
struct BTreeNodeId(usize);

#[derive(Debug, PartialEq, Copy, Clone)]
enum BTreeChild {
    Leaf(usize),
    Node(BTreeNodeId),
}

#[derive(Debug, Copy, Clone)]
struct BTreeLink<K> {
    end: K,
    /// Link to Child node
    child: BTreeChild,
}

#[derive(Debug)]
struct BTreeNode<K> {
    /// Link to First Child
    first_child: BTreeLink<K>,
    /// Links to Remaining Children
    links: [Option<BTreeLink<K>>; BTREE_LINKS],
    /// Link to Last Child
    last_child: BTreeChild,
}

/// This is only implemented to hold space in the nodes array.
/// It is not considered useful or valid. I am only using this
/// because I don't trust the MaybeUninit docs wrt Vec.
impl<K> Default for BTreeNode<K>
where
    K: Default,
    Option<BTreeLink<K>>: core::marker::Copy,
{
    fn default() -> Self {
        Self {
            first_child: BTreeLink {
                end: K::default(),
                child: BTreeChild::Leaf(0),
            },
            links: [None; BTREE_LINKS],
            last_child: BTreeChild::Leaf(0),
        }
    }
}

#[derive(Debug)]
pub struct BTreeLite<K> {
    nodes: Vec<BTreeNode<K>>,
}

impl<K> BTreeLite<K>
where
    K: Ord + Copy + Default + core::fmt::Debug,
{
    pub fn new<V, F>(values: &[V], mut get_key: F) -> Option<Self>
    where
        F: Fn(&V) -> K,
    {
        let mut nodes = vec![];
        if values.len() > 1 {
            let id = Self::build_btree_internal(&mut nodes, 0..values.len(), values, &mut get_key);
            assert_eq!(id, BTreeChild::Node(BTreeNodeId(0)));
            Some(Self { nodes })
        } else {
            None
        }
    }

    fn build_btree_internal<V, F>(
        nodes: &mut Vec<BTreeNode<K>>,
        range: Range<usize>,
        funcs: &[V],
        get_key: &mut F,
    ) -> BTreeChild
    where
        F: Fn(&V) -> K,
    {
        if range.len() == 1 {
            return BTreeChild::Leaf(range.start);
        }
        let node_idx = BTreeNodeId(nodes.len());
        nodes.push(Default::default());

        let step = if range.len() <= BRANCH_FACTOR {
            1
        } else {
            // Limiting the min step to BRANCH_FACTOR should reduce the number
            // of leaf Btree nodes.
            core::cmp::max(
                range.len().checked_div(BRANCH_FACTOR).unwrap_or(0),
                BRANCH_FACTOR,
            )
        };
        let mut last_pivot = None;
        let mut branches = (0..)
            .scan(range.start, |prev, _| {
                let r = *prev..*prev + step;
                *prev += step;
                Some(r)
            })
            .take_while(|r| r.end < range.end)
            .take(BRANCH_FACTOR - 1)
            .map(|r| {
                last_pivot = Some(r.end);
                BTreeLink {
                    end: get_key(&funcs[r.end]),
                    child: Self::build_btree_internal(nodes, r, funcs, get_key),
                }
            });

        let first_child = branches.next().unwrap();
        let mut links = [None; BTREE_LINKS];
        links
            .iter_mut()
            .zip(branches.by_ref())
            .for_each(|(l, link)| {
                *l = Some(link);
            });

        assert!(branches.next().is_none());

        let last_child_range = last_pivot.unwrap()..range.end;
        let last_child = Self::build_btree_internal(nodes, last_child_range, funcs, get_key);

        let node = BTreeNode {
            first_child,
            links,
            last_child,
        };

        nodes[node_idx.0] = node;

        BTreeChild::Node(node_idx)
    }

    pub fn find(&self, key: &K) -> Option<usize> {
        if self.nodes.is_empty() {
            return None;
        }
        let mut next_idx = BTreeChild::Node(BTreeNodeId(0));
        while let BTreeChild::Node(node_idx) = next_idx {
            let node = &self.nodes[node_idx.0];
            let new_node = if *key < node.first_child.end {
                node.first_child.child
            } else {
                node.links
                    .iter()
                    .take_while(|link| link.is_some())
                    .flatten()
                    .find(|link| *key < link.end)
                    .map(|link| link.child)
                    .unwrap_or(node.last_child)
            };
            next_idx = new_node;
        }

        match next_idx {
            BTreeChild::Leaf(id) => Some(id),
            _ => None,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test() {
        let items = [1u32, 2, 3, 4, 5, 6];
        let btree = BTreeLite::new(&items, |id| *id).unwrap();

        for (idx, i) in items.iter().enumerate() {
            assert_eq!(btree.find(i), Some(idx));
        }

        for i in 2..100 {
            let items: Vec<_> = (0..i).collect();
            println!("I: {}", i);
            let btree = BTreeLite::new(&items, |id| *id).unwrap();

            for (idx, i) in items.iter().enumerate() {
                assert_eq!(btree.find(i), Some(idx));
            }
        }

        let items: Vec<_> = (0..1000).step_by(10).collect();
        let btree = BTreeLite::new(&items, |id| *id).unwrap();

        for (i, idx) in (0..1000).zip((0..items.len()).flat_map(|v| core::iter::repeat(v).take(10)))
        {
            assert_eq!(btree.find(&i), Some(idx));
        }
    }

    #[test]
    fn test2() {
        use rand::Rng;
        let mut rng = rand::thread_rng();
        let items: Vec<_> = (0..10000)
            .map(|_| rng.gen_range(0usize..100))
            .scan(100, |state, off| {
                let cur = *state;
                *state += off;
                Some(cur)
            })
            .collect();

        let btree = BTreeLite::new(&items, |id| *id).unwrap();

        let first = 0;
        let last = items.last().copied().unwrap() + 100;

        for search in first..last {
            let find1 = items.partition_point(|k| *k <= search).saturating_sub(1);
            let find2 = btree.find(&search);
            assert_eq!(Some(find1), find2);
        }
    }
}
