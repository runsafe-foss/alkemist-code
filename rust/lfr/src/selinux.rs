/* Copyright (c) 2022 RunSafe Security Inc. */
//! SE Linux Context
use crate::log;
use bionic::io::Error as BionicIoError;
use bionic::io::Write;
use bionic::prelude::*;
use core::fmt::Display;

/// Path to Proc Filesystem Current Attributes
static PROC_CURRENT: &str = "/proc/self/attr/current";
/// Path to SELinux Sys Information
static SYS_SELINUX: &str = "/sys/fs/selinux/";
/// Path to SELinux Enforcement Information
static SYS_ENFORCE: &str = "/sys/fs/selinux/enforce";

/// Reports an SELinux Error
fn report_error(res: &Result<Option<SeContext>, ContextError>) {
    match res {
        Err(ContextError::UnableToWriteContext(enforce)) => {
            log::debugln!(
                1,
                "LFR SELinux: Warning: Unable to write context in confined domain ({:?})",
                enforce
            );
        }
        Err(err) => {
            log::debugln!(1, "LFR SELinux: Error: {:?}", err);
        }
        Ok(_) => {
            log::debugln!(2, "LFR SELinux: OK");
        }
    }
}

/// Whether this Result indicates randomization should still occur.
pub fn should_randomize(res: Result<Option<SeContext>, ContextError>) -> bool {
    !matches!(
        res,
        Err(ContextError::UnableToWriteContext(SeEnforce::Enforcing))
    )
}

/// The Targeted Distro
#[derive(PartialEq, Copy, Clone)]
enum Distro {
    /// Android
    Android,
    /// Windows, Debian, etc.
    Other,
}

/// The SE Linux Enforcement Levels
#[derive(PartialEq, Copy, Clone, Debug)]
pub enum SeEnforce {
    /// SELinux is Currently Disabled
    Disabled = 1,
    /// SELinux is in permissive mode.
    Permissive = 2,
    /// SELinux is in enforcing mode.
    Enforcing = 3,
}

impl TryFrom<i32> for SeEnforce {
    type Error = ();
    fn try_from(val: i32) -> Result<SeEnforce, Self::Error> {
        match val {
            1 => Ok(SeEnforce::Disabled),
            2 => Ok(SeEnforce::Permissive),
            3 => Ok(SeEnforce::Enforcing),
            _ => Err(()),
        }
    }
}

/// The Current SE Linux Context
#[derive(Debug)]
pub struct SeLinuxContext {
    /// The former SE Linux Context.
    old_mode: SeEnforce,
    /// The current context.
    context: Option<SeContext>,
}

impl SeLinuxContext {
    /// Create a new SELinux Enforcement Context.
    pub fn new() -> Result<Self, ContextError> {
        let old_mode = get_enforce(None);
        let mode_switch = rando_selinux_to_lfr(old_mode);
        let new_context = mode_switch?;
        Ok(Self {
            old_mode,
            context: new_context,
        })
    }

    pub fn context(&self) -> Option<&SeContext> {
        self.context.as_ref()
    }
}

impl Drop for SeLinuxContext {
    fn drop(&mut self) {
        let distro = if cfg!(target_os = "android") {
            Distro::Android
        } else {
            Distro::Other
        };
        report_error(&rando_selinux_to_base(
            distro,
            get_enforce(Some(self.old_mode)),
        ));
    }
}

/// Get the Current Enforcement level.
fn get_enforce(semode: Option<SeEnforce>) -> SeEnforce {
    if let Some(mode) = semode {
        return mode;
    }
    // If SYS_SELINUX does not exist, selinux is either not present or disabled
    let selinux_enabled = match bionic::fs::metadata(SYS_SELINUX) {
        Err(_) => {
            log::debugln!(1, "LFR SELinux: CANT read {}", SYS_SELINUX);
            false
        }
        Ok(enabled) => {
            log::debugln!(1, "LFR SELinux: READ {}", SYS_SELINUX);
            enabled.file_type().is_dir()
        }
    };
    if !selinux_enabled {
        log::debugln!(1, "LFR SELinux: Disabled");
        return SeEnforce::Disabled;
    }

    match bionic::fs::read_to_string(SYS_ENFORCE).as_deref() {
        Ok("1") => {
            log::debugln!(1, "LFR SELinux: Enabled (Enforcing)");
            SeEnforce::Enforcing
        }
        Ok("0") => {
            log::debugln!(1, "LFR SELinux: Enabled (Permissive)");
            SeEnforce::Permissive
        }
        Ok(enforce) => {
            log::debugln!(1, "LFR SELinux: Enabled (UNKNOWN) {}", enforce);
            //TODO: Need to rethink this do we need to do more?  This is
            //  Probably the safest option
            SeEnforce::Enforcing
        }
        Err(_) => {
            log::debugln!(1, "LFR SELinux: Enabled (Enforcing)");
            SeEnforce::Enforcing
        }
    }
}

/// SE Linux Context Errors
#[derive(Debug, PartialEq, Copy, Clone)]
pub enum ContextError {
    /// IO errors
    Io(BionicIoError),
    /// No Context Found
    MissingContext,
    /// Unable to modify context.
    UnableToWriteContext(SeEnforce),
}

impl From<BionicIoError> for ContextError {
    fn from(io: BionicIoError) -> Self {
        Self::Io(io)
    }
}

/// SEContext as read from the system.
#[derive(Debug, PartialEq, Clone)]
pub struct SeContext {
    /// The User
    user: String,
    /// Role
    role: String,
    /// The Type of the Context
    typ: String,
    /// The Level of the Context
    level: String,
    /// Extraneous suffix.
    other: Vec<String>,
}

impl Display for SeContext {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{}:{}:{}:{}", self.user, self.role, self.typ, self.level)?;
        if !self.other.is_empty() {
            write!(f, ":{}", self.other.join(":"))?;
        };
        Ok(())
    }
}

impl SeContext {
    /// Getting for Type of SeLinux
    pub fn typ(&self) -> &str {
        &self.typ
    }
}

impl core::str::FromStr for SeContext {
    type Err = ContextError;
    /// Create a context from the String.

    fn from_str(context: &str) -> Result<Self, Self::Err> {
        fn secontext(context: &str) -> Option<SeContext> {
            let mut context = context.trim_end_matches('\0').split(':');

            let context = SeContext {
                user: context.next()?.into(),
                role: context.next()?.into(),
                typ: context.next()?.into(),
                level: context.next()?.into(),
                other: context.map(|x| x.to_string()).collect(),
            };
            Some(context)
        }
        secontext(context).ok_or(ContextError::MissingContext)
    }
}
/// Convert the current SELinux to something that works with LFR.
fn conv_selinux_to_lfr(mut context: SeContext) -> Result<SeContext, ContextError> {
    log::debugln!(1, "LFR SELinux: Current context - {}", context);

    if context.typ.strip_suffix("_lfr_t").is_some() {
        log::debugln!(2, "LFR SELinux: Already in proper lfr_t context");
        log::debugln!(2, "LFR SELinux: Found _lfr_t suffix");
    } else if let Some(prefix) = context.typ.strip_suffix("_t") {
        log::debugln!(2, "LFR SELinux: Found _t suffix");
        context.typ = format!("{}_lfr_t", prefix);
    } else {
        log::debugln!(2, "LFR SELinux: Couldn't find _t suffix");
        context.typ = format!("{}_lfr_t", context.typ);
    }

    Ok(context)
}

/// Convert the current context to LFR
pub fn rando_selinux_to_lfr(enforce: SeEnforce) -> Result<Option<SeContext>, ContextError> {
    update_context(enforce, conv_selinux_to_lfr)
}

/// Restore the context to the base context.
fn conv_selinux_to_base(distro: Distro, mut context: SeContext) -> Result<SeContext, ContextError> {
    if let Some(prefix) = context.typ.strip_suffix("_lfr_t") {
        log::debugln!(2, "LFR SELinux: found _lfr_t: prefix {}", prefix);
        let replace = match distro {
            Distro::Android => "",
            Distro::Other => "_t",
        };
        context.typ = format!("{}{}", prefix, replace);
    }

    Ok(context)
}

/// Read the current SE Context String.
pub fn get_se_context_string() -> Result<Option<String>, ContextError> {
    match bionic::fs::read_to_string(PROC_CURRENT) {
        Err(BionicIoError::InvalidFile) => {
            log::debugln!(1, "LFR SELinux: Unable to read {}", PROC_CURRENT);
            Ok(None)
        }
        Err(e) => Err(ContextError::Io(e)),
        Ok(context) => Ok(Some(context)),
    }
}

/// Restore the context back to the base level.
fn rando_selinux_to_base(
    distro: Distro,
    enforce: SeEnforce,
) -> Result<Option<SeContext>, ContextError> {
    update_context(enforce, |old_context| {
        conv_selinux_to_base(distro, old_context)
    })
}

/// The BufferSingleWrite is used as a wrapper to write to special /sys/files! /attr/current presumes that
/// you only issue a single write() call with the entire value, not a sequence of write()
/// calls.  Just doing `write!(file, "{}", new_context)` does not properly write the certain structures.
struct BufferSingleWrite<I>
where
    I: bionic::io::Write,
{
    inner: I,
    buffer: Vec<u8>,
    wrote: bool,
}

impl<I> BufferSingleWrite<I>
where
    I: bionic::io::Write,
{
    fn new(inner: I) -> Self {
        Self {
            inner,
            buffer: Vec::new(),
            wrote: false,
        }
    }

    fn finish(mut self) -> bionic::io::Result<()> {
        self.flush()
    }
}

impl<I> Drop for BufferSingleWrite<I>
where
    I: bionic::io::Write,
{
    fn drop(&mut self) {
        let _ = self.flush();
    }
}

impl<I> bionic::io::Write for BufferSingleWrite<I>
where
    I: bionic::io::Write,
{
    fn write(&mut self, buf: &[u8]) -> bionic::io::Result<usize> {
        self.buffer.extend(buf);
        Ok(buf.len())
    }
    fn flush(&mut self) -> bionic::io::Result<()> {
        if self.wrote && self.buffer.is_empty() {
            return Ok(());
        } else if self.wrote {
            return Err(bionic::io::Error::Unknown);
        }
        self.wrote = true;
        self.inner.write_all(&self.buffer)?;
        self.buffer.clear();
        self.inner.flush()
    }
}

/// Update the context based on the provided function.
fn update_context<F>(
    enforce: SeEnforce,
    modify_context: F,
) -> Result<Option<SeContext>, ContextError>
where
    F: FnOnce(SeContext) -> Result<SeContext, ContextError>,
{
    if enforce == SeEnforce::Disabled {
        return Ok(None);
    }

    log::debugln!(1, "LFR SELinux: Updating context");
    let old_context = match get_se_context_string()? {
        Some(context) => context,
        None => return Ok(None),
    };
    let old_context: SeContext = old_context.parse()?;

    // If unconfined_t is in old_context and we can just go ahead and randomize without
    // any further context switching
    match old_context.typ.as_ref() {
        "unconfined_t" | "unconfined_service_t" => {
            log::debugln!(1, "LFR SELinux: Unconfined Context {}", old_context);
            return Ok(Some(old_context));
        }
        _ => {}
    }
    let new_context = modify_context(old_context.clone())?;

    match enforce {
        SeEnforce::Enforcing if new_context == old_context => {
            log::debugln!(1, "LFR SELinux: No change required - {}", new_context);
            Ok(Some(old_context))
        }
        SeEnforce::Enforcing => {
            log::debugln!(1, "LFR SELinux: Writing new context - {}", new_context);
            let file = bionic::fs::OpenOptions::default()
                .write(true)
                .append(true)
                .open(PROC_CURRENT)?;

            let mut file = BufferSingleWrite::new(file);

            write!(file, "{}", new_context)
                .and_then(|_| file.finish())
                .map_err(|_| ContextError::UnableToWriteContext(enforce))?;
            log::debugln!(1, "LFR SELinux: Update successful");
            Ok(Some(new_context))
        }
        SeEnforce::Disabled | SeEnforce::Permissive => {
            log::debugln!(
                1,
                "LFR SELinux: No change required in permissive/disabled mode."
            );
            Ok(Some(old_context))
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn secontext(s: &str) -> SeContext {
        s.parse().expect("Valid SEContext")
    }
    #[test]
    fn selinux() {
        assert_eq!(
            conv_selinux_to_lfr(secontext("user:role:test_lfr_t:level")),
            Ok(secontext("user:role:test_lfr_t:level"))
        );
        assert_eq!(
            conv_selinux_to_lfr(secontext("user:role:test_t:level")),
            Ok(secontext("user:role:test_lfr_t:level"))
        );
        assert_eq!(
            conv_selinux_to_lfr(secontext("user:role:test:level")),
            Ok(secontext("user:role:test_lfr_t:level"))
        );
        assert_eq!(
            conv_selinux_to_lfr(secontext("user:role:type:level:other")),
            Ok(secontext("user:role:type_lfr_t:level:other"))
        );

        assert_eq!(
            format!("{}", secontext("user:role:test_lfr_t:level")),
            "user:role:test_lfr_t:level".to_string()
        );

        assert_eq!(
            conv_selinux_to_base(Distro::Android, secontext("user:role:test_lfr_t:level")),
            Ok(secontext("user:role:test:level"))
        );
        assert_eq!(
            conv_selinux_to_base(Distro::Other, secontext("user:role:test_lfr_t:level")),
            Ok(secontext("user:role:test_t:level"))
        );
        assert_eq!(
            conv_selinux_to_base(Distro::Other, secontext("user:role:test:level")),
            Ok(secontext("user:role:test:level"))
        );
    }
}
