// Copyright (c) 2022 RunSafe Security Inc.

//! LFR Logging Functionality.
use crate::env;
use alloc::boxed::Box;
use alloc::format;
use alloc::string::ToString;
use bionic::io::Write;
use bionic::io::{AsRawFd, FromRawFd, IntoRawFd, OwnedFd};
#[allow(unused)]
use core::ffi::CStr;
use once_cell::race::OnceBox;

/// Log Interface
pub struct Log {
    /// The level of log specified
    pub level: u32,
    /// The optional log file descriptor.
    fd: Option<OwnedFd>,
}

/// LogWrapper which allows us to implement [`bionic::io::Write`] without taking writable ownership of the [`Log`].
pub struct LogWrapper<'a> {
    pub log: &'a Log,
}

pub struct LineBufferedLogWrapper<'a> {
    buf: alloc::vec::Vec<u8>,
    log: &'a mut LogWrapper<'a>,
}

impl<'a> LineBufferedLogWrapper<'a> {
    pub fn new(log: &'a mut LogWrapper<'a>) -> Self {
        Self {
            buf: alloc::vec::Vec::default(),
            log,
        }
    }
    fn try_write(&mut self) -> Result<usize, bionic::io::Error> {
        let mut write_buf = &self.buf[..];
        while let Some(pos) = write_buf.iter().position(|c| *c == b'\n') {
            let (write, remain) = write_buf.split_at(pos + 1);
            match self.log.write_all(write) {
                Ok(_) => {}
                Err(e) => {
                    let written = self.buf.len() - write_buf.len();
                    if written > 0 {
                        self.buf.drain(0..written);
                        return Ok(written);
                    } else {
                        return Err(e);
                    }
                }
            }
            write_buf = &remain[0..];
        }
        let written = self.buf.len() - write_buf.len();
        self.buf.drain(0..written);
        Ok(written)
    }
}

impl<'a> bionic::io::Write for LineBufferedLogWrapper<'a> {
    fn write(&mut self, buf: &[u8]) -> Result<usize, bionic::io::Error> {
        self.buf.extend(buf);
        self.try_write()?;
        Ok(buf.len())
    }
    fn flush(&mut self) -> Result<(), bionic::io::Error> {
        if self.buf.is_empty() {
            Ok(())
        } else {
            self.buf.push(b'\n');
            self.try_write().map(|_| ())
        }
    }
}
/// Global Static Log.
pub static LOG: OnceBox<Log> = OnceBox::new();

impl bionic::io::Write for LogWrapper<'_> {
    fn write(&mut self, buf: &[u8]) -> Result<usize, bionic::io::Error> {
        cfg_if::cfg_if! {
        if #[cfg(feature = "log_console")] {
            bionic::io::Stderr.write_all(buf)?;
            Ok(buf.len())
        } else if #[cfg(any(feature = "log_file", feature = "log_default"))] {
            if let Some(fd) = &self.log.fd {
                let mut file = unsafe { bionic::fs::File::from_raw_fd(fd.as_raw_fd()) };
                let r = file.write_all(buf);
                file.into_raw_fd();
                r?;
            }
            Ok(buf.len())
        } else if #[cfg(all(feature = "log_system", target_os="android"))] {
            #[repr(i32)]
            enum AndroidLogPriority {
                /// For internal use only.
                Unknown = 0,
                /// The default priority, for internal use only.
                Default,
                /// Verbose logging. Should typically be disabled for a release apk.
                Verbose,
                /// Debug logging. Should typically be disabled for a release apk.
                Debug,
                /// Informational logging. Should typically be disabled for a release apk.
                Info,
                /// Warning logging. For use with recoverable failures.
                Warn,
                /// Error logging. For use with unrecoverable failures.
                Error,
                /// Fatal logging. For use when aborting.
                Fatal,
                /// For internal use only.
                Silent,
            };
            #[link(name = "log")]
            extern "C" {
                fn __android_log_write(
                    priority: libc::c_int,
                    tag: *const libc::c_char,
                    text: *const libc::c_char,
                );
            }
            let tag = CStr::from_bytes_with_nul(b"selfrando\0").unwrap();
            let text = CStr::from_bytes_with_nul(buf).map_err(|_| bionic::io::Error::InputContainsNull)?;
            unsafe {
                __android_log_write(
                    AndroidLogPriority::Info as i32,
                    tag.as_ptr(),
                    text.as_ptr(),
                );
            }
            Ok(buf.len())
        } else if #[cfg(feature="log_system")] {
            Err(bionic::io::Error::DoesNotExist)
        } else {
            Ok(buf.len())
        }}
    }
    fn flush(&mut self) -> Result<(), bionic::io::Error> {
        let log = self.log;
        if cfg!(feature = "log_console") {
            bionic::io::Stderr.flush()?;
        } else if cfg!(any(feature = "log_file", feature = "log_default")) {
            if let Some(fd) = &log.fd {
                let mut file = unsafe { bionic::fs::File::from_raw_fd(fd.as_raw_fd()) };
                let r = file.flush();
                file.into_raw_fd();
                r?;
            }
        } else if cfg!(any(feature = "log_system")) {
            /* do nothing */
        }
        Ok(())
    }
}

/// Initializes the logging mechanism.
pub fn init_log() -> Box<Log> {
    let new_level = if cfg!(feature = "log_env") {
        env::DebugLevel::get()
    } else {
        0
    };

    let fd = if cfg!(any(feature = "lfr_secure", feature = "log_none")) {
        None
    } else if cfg!(any(feature = "log_file", feature = "log_default")) && new_level > 0 {
        let mut options = bionic::fs::OpenOptions::default();
        options.create(true).write(true);
        if env::ForceSync::get() {
            options.sync(true);
        }
        if cfg!(feature = "log_append") {
            options.append(true);
        }

        let log_name = option_env!("RANDOLIB_LOG_FILENAME").unwrap_or("lfr");
        let filename = if cfg!(feature = "log_pid") {
            format!("{}-{}", log_name, bionic::getpid().0)
        } else {
            log_name.to_string()
        };
        options.mode(0o660);
        options.open(filename).ok().map(|x| x.into())
    } else {
        None
    };

    alloc::boxed::Box::new(Log {
        level: new_level,
        fd,
    })
}

/// Writes a debug statement of at most log level `n` without a newline.
#[macro_export]
macro_rules! debug {
    (@ $log:path, $fmt:expr, $($arg:tt)*) => {
        use bionic::io::Write;
        let mut wrapper = $crate::log::LogWrapper { log: $log };
        #[cfg(all(feature="log_system", target_os="android"))]
        let mut wrapper = $crate::log::LineBufferedLogWrapper::new( &mut wrapper );

        let _ = write!(wrapper, "{}", format_args!($fmt, $($arg)*));
        let _ = wrapper.flush();
    };
    (0, $fmt:expr) => {
        debug!(0, $fmt,)
    };
    (0, $fmt:expr, $($arg:tt)*) => {
        {
            let log = $crate::log::LOG.get_or_init($crate::log::init_log);
            debug!(@ log, $fmt, $($arg)*)
        }
    };
    ($level:literal,$fmt:expr) => {
        debug!($level, $fmt,)
    };
    ($level:literal,$fmt:expr, $($arg:tt)*) => {
        {
            let (enabled, log) = $crate::log::log_enabled!(@ $level);
            if enabled {
                $crate::log::debug!(@ log, $fmt, $($arg)*);
            }
        }
    };
}

/// Writes a debug statement of at most log level `n` with a newline.
#[macro_export]
macro_rules! debugln {
    (0, $fmt:expr) => {
        $crate::log::debugln!(0, $fmt,)
    };
    ($val:literal, $fmt:expr) => {
        $crate::log::debugln!($val, $fmt,)
    };
    (0, $fmt:expr, $($arg:tt)*) => {
        $crate::log::debug!(0, "{}\n", format_args!($fmt, $($arg)*))
    };
    ($val:literal, $fmt:expr, $($arg:tt)*) => {
        $crate::log::debug!($val, "{}\n", format_args!($fmt, $($arg)*))
    };
}

#[macro_export]
macro_rules! log_enabled {
    (@ $level:literal) => {{
        let log = $crate::log::LOG.get_or_init($crate::log::init_log);
        (log.level >= $level, log)
    }};
    ($level:literal) => {{
         $crate::log::log_enabled!(@ $level).0
    }};
}

pub(crate) use debug;
pub(crate) use debugln;
pub(crate) use log_enabled;
