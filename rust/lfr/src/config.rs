/* Copyright (c) 2022 RunSafe Security Inc. */

//! TOML Configuration reading.
use crate::log::debugln;
use crate::selinux;
use alloc::borrow::Cow;
use alloc::collections::BTreeMap;
use alloc::string::String;
use alloc::vec::Vec;
use bionic::path::{Path, PathBuf};
use serde::Deserialize;

/// Global Settings for LFR
///
/// It is very important that we do not have ANY floating point
/// numbers in our config as we are missing the intrinsics to do floating
/// point number deserialiation.
#[derive(Debug, Deserialize, Default)]
struct Global {
    /// Whether Randomization should be skipped.
    #[serde(default)]
    skip_rando: bool,
    /// List of Files with specific customization.
    #[serde(default)]
    files: Vec<LfrFile>,
    /// The Domains that may have specific customizations
    #[serde(default)]
    domains: Vec<ResDomain>,
    /// The version of the configuration.
    #[serde(default)]
    #[allow(dead_code)]
    version: u32,
}

/// CacheDaemon specific settings
#[derive(Debug, Deserialize, Default)]
pub struct CacheDaemon {
    pub uids: BTreeMap<String, PathBuf>,
}

/// Configuration relating to a specific file.
#[derive(Debug, Deserialize)]
struct LfrFile {
    /// Path of the specified file.
    path: PathBuf,
    /// Whether randomization should be applied.
    #[serde(default)]
    skip_rando: bool,
}

/// Configuration relating to a specific Domain.
#[derive(Debug, Deserialize)]
struct ResDomain {
    /// The SE Linux type
    setype: String,
    /// Whether randomization should be skipped
    #[serde(default)]
    skip_rando: bool,
}

/// TOML Config File
#[derive(Debug, Deserialize, Default)]
pub struct Config {
    /// The Global Options.
    #[serde(default)]
    global: Global,

    #[serde(default)]
    pub cachedaemon: CacheDaemon,
}

#[derive(Debug)]
pub struct Perms {
    uid: u32,
    gid: u32,
    mode: u32,
}

/// Configuration Errors
pub enum ConfigError {
    /// Desrialization issues.
    Deserialization(toml::de::Error),
    /// IO failures.
    Io(bionic::io::Error),
    /// Invalid Permissions
    BadPerm(Perms),
}

impl alloc::fmt::Display for ConfigError {
    fn fmt(&self, f: &mut alloc::fmt::Formatter) -> alloc::fmt::Result {
        match self {
            Self::BadPerm(meta) => {
                write!(
                    f,
                    "Config file is not owned by root with 644 permissions. (uid: {}, gid: {}, perm: {:o})",
                    meta.uid,
                    meta.gid,
                    meta.mode
                )
            }
            Self::Deserialization(d) => write!(f, "Config file deserialization error: {:?}", d),
            Self::Io(d) => write!(f, "Config file io error: {:?}", d),
        }
    }
}

impl From<bionic::io::Error> for ConfigError {
    fn from(e: bionic::io::Error) -> Self {
        ConfigError::Io(e)
    }
}

impl From<toml::de::Error> for ConfigError {
    fn from(e: toml::de::Error) -> Self {
        ConfigError::Deserialization(e)
    }
}

impl Config {
    /// Read a TOML config file at specified location.
    pub fn new<P>(path: P, allow_default: bool) -> Result<Self, ConfigError>
    where
        P: AsRef<Path>,
    {
        use bionic::fs::MetadataExt;
        const ROOT: u32 = 0;
        const PERMS: u32 = 0o644;
        const PERM_MASK: u32 = 0x1FF;
        match bionic::fs::File::open(path) {
            Ok(mut file)
                if file.metadata().uid() == ROOT
                    && file.metadata().gid() == ROOT
                    && file.metadata().mode() & PERM_MASK == PERMS =>
            {
                use bionic::io::Read;
                let mut conf = alloc::string::String::new();
                file.read_to_string(&mut conf)?;
                Ok(toml::from_str(&conf)?)
            }
            Ok(file) => {
                let meta = file.metadata();
                let perms = Perms {
                    uid: meta.uid(),
                    gid: meta.gid(),
                    mode: meta.mode(),
                };
                Err(ConfigError::BadPerm(perms))
            }
            Err(bionic::io::Error::DoesNotExist) if allow_default => Ok(Self::default()),
            Err(e) => Err(e.into()),
        }
    }

    /// Whether the specified file should be skipped.
    fn skip_rando_file<P>(&self, path: Option<P>) -> bool
    where
        P: AsRef<Path>,
    {
        let path: Cow<Path> = if let Some(path) = &path {
            Cow::Borrowed(path.as_ref())
        } else {
            match bionic::fs::read_link("/proc/self/exe") {
                Ok(path) => Cow::Owned(path),
                Err(e) => {
                    debugln!(1, "Unable to read /proc/self/exe {:?}", e);
                    return false;
                }
            }
        };
        self.global.skip_rando
            || self
                .global
                .files
                .iter()
                .find_map(|l| {
                    if l.path.as_ref() == path.as_ref() {
                        Some(l.skip_rando)
                    } else {
                        None
                    }
                })
                .unwrap_or(false)
    }

    /// Whether the specified domain should be skipped.
    fn skip_rando_domain(&self, domain: &selinux::SeContext) -> bool {
        self.global.skip_rando
            || self
                .global
                .domains
                .iter()
                .find_map(|d| {
                    if domain.typ() == d.setype {
                        Some(d.skip_rando)
                    } else {
                        None
                    }
                })
                .unwrap_or(false)
    }

    fn skip_rando_context(&self, context: Option<&selinux::SeContext>) -> bool {
        if let Some(search_context) = context {
            debugln!(1, "checking {:?} domain against lfr.toml", search_context);
            if self.skip_rando_domain(search_context) {
                debugln!(
                    1,
                    "Type domain matches restricted context in lfr.toml: {:?}",
                    search_context
                );
                return true;
            } else {
                debugln!(1, "{:?} domain NOT FOUND lfr.toml", search_context);
            }
        }
        false
    }
    /// Whether randomization should be applied to a given file.
    pub fn skip_rando<P>(&self, path: Option<P>, context: &selinux::SeLinuxContext) -> bool
    where
        P: AsRef<Path>,
    {
        self.skip_rando_context(context.context()) || self.skip_rando_file(path)
    }
}
