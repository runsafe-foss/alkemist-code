// Copyright (c) 2022 RunSafe Security Inc.

//! # Modifying the `eh_frame` for LFR.
//! Using [`lfrgdb_update_eh_frame`] will updated all initial addresses
//! found in `eh_frame` `FDE`s. This is something not currently done
//! in `liblfr.so` and allows common stack unwinders to correctly
//! validate they are in the correct entry. Some don't use this sanity check.

use crate::gimli_writer::Writer;
use alloc::vec::Vec;
use anyhow::{anyhow, Error, Result};
use fallible_iterator::FallibleIterator;

/// Data Presented to [`lfrgdb_update_eh_frame`]
pub type RelocateData = *const libc::c_void;
/// Function Presented to [`lfrgdb_update_eh_frame`]
pub type RelocateFn = unsafe extern "C" fn(RelocateData, usize) -> usize;

use gimli::Endianity;

/// [`OffsetEndianVec`] is a fake EndianVec that allows
/// the user to set an arbitrary starting address.
/// This allows pointers written into the vector to take into consideration
/// PCRel offsets without having to write the entire Entry.
#[derive(Debug)]
pub struct OffsetEndianVec<'a, Endian>
where
    Endian: Endianity,
{
    /// Slice Starting Address
    base: usize,
    /// Desired Starting Offset
    offset: usize,
    /// Buffer
    buf: &'a mut [u8],
    /// Endianness of Buffer
    endian: Endian,
}

impl<'a, Endian> OffsetEndianVec<'a, Endian>
where
    Endian: Endianity,
{
    /// Create a new OffsetEndianVec at a specified address.
    fn new(base: usize, offset: usize, endian: Endian, buf: &'a mut [u8]) -> Self {
        Self {
            base,
            offset,
            endian,
            buf,
        }
    }
}

/// Implementation of [`crate::gimli_writer::Writer`] for OffsetEndianVec.
impl<Endian> Writer for OffsetEndianVec<'_, Endian>
where
    Endian: Endianity,
{
    type Endian = Endian;
    fn endian(&self) -> Self::Endian {
        self.endian
    }
    /// The len method reports that it has a different length than the internal
    /// buffer would imply.
    fn len(&self) -> usize {
        self.base + self.offset
    }
    fn write(&mut self, bytes: &[u8]) -> crate::gimli_writer::Result<()> {
        if self.offset > self.buf.len() {
            Err(crate::gimli_writer::Error::OffsetOutOfBounds)
        } else if self.buf.len().saturating_sub(self.offset) < bytes.len() {
            Err(crate::gimli_writer::Error::LengthOutOfBounds)
        } else {
            self.buf[self.offset..][..bytes.len()].copy_from_slice(bytes);
            self.offset += bytes.len();
            Ok(())
        }
    }
    /// Writes after the starting offset, but not before it. If this was
    /// used as a general purpose buffer, handling that case might make sense
    /// but we will only be writing after the start.
    fn write_at(&mut self, offset: usize, bytes: &[u8]) -> crate::gimli_writer::Result<()> {
        let old_offset = self.offset;
        self.offset = offset
            .checked_sub(self.base)
            .ok_or(crate::gimli_writer::Error::OffsetOutOfBounds)?;
        let result = self.write(bytes);
        self.offset = old_offset;
        result
    }
}

#[repr(C)]
pub struct RustModuleInfo {
    pub args: libc::uintptr_t,
    pub dynamic: libc::uintptr_t,
}

pub fn update_eh_frame_new<F>(
    mem: &mut crate::memregion::MemRegion,
    eh_frame_hdr: usize,
    relocate: F,
) -> Result<()>
where
    F: FnMut(usize) -> usize,
{
    if eh_frame_hdr == 0 {
        // we don't know where to look to find .eh_frame
        return Err(anyhow!("EhFrameHdr address unspecified"));
    }
    let eh_frame_hdr_segment = mem.longest_slice_mut(eh_frame_hdr).unwrap();

    let bases = gimli::BaseAddresses::default().set_eh_frame_hdr(eh_frame_hdr as u64);

    let eh_frame_ptr = {
        // Find the Elf Hdr and the pointer to the targeted eh_frame
        let eh_frame_hdr = gimli::read::EhFrameHdr::new(eh_frame_hdr_segment, gimli::NativeEndian);
        let eh_frame_hdr = eh_frame_hdr
            .parse(&bases, core::mem::size_of::<usize>() as u8)
            .expect("Valid Header");
        let eh_frame_ptr = eh_frame_hdr
            .eh_frame_ptr()
            .direct()
            .expect("Eh_frame should always be a direct pointer");

        eh_frame_ptr as usize
    };

    let bases = bases.set_eh_frame(eh_frame_ptr as u64);

    let first = core::cmp::min(eh_frame_hdr, eh_frame_ptr);
    let last = core::cmp::max(eh_frame_hdr, eh_frame_ptr);

    let eh_segment = mem.longest_slice_mut(first as usize).unwrap();

    let (segment1, segment2) = eh_segment.split_at_mut((last - first) as usize);

    let (eh_frame_hdr_mem, eh_frame_mem) = if first == eh_frame_hdr {
        (segment1, segment2)
    } else {
        (segment2, segment1)
    };
    process_eh_frame(&bases, eh_frame_hdr_mem, eh_frame_mem, relocate)
}

pub fn update_eh_frame<F>(
    module_info: Option<&bionic::phdr::RustModuleInfo>,
    eh_frame_hdr: usize,
    relocate: F,
) -> Result<()>
where
    F: FnMut(usize) -> usize,
{
    if eh_frame_hdr == 0 {
        // we don't know where to look to find .eh_frame
        return Err(anyhow!("EhFrameHdr address unspecified"));
    }
    let eh_frame_hdr_segment = bionic::phdr::find_segment_trim(module_info, eh_frame_hdr);
    let res = eh_frame_hdr_segment
        .map(|whole_seg| {
            let bases = gimli::BaseAddresses::default().set_eh_frame_hdr(whole_seg.start as u64);
            let exclusive_segment = unsafe {
                core::slice::from_raw_parts_mut(
                    whole_seg.start as *mut _,
                    whole_seg.end - whole_seg.start,
                )
            };
            let eh_frame_ptr = {
                // Find the Elf Hdr and the pointer to the targeted eh_frame
                let eh_frame_hdr =
                    gimli::read::EhFrameHdr::new(exclusive_segment, gimli::NativeEndian);
                let eh_frame_hdr = eh_frame_hdr
                    .parse(&bases, core::mem::size_of::<usize>() as u8)
                    .expect("Valid Header");
                let eh_frame_ptr = match eh_frame_hdr.eh_frame_ptr() {
                    gimli::Pointer::Direct(d) => d,
                    _ => panic!("Eh_Frame should always be a direct pointer"),
                };

                eh_frame_ptr as usize
            };
            let (elf_hdr, eh_frame_mem) = if whole_seg.contains(&eh_frame_ptr) {
                exclusive_segment.split_at_mut(eh_frame_ptr - whole_seg.start)
            } else {
                let mut eh_frame_segment =
                    bionic::phdr::find_segment_trim(module_info, eh_frame_ptr)
                        .ok_or_else(|| anyhow!("Can't find eh_frame"))?;

                // If eh_frame_hdr is after eh_frame in the segment, reduce the eh_frame_hdr_segment size.
                if eh_frame_segment.contains(&whole_seg.start) {
                    eh_frame_segment.end = whole_seg.start;
                }

                let eh_frame_mem = unsafe {
                    core::slice::from_raw_parts_mut(
                        eh_frame_segment.start as *mut _,
                        eh_frame_segment.end - eh_frame_segment.start,
                    )
                };
                (exclusive_segment, eh_frame_mem)
            };
            // Get the eh_frame and update it
            let bases = bases.set_eh_frame(eh_frame_ptr as u64);
            process_eh_frame(&bases, elf_hdr, eh_frame_mem, relocate)
        })
        .transpose();

    res.map(|_| ())
}

pub fn process_eh_frame<F>(
    bases: &gimli::BaseAddresses,
    elf_hdr: &[u8],
    eh_frame_mem: &mut [u8],
    relocate: F,
) -> Result<()>
where
    F: FnMut(usize) -> usize,
{
    let eh_frame_hdr = gimli::read::EhFrameHdr::new(elf_hdr, gimli::NativeEndian);
    let eh_frame_hdr = eh_frame_hdr
        .parse(bases, core::mem::size_of::<usize>() as u8)
        .expect("Valid Header");

    let table = eh_frame_hdr
        .table()
        .ok_or(gimli::read::Error::TypeMismatch)
        .map_err(Error::msg)?;
    //println!("Eh Frame {:x?}", eh_frame);
    determine_eh_frame_changes(&table, eh_frame_mem, bases, relocate)
}

/// Updates the memory located in [`eh_frame_mem`], with the offsets in `bases`, using the `relocate` fn.
fn determine_eh_frame_changes<F, R>(
    eh_frame_hdr_table: &gimli::read::EhHdrTable<R>,
    eh_frame_mem: &mut [u8],
    bases: &gimli::BaseAddresses,
    mut relocate: F,
) -> Result<()>
where
    F: FnMut(usize) -> usize,
    R: gimli::Reader<Offset = usize>,
{
    use gimli::read::UnwindSection;
    struct NewEhPointer {
        target: usize,
        relocated: crate::gimli_writer::Address,
        eh_pe: gimli::constants::DwEhPe,
        size: u8,
    }
    //println!("Updating eh_frames");
    let eh_frame = gimli::read::EhFrame::new(eh_frame_mem, gimli::NativeEndian);
    let mut recent_cie: Option<(
        gimli::EhFrameOffset<R::Offset>,
        gimli::read::CommonInformationEntry<_>,
    )> = None;

    let rewrites = eh_frame_hdr_table
        .iter(bases)
        .map(|(_from_ptr, to_ptr)| {
            let offset = eh_frame_hdr_table.pointer_to_offset(to_ptr)?;
            //let partial_fde = eh_frame.partial_fde_from_offset(bases, offset)?;
            let fde = eh_frame.fde_from_offset(bases, offset, |eh_frame, bases, offset| {
                if let Some(cie) = recent_cie
                    .as_ref()
                    .filter(|(cie_offset, _cie)| cie_offset == &offset)
                    .map(|r| &r.1)
                {
                    Ok(cie.clone())
                } else {
                    let new_cie = gimli::read::EhFrame::cie_from_offset(eh_frame, bases, offset)?;
                    recent_cie = Some((offset, new_cie.clone()));
                    Ok(new_cie)
                }
            })?;

            //println!("Parsing fde {:x?}", fde.offset()..(fde.offset() + fde.entry_len()));
            let mut pc = fde.offset()
                + core::mem::size_of::<u32>() //length
                + core::mem::size_of::<u32>() // cie pointer
                ;
            pc += if fde.entry_len() >= usize::try_from(u32::max_value()).unwrap() {
                4
            } else {
                0
            };

            let fde_encoding = fde
                .cie()
                .fde_address_encoding()
                .ok_or(gimli::read::Error::UnknownForm)?;

            //println!("FDE Initial Address {:x?}", fde.initial_address());
            let relocated = relocate(fde.initial_address() as usize);

            Ok(NewEhPointer {
                target: pc,
                relocated: crate::gimli_writer::Address::Constant(relocated as u64),
                eh_pe: fde_encoding,
                size: fde.cie().encoding().address_size,
            })
        })
        .collect::<Vec<_>>()
        .map_err(Error::msg)?;

    fallible_iterator::convert(
        rewrites
            .into_iter()
            .map(Result::<_, gimli::read::Error>::Ok),
    )
    .for_each(|cie_data| {
        //println!("Updated {}, {:x?}", cie_data.target, cie_data.relocated);
        let mut writer = OffsetEndianVec::new(
            bases.eh_frame.section.unwrap_or(0) as usize,
            cie_data.target,
            gimli::NativeEndian,
            eh_frame_mem,
        );
        writer
            .write_eh_pointer(cie_data.relocated, cie_data.eh_pe, cie_data.size)
            .map_err(|_| gimli::read::Error::UnknownPointerEncoding)?;

        Ok(())
    })
    .map_err(Error::msg)?;

    Ok(())
}

#[cfg(test)]
pub mod test {
    use super::*;
    use goblin;
    use std::collections::HashMap;

    #[derive(Debug)]
    pub struct Sections {
        items: HashMap<String, goblin::elf::SectionHeader>,
    }

    impl Sections {
        pub fn new(object: &goblin::Object<'_>) -> Self {
            use goblin::Object;
            let sections = match object {
                Object::Elf(elf) => elf
                    .section_headers
                    .iter()
                    .map(|s| (elf.shdr_strtab.get_at(s.sh_name), s.clone()))
                    .flat_map(|(name, s)| name.map(|name| (name.to_string(), s)))
                    .collect::<HashMap<_, _>>(),
                _ => unimplemented!(),
            };
            Self { items: sections }
        }

        fn bases(
            &self,
        ) -> Vec<(
            goblin::elf::section_header::SectionHeader,
            Box<fn(gimli::BaseAddresses, u64) -> gimli::BaseAddresses>,
        )> {
            let set: Vec<(_, fn(gimli::BaseAddresses, u64) -> gimli::BaseAddresses)> = vec![
                (".text", gimli::BaseAddresses::set_text),
                (".eh_frame", gimli::BaseAddresses::set_eh_frame),
                (".eh_frame_hdr", gimli::BaseAddresses::set_eh_frame_hdr),
                (".got", gimli::BaseAddresses::set_got),
            ];
            set.into_iter()
                .flat_map(|(name, f)| (self.items.get(name).map(|v| (v.clone(), Box::new(f)))))
                .collect()
        }
        pub fn file_bases(&self) -> gimli::BaseAddresses {
            self.bases()
                .into_iter()
                .map(|(v, f)| (v.file_range().unwrap().start as u64, f))
                .fold(gimli::BaseAddresses::default(), |state, (v, f)| f(state, v))
        }

        pub fn mem_range(
            &self,
            section: &str,
            image_base: usize,
        ) -> Option<std::ops::Range<usize>> {
            self.items.get(section).map(|s| {
                let start = image_base + s.vm_range().start;
                start..(start + s.vm_range().len())
            })
        }

        pub fn file_mut<'a>(&self, section: &str, file: &'a mut [u8]) -> Option<&'a mut [u8]> {
            self.items
                .get(section)
                .map(move |s| &mut file[s.file_range().unwrap()])
        }
    }
}
