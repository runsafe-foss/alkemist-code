/* Copyright (c) 2022 RunSafe Security Inc. */

//! Interface to safely access known valid regions of memory.
use crate::log::debugln;
use crate::os::MemGuard;
use alloc::vec::Vec;
use arch::{TargetError, TargetSlice, TargetSliceMut};
use bionic::mem::MemMut;
use bionic::phdr::SegmentType;
use core::ops::Range;
use modinfo::TargetAddr;

pub struct TargetMem<'a, 'context> {
    segments: &'a mut [MemGuard<'context, MemMut>],
    got: TargetAddr<usize>,
}

impl<'a, 'context> TargetMem<'a, 'context> {
    pub fn new(segments: &'a mut [MemGuard<'context, MemMut>], got: TargetAddr<usize>) -> Self {
        Self { segments, got }
    }
    pub fn segments(&mut self) -> impl Iterator<Item = &mut MemGuard<'context, MemMut>> {
        self.segments.iter_mut()
    }
    pub fn regions(&mut self) -> Result<MemRegion, MemRegionErr> {
        self.segments
            .iter_mut()
            .filter(|x| x.seg_type() == SegmentType::Load)
            .try_fold(MemRegion::with_got(self.got), |mut mem, seg| {
                mem.add_slice_mut(
                    seg.addr().as_ptr() as usize,
                    seg.file_addr(),
                    seg.slice_mut(),
                )?;
                Ok(mem)
            })
            .map_err(|e| {
                debugln!(1, "ERROR: Failed to construct memory interface. {:?}", e);
                e
            })
    }
}

/// Location In Memory
#[derive(Copy, Clone, Debug)]
pub struct Location {
    /// Virtual Address
    virt: usize,
    /// An optional physical address (useful for remapping from disk).
    phys: Option<usize>,
}

/// A slice of memory
///
/// It can be either mutable or immmutable. It is very useful to mark sections
/// of memory as immutable as it allows you to keep references and read while writing
/// to other mutable memory.
pub enum MemSlice<'a> {
    /// Mutable Slice
    RefMut(Location, &'a mut [u8]),
    /// Immutable Slice
    Ref(Location, &'a [u8]),
}

impl core::fmt::Debug for MemSlice<'_> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> Result<(), core::fmt::Error> {
        f.debug_struct("MemSlice")
            .field("mutable", &self.is_mut())
            .field("range", &self.range())
            .finish()
    }
}

#[derive(Default, Debug)]
struct Slices<'a> {
    v: Vec<MemSlice<'a>>,
}

impl<'a> Slices<'a> {
    fn find_partition(&self, addr: usize) -> usize {
        self.v.partition_point(|s| s.range().end <= addr)
    }
    fn remove(&mut self, addr: usize) -> Option<MemSlice<'a>> {
        let idx = self.find_partition(addr);
        self.v.get(idx).filter(|s| s.loc().virt == addr)?;
        Some(self.v.remove(idx))
    }
    fn insert(&mut self, slice: MemSlice<'a>) {
        let idx = self.find_partition(slice.loc().virt);
        self.v.insert(idx, slice);
    }
    fn find_contains(&self, addr: usize) -> Option<&MemSlice<'a>> {
        let idx = self.find_partition(addr);
        self.v.get(idx).filter(|s| s.range().contains(&addr))
    }
    fn find_region(&self, search: &Range<usize>) -> Option<&MemSlice<'a>> {
        self.find_contains(search.start)
            .filter(|s| s.contains(search))
    }
    fn find_region_mut(&mut self, search: &Range<usize>) -> Option<&mut MemSlice<'a>> {
        let idx = self.find_partition(search.start);
        self.v.get_mut(idx).filter(|s| s.contains(search))
    }

    fn slices(&self) -> impl Iterator<Item = &MemSlice<'a>> {
        self.v.iter()
    }
    fn slices_mut(&mut self) -> impl Iterator<Item = &mut MemSlice<'a>> {
        self.v.iter_mut()
    }
}

/// A collection of slices of memory that can be safely accessed.
#[derive(Default, Debug)]
pub struct MemRegion<'a> {
    /// All accessible slices of memory.
    slices: Slices<'a>,
    /// The Global Offset Table Address
    got_address: Option<TargetAddr<usize>>,
}

impl arch::Target<usize> for MemRegion<'_> {
    fn contains(&self, vaddr: TargetAddr<usize>) -> bool {
        self.slice(vaddr.absolute_addr(), 1).is_some()
    }
    fn contains_range(&self, vaddr: TargetAddr<usize>, len: usize) -> bool {
        self.slice(vaddr.absolute_addr(), len).is_some()
    }
    fn got_address(&self) -> Option<TargetAddr<usize>> {
        self.got_address
    }
}
impl arch::TargetRead<usize> for MemRegion<'_> {
    fn read_vaddr_max_slice(
        &self,
        vaddr: TargetAddr<usize>,
    ) -> Result<TargetSlice<'_, usize>, TargetError> {
        let slice = self
            .longest_slice(vaddr.into())
            .ok_or(TargetError::OutOfRange)?;

        Ok(TargetSlice::new(slice, vaddr, self.got_address))
    }
}

impl arch::TargetWrite<usize> for MemRegion<'_> {
    fn write_vaddr_slice(
        &mut self,
        vaddr: TargetAddr<usize>,
        len: usize,
    ) -> Result<TargetSliceMut<'_, usize>, TargetError> {
        let got_addr = self.got_address;
        let slice = self
            .slice_mut(vaddr.into(), len)
            .ok_or(TargetError::OutOfRange)?;

        Ok(TargetSliceMut::new(slice, vaddr, got_addr))
    }
    fn contiguous_slice_containing_mut(
        &mut self,
        vaddr: TargetAddr<usize>,
    ) -> Result<TargetSliceMut<'_, usize>, TargetError> {
        let got_addr = self.got_address;
        let search = vaddr.absolute_addr()..vaddr.absolute_addr() + 1;
        let (addr, slice) = self
            .slices
            .find_region_mut(&search)
            .and_then(|x| match x {
                MemSlice::RefMut(addr, slice) => Some((addr, slice)),
                _ => None,
            })
            .ok_or(TargetError::OutOfRange)?;

        Ok(TargetSliceMut::new(
            slice,
            TargetAddr::from_raw(addr.virt),
            got_addr,
        ))
    }
}

impl<'a> MemSlice<'a> {
    /// Location of slice.
    pub fn loc(&self) -> &Location {
        match self {
            MemSlice::RefMut(addr, _) | MemSlice::Ref(addr, _) => addr,
        }
    }
    /// Virtual address of slice.
    pub fn addr(&self) -> usize {
        self.loc().virt
    }
    /// Physical address of slice.
    pub fn phys(&self) -> Option<usize> {
        self.loc().phys
    }
    /// The length of the slice.
    pub fn len(&self) -> usize {
        self.slice().len()
    }
    /// Whether this slice is mutable.
    fn is_mut(&self) -> bool {
        match self {
            MemSlice::RefMut(_, _) => true,
            MemSlice::Ref(_, _) => false,
        }
    }
    /// Get the immutable access to the slice.
    pub fn slice(&self) -> &[u8] {
        match self {
            MemSlice::RefMut(_, slice) => slice,
            MemSlice::Ref(_, slice) => slice,
        }
    }
    /// Get the mutable access to mutable slices.
    pub fn slice_mut(&mut self) -> Option<&mut [u8]> {
        match self {
            MemSlice::RefMut(_, slice) => Some(slice),
            MemSlice::Ref(_, _slice) => None,
        }
    }
    /// The range of the given slice.
    pub fn range(&self) -> Range<usize> {
        let loc = self.loc();
        let slice = self.slice();
        loc.virt..loc.virt + slice.len()
    }

    // The physical address range of the slice.
    pub fn phys_range(&self) -> Option<Range<usize>> {
        let loc = self.loc();
        let slice = self.slice();
        Some(loc.phys?..loc.phys? + slice.len())
    }

    /// Split the slice into two mutable pieces.
    ///
    /// Panics if the address is not in the slice.
    pub fn split_at_mut(self, virt: usize) -> (MemSlice<'a>, MemSlice<'a>) {
        match self {
            MemSlice::RefMut(loc, slice) => {
                let (left, right) = slice.split_at_mut(virt - loc.virt);
                (
                    MemSlice::RefMut(loc, left),
                    MemSlice::RefMut(
                        Location {
                            virt,
                            phys: loc.phys.map(|p| p + virt - loc.virt),
                        },
                        right,
                    ),
                )
            }
            _ => panic!("Can't split regular ref."),
        }
    }
    /// Convert the slice into an immutable slice.
    pub fn into_ro(self) -> MemSlice<'a> {
        match self {
            MemSlice::RefMut(loc, slice) => MemSlice::Ref(loc, slice),
            s @ MemSlice::Ref(_, _) => s,
        }
    }
    /// Whether this slice contains the entire address range.
    fn contains(&self, needle: &Range<usize>) -> bool {
        let haystack = self.range();
        haystack.contains(&needle.start)
            && (haystack.end == needle.end || haystack.contains(&needle.end))
    }
    /// Whether the slice overlaps with any portion of the other range.
    fn overlap(&self, other: &Range<usize>) -> bool {
        self.overlap_range(other).is_some()
    }
    /// Provide the overlapping region of the other range.
    fn overlap_range(&self, other: &Range<usize>) -> Option<Range<usize>> {
        let a = self.range();
        let b = other;

        if b.contains(&a.start) {
            Some(a.start..core::cmp::min(a.end, b.end))
        } else if a.contains(&b.start) {
            Some(b.start..core::cmp::min(a.end, b.end))
        } else {
            None
        }
    }

    /// Whether slice is empty.
    pub fn is_empty(&self) -> bool {
        self.slice().is_empty()
    }
}

/// Errors relating the [`MemRegion`].
#[derive(Debug)]
pub enum MemRegionErr {
    /// There was an overlapping region detected.
    OverlappingSlice,
}

impl<'a> MemRegion<'a> {
    /// Create a new [`MemRegion`] with the Global Offset Table Address.
    pub fn with_got(got_address: TargetAddr<usize>) -> Self {
        Self {
            slices: Slices { v: Vec::default() },
            got_address: Some(got_address),
        }
    }
    /// Copy into the [`MemRegion`] the entire specified slice to the target address.
    pub fn copy_from_slice(&mut self, start: usize, slice: &[u8]) -> Result<(), MemRegionErr> {
        debugln!(1, "Want copy {:x}::{}", start, slice.len());

        let range = start..start + slice.len();
        self.slices
            .slices_mut()
            .filter(|x| x.overlap(&range))
            .for_each(|x| {
                let overlap = x.overlap_range(&range).unwrap();
                match x {
                    MemSlice::RefMut(slice_start, mut_slice) => {
                        debugln!(
                            1,
                            "Copied to {:x}::{}",
                            overlap.start,
                            overlap.end - overlap.start
                        );
                        mut_slice[overlap.start - slice_start.virt..overlap.end - slice_start.virt]
                            .copy_from_slice(&slice[overlap.start - start..overlap.end - start]);
                    }
                    _ => {
                        debugln!(1, "Tried to copy to read only region");
                        //maybe fail? or panic?
                    }
                }
            });
        Ok(())
    }
    /// Provide an iterator of all slices (immutable)
    pub fn slices(&self) -> impl Iterator<Item = &MemSlice> {
        self.slices.slices()
    }
    /// Provide an iterator of all slices (mutable)
    pub fn slices_mut(&mut self) -> impl Iterator<Item = &mut MemSlice<'a>> {
        self.slices.slices_mut()
    }

    /// Add a new mutable slice of memory.
    pub fn add_slice_mut(
        &mut self,
        addr: usize,
        phys: Option<usize>,
        slice: &'a mut [u8],
    ) -> Result<(), MemRegionErr> {
        let search = addr..addr + slice.len();

        if let Some(maybe_overlap) = self.slices.find_contains(search.end) {
            if maybe_overlap.overlap(&search) {
                return Err(MemRegionErr::OverlappingSlice);
            }
        }

        self.slices
            .insert(MemSlice::RefMut(Location { virt: addr, phys }, slice));

        #[cfg(test)]
        self.sanity();
        Ok(())
    }
    /// Add a new immutable slice of memory.
    pub fn add_slice(
        &mut self,
        addr: usize,
        phys: Option<usize>,
        slice: &'a [u8],
    ) -> Result<(), MemRegionErr> {
        let search = addr..addr + slice.len();

        if let Some(maybe_overlap) = self.slices.find_contains(search.end) {
            if maybe_overlap.overlap(&search) {
                return Err(MemRegionErr::OverlappingSlice);
            }
        }

        self.slices
            .insert(MemSlice::Ref(Location { virt: addr, phys }, slice));

        #[cfg(test)]
        self.sanity();
        Ok(())
    }

    /// Get a slice that is explicitly only immutable.
    pub fn ro_slice(&self, addr: usize, len: usize) -> Option<&'a [u8]> {
        let search = addr..addr + len;
        self.slices.find_region(&search).and_then(|x| match x {
            MemSlice::Ref(addr, slice) => Some(&slice[search.start - addr.virt..][..len]),
            _ => None,
        })
    }

    /// Get a slice of the specified target region.
    pub fn slice(&self, addr: usize, len: usize) -> Option<&[u8]> {
        self.longest_slice(addr).and_then(|slice| slice.get(..len))
    }

    /// Get the longest contiguous slice of the target region starting at `addr`.
    pub fn longest_slice(&self, addr: usize) -> Option<&[u8]> {
        let search = addr..addr + 1;
        self.slices
            .find_region(&search)
            .map(|x| match x {
                MemSlice::Ref(addr, slice) => (addr, *slice),
                MemSlice::RefMut(addr, slice) => (addr, &**slice),
            })
            .map(|(addr, slice)| &slice[search.start - addr.virt..])
    }

    /// Get the longest mutable contiguous slice of the target region start at `addr`.
    pub fn longest_slice_mut(&mut self, addr: usize) -> Option<&mut [u8]> {
        let search = addr..addr + 1;
        self.slices.find_region_mut(&search).and_then(|x| match x {
            MemSlice::RefMut(addr, slice) => Some(&mut slice[search.start - addr.virt..]),
            _ => None,
        })
    }

    /// Get a mutable slice of the region at the specified location and length.
    pub fn slice_mut(&mut self, addr: usize, len: usize) -> Option<&mut [u8]> {
        self.longest_slice_mut(addr)
            .and_then(|slice| slice.get_mut(..len))
    }

    #[cfg(test)]
    fn sanity(&self) {
        let mut prev: Option<&MemSlice<'a>> = None;
        self.slices.slices().for_each(|r| {
            if let Some(p) = prev {
                assert!(p.addr() < r.addr());
                assert!(p.addr() + p.len() <= r.addr());
            }
            prev = Some(r);
        });

        prev = None;
        self.slices.slices().for_each(|r| {
            if let Some(p) = prev {
                assert!(p.range().end <= r.range().start);
            }
            prev = Some(r);
        });
    }

    /// Force a region to be explicitly read only.
    pub fn force_read_only(&mut self, addr: usize, len: usize) -> Option<&'a [u8]> {
        if len == 0 {
            return Some(&[]);
        }
        if let Some(slice) = self.ro_slice(addr, len) {
            return Some(slice);
        }

        let search = addr..addr + len;

        let found_key = self
            .slices
            .find_region(&search)
            .filter(|x| matches!(x, MemSlice::RefMut(_, _)) && x.contains(&search))
            .map(|x| x.loc().virt)?;

        let removed = self.slices.remove(found_key)?;

        let (left, mid) = removed.split_at_mut(search.start);
        let (mid, right) = mid.split_at_mut(search.end);

        let mid = mid.into_ro();

        [left, mid, right]
            .into_iter()
            .filter(|s| !s.is_empty())
            .for_each(|s| {
                self.slices.insert(s);
            });

        #[cfg(test)]
        self.sanity();
        self.ro_slice(addr, len)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use proptest::prelude::*;

    fn create_mem_strat() -> impl Strategy<Value = Vec<(usize, Vec<u8>)>> {
        //let mut addr = 0;
        proptest::collection::vec(proptest::collection::vec(any::<u8>(), 1..100), 0..200)
            .prop_flat_map(|v| {
                let mut addr = 0;
                Just(
                    v.into_iter()
                        .map(|x| {
                            let len = x.len();
                            let item = (addr, x);
                            addr += len;
                            item
                        })
                        .collect::<Vec<_>>(),
                )
            })
            .prop_shuffle()
    }
    proptest! {
        #[test]
        fn test_lookup(mut mem_vecs in create_mem_strat()) {
            let mut mem = MemRegion::default();
            let mut dupe_mem = mem_vecs.clone();

            for (addr, e) in &mut mem_vecs {
                mem.add_slice_mut(*addr, None, &mut e[..]).unwrap();
            }

            for (addr, e) in &mut dupe_mem {
                for off in 0..e.len() {
                    assert_eq!(mem.slice(*addr + off, e.len() - off), Some(&e.as_slice()[off..]));
                    assert_eq!(mem.slice_mut(*addr + off, e.len() - off), Some(&mut e.as_mut_slice()[off..]));
                    assert_eq!(mem.longest_slice(*addr + off), Some(&e.as_slice()[off..]));
                }
            }

            let regions = mem.slices().map(|x| (x.loc().virt, x.slice().to_owned())).collect::<Vec<_>>();
            let mut orig = mem_vecs.iter().map(|(k, v)| (*k, v.as_slice().to_owned())).collect::<Vec<_>>();
            orig.sort_by_key(|(k, _)| *k);
            assert_eq!(regions, orig);
        }
    }
    #[test]
    fn test2() {
        let mut mem = MemRegion::default();

        let s1 = &mut [1, 2, 3, 4, 5];
        mem.add_slice_mut(0, Some(0), s1).unwrap();
        let s2 = &mut [10, 9, 8, 7, 6];
        mem.add_slice_mut(100, Some(100), s2).unwrap();

        let s3 = &mut [1, 1, 2, 3, 5, 8, 13];
        mem.add_slice_mut(200, Some(200), s3).unwrap();

        let s4 = &mut [3, 1, 4, 1, 5];
        mem.add_slice_mut(300, Some(300), s4).unwrap();

        println!("{:?}", mem);
        mem.force_read_only(0, 5).unwrap();
        println!("{:?}", mem);
        mem.force_read_only(100, 5).unwrap();

        mem.sanity();

        {
            let r1 = mem.slice(2, 2);
            assert_eq!(r1, Some(&[3, 4][..]));
            let r2 = mem.slice(101, 2);
            assert_eq!(r2, Some(&[9, 8][..]));

            let r3 = mem.ro_slice(102, 2);
            assert_eq!(r3, Some(&[8, 7][..]));

            assert!(r1.is_some() && r2.is_some() && r3.is_some());
        }

        let write = mem.slice_mut(300, 3).unwrap();
        let written = &[6, 2, 8];
        write.copy_from_slice(written);

        assert_eq!(mem.slice(300, 3), Some(&written[..]));

        {
            let r3 = mem.ro_slice(102, 2);
            assert_eq!(r3, Some(&[8, 7][..]));

            let write = mem.slice_mut(300, 3).unwrap();
            write.copy_from_slice(&[6, 2, 8]);

            assert_eq!(r3, Some(&[8, 7][..]));
        }

        mem.sanity();
    }
    #[test]
    fn test3() {
        let s1 = &mut [1, 2, 3, 4, 5];
        {
            let mut mem = MemRegion::default();

            mem.add_slice_mut(0, Some(0), s1).unwrap();

            mem.force_read_only(2, 2);

            let r = mem.ro_slice(2, 2);
            assert_eq!(r, Some(&[3, 4][..]));

            assert_eq!(mem.slice_mut(0, 2), Some(&mut [1, 2][..]));
            assert_eq!(mem.slice_mut(4, 1), Some(&mut [5][..]));
            let w = mem.slice_mut(0, 2).unwrap();
            w.copy_from_slice(&[10, 11]);
            assert_eq!(mem.slice_mut(0, 2), Some(&mut [10, 11][..]));

            assert_eq!(r, Some(&[3, 4][..]));
        }
        assert_eq!(s1, &mut [10, 11, 3, 4, 5]);
    }

    #[test]
    fn test4() {
        let s1 = &mut [1, 2, 3, 4, 5];
        let s2 = &mut [6, 7, 8, 9, 10];
        let mut mem = MemRegion::default();
        mem.add_slice_mut(0, None, s1).unwrap();
        mem.add_slice_mut(5, None, s2).unwrap();

        for i in 0..10 {
            println!("{:?}", i..i + 1);
            mem.slice_mut(i, 1).expect("Legal read");
        }
        for i in 0..5 {
            for j in i + 1..5 {
                println!("{:?}", i..j - i);
                mem.slice_mut(i, j - i).expect("Legal read");
            }
        }
        assert!(mem.slice_mut(10, 1).is_none());
    }
}
