// Copyright (c) 2022 RunSafe Security Inc.

#![cfg_attr(not(any(test, feature = "std")), no_std)]
#![allow(internal_features)]
#![feature(core_intrinsics)]
#![feature(lang_items)]
#![feature(maybe_uninit_slice)]
#![feature(maybe_uninit_write_slice)]
#![feature(linkage)]
#![feature(atomic_from_mut)]
#![feature(is_sorted)]

//! # LFR GDB API

//! LFR GDB API currently contains an entire reimplementation of LFR in Rust. It grew to encapsulate
//! everything and is not stricly an API anymore. Go see [`crate::randolib`] for more information.
//!
//! This used to be simply to allow a client (like `liblfr.so`) to interact
//! with a protected chunk of memory and store information about the randomized process there.
//! This allows another external memory accesser (e.g. GDB) to read that memory
//! when triggered by calls to a designated function. This allows GDB to use that information
//! to generate the symbols for a randomized process.
//!
pub mod config;
pub mod eh_frame;
pub mod env;
pub mod gdb;
/// cbindgen:ignore
mod gimli_writer;
/// cbindgen:ignore
mod hide;
pub mod log;
#[cfg(not(any(test, feature = "std")))]
/// cbindgen:ignore
mod magic;
pub mod memregion;
pub mod randolib;
pub mod selinux;
pub use modinfo;
mod arch;
mod os;

use crate::hide::{HiddenVec, HideMem};
use anyhow::{Error, Result};
use lazy_static::lazy_static;
use lfr_meta::symbols::{CopyRange, CoreSymbolData, CoreSymbols, SymbolEntryV1, VecLike};
use spin::mutex::Mutex;

use core::ops::DerefMut;

extern crate alloc;

/// The Metadata will be stored at a location pointed to by this address with a length
/// at [`__liblfr_symbol_table_size`].
#[no_mangle]
#[used]
pub static mut __liblfr_symbol_table: *mut u8 = core::ptr::null_mut();

/// The Metadata will be of this length and is pointed to by [`__liblfr_symbol_table`].
#[no_mangle]
#[used]
pub static mut __liblfr_symbol_table_size: u64 = 0;

/// The Metadata that will be accessed by LFR GDB Support
#[derive(Default)]
struct Metadata {
    /// The serialized form of all entries. APPEND ONLY.
    entries: HiddenVec<u8>,
    /// The offsets to the entries in [`entries`]. APPEND ONLY.
    offsets: HiddenVec<u64>,
    /// The unserialized form of the metadata.
    serialize: HideMem<CoreSymbols>,
}

lazy_static! {
    /// The metadata is stored in a protected memory allocation, which has it's access controlled
    /// by the [`HideMem`] behind this mutex. The mutex isn't strictly neccesary because this will
    /// will never run in a multi-threaded context.
    static ref SYMBOL_MEM: Mutex<Metadata> = Mutex::new(Metadata::default());
}

/// Trim Info represents a function that has been trimmed. We need to keep track of this
/// because we cannot determine after a function has been trimmed whether it has been trimmed
/// or not and we need consistency.
#[repr(C)]
pub struct TrimInfo {
    /// Undiversified Starting Address
    pub undiv_start: u64,
    /// The newly trimmed size.
    pub new_size: u64,
}

/// Updates the information for gdb to find the metadata.
fn set_memory_info(ptr: *mut u8, len: usize) {
    unsafe {
        __liblfr_symbol_table = ptr;
        __liblfr_symbol_table_size = len as u64;
    }
}

/// Allows the modification of the protected memory to manipulate the symbols
/// contained therein and keeps the corresponding symbols up to date for GDB.
fn preserve_mem(entry: SymbolEntryV1) -> Result<bool> {
    //TODO : Deregistration should remove the metadata
    let mut lock = SYMBOL_MEM.lock();
    let metadata = lock.deref_mut();

    metadata.serialize.map(
        |mem| match mem {
            Ok(Some(CoreSymbols::V1(mut entries))) => {
                if !entries.iter().any(|e| e == &entry) {
                    entries.push(entry);
                }
                Ok(CoreSymbols::V1(entries))
            }
            Ok(None | Some(CoreSymbols::V2(_))) => {
                let mut encoded = alloc::vec![];
                ciborium::into_writer(&entry, &mut encoded).map_err(Error::msg)?;
                let range = metadata
                    .entries
                    .extend_range(encoded.into_iter())
                    .map_err(Error::msg)?;
                metadata
                    .offsets
                    .push(
                        <CopyRange<_>>::from(range)
                            .to_u64()
                            .map_err(Error::msg)?
                            .end,
                    )
                    .map_err(Error::msg)?;
                Ok(CoreSymbols::V2(CoreSymbolData {
                    entries: metadata.entries.stored_vec(),
                    offsets: metadata.offsets.stored_vec(),
                }))
            }
            Ok(Some(_)) => {
                anyhow::bail!("Unknown data storage");
            }
            Err(e) => Err(e),
        },
        Some(set_memory_info),
    )?;
    Ok(true)
}

/// Representation of a shuffled function that is FFI-safe.
#[repr(C)]
pub struct LfrGdbFunc {
    /// Undiversified Address
    pub undiv_start: u64,
    /// Diversified Address
    pub div_start: u64,
    /// Function Size
    pub size: u32,
}
