/* Copyright (c) 2022 RunSafe Security Inc. */
//! Architecture specific implementations for updating entrypoints, trampolines, etc.
use crate::memregion::MemRegion;
use crate::os::OsModule;
use crate::randolib::{FunctionList, Relocation};
use arch::Architecture;
use core::ops::Range;
use modinfo::{ModuleInfo, TargetAddr};

/// Error for when ICache is unable to flush.
pub struct FlushErr {}

/// Provides methods to manipulate a given [`MemRegion`] on a specific Architecture.
pub(crate) trait ArchRuntime<'a>:
    core::fmt::Debug + Architecture<MemRegion<'a>, Relocation>
where
    Relocation: arch::Relocation<<Self as Architecture<MemRegion<'a>, Relocation>>::UsizePtrTy>,
    MemRegion<'a>: arch::Target<
        <Self as Architecture<MemRegion<'a>, crate::randolib::Relocation>>::UsizePtrTy,
    >,
{
    /// Prepare a given architecture for randomization. This is run prior to ANY randomization.
    fn preprocess_arch();
    /// Relocate architecture specific structures to match the newly shuffled memory.
    fn relocate_arch(
        build: &mut MemRegion,
        module: &OsModule,
        info: &ModuleInfo,
        funcs: &FunctionList,
    );
    /// Update the entry points to match the newly shuffled memory.
    fn fixup_entry_point(
        mem: &mut MemRegion,
        entry_point: TargetAddr<Self::UsizePtrTy>,
        target: TargetAddr<Self::UsizePtrTy>,
    );
    /// Fix the export trampolines for a given architecture after randomization.
    fn fixup_export_trampoline(
        target: &mut MemRegion,
        export_section: Range<TargetAddr<usize>>,
        funcs: &FunctionList,
    );
}

/// Flush an Instruction cache for a given architecture.
pub trait FlushICache {
    /// Flushes the cache.
    fn flush_icache(segment: Range<usize>) -> Result<(), FlushErr>;
}

#[cfg(any(target_arch = "arm", test))]
mod arm;
#[cfg(target_arch = "aarch64")]
mod arm64;
#[cfg(target_arch = "x86")]
mod x86;
#[cfg(target_arch = "x86_64")]
mod x86_64;

/// Shared implementations for X86/X86_64 for relocating.
#[cfg(any(target_arch = "x86", target_arch = "x86_64"))]
mod x86_shared {
    use super::*;
    use crate::log::debugln;
    use arch::{RelocationIdx, TargetWrite};
    use trap_parser::TrapRelocAddend;
    pub(super) fn fixup_export_trampoline(
        mem: &mut MemRegion,
        reloc_type: RelocationIdx,
        mut export_section: Range<TargetAddr<usize>>,
        funcs: &FunctionList,
    ) {
        debugln!(
            3,
            "Export Trampoline Len: {}",
            TargetAddr::offset(&export_section.end, &export_section.start)
        );

        'done: while let Some(mut export) = mem.slice_mut(
            export_section.start.absolute_addr(),
            TargetAddr::offset(&export_section.end, &export_section.start),
        ) {
            let start_len = export.len();

            while !export.is_empty() && export[0] == 0xEB {
                // We hit the placeholder in Textramp.S, skip over it
                export = &mut export[2..];
            }
            if export.is_empty() {
                break 'done;
            }

            // Allow the first byte of the export trampoline to be 0xCC, which
            // is the opcode for the breakpoint instruction that gdb uses (INT 3)
            assert!(matches!(export[0], 0x90 | 0xCC));
            assert_eq!(export.as_ptr() as usize & 1, 0);
            export = &mut export[1..];

            // Skip any calls TrapLinker prepends, e.g., for the trampoline counter
            while 0xE8 == export[0] {
                let skip = &[0x0f, 0x1f, 0x44, 0x00, 0x00];
                let (copy_to, remain) = export.split_at_mut(skip.len());
                copy_to.copy_from_slice(skip);
                export = remain;
            }

            assert_eq!(export[0], 0xE9);

            let mut reloc = Relocation::new(
                reloc_type,
                export_section
                    .start
                    .wrapping_add(start_len + 1)
                    .wrapping_sub(export.len()),
                None,
                Some(TrapRelocAddend::Generic(-4)),
            );
            export = &mut export[5..];

            // If TrapLinker added one extra NOP to make the trampoline
            // even in size, then skip over that NOP too
            let trampoline_size = start_len - export.len();
            if trampoline_size & 1 != 0 {
                export = &mut export[1..];
            }
            export_section.start = export_section.start.wrapping_add(start_len - export.len());

            funcs.adjust_relocation(mem, &mut reloc, None, None);
        }
    }
    pub(super) fn fixup_entry_point(
        mem: &mut MemRegion,
        reloc_type: RelocationIdx,
        entry_point: TargetAddr<usize>,
    ) -> Relocation {
        let mut slice = mem.write_vaddr_slice(entry_point - 5, 7).unwrap();
        assert!(matches!(slice.as_mut(), &mut [0xE9, _, _, _, _, 0xEB, _]));
        slice.as_mut()[6] = -7i8 as u8;
        Relocation::new(
            reloc_type,
            entry_point - 4,
            None,
            Some(TrapRelocAddend::Generic(-4)),
        )
    }
}
