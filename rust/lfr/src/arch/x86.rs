/* Copyright (c) 2022 RunSafe Security Inc. */

//! X86 Specific Relocating
use super::{ArchRuntime, FlushErr, FlushICache};
use crate::memregion::MemRegion;
use crate::os::OsModule;
use crate::randolib::FunctionList;
use arch::{ArchX86, Architecture, RelocationIdx};
use core::ops::Range;
use modinfo::{ModuleInfo, TargetAddr};

impl<'a> ArchRuntime<'a> for ArchX86 {
    fn preprocess_arch() {}
    fn relocate_arch(
        _mem: &mut MemRegion,
        _module: &OsModule,
        _info: &ModuleInfo,
        _functions: &FunctionList,
    ) {
    }
    fn fixup_export_trampoline(
        mem: &mut MemRegion,
        export_section: Range<TargetAddr<usize>>,
        funcs: &FunctionList,
    ) {
        super::x86_shared::fixup_export_trampoline(
            mem,
            RelocationIdx(goblin::elf::reloc::R_386_PC32),
            export_section,
            funcs,
        )
    }
    fn fixup_entry_point(
        mem: &mut MemRegion,
        entry_point: TargetAddr<Self::UsizePtrTy>,
        target: TargetAddr<Self::UsizePtrTy>,
    ) {
        let reloc = super::x86_shared::fixup_entry_point(
            mem,
            RelocationIdx(goblin::elf::reloc::R_386_PC32),
            entry_point,
        );

        Self::set_target_ptr(&reloc, mem, target)
            .or_else(crate::randolib::ignore_unimplemented_reloc)
            .unwrap()
    }
}

impl FlushICache for arch::ArchX86 {
    fn flush_icache(_segment: Range<usize>) -> Result<(), FlushErr> {
        Ok(())
    }
}
