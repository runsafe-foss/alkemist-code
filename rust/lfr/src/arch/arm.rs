/* Copyright (c) 2022 RunSafe Security Inc. */
//! Arm Specific Relocating
use super::{ArchRuntime, FlushErr, FlushICache};
use crate::log::debugln;
use crate::memregion::MemRegion;
use crate::os::{OsModExt, OsModule};
use crate::randolib::{Function, FunctionList, Relocation};
use arch::{ArchArm, RelocationIdx, SignExtend, TargetRead, TargetWrite};
use bionic::phdr::SegmentType;
use bytemuck::{Pod, Zeroable};
use core::ops::Range;
use goblin::elf::reloc as Rel;
use modinfo::{ModuleInfo, TargetAddr};
use trap_parser::TrapRelocAddend;

const EXIDX_CANT_UNWIND: u32 = 1;

fn bit(v: u32, b: u32) -> bool {
    (v & (1u32 << b)) != 0u32
}

// This doesn't have any associated tests (yet)
#[allow(unused)]
fn update_nontrap_functions(func: &Function, functions: &FunctionList, mem: &mut MemRegion) {
    use itertools::Itertools;
    let mut start: usize = 0;

    'relocs_done: loop {
        type ArmInstr = u32;
        let func_mem = mem
            .slice_mut(
                func.div_start.into(),
                // Avoid bytemuck slop for oddly sized functions by masking off invalid function lengths.
                func.size.unwrap() & !(core::mem::size_of::<ArmInstr>() - 1),
            )
            .unwrap();

        let func_mem = match bytemuck::try_cast_slice_mut::<_, ArmInstr>(func_mem) {
            Ok(slice) => slice,
            Err(e) => {
                debugln!(10, "WARN: Unable to create function slice: {:?}", e);
                return;
            }
        };

        let reloc = func_mem
            .iter()
            .copied()
            .enumerate()
            .skip(start)
            .batching(|it| {
                use goblin::elf::reloc as Reloc;

                let (offset, first) = it.next()?;
                let mut preceded_by_thumb = false;
                let (offset, first) = if first == 0x46c04778 {
                    debugln!(
                        10,
                        "Found Thumb linker stub @{:x}",
                        func.undiv_start.wrapping_add(offset)
                    );
                    preceded_by_thumb = true;
                    it.next()?
                } else {
                    (offset, first)
                };

                let build_reloc = |offset: usize,
                                   typ: RelocationIdx,
                                   addend: Option<TrapRelocAddend>|
                 -> Option<(usize, Relocation)> {
                    Some((
                        offset + 1,
                        Relocation::new(
                            typ,
                            func.undiv_start
                                .wrapping_add(offset * core::mem::size_of::<u32>()),
                            None,
                            addend,
                        ),
                    ))
                };
                match first {
                    0xe51ff004 => {
                        debugln!(
                            10,
                            "Found ARM/Thumb linker stub A@{:x}",
                            func.undiv_start.wrapping_add(offset)
                        );
                        let (offset, _) = it.next()?;
                        Some(build_reloc(offset, RelocationIdx(Reloc::R_ARM_ABS32), None))
                    }
                    0xe59fc000 => {
                        debugln!(
                            10,
                            "Found ARM/Thumb linker stub B@{:x}",
                            func.undiv_start.wrapping_add(offset)
                        );
                        let (_, linker_stub) = it.next()?;
                        let (offset, _) = it.next()?;

                        let (reloc, addend) = match linker_stub {
                            0xe12fff1c => (RelocationIdx(Reloc::R_ARM_ABS32), None),
                            0xe08cf00f | 0xe08ff00c => (
                                RelocationIdx(Reloc::R_ARM_REL32),
                                Some(TrapRelocAddend::Generic(-4)),
                            ),
                            v => {
                                panic!(
                                    "Unexpected Linker Stub {:x}@{:x}",
                                    v,
                                    func.undiv_start.wrapping_add(offset)
                                )
                            }
                        };

                        Some(build_reloc(offset, reloc, addend))
                    }
                    0xe59fc004 => {
                        debugln!(
                            10,
                            "Found ARM/Thumb linker stub C@{:x}",
                            func.undiv_start.wrapping_add(offset)
                        );
                        assert!(matches!(it.next(), Some((_, 0xe08fc00c))));
                        assert!(matches!(it.next(), Some((_, 0xe12fff1c))));

                        let (offset, _) = it.next()?;
                        Some(build_reloc(offset, RelocationIdx(Reloc::R_ARM_REL32), None))
                    }
                    val if preceded_by_thumb => {
                        debugln!(
                            10,
                            "Found Thumb short branch stub @{:x}",
                            func.undiv_start.wrapping_add(offset)
                        );
                        assert_eq!(val >> 24, 0xea); // Make sure we're patching a B instruction
                        Some(build_reloc(
                            offset,
                            RelocationIdx(Reloc::R_ARM_JUMP24),
                            Some(TrapRelocAddend::Generic(-8)),
                        ))
                    }
                    val if (val & 0xd000f800) == 0x9000f000 => {
                        debugln!(
                            10,
                            "Found A8 veneer stub @{:x}",
                            func.undiv_start.wrapping_add(offset)
                        );
                        Some(build_reloc(
                            offset,
                            RelocationIdx(Reloc::R_ARM_THM_JUMP24),
                            Some(TrapRelocAddend::Generic(-4)),
                        ))
                    }
                    _ => Some(None),
                }
            })
            .flatten()
            .next();

        if let Some((offset, mut reloc)) = reloc {
            start = offset;
            functions
                .adjust_relocation(mem, &mut reloc, Some(func), None)
                .unwrap();
        } else {
            break 'relocs_done;
        }
    }
}

#[derive(Pod, Zeroable, Copy, Clone, Debug, PartialEq)]
#[repr(C)]
struct ExIdxEntry {
    key: u32,
    val: u32,
}

impl ExIdxEntry {
    fn absolute(&self, start: TargetAddr<u32>, idx: usize) -> ExIdxEntry {
        // We need to make every key word an absolute address,
        // so qsort can just compare them by value
        assert!(!bit(self.key, 31));

        let key = ExIdx::entry_addr(start, idx)
            .wrapping_add(self.key.sign_extend(31) as u32)
            .into();

        // Adjust every exidx value so it becomes relative to the start
        // of the section, instead of PC-relative
        //
        // We accomplish this by adding (4 * idx) to the PC-relative
        // word, since the following applies:
        // (Word - Exidx)
        //   = (Word - PC) + (PC - Exidx)
        //   = (Word - PC) + idx * sizeof(int32_t)
        //
        // We need to do this so we can move them around using qsort

        let val = if !bit(self.val, 31) && self.val != EXIDX_CANT_UNWIND {
            let delta = ExIdx::val_delta(idx);
            self.val.wrapping_add(u32::from(delta))
        } else {
            self.val
        };

        ExIdxEntry { key, val }
    }

    fn relative(&self, start: TargetAddr<u32>, idx: usize) -> ExIdxEntry {
        let delta = self
            .key
            .wrapping_sub(u32::from(ExIdx::entry_addr(start, idx)));
        let key = delta & 0x7fffffff;

        // Restore to PC-relative
        let val = if !bit(self.val, 31) && self.val != EXIDX_CANT_UNWIND {
            let delta = ExIdx::val_delta(idx);
            self.val.wrapping_sub(u32::from(delta))
        } else {
            self.val
        };

        ExIdxEntry { key, val }
    }
}

struct ExIdx<'a> {
    start: TargetAddr<u32>,
    entries: &'a mut [ExIdxEntry],
}

impl<'a> ExIdx<'a> {
    fn entry_addr(start: TargetAddr<u32>, idx: usize) -> TargetAddr<u32> {
        start.wrapping_add(u32::try_from(idx * core::mem::size_of::<ExIdxEntry>()).unwrap())
    }
    fn val_delta(idx: usize) -> TargetAddr<u32> {
        TargetAddr::from_raw(
            u32::try_from(
                (idx * core::mem::size_of::<ExIdxEntry>())
                    .wrapping_add(core::mem::size_of::<u32>()),
            )
            .unwrap(),
        )
    }

    fn relocate<F>(&mut self, mut relocate: F)
    where
        F: FnMut(TargetAddr<u32>) -> TargetAddr<u32>,
    {
        debugln!(
            1,
            ".ARM.exidx found {} entries at {:x}",
            self.entries.len(),
            self.start
        );

        self.entries
            .iter_mut()
            .enumerate()
            .for_each(|(idx, entry)| {
                let mut absolute = entry.absolute(self.start, idx);

                let new_addr = relocate(absolute.key.into());
                debugln!(6, "exidx[{}]: {:x} => {:x}", idx, entry.key, new_addr);
                absolute.key = new_addr.into();
                *entry = absolute;
            });

        self.entries.sort_by_key(|entry| entry.key);

        // Make the entry words PC-relative again
        self.entries
            .iter_mut()
            .enumerate()
            .for_each(|(idx, entry)| {
                *entry = entry.relative(self.start, idx);
            });
    }
}

#[cfg(target_arch = "arm")]
impl<'a> ArchRuntime<'a> for ArchArm {
    fn preprocess_arch() {}
    fn fixup_export_trampoline(
        mem: &mut MemRegion,
        export_section: Range<TargetAddr<usize>>,
        funcs: &FunctionList,
    ) {
        let range_iter = export_section.step_by(core::mem::size_of::<u32>());

        for ptr in range_iter {
            let reloc_type = match mem.read_vaddr::<u8>(ptr.wrapping_add(3u32)) {
                Ok(0xea) => RelocationIdx(Rel::R_ARM_JUMP24),
                Ok(_) => RelocationIdx(Rel::R_ARM_THM_JUMP24),
                Err(_e) => {
                    panic!("ERROR: Couldn't read export section ptr: {:x?}", ptr)
                }
            };

            let mut reloc = Relocation::new(reloc_type, ptr, None, None);
            funcs.adjust_relocation(mem, &mut reloc, None, None);
        }
    }
    fn relocate_arch(
        mem: &mut MemRegion,
        module: &OsModule,
        info: &ModuleInfo,
        functions: &FunctionList,
    ) {
        module
            .header_info()
            .iter()
            .filter(|h| h.segment_type == SegmentType::ArmExIdx)
            .for_each(|h| {
                let exidx_start = module.rva2address(h.vaddr);
                let exidx = mem.slice_mut(exidx_start.into(), h.memsz).unwrap();

                let entries = bytemuck::cast_slice_mut::<_, ExIdxEntry>(exidx);
                let mut exidx = ExIdx {
                    start: exidx_start.into(),
                    entries,
                };
                exidx.relocate(|old| {
                    let (_, new) = functions.adjust_target(old.into(), None);
                    new.into()
                });
            });

        let sections: &[_] = info.sections.into();
        sections.iter().for_each(|sec| {
            functions
                .funcs()
                .iter()
                .filter(|x| Range::from(sec.exec).contains(&x.div_start))
                .filter(|x| !(x.skip_copy || x.from_trap))
                .for_each(|func| update_nontrap_functions(func, functions, mem))
        });
    }
    fn fixup_entry_point(
        mem: &mut MemRegion,
        entry_point: TargetAddr<Self::UsizePtrTy>,
        target: TargetAddr<Self::UsizePtrTy>,
    ) {
        mem.write_vaddr::<u32>(
            entry_point.wrapping_sub(8u32),
            target.wrapping_sub(entry_point).into(),
        )
        .unwrap();
        mem.write_vaddr::<u32>(
            entry_point.wrapping_sub(4u32),
            target.wrapping_sub(entry_point).into(),
        )
        .unwrap();
    }
}

#[cfg(target_arch = "arm")]
impl FlushICache for arch::ArchArm {
    fn flush_icache(segment: Range<usize>) -> Result<(), FlushErr> {
        bionic::cache_flush(segment.start, segment.end, 0).map_err(|_e| FlushErr {})
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use proptest::prelude::*;

    struct ExIdxIter<'a> {
        exidx: &'a ExIdx<'a>,
        idx: usize,
    }
    impl<'a> ExIdx<'a> {
        fn resolve_iter(&self) -> ExIdxIter<'_> {
            ExIdxIter {
                exidx: self,
                idx: 0,
            }
        }
    }

    impl<'a> Iterator for ExIdxIter<'a> {
        type Item = ExIdxEntry;
        fn next(&mut self) -> Option<Self::Item> {
            let entry = self.exidx.entries.get(self.idx)?;

            let ret = Some(entry.absolute(self.exidx.start, self.idx));
            self.idx += 1;
            ret
        }
    }

    fn exidx_entry_strategy() -> impl Strategy<Value = ExIdxEntry> {
        (any::<u16>(), any::<u16>()).prop_map(|(key, val)| ExIdxEntry {
            key: key.into(),
            val: val.into(),
        })
    }
    proptest! {
        #[test]
        fn test_exidx_entry_roundtrip(
            exidx_start in any::<u16>(),
            idx in 0..u16::MAX,
            exidx in exidx_entry_strategy())
        {
            let absolute = exidx.absolute(exidx_start.into(), idx.into());
            let relative = absolute.relative(exidx_start.into(), idx.into());
            assert_eq!(relative, exidx);
        }

        #[test]
        fn test_exidx_no_relocate(
            exidx_start in any::<u16>(),
            mut exidx in prop::collection::vec(exidx_entry_strategy(), 0..20))
        {
            let mut exidx = ExIdx {
                start: exidx_start.into(),
                entries: exidx.as_mut(),
            };
            let count = exidx.entries.len();
            exidx.entries.iter_mut().enumerate()
                .for_each(|(_i, entry)| {
                    if entry.val != EXIDX_CANT_UNWIND {
                        // Make sure that relative addresses do not point into our own table.
                        entry.val += u32::from(ExIdx::entry_addr(0u32.into(), count))
                    }
                });

            let mut resolved_orig: Vec<_> = exidx.resolve_iter().collect();
            resolved_orig.sort_by_key(|e| e.key);
            exidx.relocate(|old| old);

            let mut resolved_new: Vec<_> = exidx.resolve_iter().collect();
            resolved_new.sort_by_key(|e| e.key);
            assert_eq!(resolved_orig, resolved_new);
        }
    }
}
