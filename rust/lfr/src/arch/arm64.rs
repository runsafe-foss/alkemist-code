/* Copyright (c) 2022 RunSafe Security Inc. */

//! Arm64 Specific Relocating
use super::{ArchRuntime, FlushErr, FlushICache};
use crate::log::debugln;
use crate::memregion::MemRegion;
use crate::os::OsModule;
use crate::randolib::{FunctionList, Relocation};
use arch::{ArchArm64, Architecture, RelocationIdx, TargetRead};
use core::arch::asm;
use core::ops::Range;
use goblin::elf::reloc as Rel;
use modinfo::{ModuleInfo, TargetAddr};

impl<'a> ArchRuntime<'a> for ArchArm64 {
    fn preprocess_arch() {}
    fn relocate_arch(
        _mem: &mut MemRegion,
        _module: &OsModule,
        _info: &ModuleInfo,
        _functions: &FunctionList,
    ) {
    }

    fn fixup_export_trampoline(
        mem: &mut MemRegion,
        export_section: Range<TargetAddr<usize>>,
        funcs: &FunctionList,
    ) {
        // RANDO_ASSERT(**export_ptr == 0xE9);
        //  According to the AArch64 encoding document I found,
        //  unconditional branches are encoded as:
        //  000101bb bbbbbbbb bbbbbbbb bbbbbbbb == (5 << 26) | offset
        // RANDO_ASSERT((**export_ptr >> 26) == 0x5);
        // RANDO_ASSERT(**export_ptr == 0xff ||**export_ptr == 0xfe ||**export_ptr == 0x94 ||
        // **export_ptr == 0x97 ||
        //              **export_ptr == 0x14 || **export_ptr == 0x17);
        let range_iter = export_section.step_by(core::mem::size_of::<u32>());
        for ptr in range_iter {
            let mut reloc = Relocation::new(RelocationIdx(Rel::R_AARCH64_JUMP26), ptr, None, None);
            funcs.adjust_relocation(mem, &mut reloc, None, None);
        }
    }
    fn fixup_entry_point(
        mem: &mut MemRegion,
        entry_point: TargetAddr<Self::UsizePtrTy>,
        target: TargetAddr<Self::UsizePtrTy>,
    ) {
        assert_eq!(mem.read_vaddr::<u32>(entry_point), Ok(0x14000001));
        let reloc = Relocation::new(
            RelocationIdx(Rel::R_AARCH64_JUMP26),
            entry_point,
            None,
            None,
        );
        Self::set_target_ptr(&reloc, mem, target)
            .or_else(crate::randolib::ignore_unimplemented_reloc)
            .unwrap();

        // Also patch the branch immediately before our entry point
        let reloc_pre = Relocation::new(
            RelocationIdx(Rel::R_AARCH64_JUMP26),
            entry_point.wrapping_sub(4u32),
            None,
            None,
        );
        Self::set_target_ptr(&reloc_pre, mem, target)
            .or_else(crate::randolib::ignore_unimplemented_reloc)
            .unwrap();

        // Flush the icache line containing this entry point
        // FIXME: this might be slow to do twice (once per entry point),
        // we might want to merge both flushes
        let start: usize = reloc_pre.get_source_ptr().into();
        Self::flush_icache(start..start + 2 * core::mem::size_of::<u32>())
            .map_err(|_| debugln!(1, "Unable to flush instruction cache"))
            .unwrap();
    }
}

impl FlushICache for arch::ArchArm64 {
    fn flush_icache(segment: Range<usize>) -> Result<(), FlushErr> {
        let mut ctr_value: u64;
        unsafe {
            asm!(
                "mrs {ctr_value}, ctr_el0",
                ctr_value = out(reg) ctr_value,
                options(nomem))
        };

        let icache_line_size = 4 << (ctr_value & 0xf);
        let dcache_line_size = 4 << ((ctr_value >> 16) & 0xf);

        //Flush dcache first
        debugln!(3, "Flushing icache range {}-{}", segment.start, segment.end);
        for ptr in segment.clone().step_by(dcache_line_size) {
            unsafe {
                asm! (
                    "dc cvau, {ptr}", ptr = in(reg) ptr
                )
            }
        }

        //Memory barrier
        unsafe {
            asm! {
                "dsb ish"
            }
        }

        // Now flush the icache lines
        for ptr in segment.step_by(icache_line_size) {
            unsafe {
                asm! (
                    "ic ivau, {ptr}", ptr = in(reg) ptr
                )
            }
        }

        //Memory Barrier
        unsafe { asm!("isb") }

        Ok(())
    }
}
