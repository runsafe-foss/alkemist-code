// Copyright (c) 2022 RunSafe Security Inc.

//! Memory that can be selectively hidden or not.
//!
//! This module manages a hunk of memory that is intended to be inaccessible
//! from the target process except when manipulated by `liblfr` and externally
//! from GDB which ignores this access restriction.

use anyhow::{Error, Result};
use bionic::hidden::HiddenMem;
use core::cell::RefCell;
use lfr_meta::symbols::{StoredVector, VecLike, VecLikeErr};
use serde::{de::DeserializeOwned, Serialize};

/// Gives access to the memory via a convenient interface which allows
/// Serializing and Deserializing the memory to ensure that the memory
/// is never directly accessible to the user of [`HideMem`]
pub struct HideMem<T> {
    mmap: HiddenMem<u8>,
    _phantom: core::marker::PhantomData<T>,
}

impl<T> Default for HideMem<T> {
    fn default() -> HideMem<T> {
        HideMem {
            mmap: HiddenMem::default(),
            _phantom: core::marker::PhantomData,
        }
    }
}

impl<T> HideMem<T> {
    /// Allows reading a deserializable object from a hidden memory chunk.
    fn read(&mut self) -> Result<Option<T>>
    where
        T: DeserializeOwned,
    {
        let view = self.mmap.view()?;
        let decoded = if view.is_empty() {
            None
        } else {
            Some(ciborium::from_reader::<_, &[u8]>(view.as_ref()).map_err(Error::msg)?)
        };
        Ok(decoded)
    }
    /// Allows writing a deserializable object from a hidden memory chunk.
    fn write(&mut self, v: T) -> Result<()>
    where
        T: Serialize,
    {
        let mut encoded = alloc::vec![];
        ciborium::into_writer(&v, &mut encoded).map_err(Error::msg)?;
        let mut view = self.mmap.view_mut()?;
        view.clear();
        view.extend_from_slice(&encoded);
        Ok(())
    }

    /// Allows manipulating the memory with a given function.
    pub fn map<F, F2>(&mut self, modify_fn: F, mut update_addr: Option<F2>) -> Result<()>
    where
        T: Serialize + DeserializeOwned,
        F: FnOnce(Result<Option<T>>) -> Result<T>,
        F2: FnMut(*mut u8, usize),
    {
        if let Some(f) = update_addr.as_mut() {
            f(core::ptr::null_mut(), 0)
        };
        let data = self.read();
        let new_data = modify_fn(data)?;
        self.write(new_data)?;
        if let Some(f) = update_addr.as_mut() {
            let (addr, len) = self.mem_ptr();
            f(addr, len);
        }
        Ok(())
    }

    /// Allows end user to know where protected memory chunk is stored.
    fn mem_ptr(&self) -> (*mut u8, usize) {
        self.mmap.mem_ptr()
    }
}

pub struct HiddenVec<T> {
    mem: RefCell<HiddenMem<T>>,
    _phantom: core::marker::PhantomData<T>,
}

impl<T> Default for HiddenVec<T> {
    fn default() -> Self {
        Self {
            mem: RefCell::new(HiddenMem::default()),
            _phantom: core::marker::PhantomData,
        }
    }
}

impl<T> HiddenVec<T>
where
    T: Copy,
{
    pub fn stored_vec(&self) -> StoredVector<T> {
        let mem = self.mem.borrow().mem_ptr();
        StoredVector::new(mem.1, mem.0 as *mut u8)
    }
}

use alloc::borrow::Cow;

impl<T> VecLike<T> for HiddenVec<T>
where
    T: Copy,
{
    fn push(&mut self, val: T) -> core::result::Result<(), VecLikeErr> {
        let mut mem = self.mem.borrow_mut();
        let mut mem = mem.view_mut().map_err(|_| VecLikeErr::NoMem)?;
        mem.push(val);
        Ok(())
    }
    fn extend<E>(&mut self, val: E) -> Result<(), VecLikeErr>
    where
        E: Iterator<Item = T>,
    {
        let mut mem = self.mem.borrow_mut();
        let mut mem = mem.view_mut().map_err(|_| VecLikeErr::NoMem)?;
        mem.extend(val);
        Ok(())
    }
    fn get(&self, range: core::ops::Range<usize>) -> Option<Cow<[T]>> {
        let mut mem = self.mem.borrow_mut();
        let mem = mem.view().ok()?;
        Some(Cow::Owned(mem.get(range)?.to_vec()))
    }
    fn len(&self) -> usize {
        let mut mem = self.mem.borrow_mut();
        mem.view().map(|m| m.len()).unwrap_or(0)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use alloc::vec;

    #[test]
    fn hide_memory() {
        let mut hide = HideMem::default();
        {
            assert!(matches!(hide.read(), Ok(None)));
            let tmp = vec![0, 1, 2, 3, 4, 5, 6];
            hide.write(tmp.clone()).expect("Writable");
            assert!(matches!(hide.read(), Ok(Some(_))));
            let new_tmp = hide.read().expect("Readable").expect("value");
            assert_eq!(tmp, new_tmp);
        }
        {
            let tmp = vec![0, 1, 2];
            hide.write(tmp.clone()).expect("Writable");
            assert!(matches!(hide.read(), Ok(Some(_))));
            let new_tmp = hide.read().expect("Readable").expect("value");
            assert_eq!(tmp, new_tmp);
        }
        {
            let mut addr = std::ptr::null_mut();
            let mut len = 0;
            hide.write(vec![7]).unwrap();
            hide.map(
                |mem| {
                    let mut vec = mem.unwrap().unwrap();
                    vec.push(10);
                    Ok(vec)
                },
                Some(|t_addr, t_len| {
                    addr = t_addr;
                    len = t_len;
                }),
            )
            .unwrap();

            assert!(len != 0);
            assert!(!addr.is_null());
            let new = hide.read().expect("readable");
            assert_eq!(new, Some(vec![7, 10]));
        }
    }
}
