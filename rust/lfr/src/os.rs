/* Copyright (c) 2022 RunSafe Security Inc. */
//! Posix system specific routines.
use crate::arch::{ArchRuntime, FlushICache};
use crate::log::debugln;
use crate::memregion::MemRegion;
use crate::randolib::{FunctionList, Relocation};
use crate::selinux::SeLinuxContext;
use alloc::borrow::ToOwned;
use alloc::collections::BTreeSet;
use alloc::ffi::CString;
use alloc::vec::Vec;
use arch::{ArchUtil, TargetRead};
use bionic::mem::{MemMut, MemPerm, MemPermErr, MemPerms, Memory};
use bionic::phdr::{AuxvEntry, SegmentType};
use bionic::posix::Posix;
use bytemuck::{Pod, Zeroable};
use core::ffi::CStr;
use core::marker::PhantomData;
use core::ops::Range;
use core::ptr::NonNull;
use modinfo::{ModuleInfo, TargetAddr};
use scroll::Pread;

#[cfg(target_pointer_width = "32")]
use goblin::elf::reloc::reloc32::{Rel, Rela};
#[cfg(target_pointer_width = "64")]
use goblin::elf::reloc::reloc64::{Rel, Rela};

pub mod cache;

use cache::Cache;

/// Posix specific data needed by LFR.
#[derive(Debug)]
pub struct OsModule {
    /// PHdrs
    phdr_info: bionic::phdr::PhdrInfo<'static>,
    /// Eh Frame Header Location
    eh_frame_hdr: Option<TargetAddr<usize>>,
    /// Reloc ELF Sections
    relocs: RelocSections,
    /// The name of the module
    module_name: CString,
    /// Whether this module is statically compiled.
    statik: bool,
}

/// OS Specific Module Extensions
pub trait OsModExt {
    type WriteContext;
    /// Compute the Virtual Address of `addr`.
    fn rva2address_raw(&self, addr: usize) -> usize;

    /// Compute the Virtual Address of `addr`.
    fn rva2address(&self, addr: usize) -> TargetAddr<usize> {
        self.rva2address_raw(addr).into()
    }
    /// Provide the virtual address for the image base.
    fn image_base(&self) -> TargetAddr<usize>;
    /// Provide the name of the given module.
    fn module_name(&self) -> Option<&CStr>;
    /// Provide the address of the `.eh_frmae_hdr`.
    fn eh_frame_hdr(&self) -> Option<TargetAddr<usize>> {
        None
    }
    /// Convert executable memory into a droppable [`MemGuard`].
    unsafe fn guard_exec_mem<'a, M>(
        &self,
        mem: M,
        context: &'a Self::WriteContext,
    ) -> MemGuard<'a, M>
    where
        M: Memory;

    /// Provide the Phdrs.
    fn header_info(&self) -> &bionic::phdr::PhdrInfo<'static>;
}

impl OsModExt for OsModule {
    type WriteContext = SeLinuxContext;
    fn header_info(&self) -> &bionic::phdr::PhdrInfo<'static> {
        &self.phdr_info
    }
    fn image_base(&self) -> TargetAddr<usize> {
        self.phdr_info.base.into()
    }

    fn rva2address_raw(&self, addr: usize) -> usize {
        self.phdr_info.base + addr
    }
    fn module_name(&self) -> Option<&CStr> {
        if !self.module_name.as_bytes().is_empty() {
            Some(&self.module_name)
        } else {
            None
        }
    }
    fn eh_frame_hdr(&self) -> Option<TargetAddr<usize>> {
        self.eh_frame_hdr
    }

    /// # Safety
    /// It is unsafe to arbitrarily give memory executable permissions. The memory referenced here
    /// must be intended to be executable before applying the MemGuard.
    unsafe fn guard_exec_mem<'a, M>(
        &self,
        mem: M,
        context: &'a Self::WriteContext,
    ) -> MemGuard<'a, M>
    where
        M: Memory,
    {
        MemGuard {
            _context: context,
            seg_type: SegmentType::Load,
            old_perms: MemPerm::R | MemPerm::X,
            file_addr: None,
            mem,
        }
    }
}

/// Reloc Section methods
trait RelSection {
    /// Whether this is Rela or not.
    const RELA: bool;
    /// The number of entries of the section
    fn count(&self) -> usize {
        self.len_bytes() / self.entry_size()
    }
    /// Length of Section in bytes
    fn len_bytes(&self) -> usize;
    /// The address the rel section starts at.
    fn start(&self) -> TargetAddr<usize>;

    /// Length of individual entry
    fn entry_size(&self) -> usize {
        goblin::elf::reloc::Reloc::size(Self::RELA, goblin::container::Ctx::default())
    }
    /// Provide a RelSection from the target memory space.
    fn with_mem<'a>(
        &self,
        mem: &'a MemRegion<'a>,
    ) -> Result<goblin::elf::reloc::RelocSection<'a>, ()> {
        let bytes = mem
            .slice(self.start().absolute_addr(), self.len_bytes())
            .ok_or(())?;
        goblin::elf::reloc::RelocSection::parse(
            bytes,
            0,
            self.len_bytes(),
            Self::RELA,
            goblin::container::Ctx::default(),
        )
        .map_err(|_| ())
    }
}

#[derive(Default)]
struct RelocSectionBuilder<T> {
    addr: Option<TargetAddr<usize>>,
    entries: Option<usize>,
    size: Option<usize>,
    phantom: PhantomData<T>,
}

impl<T> RelocSectionBuilder<T>
where
    RelocSection<T>: RelSection,
{
    fn with_addr(&mut self, addr: TargetAddr<usize>) -> &mut Self {
        self.addr = Some(addr);
        self
    }
    fn with_size(&mut self, size: usize) -> &mut Self {
        self.size = Some(size);
        self
    }
    fn with_entries(&mut self, entries: usize) -> &mut Self {
        self.entries = Some(entries);
        self
    }
    fn build(self) -> Option<RelocSection<T>> {
        let addr = self.addr?;
        let size = self.size?;
        let entries = self.entries?;
        Some(RelocSection::new(addr, entries, size))
    }
}

/// A Reloc ELF Section
#[derive(Debug)]
struct RelocSection<T> {
    /// Starting address
    addr: TargetAddr<usize>,
    /// Number of Bytes
    bytes: usize,
    phantom: PhantomData<T>,
}

impl<T> RelocSection<T>
where
    Self: RelSection,
{
    /// Create new Reloc Section
    fn new(addr: TargetAddr<usize>, entry_size: usize, bytes: usize) -> Self {
        let relocs = Self {
            addr,
            bytes,
            phantom: PhantomData,
        };
        assert_eq!(entry_size, relocs.entry_size());
        relocs
    }
}

impl RelSection for RelocSection<Rela> {
    const RELA: bool = true;
    fn len_bytes(&self) -> usize {
        self.bytes
    }
    fn start(&self) -> TargetAddr<usize> {
        self.addr
    }
}

impl RelSection for RelocSection<Rel> {
    const RELA: bool = false;

    fn len_bytes(&self) -> usize {
        self.bytes
    }
    fn start(&self) -> TargetAddr<usize> {
        self.addr
    }
}

/// Types of ELF hash sections
#[derive(Debug)]
enum HashSection {
    /// Old Hash Style
    Hash(TargetAddr<usize>),
    /// New GNU Hash
    GnuHash(TargetAddr<usize>),
}

struct GnuHash<'a> {
    bytes: &'a [u8],
}

impl GnuHash<'_> {
    fn count(&self) -> Option<usize> {
        #[repr(C)]
        #[derive(Pread, Copy, Clone, Debug)]
        struct ElfHashTable {
            nbucket: u32,
            symbol_base: u32,
            bitmask_nwords: u32,
            gnu_shift: u32,
        }

        fn split_slice(slice: &[u8], at: usize) -> Option<(&[u8], &[u8])> {
            match (slice.get(..at), slice.get(at..)) {
                (Some(s1), Some(s2)) => Some((s1, s2)),
                _ => None,
            }
        }

        let (table, slice) = split_slice(self.bytes, core::mem::size_of::<ElfHashTable>())?;

        let table: ElfHashTable = table.pread(0).ok()?;

        let (_bitmasks, slice) = split_slice(
            slice,
            table.bitmask_nwords as usize * core::mem::size_of::<usize>(),
        )?;
        let (buckets, slice) =
            split_slice(slice, table.nbucket as usize * core::mem::size_of::<u32>())?;

        let buckets = bytemuck::cast_slice::<_, u32>(buckets);
        let max_idx = buckets.iter().copied().max().unwrap_or(table.symbol_base) as usize;

        // We may not know the entire length of the chain, since an adjacent section may follow it.
        // We truncate the remainder of the section on a u32 boundary.
        let chain_len = slice.len() & !((1 << core::mem::size_of::<u32>().ilog2()) - 1);
        let chain = bytemuck::cast_slice::<_, u32>(&slice[..chain_len]);

        chain
            .iter()
            .skip(max_idx - table.symbol_base as usize)
            .position(|&c| (c & 1) == 1)
            .map(|offset_idx| max_idx + offset_idx + 1)
    }
}

impl HashSection {
    /// Find the number of entries in the Symbol Table from the Hash
    ///
    /// # Reference
    /// * <http://deroko.phearless.org/dt_gnu_hash.txt>
    /// * <https://flapenguin.me/elf-dt-gnu-hash>
    /// * <https://flapenguin.me/elf-dt-hash>
    fn find_count(&self, mem: &MemRegion<'_>) -> Option<usize> {
        match self {
            HashSection::Hash(hash) => {
                #[repr(C)]
                #[derive(Pread, Copy, Clone)]
                struct ElfHashTable {
                    nbucket: u32,
                    nchain: u32,
                }
                let table: ElfHashTable = mem.read_vaddr(*hash).ok()?;
                Some(usize::try_from(table.nchain).ok()?)
            }
            HashSection::GnuHash(hash) => {
                let slice = mem.read_vaddr_max_slice(*hash).ok()?;
                GnuHash {
                    bytes: slice.as_slice(),
                }
                .count()
            }
        }
    }
}

/// Determination of how many entries are in the SymTab.
#[derive(Debug, Clone)]
enum SymTabCount {
    /// We havne't yet checked.
    Unknown,
    /// We checked and optionally found a size.
    ///
    /// If the size is Some, we will enforce that as a maximum upper bound. If not, we assume that all accesses are valid.
    Known(Option<usize>),
}

#[derive(Default)]
struct SymTabBuilder {
    addr: Option<TargetAddr<usize>>,
    entries: Option<usize>,
    hash: Option<HashSection>,
}

impl SymTabBuilder {
    fn with_addr(&mut self, addr: TargetAddr<usize>) -> &mut Self {
        self.addr = addr.into();
        self
    }
    fn with_entries(&mut self, entries: usize) -> &mut Self {
        self.entries = entries.into();
        self
    }
    fn with_hash(&mut self, hash: HashSection) -> &mut Self {
        match self.hash {
            Some(HashSection::Hash(_)) => { /* we prefer to keep the simpler hash*/ }
            _ => self.hash = hash.into(),
        }
        self
    }
    fn build(self) -> Option<SymTab> {
        let addr = self.addr?;
        let entries = self.entries?;
        Some(SymTab::new(addr, entries, self.hash))
    }
}

/// Symtabe Information
#[derive(Debug)]
struct SymTab {
    /// Starting address of SymTab
    addr: TargetAddr<usize>,
    /// The length in bytes
    size: usize,
    /// The hash section (if known)
    hash: Option<HashSection>,
    /// The determine amount of entries from the SymTab.
    count: core::cell::RefCell<SymTabCount>,
}

impl SymTab {
    /// Get the specified entry from the Symbol Table.
    fn get(&self, mem: &mut MemRegion<'_>, idx: usize) -> Option<goblin::elf::sym::Sym> {
        let count = match *self.count.borrow() {
            SymTabCount::Known(count) => count,
            SymTabCount::Unknown => {
                let count = self.hash.as_ref().and_then(|h| h.find_count(mem));

                if let Ok(mut cache) = self.count.try_borrow_mut() {
                    if count.is_none() {
                        debugln!(1, "WARNING: Could not determine symbol table length. All accesses will be unchecked.");
                    }
                    *cache = SymTabCount::Known(count);
                }
                count
            }
        };
        debugln!(10, "Symtab has {:?} entries", count);
        if count.is_none() {
            debugln!(
                1,
                "WARNING: Could not determine symbol table length. Unchecked access {}.",
                idx
            );
        }
        let count = count.unwrap_or(idx + 1);
        let bytes = mem.slice(self.addr.absolute_addr(), self.size * count)?;
        let symtab =
            goblin::elf::sym::Symtab::parse(bytes, 0, count, goblin::container::Ctx::default())
                .ok()?;
        symtab.get(idx)
    }
    /// Create a new symbol table.
    fn new(addr: TargetAddr<usize>, size: usize, hash: Option<HashSection>) -> Self {
        Self {
            addr,
            size,
            hash,
            count: core::cell::RefCell::new(SymTabCount::Unknown),
        }
    }
}

/// All the known Reloc sections and the symbol table.
#[derive(Debug)]
pub struct RelocSections {
    /// Rela Section
    rela: Option<RelocSection<Rela>>,
    /// Rel Section
    rel: Option<RelocSection<Rel>>,
    /// Symbol Table.
    symtab: Option<SymTab>,
}

/// Fixup all copy relocs found in Rel/Rela sections.
fn fixup_copy_reloc_list<R>(
    mem: &mut MemRegion,
    os_mod: &OsModule,
    functions: &FunctionList,
    rel_table: &R,
    symtab: &SymTab,
) where
    R: RelSection + core::fmt::Debug,
{
    // TODO: We should verify the copied symbol against the randomized module's
    // symbol table and just apply relocations to offsets where we have marked
    // relocs.
    debugln!(1, "{:x?}", mem);
    debugln!(1, "{:x?}", rel_table);

    let relocs = rel_table.count();
    debugln!(1, "Number of entries {}", relocs);
    (0..relocs).for_each(|i| {
        let rel_section = rel_table.with_mem(mem).unwrap();
        let reloc = rel_section.get(i).unwrap();
        debugln!(5, "{}: {:x?}", i, reloc);
        if reloc.r_type != arch::TargetArch::COPY_RELOC {
            return;
        }
        let sym = symtab.get(mem, reloc.r_sym).unwrap();
        let size = sym.st_size as usize;

        debugln!(5, "Sym {:x?}", sym);
        (0..size)
            .step_by(core::mem::size_of::<*const libc::c_void>())
            .for_each(|offset| {
                let addr = os_mod
                    .image_base()
                    .wrapping_add(reloc.r_offset as usize)
                    .wrapping_add(offset);

                debugln!(5, "Relocate addr: {:x?}", addr);
                let mut reloc = Relocation::new_pointer_reloc(addr);
                functions.adjust_relocation(mem, &mut reloc, None, None);
            });
    })
}

/// Conver the Module Specific Phdr Information into a useful
/// iterator.
fn convert_phdr_info(
    args: Option<NonNull<libc::uintptr_t>>,
    dynamic: Option<NonNull<libc::uintptr_t>>,
    text_start: NonNull<libc::uintptr_t>,
) -> bionic::phdr::PhdrInfo<'static> {
    fn find_phdr_from_auxv(args: NonNull<libc::uintptr_t>) -> Option<bionic::phdr::Phdrs<'static>> {
        let mut phdr = None;
        let mut phnum = None;
        bionic::phdr::Auxv::from_args(args)?.for_each(|e| {
            debugln!(10, "AUXV = {:?}", e);
            match e {
                AuxvEntry::Phdr(phdr_in) => phdr = Some(phdr_in),
                AuxvEntry::PhNum(phnum_in) => phnum = Some(phnum_in),
                _ => {}
            }
        });

        let phdr_start = phdr?;
        let phnum = phnum?;

        let phdrs = unsafe { core::slice::from_raw_parts(phdr_start as *const _, phnum) };
        Some(bionic::phdr::Phdrs::new(phdrs))
    }
    // We need to convert the ELF PHdr module information into our own format
    // We have 3 possible sources:
    // 1) the `phdr_info` argument passed to the constructor
    // 2) the auxiliary vector from the kernel, if we have it
    // 3) `dl_iterate_phdr`, as a last resort

    // Option 1: try `phdr_info`
    // This function is no longer called if phdr_info is provided.

    // Option 2: scan the auxiliary vector
    if let Some(hdrs) = args.and_then(find_phdr_from_auxv) {
        let mut name = None;
        if let Some(args) = args {
            let ptr = args.as_ptr() as *const usize;
            let argc = unsafe { *ptr };
            if argc > 0 {
                let name_ptr = unsafe { *ptr.add(1) as *const u8 };
                name = Some(unsafe { CStr::from_ptr(name_ptr.cast()) });
            }
        }
        let name = name.unwrap_or_else(|| CStr::from_bytes_with_nul(b"\0").unwrap());

        // Try to extract the image base for the PT_PHDR header;
        // if we can't find it, then it's very likely that the binary
        // hasn't been moved, and the image base is 0
        // https://android-review.googlesource.com/c/toolchain/gcc/+/711193

        let image_base = hdrs
            .iter()
            .inspect(|phdr| {
                debugln!(10, "{:?}", phdr);
            })
            .find_map(|phdr| match phdr.segment_type {
                SegmentType::Phdr => Some(hdrs.start() - phdr.vaddr),
                SegmentType::Dynamic => dynamic.map(|p| p.as_ptr() as usize - phdr.vaddr),
                _ => None,
            })
            .unwrap_or(0);
        return hdrs.to_info(name, image_base);
    }

    let mut phdr_info = None;
    bionic::phdr::dl_iterate_phdr(|info| {
        let found = info
            .iter()
            .filter(|phdr| phdr.segment_type == SegmentType::Load)
            .map(|phdr| {
                let base = info.base + phdr.vaddr;
                (phdr, base..(base + phdr.memsz))
            })
            .inspect(|(phdr, range)| {
                debugln!(8, "dl_iterate_phdr {:x?}", phdr);
                debugln!(8, "Target Range {:x?} Search: {:x?}", range, text_start);
            })
            .find(|(_phdr, range)| range.contains(&(text_start.as_ptr() as usize)))
            .map(|(phdr, _range)| phdr);
        if found.is_some() {
            phdr_info = Some(info.clone());
            true
        } else {
            false
        }
    });

    phdr_info.expect("Expected to find phdr_info")
}

/// Find all the Relocation Sections.
fn find_reloc_sections(phdr_info: &bionic::phdr::PhdrInfo<'static>) -> RelocSections {
    let mut rela = RelocSectionBuilder::<Rela>::default();
    let mut rel = RelocSectionBuilder::<Rel>::default();
    let mut symtab = SymTabBuilder::default();

    let offset = |addr: usize| {
        if addr < phdr_info.base {
            phdr_info.base + addr
        } else {
            addr
        }
    };

    phdr_info
        .iter()
        .filter(|phdr| matches!(phdr.segment_type, SegmentType::Dynamic))
        .for_each(|phdr| {
            let dyn_section = {
                cfg_if::cfg_if! {
                    if #[cfg(target_pointer_width = "32")] {
                        unsafe {
                            goblin::elf::dynamic::dyn32::from_raw(phdr_info.base, phdr.vaddr)
                        }
                    } else if #[cfg(target_pointer_width = "64")] {
                        unsafe {
                            goblin::elf::dynamic::dyn64::from_raw(
                                phdr_info.base,
                                phdr.vaddr,
                            )
                        }
                    } else {
                        compile_error!("Unsupport target_pointer_width")
                    }
                }
            };
            for d in dyn_section {
                use goblin::elf::dynamic as D;
                #[cfg(target_pointer_width = "64")]
                let tag = d.d_tag;
                #[cfg(target_pointer_width = "32")]
                let tag = u64::from(d.d_tag);
                match tag {
                    D::DT_RELASZ => {
                        rela.with_size(d.d_val.try_into().unwrap());
                    }
                    D::DT_RELA => {
                        rela.with_addr(offset(d.d_val.try_into().unwrap()).into());
                    }
                    D::DT_RELAENT => {
                        rela.with_entries(d.d_val.try_into().unwrap());
                    }
                    D::DT_RELSZ => {
                        rel.with_size(d.d_val.try_into().unwrap());
                    }
                    D::DT_REL => {
                        rel.with_addr(offset(d.d_val.try_into().unwrap()).into());
                    }
                    D::DT_RELENT => {
                        rel.with_entries(d.d_val.try_into().unwrap());
                    }
                    D::DT_SYMTAB => {
                        symtab.with_addr(offset(d.d_val.try_into().unwrap()).into());
                    }
                    D::DT_SYMENT => {
                        symtab.with_entries(d.d_val.try_into().unwrap());
                    }
                    D::DT_HASH => {
                        symtab.with_hash(HashSection::Hash(
                            offset(d.d_val.try_into().unwrap()).into(),
                        ));
                    }
                    D::DT_GNU_HASH => {
                        symtab.with_hash(HashSection::GnuHash(
                            offset(d.d_val.try_into().unwrap()).into(),
                        ));
                    }
                    _ => {}
                }
            }
        });

    let reloc_sections = RelocSections {
        rela: rela.build(),
        rel: rel.build(),
        symtab: symtab.build(),
    };
    debugln!(10, "{:?}", reloc_sections);
    reloc_sections
}

/// OS Specific Methods
pub trait OsExt {
    type WriteContext;

    /// Fixup relocations
    fn fixup_relocations(
        mem: &mut MemRegion,
        info: &ModuleInfo,
        os_module: &OsModule,
        functions: &FunctionList,
        got_entries: &BTreeSet<TargetAddr<usize>>,
        cache: Option<&mut Cache>,
    );
    /// Collect all OS Specific information that can't reside in the Module.
    fn collect_os_info(
        info: &ModuleInfo,
        phdr_info: Option<bionic::phdr::PhdrInfo<'static>>,
    ) -> OsModule;

    unsafe fn establish_write_context() -> Result<SeLinuxContext, ()>;

    /// Collect all segments found by traversing Phdrs into guarded memory segments.
    ///
    /// # Safety
    /// This method can realistically only be called once for a given module or will
    /// provide the same aliased memory regions.
    unsafe fn collect_segments<'a>(
        os_mod: &'a OsModule,
        context: &'a Self::WriteContext,
    ) -> Result<Vec<MemGuard<'a, MemMut>>, MemPermErr>;

    /// Update the EH Frame
    fn update_eh_frame<F>(
        mem: &mut crate::memregion::MemRegion,
        os_mod: &OsModule,
        relocate: F,
    ) -> Result<(), anyhow::Error>
    where
        F: FnMut(usize) -> usize;
}

impl OsExt for Posix {
    type WriteContext = SeLinuxContext;

    fn update_eh_frame<F>(
        mem: &mut crate::memregion::MemRegion,
        os_mod: &OsModule,
        relocate: F,
    ) -> Result<(), anyhow::Error>
    where
        F: FnMut(usize) -> usize,
    {
        crate::eh_frame::update_eh_frame_new(
            mem,
            os_mod
                .eh_frame_hdr
                .unwrap_or_else(|| TargetAddr::from_raw(0usize))
                .absolute_addr(),
            relocate,
        )
    }

    fn fixup_relocations(
        mem: &mut MemRegion,
        info: &ModuleInfo,
        os_mod: &OsModule,
        functions: &FunctionList,
        got_entries: &BTreeSet<TargetAddr<usize>>,
        mut cache: Option<&mut Cache>,
    ) {
        let entries = [
            ("lfr_init", info.lfr_init, info.orig_dt_init),
            ("lfr_entry", info.lfr_entry, info.orig_entry),
        ];

        for (name, lfr, orig) in entries {
            // Fix up the original entry point and init addresses
            let new_entry = if !orig.is_null() {
                functions.adjust_target(orig, None).1
            } else {
                // Point the branch to the return instruction
                info.lfr_return
            };
            //Patch the initial branch to point directly to the relocated function
            <arch::TargetArch as ArchRuntime>::fixup_entry_point(mem, lfr, new_entry);
            debugln!(1, "New {} {:x} -> {:x}", name, orig, new_entry);
        }

        //Fixup our preinit function
        <arch::TargetArch as ArchRuntime>::fixup_entry_point(
            mem,
            info.lfr_preinit,
            info.lfr_return,
        );

        <arch::TargetArch as ArchRuntime>::relocate_arch(mem, os_mod, info, functions);

        // Apply relocations to known GOT entries
        for ge in got_entries {
            debugln!(5, "GOT Entry {:?}", ge);
            let mut reloc = Relocation::new_pointer_reloc(*ge);
            let cacheable = functions.adjust_relocation(mem, &mut reloc, None, None);

            if let Some(cache) = &mut cache {
                cache.add_reloc(cacheable);
            }
        }

        if let (Some(rel_table), Some(symtab)) = (&os_mod.relocs.rel, &os_mod.relocs.symtab) {
            debugln!(3, "Fixing up REL");
            fixup_copy_reloc_list(mem, os_mod, functions, rel_table, symtab);
        }

        if let (Some(rela), Some(symtab)) = (&os_mod.relocs.rela, &os_mod.relocs.symtab) {
            debugln!(3, "Fixing up RELA");
            fixup_copy_reloc_list(mem, os_mod, functions, rela, symtab);
        }

        if let Some(eh_frame_hdr) = os_mod.eh_frame_hdr {
            debugln!(3, "Fixing up EhFrameHdr {:x?}", eh_frame_hdr);
            #[derive(Pread)]
            struct EhFrameHeader {
                encoding: u32,
                _eh_frame_ptr: u32,
                num_entries: u32,
            }
            #[repr(C)]
            #[derive(Debug, Pod, Zeroable, Copy, Clone)]
            struct EhFrameHdrEntry {
                initial_location: i32,
                address: u32,
            }
            let offset = &mut 0;
            let header_mem = mem.longest_slice_mut(eh_frame_hdr.absolute_addr()).unwrap();
            let hdr: EhFrameHeader = header_mem.gread(offset).unwrap();
            if hdr.encoding != 0x3b031b01 {
                debugln!(1, "Unknown .eh_frame_hdr encoding {:x}", hdr.encoding);
            } else {
                debugln!(8, ".eh_frame_hdr found {} entries", hdr.num_entries);
                let search_table = &mut header_mem[*offset..];
                let search_table = bytemuck::cast_slice_mut::<_, EhFrameHdrEntry>(
                    &mut search_table[..usize::try_from(hdr.num_entries).unwrap()
                        * core::mem::size_of::<EhFrameHdrEntry>()],
                );

                let mut hint = None;
                search_table.iter_mut().for_each(|entry| {
                    let epc = eh_frame_hdr.wrapping_add(entry.initial_location as isize as usize);

                    // We know that EH Frames are in order, so our best chance of finding the next
                    // function is to just search a little forward from the last one.
                    // We fall back to unhinted if this fails.
                    let (new_hint, new_addr) = functions.adjust_target_asc(epc, hint);
                    hint = new_hint;

                    debugln!(8, "Eh Frame Entry: {:x?} -> {:x?}", epc, new_addr);
                    // The original version had what appears to be a bug here where it was
                    // attempting to add a stack location to the cacheable relocs.
                    // I suspect it was filtered out by checking the writable segments.
                    entry.initial_location =
                        new_addr.wrapping_sub(eh_frame_hdr).absolute_addr() as i32;
                });

                search_table.sort_by_key(|v| v.initial_location);

                for (i, e) in search_table.iter().enumerate() {
                    debugln!(10, "entry_pc[{}]={:x?}", i, e);
                    if i > 0 {
                        assert!(
                            search_table[i].initial_location
                                >= search_table[i - 1].initial_location
                        );
                    }
                }
            }
        } else {
            debugln!(1, "No .ehframehdr section found");
        }
    }

    fn collect_os_info(
        info: &ModuleInfo,
        phdr_info: Option<bionic::phdr::PhdrInfo<'static>>,
    ) -> OsModule {
        let sections: &[_] = info.sections.into();
        let start = Range::from(sections.iter().next().unwrap().exec).start;

        let phdr_info = phdr_info.unwrap_or_else(|| {
            convert_phdr_info(
                NonNull::new(info.args as *mut _),
                info.dynamic.into(),
                Option::<_>::from(start).unwrap(),
            )
        });

        let module_name = phdr_info.name.to_owned();

        let eh_frame_hdr = phdr_info
            .iter()
            .find(|phdr| matches!(phdr.segment_type, SegmentType::EhFrame))
            .map(|phdr| TargetAddr::from_raw(phdr.vaddr + phdr_info.base));

        // We found the dynamic tags, so dynamically linked
        let statik = !phdr_info
            .iter()
            .any(|phdr| matches!(phdr.segment_type, SegmentType::Dynamic));

        // We need to call `dl_iterate_phdr` very early before we
        // randomize anything, otherwise it will break during glibc randomization
        let relocs = find_reloc_sections(&phdr_info);

        OsModule {
            phdr_info,
            module_name,
            eh_frame_hdr,
            statik,
            relocs,
        }
    }

    unsafe fn establish_write_context() -> Result<SeLinuxContext, ()> {
        let selinux_context = SeLinuxContext::new().map_err(|e| {
            debugln!(1, "Couldn't establish SeLinuxContext {:?}", e);
        })?;
        Ok(selinux_context)
    }

    unsafe fn collect_segments<'a>(
        os_mod: &'a OsModule,
        context: &'a Self::WriteContext,
    ) -> Result<Vec<MemGuard<'a, MemMut>>, MemPermErr> {
        os_mod
            .phdr_info
            .iter()
            .filter(|phdr| match phdr.segment_type {
                SegmentType::GnuRelRo if !os_mod.statik => true,
                SegmentType::Load => true,
                _ => false,
            })
            .map(|phdr| {
                let perms = MemPerms::from_phdr_flags(phdr.flags);
                let start = os_mod.rva2address(phdr.vaddr).absolute_addr();
                debugln!(4, "Mapping segment {:x?}-{:x?}", start, start + phdr.memsz);

                let mut mem = unsafe {
                    MemMut::from_raw(
                        NonNull::new(start as *mut _).unwrap(),
                        phdr.memsz,
                        MemPerms::from_phdr_flags(phdr.flags),
                    )
                };

                debugln!(
                    3,
                    "Checking permissions for Segment::{:?} {:?}",
                    phdr.segment_type,
                    mem.addr()
                );
                let old_perms = perms;
                let new_perms = perms | MemPerm::W;
                let perm_change = unsafe { mem.force_change_perms(new_perms) };
                match perm_change {
                    Ok(_) => {
                        debugln!(
                            3,
                            "Successfully changed permissions {} -> {} for {:?}",
                            old_perms,
                            new_perms,
                            mem.addr()
                        );
                    }
                    Err(e) => {
                        debugln!(
                            3,
                            "Failed to change permissions {} -> {} for {:?}",
                            old_perms,
                            new_perms,
                            mem.addr()
                        );
                        debugln!(1, "WARNING: NOT randomizing due to segment mapping issue");
                        return Err(e);
                    }
                }

                Ok(MemGuard {
                    _context: context,
                    seg_type: phdr.segment_type,
                    file_addr: Some(phdr.offset),
                    old_perms: perms,
                    mem,
                })
            })
            .collect()
    }
}

/// Memory Guard provides the ability to drop a segmeent and remap it to its original permissions.
#[derive(Debug)]
pub struct MemGuard<'a, T>
where
    T: Memory,
{
    /// The SELinuxContext token to prevent you from getting memory segments without a valid context.
    _context: &'a SeLinuxContext,
    /// The type of the Segment.
    seg_type: SegmentType,
    /// The original permissions of the segments. All segments are assumed to be writable.
    old_perms: MemPerms,
    /// The physical starting address.
    file_addr: Option<usize>,
    /// The memory slice backing which is guarded.
    mem: T,
}

impl<'a, T> MemGuard<'a, T>
where
    T: Memory,
{
    /// The physical file address of the memory.
    pub fn file_addr(&self) -> Option<usize> {
        self.file_addr
    }
    // The Segment Type.
    pub fn seg_type(&self) -> SegmentType {
        self.seg_type
    }
}

impl<T> core::ops::Deref for MemGuard<'_, T>
where
    T: Memory,
{
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.mem
    }
}
impl<T> core::ops::DerefMut for MemGuard<'_, T>
where
    T: Memory,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.mem
    }
}

impl<T> Drop for MemGuard<'_, T>
where
    T: Memory,
{
    fn drop(&mut self) {
        //TODO: Hack in some support for RELRO
        let res = unsafe { self.mem.force_change_perms(self.old_perms) };

        debugln!(
            3,
            "Restoring permissions for {:?} -> {}, {}",
            self.mem.addr(),
            self.old_perms,
            if res.is_ok() { "PASS" } else { "FAIL" }
        );
        if self.old_perms.execute() {
            let _ = arch::TargetArch::flush_icache(self.mem.range())
                .map_err(|_| debugln!(1, "Failed to flush instruction cache"));
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_gnuhash() {
        use aligned_array::{Aligned, A16};
        let gnuhash_section: Aligned<A16, [u8; 60]> = Aligned([
            0x3, 0x0, 0x0, 0x0, 0x27, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x6, 0x0, 0x0, 0x0, 0x88,
            0x40, 0x30, 0x1, 0x1, 0x1, 0x30, 0x8, 0x27, 0x0, 0x0, 0x0, 0x2b, 0x0, 0x0, 0x0, 0x2c,
            0x0, 0x0, 0x0, 0x28, 0x1d, 0x8c, 0x1c, 0x42, 0x45, 0xd5, 0xec, 0x60, 0xad, 0x1e, 0x9d,
            0xbb, 0xe3, 0x92, 0x7c, 0xd9, 0x71, 0x58, 0x1c, 0x15, 0x76, 0x33, 0xce,
        ]);
        assert_eq!(
            GnuHash {
                bytes: gnuhash_section.as_ref()
            }
            .count(),
            Some(45)
        );
        let gnuhash_section: Aligned<A16, [u8; 48]> = Aligned([
            0x3, 0x0, 0x0, 0x0, 0x1f, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x6, 0x0, 0x0, 0x0, 0x0,
            0x1, 0x20, 0x0, 0x80, 0x1, 0x10, 0x2, 0x1f, 0x0, 0x0, 0x0, 0x20, 0x0, 0x0, 0x0, 0x0,
            0x0, 0x0, 0x0, 0x29, 0x1d, 0x8c, 0x1c, 0x66, 0x55, 0x61, 0x10, 0x39, 0xf2, 0x8b, 0x1c,
        ]);
        assert_eq!(
            GnuHash {
                bytes: gnuhash_section.as_ref()
            }
            .count(),
            Some(34)
        );
    }
    #[test]
    fn test_gnuhash_extra() {
        use aligned_array::{Aligned, A16};
        let gnuhash_section: Aligned<A16, [u8; 62]> = Aligned([
            0x3, 0x0, 0x0, 0x0, 0x27, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x6, 0x0, 0x0, 0x0, 0x88,
            0x40, 0x30, 0x1, 0x1, 0x1, 0x30, 0x8, 0x27, 0x0, 0x0, 0x0, 0x2b, 0x0, 0x0, 0x0, 0x2c,
            0x0, 0x0, 0x0, 0x28, 0x1d, 0x8c, 0x1c, 0x42, 0x45, 0xd5, 0xec, 0x60, 0xad, 0x1e, 0x9d,
            0xbb, 0xe3, 0x92, 0x7c, 0xd9, 0x71, 0x58, 0x1c, 0x15, 0x76, 0x33, 0xce,
            // The following bytes are just to make a non-u32 based length alignment.
            0xff, 0xff,
        ]);
        assert_eq!(
            GnuHash {
                bytes: gnuhash_section.as_ref()
            }
            .count(),
            Some(45)
        );
    }
}
