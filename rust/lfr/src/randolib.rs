/* Copyright (c) 2022 RunSafe Security Inc. */
//! Rust Implementation of LFR

use self::memstate::MemState;
use crate::arch::ArchRuntime;
use crate::config::Config;
use crate::env;
use crate::gdb::{Gdb, GdbAction, GdbData};
use crate::memregion::{MemRegion, MemRegionErr, TargetMem};
use crate::os::cache::{Cache, CacheableRelocation};
use crate::os::{MemGuard, OsExt, OsModExt, OsModule};
use crate::selinux::{ContextError, SeLinuxContext};
use crate::{debugln, log_enabled};
use alloc::collections::BTreeSet;
use alloc::{vec, vec::Vec};
use arch::{ArchError, ArchUtil, Architecture, RelocationIdx, TargetRead, TargetWrite};
use bionic::mem::MemMut;
use bionic::posix::{Posix, OS};
use bionic::time::Instant;
#[cfg(feature = "btree")]
use btree_lite::BTreeLite;
#[cfg(feature = "cache_fn_lookups")]
use core::cell::RefCell;
use core::ffi::CStr;
use core::ops::Range;
use fallible_iterator::FallibleIterator;
use itertools::{EitherOrBoth, Itertools};
use lfr_meta::mlf::Mlf;
use lfr_meta::rand::{LfrChaCha, LfrRandR, LfrRng};
use lfr_meta::symbols::Trim;
use memstate::MemFail;
use modinfo::{ModuleInfo, TargetAddr};
use trap_parser::{
    Parser, ParserArch, TargetArch, TrapAddress, TrapInfo, TrapRecordType, TrapReloc,
    TrapRelocAddend, TrapRelocSymbol, TrapRelocV2, TrapRelocV2Symbol, TrapV1, TrapV2,
};

pub mod memstate {
    /// A memory manager that allows access to various attributes of memory
    /// depending on the state it is in.
    ///
    /// # Why is this neccesary?
    /// In order to safely set and remove permissions around memory it is very
    /// tedious to retreive SeLinuxContext, set memory permissions, build
    /// an interface to read/write memory. Additionally sometimes we need to
    /// create/modify existing memory.
    ///
    /// This structure allows you to selectively nest to the level of memory access required.
    /// And more importantly it allows you go back up the stack too, with resetting all permissions
    /// contexts as neccesary. Each level of the stack entails more permission to manipulate memory.
    ///
    /// # General Idea
    ///  * Uninit - No access to anything. There should only ever be one of these created.
    ///  * Context - Now you can request new memory regions, modify existing memory structures (mmap).
    ///  * Access - Now you can read/write any existing memory structures.
    ///
    /// The motivating factor was largely the CacheDaemon. In order to work, the cache daemon must neccesarily
    /// replace memory regions with cached versions. If the memory access is not enforced, rust may inadvertantly
    /// continue to use stale memory references due to optimizations.
    /// Now only the correct level of access should be available at any given time and enforced at compile time.
    use super::*;
    #[derive(Debug)]
    pub enum MemFail {
        /// Unable to retreive context.
        NoContext,
        /// Unable to change memory permissions.
        NoPerm,
    }

    /// Uninitialized Memory manager has no access to anything.
    pub struct Uninit {}
    /// Context based Memory manager has access to a given SeLinuxContext. This means that we are able
    /// to modify memory.
    pub struct Context {
        context: SeLinuxContext,
    }
    // Memory manager now has access to the actual memory contents (all WRITABLE).
    pub struct Access<'a> {
        segments: Vec<MemGuard<'a, MemMut>>,
    }

    /// The State Structure that enforces all transitions between the various memory states.
    pub struct MemState<'parent, S> {
        module: &'parent Module<'parent>,
        state: S,
    }

    impl<'parent> MemState<'parent, Uninit> {
        /// # Safety
        /// It is unsafe to call this function more than once and have two MemStates alive at the same time.
        pub unsafe fn uninit(module: &'parent Module) -> Self {
            Self {
                module,
                state: Uninit {},
            }
        }
    }

    impl<'parent> MemState<'parent, Uninit> {
        /// Move into a new SELinux Context with memory manipulation permissions.
        pub fn request_context(&mut self) -> Result<MemState<'parent, Context>, MemFail> {
            let context =
                unsafe { Posix::establish_write_context() }.map_err(|()| MemFail::NoContext)?;

            Ok(MemState {
                module: self.module,
                state: Context { context },
            })
        }
    }

    impl<'parent> MemState<'parent, Context> {
        /// Retreive an existing context.
        pub fn context(&self) -> &SeLinuxContext {
            &self.state.context
        }
        /// Move into a new state where you now have read/write access to all memory segments.
        pub fn access(&mut self) -> Result<(&SeLinuxContext, MemState<'parent, Access>), MemFail> {
            let segments =
                unsafe { Posix::collect_segments(self.module.os_mod, &self.state.context) }
                    .map_err(|_| MemFail::NoPerm)?;

            Ok((
                &self.state.context,
                MemState {
                    module: self.module,
                    state: Access { segments },
                },
            ))
        }
    }

    impl<'parent> MemState<'parent, Access<'parent>> {
        /// Requst access to a target memory structure, which gives you access to mutable memory slices.
        pub fn target<'a>(&'a mut self) -> TargetMem<'a, 'parent> {
            TargetMem::new(&mut self.state.segments, self.module.info.got_start)
        }
    }
}

/// Allow us to selectively ignore unimplemented relocations when using set/get target
/// pointer. These are generally not errors but can be useful to identify when
/// a given relocation is not implemented.
pub(crate) fn ignore_unimplemented_reloc(e: ArchError) -> Result<(), ArchError> {
    if let ArchError::Unimplemented = e {
        debugln!(10, "Skipped set_target_ptr for unimplemented relocation");
        Ok(())
    } else {
        Err(e)
    }
}

/// The various types of function this can be.
#[derive(Ord, PartialOrd, Eq, PartialEq, Copy, Clone, Debug)]
pub enum FunctionType {
    Gap = 0,
    Function = 1,
    Padding = 2,
}

/// A representation of a region of bytes that represents a function.
/// TODO: Maybe use bitflags for some fields?
#[derive(Eq, PartialOrd, PartialEq, Debug, Clone)]
pub struct Function {
    /// Undiversifed starting address
    pub undiv_start: TargetAddr<usize>,
    /// Diversified starting address
    pub div_start: TargetAddr<usize>,
    /// Size of the function in bytes.
    pub size: Option<usize>,
    /// The undiversified function alignment requirements.
    pub undiv_p2align: u32,
    /// Should this function be copied?
    pub skip_copy: bool,
    /// Is this function from trap data or computed?
    pub from_trap: bool,
    /// What kind of Function is this?
    pub typ: FunctionType,
}

impl Function {
    /// The delta between where the function was and to where it has been copied.
    pub fn div_delta(&self) -> usize {
        if self.skip_copy {
            0
        } else {
            TargetAddr::offset(&self.div_start, &self.undiv_start)
        }
    }
    /// The end of the undiversified function.
    pub fn undiv_end(&self) -> Option<TargetAddr<usize>> {
        Some(self.undiv_start.wrapping_add(self.size?))
    }
    /// The end of the diversified function.
    pub fn div_end(&self) -> Option<TargetAddr<usize>> {
        Some(self.div_start.wrapping_add(self.size?))
    }
    /// Undiversified address range..
    pub fn undiv_range(&self) -> Range<TargetAddr<usize>> {
        self.undiv_start..self.undiv_end().unwrap()
    }
    /// Whether the `addr` can be found within the undiversified function.
    pub fn undiv_contains(&self, addr: TargetAddr<usize>) -> bool {
        self.undiv_range().contains(&addr)
    }
    /// Whether the `addr` can be found within the diversified function.
    pub fn div_contains(&self, addr: TargetAddr<usize>) -> bool {
        (self.div_start..self.div_end().unwrap()).contains(&addr)
    }
    /// The updated address of a given `addr` after diversification.
    pub fn post_div_address(&self, addr: TargetAddr<usize>) -> TargetAddr<usize> {
        assert!(self.undiv_contains(addr));
        addr.wrapping_add(self.div_delta())
    }

    /// The ordering that is used to sort a set of functions.
    pub fn sort_rank(&self) -> u32 {
        match (self.size, self.typ) {
            (None, FunctionType::Gap) => 1,
            (None, _) => 2,
            (Some(0), _) => 3,
            _ => 4,
        }
    }
}

/// A runtime representation of what the associated data is for a Relocation.
#[derive(Debug)]
enum RelocationSymbol {
    /// Generic data, often an instruction.
    Data(u32),
    /// The address of the symbol.
    Addr(TargetAddr<usize>),
}

/// A runtime representation fo a relocation.
#[derive(Debug)]
pub struct Relocation {
    /// Type of relocation found in [`goblin::elf::reloc`]
    typ: RelocationIdx,
    /// The original address of the relocation (prior to diversification)
    orig_src_ptr: TargetAddr<usize>,
    /// The current address of the relocation.
    src_ptr: TargetAddr<usize>,
    /// Relocation addend information.
    addend: Option<TrapRelocAddend>,
    /// Relocation symbol information
    symbol: Option<RelocationSymbol>,
}

impl arch::Relocation<usize> for Relocation {
    fn typ(&self) -> RelocationIdx {
        self.typ
    }
    fn addend(&self) -> Option<TrapRelocAddend> {
        self.addend
    }
    fn symbol_address(&self) -> Option<TargetAddr<usize>> {
        match self.symbol {
            Some(RelocationSymbol::Addr(addr)) => Some(addr),
            _ => panic!("Symbol does not have an address"),
        }
    }
    fn symbol_data(&self) -> Option<u32> {
        match self.symbol {
            Some(RelocationSymbol::Data(d)) => Some(d),
            _ => panic!("Symbol does not have data"),
        }
    }

    fn address(&self) -> TargetAddr<usize> {
        self.src_ptr
    }
    fn orig_address(&self) -> TargetAddr<usize> {
        self.orig_src_ptr
    }
    fn new(
        typ: RelocationIdx,
        ptr: TargetAddr<usize>,
        symbol: Option<TargetAddr<usize>>,
        addend: Option<TrapRelocAddend>,
    ) -> Self {
        Relocation::new(typ, ptr, symbol, addend)
    }
}

impl Relocation {
    /// Create a new relocation.
    pub fn new(
        typ: RelocationIdx,
        ptr: TargetAddr<usize>,
        symbol: Option<TargetAddr<usize>>,
        addend: Option<TrapRelocAddend>,
    ) -> Self {
        Self {
            typ,
            src_ptr: ptr,
            orig_src_ptr: ptr,
            symbol: symbol.map(RelocationSymbol::Addr),
            addend,
        }
    }
    /// Type of relocation [`goblin::elf::reloc`]
    pub fn typ(&self) -> RelocationIdx {
        self.typ
    }
    /// Get the current relocation address
    pub fn get_source_ptr(&self) -> TargetAddr<usize> {
        self.src_ptr
    }
    /// Create a new pointer relocation.
    pub fn new_pointer_reloc(addr: TargetAddr<usize>) -> Relocation {
        Relocation {
            typ: RelocationIdx(arch::TargetArch::POINTER_RELOC),
            orig_src_ptr: addr,
            src_ptr: addr,
            addend: None,
            symbol: None,
        }
    }

    /// Create a new copy relocation.
    pub fn new_copy_reloc(addr: TargetAddr<usize>) -> Relocation {
        Relocation {
            typ: RelocationIdx(arch::TargetArch::COPY_RELOC),
            orig_src_ptr: addr,
            src_ptr: addr,
            addend: None,
            symbol: None,
        }
    }

    pub fn adjust_source(&mut self, source: Option<&Function>) {
        if let Some(source) = source.as_ref().filter(|s| !s.skip_copy) {
            assert!(source.undiv_contains(self.orig_src_ptr));
            let source_ptr = source.post_div_address(self.orig_src_ptr);
            self.src_ptr = source_ptr;
        }
    }
}

/// A module representation that contains the the information provided from the entrypoint
/// and the OS specific module information.
#[derive(Debug)]
pub struct Module<'a> {
    /// Entryptoint Provided information
    pub info: &'a ModuleInfo<'a>,
    /// OS Specific module related information.
    pub os_mod: &'a OsModule,
    /// Configuration file read for this randomization
    pub config: Option<Config>,
}

impl<'a> Module<'a> {
    //// Create a new module
    fn new(module_info: &'a ModuleInfo, os_mod: &'a OsModule, config: Option<Config>) -> Self {
        assert!(!module_info.got_start.is_null());

        <arch::TargetArch as ArchRuntime>::preprocess_arch();

        Self {
            info: module_info,
            os_mod,
            config,
        }
    }

    /// Run a given function for each executable section
    ///
    /// For each section we read the GOT entries for each relocation and call the
    /// appropriate callback function which is a version of randomizing the executable
    /// functions. At the beginning we set the permissions to RWX for all the segments
    /// so that we can edit them, but return the original permission levels at the end
    fn for_all_exec_sections<F>(&self, mem: &mut MemRegion<'_>, mut f: F) -> Result<(), ()>
    where
        F: FnMut(
            &mut crate::memregion::MemRegion<'_>,
            Range<TargetAddr<usize>>,
            &trap_parser::TrapInfo<'_, trap_parser::TargetArch>,
        ),
    {
        let sections: &[_] = self.info.sections.into();
        sections
            .iter()
            .inspect(|sec| {
                debugln!(1, "Section {:x?}", sec);
            })
            .filter(|sec| !sec.exec.is_empty() && !sec.trap.is_empty())
            .try_for_each(|sec| {
                let trap_range: Range<_> = sec.trap.into();
                let trap_len = TargetAddr::raw_range(&trap_range).len();
                debugln!(10, "Marking TRaP as readonly {:?}", trap_range);
                mem.force_read_only(trap_range.start.absolute_addr(), trap_len)
                    .expect("Couldn't make trap section read only");

                let slice = mem
                    .ro_slice(trap_range.start.absolute_addr(), trap_len)
                    .expect("Couldn't read trap");

                let trap = trap_parser::TrapParser::new(
                    trap_range.start.absolute_addr() as u64,
                    usize::from(self.os_mod.image_base()) as u64,
                    false,
                )
                .target()
                .parse(slice)
                .map_err(|e| {
                    debugln!(1, "Trouble decoding trap data {:?}", e);
                })?;

                debugln!(5, "Successfully read trap data");
                f(mem, sec.exec.into(), &trap);
                Ok(())
            })?;

        Ok(())
    }
    /// Gets the current value of the trampoline counter.
    fn get_trampoline_counter(&self) -> u32 {
        let tc = &core::sync::atomic::AtomicU32::from_mut(unsafe {
            self.info.trampoline_counter_ptr.as_mut().unwrap()
        });
        tc.load(core::sync::atomic::Ordering::Relaxed)
    }
    /// Gets the module name (as a [`CStr`])
    fn module_name(&self) -> Option<&CStr> {
        self.os_mod.module_name()
    }

    /// The data that is required for GDB to have knowledge of the given randomization.
    pub fn gdb_data(&self, exec_region: Range<u64>, copied_exec: Option<Range<u64>>) -> GdbData {
        let xptramp_range: Range<_> = self.info.xptramp.into();

        GdbData {
            exec_region: if let Some(copy) = copied_exec {
                copy
            } else {
                exec_region
            },
            textramp_region: xptramp_range.start.absolute_addr() as u64
                ..xptramp_range.end.absolute_addr() as u64,
            map_base: Some(usize::from(self.os_mod.image_base()) as u64),
            eh_frame_hdr: self
                .os_mod
                .eh_frame_hdr()
                .map(|addr| addr.absolute_addr())
                .unwrap_or(0) as u64,
            got: self.info.got_start.absolute_addr() as u64,
            module_path: self.os_mod.module_name(),
        }
    }
}

/// Provides methods that allow easy creation of Relocs from TRaP data.
/// (e.g. Different TRaP forms have different requirements for address conversion.)
trait RelocFromTrap {
    /// The type of relocations used for TRaP.
    type TrapReloc;
    /// Creates a relocation from the TRaP data.
    fn create_reloc(module: &Module<'_>, reloc: &Self::TrapReloc) -> Relocation;
}

/// Provides methods to convert a given address to the specified target address region.
/// This helper prevents unintended physical/virtual address range mismatches.
trait ToTarget {
    /// Convert the address with a given Module into a target address (usually a virtual address).
    fn to_target(addr: &TrapAddress, module: &Module<'_>) -> TargetAddr<usize>;
}

impl<T> ToTarget for TrapV1<'_, T> {
    fn to_target(addr: &TrapAddress, _module: &Module<'_>) -> TargetAddr<usize> {
        addr.absolute_addr(0)
    }
}

impl<T> ToTarget for TrapV2<'_, T> {
    fn to_target(addr: &TrapAddress, module: &Module<'_>) -> TargetAddr<usize> {
        addr.absolute_addr(module.info.trap_load_bias)
    }
}

impl<T> RelocFromTrap for TrapV1<'_, T> {
    type TrapReloc = TrapReloc;
    fn create_reloc(module: &Module<'_>, reloc: &Self::TrapReloc) -> Relocation {
        let sym = reloc.symbol.map(|sym| match sym {
            TrapRelocSymbol::Instruction(i) => RelocationSymbol::Data(i),
            TrapRelocSymbol::Address(addr) => {
                RelocationSymbol::Addr(Self::to_target(&addr, module))
            }
        });

        Relocation {
            typ: RelocationIdx(reloc.typ.try_into().unwrap()),
            orig_src_ptr: Self::to_target(&reloc.address, module),
            src_ptr: Self::to_target(&reloc.address, module),
            addend: reloc.addend,
            symbol: sym,
        }
    }
}

impl<P> RelocFromTrap for TrapV2<'_, P>
where
    P: ParserArch,
{
    type TrapReloc = TrapRelocV2;
    fn create_reloc(module: &Module<'_>, reloc: &Self::TrapReloc) -> Relocation {
        let sym = match reloc.symbol {
            Some(TrapRelocV2Symbol::Symbol(sym)) => Some(RelocationSymbol::Addr(Self::to_target(
                &TrapAddress(sym),
                module,
            ))),
            _ => None,
        };
        Relocation {
            typ: RelocationIdx(reloc.typ.into()),
            orig_src_ptr: Self::to_target(&reloc.address, module),
            src_ptr: Self::to_target(&reloc.address, module),
            addend: Some(TrapRelocAddend::Generic(reloc.addend)),
            symbol: sym,
        }
    }
}

#[derive(Debug)]
pub struct CacheEntry {
    range: Range<TargetAddr<usize>>,
    fn_id: Option<usize>,
}

#[derive(Debug)]
/// A collection of Functions that are sorted in undiversified order by `sort_rank`.
pub struct FunctionList {
    /// A btree form of the functions to aid in faster lookups.
    #[cfg(feature = "btree")]
    tree: Option<BTreeLite<TargetAddr<usize>>>,
    /// The list of functions.
    funcs: Vec<Function>,
    /// The range for the executable region that all functions are contained within.
    exec_section: Range<TargetAddr<usize>>,
    /// A cache of recently looked up functions.
    #[cfg(feature = "cache_fn_lookups")]
    fn_cache: RefCell<[Option<CacheEntry>; 2]>,
}

impl Default for FunctionList {
    fn default() -> Self {
        Self {
            exec_section: 0usize.into()..0usize.into(),
            #[cfg(feature = "btree")]
            tree: None,
            funcs: vec![],
            #[cfg(feature = "cache_fn_lookups")]
            fn_cache: RefCell::new(core::array::from_fn(|_| None)),
        }
    }
}

impl FunctionList {
    /// Builds the btree representation of the function vector.
    #[cfg(feature = "btree")]
    fn build_tree(&mut self) {
        self.tree = BTreeLite::new(&self.funcs, |f| f.undiv_start)
    }
    pub fn funcs(&self) -> &[Function] {
        &self.funcs
    }
    /// Finds the corresponding function for the specified address.
    fn find_function<'a>(
        &'a self,
        addr: TargetAddr<usize>,
        hint: Option<&'a Function>,
    ) -> Option<&'a Function> {
        match hint {
            Some(h) if h.undiv_contains(addr) => hint,
            _ => self.find_function_idx(addr).map(|(func, _idx)| func),
        }
    }

    /// Finds the corresponding function idx for the specified address.
    pub fn find_function_idx(&self, addr: TargetAddr<usize>) -> Option<(&Function, usize)> {
        fn find_function_inner(fn_list: &FunctionList, addr: TargetAddr<usize>) -> Option<usize> {
            #[cfg(feature = "btree")]
            if let Some(tree) = &fn_list.tree {
                return tree.find(&addr);
            }
            let search = fn_list.funcs.partition_point(|f| f.undiv_start <= addr);
            Some(search.saturating_sub(1))
        }

        if self.funcs.is_empty() {
            return None;
        }
        if !self.exec_section.contains(&addr) {
            return None;
        }
        #[cfg(feature = "cache_fn_lookups")]
        if let Ok(cache) = self.fn_cache.try_borrow() {
            if let Some(CacheEntry { fn_id: idx, .. }) = cache
                .iter()
                .flatten()
                .find(|entry| entry.range.contains(&addr))
            {
                return idx.and_then(|idx| self.funcs.get(idx).map(|f| (f, idx)));
            }
        }

        let found = if let Some(idx) = find_function_inner(self, addr) {
            let func = self.funcs.get(idx)?;
            if !func.skip_copy && func.undiv_contains(addr) {
                Some((func, idx))
            } else {
                None
            }
        } else {
            None
        };
        #[cfg(feature = "cache_fn_lookups")]
        if let Ok(mut cache) = self.fn_cache.try_borrow_mut() {
            cache.rotate_right(1);
            let range = found
                .map(|(func, _idx)| func.undiv_range())
                .unwrap_or(addr..addr + 1);
            cache[0] = Some(CacheEntry {
                range,
                fn_id: found.map(|(_func, idx)| idx),
            });
        }
        found
    }
    /// Adjust a given address, with an optional function hint, from undiversified to diversified
    /// address ranges and tells you which function was found.
    /// This allows the hint to search forward an arbitrary amount based on assumptions of the
    /// region. This will be slower for random access targets.
    pub fn adjust_target_asc<'a>(
        &'a self,
        target_ptr: TargetAddr<usize>,
        hint: Option<(&'a Function, usize)>,
    ) -> (Option<(&'a Function, usize)>, TargetAddr<usize>) {
        let hint = match hint {
            None if self.exec_section.contains(&target_ptr) => self.find_function_idx(target_ptr),
            None => None,
            Some(hint) if hint.0.undiv_contains(target_ptr) => Some(hint),
            Some(hint) => self
                .funcs()
                .iter()
                .enumerate()
                .skip(hint.1)
                .take(5) // Arbitrary forward search limit
                .find(|(_idx, f)| f.undiv_contains(target_ptr))
                .map(|(idx, f)| (f, idx)),
        };
        let (_new_hint, addr) = self.adjust_target(target_ptr, hint.map(|(f, _idx)| f));
        (hint, addr)
    }
    /// Adjust a given address, with an optional function hint, from undiversified to diversified
    /// address ranges and tells you which function was found.
    pub fn adjust_target<'a>(
        &'a self,
        target_ptr: TargetAddr<usize>,
        hint: Option<&'a Function>,
    ) -> (Option<&'a Function>, TargetAddr<usize>) {
        let target_func = self.find_function(target_ptr, hint);
        let target_ptr = if let Some(target) = target_func {
            target.post_div_address(target_ptr)
        } else {
            target_ptr
        };
        (target_func, target_ptr)
    }
    /// Adjusts a relocation address based on the known set of functions
    /// from undiversified to diversified and then applies that tranformation
    /// the relocation.
    pub fn adjust_relocation<T>(
        &self,
        mem: &mut T,
        reloc: &mut Relocation,
        source: Option<&Function>,
        hint: Option<&Function>,
    ) -> Option<CacheableRelocation>
    where
        T: TargetRead<usize> + TargetWrite<usize>,
    {
        use arch::Relocation;
        // We attempt to get the contiguous subslice here, even though we may already be receiving one.
        // If it's one already, this is basically a no-op, otherwise we still end up saving big on the
        // number of lookups that are about to follow via get/set_target_ptr.
        let mut mem = mem.contiguous_slice_containing_mut(reloc.address()).ok()?;

        reloc.adjust_source(source);

        debugln!(5, "Reloc {:?}", reloc);

        let target_ptr: TargetAddr<usize> =
            if let Some(ptr) = arch::TargetArch::get_target_ptr(reloc, &mem) {
                ptr
            } else {
                debugln!(5, "WARNING: Couldn't get target_ptr");
                return None;
            };

        let (target_func, target_ptr) = self.adjust_target(target_ptr, hint);

        if source.map(|x| x.skip_copy).unwrap_or(true)
            && target_func.map(|x| x.skip_copy).unwrap_or(true)
        {
            /* no adjustment */
            None
        } else {
            debugln!(6, "  setting => {:?}", target_ptr);
            arch::TargetArch::set_target_ptr(reloc, &mut mem, target_ptr)
                .or_else(ignore_unimplemented_reloc)
                .unwrap();

            Some(CacheableRelocation::new(reloc, target_ptr))
        }
    }
}

/// Useful macro that takes effectively a comment string and an expression and reports the amount
/// of time spent running that particular expression.
macro_rules! timeit {
    ($name:literal, $call:expr) => {{
        // We use the `rdtsc` instruction to compute "good enough" instruction counts. It's not exactly accurate
        // on certain platforms but it's better than straight wall clock time. YMMV.
        // See https://stackoverflow.com/questions/42189976/calculate-system-time-using-rdtsc
        cfg_if::cfg_if! {
            if #[cfg(target_arch = "x86")] {
                use core::arch::x86::_rdtsc as rdtsc;
            } else if #[cfg(target_arch = "x86_64")] {
                use core::arch::x86_64::_rdtsc as rdtsc;
            }
        }
        #[cfg(any(target_arch = "x86_64", target_arch = "x86"))]
        let cycles_start = unsafe { rdtsc() };
        let start = Instant::now();
        let res = $call;

        cfg_if::cfg_if! {
            if #[cfg(any(target_arch = "x86_64", target_arch = "x86"))] {
                let cycles_count = unsafe { rdtsc() }.saturating_sub(cycles_start);
                debugln!(
                    1,
                    "Step {} time: {}us {}cycles",
                    $name,
                    start.elapsed().as_micros(),
                    cycles_count
                );
            } else {
                debugln!(
                    1,
                    "Step {} time: {}us",
                    $name,
                    start.elapsed().as_micros()
                );
            }
        };
        #[allow(path_statements)]
        res
    }};
}

/// A catch-all for the state required to convert an executable region from
/// undiversified to diversified (aka Shuffling).
struct ExecSectionProcessor<'a, 'b, 'context, Trap, Rand> {
    /// Whether this diversification should be done in place.
    in_place: bool,
    /// The TRaP data read from memory.
    trap_info: &'a Trap,
    /// The set of functions that are to diversified.
    functions: FunctionList,
    /// Random number generator
    rand: &'a mut Rand,
    /// The module information about the current randomization.
    module: &'a Module<'a>,
    /// An optional cache that can store relocations and diversified sections.
    cache: Option<&'a mut Cache>,
    /// The copy made of the executable section for diversification (if not in place)
    exec_section_copy: Option<MemGuard<'context, MemMut>>,
    /// The actual undiversified region and eventual diversified region (if in place).
    exec_section: Range<TargetAddr<usize>>,
    /// The target memory space.
    mem: &'a mut crate::memregion::MemRegion<'b>,
    /// The gdb interface for communication randomizations to GDB.
    gdb: Option<&'a mut Gdb>,
}

/// Methods used to manipulate the target memory space based on given TRaP data.
trait IterateTrapFunctions<'a>: RelocFromTrap<TrapReloc = Self::RelocType> {
    /// The Relocation Type for TRaP
    type RelocType;
    /// Walk all the computed functions and apply the given predicate function `F`.
    fn iterate<F>(&'a self, module: &Module<'_>, exec_section: &Range<TargetAddr<usize>>, pred: F)
    where
        F: FnMut(Function);
    /// Prepares functions by computing useful information about each function (if neccesary).
    fn prepare_functions(
        &self,
        _exec_section: &Range<TargetAddr<usize>>,
        _mem: &mut MemRegion<'_>,
        _funcs: &mut Vec<Function>,
        _trim_info: Option<&mut Vec<Trim>>,
    ) {
    }

    /// Patches any necesary data references that are within of the executable region.
    fn patch_data_refs(
        &self,
        _module: &Module<'_>,
        _mem: &mut MemRegion<'_>,
        _exec_section: &Range<TargetAddr<usize>>,
        _funcs: &mut FunctionList,
    ) {
    }
    /// Process all relocations within the executable region.
    fn process_relocations(
        &self,
        module: &Module<'_>,
        cache: Option<&mut Cache>,
        mem: &mut MemRegion<'_>,
        funcs: &mut FunctionList,
    ) -> BTreeSet<TargetAddr<usize>>;

    /// Apply the function `F` to all available relocation.s
    fn for_all_relocations<F>(&self, func: F)
    where
        F: FnMut(Self::RelocType);

    /// Read the Global Offset Table reloccations from the TRaP data.
    fn read_got_relocation<T>(&self, reloc: &Relocation, mem: &T) -> Option<TargetAddr<usize>>
    where
        T: TargetRead<usize> + TargetWrite<usize>,
    {
        match arch::TargetArch::get_got_entry(reloc, mem) {
            Err(e) => {
                debugln!(1, "ERROR: reading GOT entry {:?}", e);
                None
            }
            Ok(None) => None,
            Ok(Some(got_addr)) => {
                debugln!(
                    10,
                    "GOT entry @{:x?} relocation{:?}",
                    got_addr,
                    reloc.get_source_ptr(),
                );
                Some(got_addr)
            }
        }
    }
}

/// Patches trampoline region jump offsets to match diversified region.
/// TODO: Move to a new OS type for Windows.
fn patch_trampoline(mem: &mut MemRegion<'_>, at: &TargetAddr<usize>, to: &TargetAddr<usize>) {
    // We add the MOV EDI, EDI here to support hot-patching
    // FIXME: only really need this on Windows
    let instr = &[
        0x8B, //MOV EDI, EDI
        0xFF, 0xE9, // JMP <pcrel>
    ];
    let slice = mem.slice_mut(at.absolute_addr(), instr.len()).unwrap();
    slice.copy_from_slice(instr);

    mem.write_vaddr::<i32>(
        at.wrapping_add(instr.len()),
        arch::assert_cast::<i32, _, _>(to.wrapping_sub(at.wrapping_add(4usize))),
    )
    .unwrap();
}

impl<'a> IterateTrapFunctions<'a> for TrapV2<'a, TargetArch> {
    type RelocType = TrapRelocV2;
    /// Traverse all relocations
    fn for_all_relocations<F>(&self, mut func: F)
    where
        F: FnMut(TrapRelocV2),
    {
        self.relocs()
            .for_each(|reloc| {
                func(reloc);
                Ok(())
            })
            .expect("valid relocs");
    }
    /// Adjust the relocations to match the new diversified addresses, and if it is a
    /// non-executable relocation, add it to the list of cached relocations
    fn process_relocations(
        &self,
        module: &Module<'_>,
        mut cache: Option<&mut Cache>,
        mem: &mut MemRegion<'_>,
        funcs: &mut FunctionList,
    ) -> BTreeSet<TargetAddr<usize>> {
        let mut got = BTreeSet::default();

        let mut records = self
            .records()
            .zip(fallible_iterator::convert(funcs.funcs.iter().map(Ok)));
        while let Some((record, source_func)) = records.next().expect("valid record") {
            let mut relocs = record.relocs(self);
            while let Some(reloc) = relocs.next().expect("valid reloc") {
                let hint = match reloc.symbol {
                    Some(TrapRelocV2Symbol::Index(idx)) => funcs.funcs.get(idx),
                    _ => None,
                };
                let mut reloc = Self::create_reloc(module, &reloc);
                reloc.adjust_source(Some(source_func));
                if let Some(entry) = self.read_got_relocation(&reloc, mem) {
                    got.insert(entry);
                }
                let cacheable = funcs.adjust_relocation(mem, &mut reloc, Some(source_func), hint);
                if let Some(cache) = &mut cache {
                    cache.add_reloc(cacheable);
                }
            }
        }
        got
    }

    /// Traverse all functions, producing Gap, Padding, Function as neccesary.
    fn iterate<F>(
        &'a self,
        module: &Module<'_>,
        _exec_section: &Range<TargetAddr<usize>>,
        mut pred: F,
    ) where
        F: FnMut(Function),
    {
        let mut records = self.records();
        while let Some(entry) = records.next().expect("valid record") {
            let start_addr = Self::to_target(&entry.address, module);
            #[cfg(target_arch = "arm")]
            let start_addr = start_addr.mask(!1);

            let func = Function {
                undiv_start: start_addr,
                div_start: start_addr,
                skip_copy: false,
                from_trap: true,
                undiv_p2align: if self.header().flags.has_symbol_p2align() {
                    entry.align.into()
                } else {
                    Posix::FUNCTION_P2ALIGN
                },
                size: Some(entry.size),
                typ: FunctionType::Function,
            };
            let func = match entry.typ {
                TrapRecordType::Function => func,
                TrapRecordType::Padding => Function {
                    skip_copy: true,
                    typ: FunctionType::Padding,
                    ..func
                },
                TrapRecordType::Gap => Function {
                    from_trap: false,
                    typ: FunctionType::Gap,
                    ..func
                },
                TrapRecordType::NonExecRegion => Function {
                    skip_copy: true,
                    ..func
                },
                TrapRecordType::Got => func,
                TrapRecordType::Unknown => {
                    panic!("Unknown Record Type")
                }
            };
            pred(func);
        }
    }
}

impl<'a, T> IterateTrapFunctions<'a> for TrapV1<'a, T>
where
    T: ParserArch,
{
    type RelocType = TrapReloc;

    /// Traverse all relocations.
    fn for_all_relocations<F>(&self, mut func: F)
    where
        F: FnMut(TrapReloc),
    {
        if self.header().flags.has_nonexec_relocs() {
            self.nonexec_relocs()
                .for_each(|reloc| {
                    func(reloc);
                    Ok(())
                })
                .expect("valid relocs");
        }
        let mut records = self.records();
        while let Some(entry) = records.next().expect("valid record") {
            entry
                .relocs()
                .for_each(|reloc| {
                    func(reloc);
                    Ok(())
                })
                .expect("valid relocs");
        }
    }

    /// Adjust the relocations to match the new diversified addresses, and if it is a
    /// non-executable relocation, add it to the list of cached relocations
    fn process_relocations(
        &self,
        module: &Module<'_>,
        mut cache: Option<&mut Cache>,
        mem: &mut MemRegion,
        funcs: &mut FunctionList,
    ) -> BTreeSet<TargetAddr<usize>> {
        let mut got = BTreeSet::default();
        let mut last_lookup = None;
        self.for_all_relocations(|reloc| {
            let mut reloc = Self::create_reloc(module, &reloc);
            let mut mem = mem.contiguous_slice_containing_mut(reloc.address()).ok();
            use arch::Relocation;

            if let Some(mem) = &mut mem {
                last_lookup = funcs.find_function(reloc.get_source_ptr(), last_lookup);
                reloc.adjust_source(last_lookup);
                if let Some(entry) = self.read_got_relocation(&reloc, mem) {
                    got.insert(entry);
                }
                let cacheable = funcs.adjust_relocation(mem, &mut reloc, last_lookup, None);

                if let Some(cache) = &mut cache {
                    cache.add_reloc(cacheable);
                }
            }
        });
        got
    }
    /// Iterate over all constructed Functions, Gap, Padding, etc.
    fn iterate<F>(
        &'a self,
        module: &Module<'_>,
        exec_section: &Range<TargetAddr<usize>>,
        mut pred: F,
    ) where
        F: FnMut(Function),
    {
        let mut records = self.records();
        while let Some(entry) = records.next().expect("valid record") {
            if exec_section.contains(&Self::to_target(&entry.address, module)) {
                let mut symbols = entry.symbols();
                while let Some(sym) = symbols.next().expect("valid symbol") {
                    let start_addr = Self::to_target(&sym.address, module);
                    #[cfg(target_arch = "arm")]
                    let start_addr = start_addr.mask(!1);

                    let p2align = sym.align.map(|x| x.try_into().unwrap());
                    let size = sym.size.map(|x| usize::try_from(x).unwrap());
                    let new_func = Function {
                        undiv_start: start_addr,
                        div_start: start_addr,
                        skip_copy: false,
                        from_trap: true,
                        undiv_p2align: p2align.unwrap_or(Posix::FUNCTION_P2ALIGN),
                        size,
                        typ: FunctionType::Function,
                    };

                    if size.is_some() {
                        let gap = Function {
                            undiv_start: new_func.undiv_end().unwrap(),
                            div_start: new_func.undiv_end().unwrap(),
                            size: None,
                            undiv_p2align: 0,
                            skip_copy: false,
                            from_trap: false,
                            typ: FunctionType::Gap,
                        };
                        pred(gap);
                    }

                    pred(new_func);
                }
                if let Some(padding) = &entry.padding {
                    let start = Self::to_target(&padding.address(&entry), module);
                    let new_func = Function {
                        undiv_p2align: 0,
                        from_trap: false,
                        skip_copy: true,
                        undiv_start: start,
                        div_start: start,
                        typ: FunctionType::Padding,
                        size: Some(usize::try_from(padding.size).unwrap()),
                    };
                    let undiv_end = new_func.undiv_end().unwrap();
                    pred(new_func);

                    let gap_func = Function {
                        undiv_start: undiv_end,
                        div_start: undiv_end,
                        undiv_p2align: 0,
                        skip_copy: false,
                        from_trap: false,
                        typ: FunctionType::Gap,
                        size: None,
                    };
                    pred(gap_func);
                }
            }
        }
    }

    /// Patches data refs found in trampoline entries.
    fn patch_data_refs(
        &self,
        module: &Module<'_>,
        mem: &mut MemRegion<'_>,
        exec_section: &Range<TargetAddr<usize>>,
        funcs: &mut FunctionList,
    ) {
        if self.header().flags.has_data_refs() {
            let mut records = self.records();
            while let Some(entry) = records.next().expect("valid record") {
                let addr = Self::to_target(&entry.address, module);
                if exec_section.contains(&addr) {
                    let mut data_refs = entry.data_refs();
                    let mut last_lookup = None;
                    while let Some(r) = data_refs.next().expect("valid data ref") {
                        let ref_addr = Self::to_target(&TrapAddress(r), module);
                        last_lookup = funcs.find_function(ref_addr, last_lookup);
                        if let Some(func) = &last_lookup {
                            patch_trampoline(
                                mem,
                                &ref_addr,
                                &ref_addr.wrapping_add(func.div_delta()),
                            );
                        }
                    }
                }
            }
        }
    }

    /// Prepares set of functions by computing sizes, trimming, and sorting.
    fn prepare_functions(
        &self,
        exec_section: &Range<TargetAddr<usize>>,
        mem: &mut MemRegion<'_>,
        funcs: &mut Vec<Function>,
        trim_info: Option<&mut Vec<Trim>>,
    ) {
        fn compute_function_sizes(exec_section: &Range<TargetAddr<usize>>, funcs: &mut [Function]) {
            funcs
                .iter_mut()
                .rev()
                .scan(exec_section.end, |end, f| {
                    let new_size = TargetAddr::offset(end, &f.undiv_start);
                    *end = f.undiv_start;
                    Some((f, new_size))
                })
                .for_each(|(f, new_size)| {
                    f.size.get_or_insert(new_size);
                });
        }
        fn trim_gaps(
            mem: &mut MemRegion<'_>,
            exec_section: &Range<TargetAddr<usize>>,
            funcs: &mut [Function],
            trim_info: Option<&mut Vec<Trim>>,
        ) {
            let undiv_code = mem
                .slice_mut(
                    exec_section.start.absolute_addr(),
                    TargetAddr::raw_range(exec_section).len(),
                )
                .unwrap();

            let trimmed = funcs
                .iter_mut()
                .filter(|f| f.typ == FunctionType::Gap)
                .flat_map(|f| {
                    let old_start = f.undiv_start;
                    let old_size = f.size.unwrap();
                    let undiv_offset = TargetAddr::offset(&f.undiv_start, &exec_section.start);
                    let func_mem = &undiv_code[undiv_offset..][..old_size];
                    let nops = func_mem
                        .iter()
                        .copied()
                        .take_while(|&x| arch::TargetArch::is_one_byte_nop(x))
                        .count();
                    f.undiv_start = f.undiv_start.wrapping_add(nops);
                    let new_size = old_size - nops;
                    f.size = Some(new_size);
                    if old_size != new_size {
                        debugln!(
                            8,
                            "Trimming {:?} from {} -> {}",
                            old_start,
                            old_size,
                            new_size,
                        );
                        Some(Trim {
                            undiv_start: old_start.absolute_addr() as u64,
                            new_size: new_size as u64,
                        })
                    } else {
                        None
                    }
                });

            if let Some(trim_info) = trim_info {
                // Store computed trim info.
                trim_info.extend(trimmed);
            } else {
                // Run iterator, ignore result.
                trimmed.for_each(|_| ());
            }
        }
        timeit! {
            "sort_functions", {
                let compare = |f: &Function| (f.undiv_start, f.sort_rank());
                if self.header().flags.pre_sorted() {
                    assert!(funcs.is_sorted_by_key(compare));
                } else {
                    funcs.sort_unstable_by_key(compare);
                }
            }
        };
        if log_enabled!(6) {
            funcs.iter().enumerate().for_each(|(i, f)| {
                debugln!(
                    6,
                    "{} : {} {} {} {}",
                    i,
                    f.undiv_start,
                    f.sort_rank(),
                    u32::from(f.typ == FunctionType::Gap),
                    u32::from(f.size.is_some())
                );
            });
        }
        timeit!(
            "compute_function_sizes",
            compute_function_sizes(exec_section, funcs)
        );
        timeit!("trim_gaps", trim_gaps(mem, exec_section, funcs, trim_info));
        timeit!("remove_empty_functions", {
            let count = funcs.len();
            funcs.retain(|f| f.size.unwrap() > 0);
            debugln!(3, "Removed {} functions", count - funcs.len())
        });
    }
}

impl<'a, 'b, 'context, Trap, Rand> ExecSectionProcessor<'a, 'b, 'context, Trap, Rand>
where
    Trap: IterateTrapFunctions<'a>,
    Rand: LfrRng,
{
    /// Create a new ExecSectionProcessor based on the available context.
    #[allow(clippy::too_many_arguments)]
    fn new(
        trap: &'a Trap,
        rand: &'a mut Rand,
        gdb: Option<&'a mut Gdb>,
        cache: Option<&'a mut Cache>,
        mem: &'a mut crate::memregion::MemRegion<'b>,
        exec_section: Range<TargetAddr<usize>>,
        module: &'a Module<'_>,
        in_place: bool,
    ) -> Self {
        let functions = FunctionList {
            exec_section: exec_section.clone(),
            ..Default::default()
        };

        Self {
            in_place,
            trap_info: trap,
            functions,
            module,
            rand,
            mem,
            cache,
            exec_section,
            exec_section_copy: None,
            gdb,
        }
    }
    /// Counts the number of functions. An optimization to allocate the vector
    /// at a specific size, without growing it.
    #[cfg(feature = "count_functions")]
    fn count_functions(&mut self) {
        let mut count = 0;
        self.trap_info
            .iterate(&self.module, &self.exec_section, |_func| count += 1);
        debugln!(1, "Trap Functions {}", count);
        self.functions.funcs.reserve_exact(count);
    }
    /// Build the set of functions from the trap info.
    fn build_functions(&mut self) {
        debugln!(1, "Building Functions...");
        self.trap_info
            .iterate(self.module, &self.exec_section, |func| {
                self.functions.funcs.push(func)
            });
    }
    /// Prepare the functions from the TRaP data.
    fn prepare_functions(&mut self) {
        self.trap_info.prepare_functions(
            &self.exec_section,
            self.mem,
            &mut self.functions.funcs,
            self.gdb.as_mut().map(|g| g.trim()),
        );
    }
    /// Shuffles all available functions.
    fn shuffle_functions(&mut self) -> Vec<usize> {
        debugln!(1, "Shuffling functions...");
        let skip_shuffle = env::SkipShuffle::get();
        let mut shuffled_order = Vec::from_iter(0..self.functions.funcs.len());
        if skip_shuffle {
            debugln!(
                1,
                "LFR: warning: applying identity transformation. No real protection!"
            );
            let idx = if let Some(offset) = env::StartLayout::get() {
                core::cmp::min(offset, shuffled_order.len())
            } else {
                0
            };

            if env::Reverse::get() {
                let (_, layout) = shuffled_order.split_at_mut(idx);
                layout.reverse();
            }
        } else {
            for i in 0..shuffled_order.len() {
                let j = self.rand.random(shuffled_order.len() - i);
                assert!(i + j < shuffled_order.len());
                if j > 0 {
                    shuffled_order.swap(i, i + j);
                }
            }
        }
        shuffled_order
    }
    /// Layout the code in the newly diversified order, to prepare to move the code.
    fn layout_code(&mut self, shuffled_order: &[usize]) -> usize {
        let orig_code = self.exec_section.start;
        let idx = if !env::SkipShuffle::get() {
            0
        } else if let Some(offset) = env::StartLayout::get() {
            core::cmp::min(offset, shuffled_order.len())
        } else {
            0
        };
        let (unchanged, layout) = shuffled_order.split_at(idx);
        let mut cur_addr = unchanged
            .last()
            .map(|l| self.functions.funcs[*l].undiv_end().unwrap())
            .unwrap_or(orig_code);

        layout.iter().copied().for_each(|si| {
            let func = &mut self.functions.funcs[si];
            if func.skip_copy {
                return;
            }
            // Align functions to either a multiple of kFunctionAlignment
            // or the same modulo as the undiversified code
            // (depending on PRESERVE_FUNCTION_OFFSET)
            // TODO: handle 5-NOP padding between consecutive functions
            let align = func.undiv_p2align;
            let old_ofs = func.undiv_start.wrapping_sub(orig_code).align_mask(align);
            let cur_ofs = cur_addr.wrapping_sub(orig_code).align_mask(align);
            let want_ofs = if Posix::PRESERVE_FUNCTION_OFFSET {
                old_ofs
            } else {
                TargetAddr::from_raw(0usize)
            };
            if cur_ofs != want_ofs {
                let padding = want_ofs.wrapping_sub(cur_ofs).align_mask(align);
                cur_addr = cur_addr.wrapping_add(padding);
            }

            // TODO: also add in Windows-specific hot-patch trampolines
            func.div_start = cur_addr;
            cur_addr = cur_addr.wrapping_add(func.size.unwrap());
        });

        let exec_code_size = TargetAddr::offset(&cur_addr, &orig_code);
        debugln!(1, "Divcode size: {}", exec_code_size);

        if exec_code_size > TargetAddr::offset(&self.exec_section.end, &self.exec_section.start) {
            debugln!(1, "Cannot randomize in place!");
            self.in_place = false;
        }

        #[cfg(feature = "force_in_place")]
        assert!(self.in_place);

        exec_code_size
    }
    /// Places the diversified layout of the code into the executable region.
    fn shuffle_code(
        &mut self,
        shuffled_order: &[usize],
        new_exec_code_size: usize,
        context: &'context SeLinuxContext,
    ) {
        let tc = self.module.get_trampoline_counter();
        debugln!(1, "Module trampoline_counter: {}", tc);
        assert_eq!(tc, 0);

        // Copy the code to a backup
        let exec_section_len = TargetAddr::raw_range(&self.exec_section).len();
        let exec_copy = if self.in_place {
            // If we're randomizing in-place, we don't care where the copy is in
            // memory, since we release it shortly anyway
            bionic::mem::MemBuilder::default()
                .build_mut(new_exec_code_size)
                .unwrap()
        } else {
            loop {
                // Pick a random address within 2GB of the original code
                // (this is required for 32-bit PC-relative relocations)
                let start_ptr = self
                    .exec_section
                    .start
                    .wrapping_add(exec_section_len)
                    .wrapping_add(Posix::PAGE_SIZE - 1);
                let start_page = start_ptr.absolute_addr() >> Posix::PAGE_SHIFT;
                // The copy is within (2GB - the size of the section), so that
                // a jump from the start of m_exec_data can reach
                // the end of m_exec_copy without overflowing
                let max_copy_delta = (2 << 30) - exec_section_len;
                let copy_page = start_page + self.rand.random(max_copy_delta >> Posix::PAGE_SHIFT);
                let hint_addr = TargetAddr::<usize>::from_raw(copy_page << Posix::PAGE_SHIFT);
                let mut mem = bionic::mem::MemBuilder::default();
                unsafe { mem.with_addr(hint_addr.try_into().unwrap()) };
                if let Ok(mem) = mem.build_mut(new_exec_code_size) {
                    break mem;
                }
            }
        };

        let mut exec_copy = exec_copy;

        debugln!(1, "Divcode@{:?}", exec_copy.slice().as_ptr());

        let undiv_code = self
            .mem
            .slice_mut(self.exec_section.start.absolute_addr(), exec_section_len)
            .unwrap();

        let copy_fns = shuffled_order
            .iter()
            .map(|&si| &self.functions.funcs[si])
            .filter(|f| !f.skip_copy);

        copy_fns
            .clone()
            .zip_longest(copy_fns.skip(1))
            .map(|x| {
                // Determine length of next function slice
                match x {
                    // We compare this function to the next to determine the available region.
                    EitherOrBoth::Both(a, b) => {
                        (a, Some(TargetAddr::offset(&b.div_start, &a.div_start)))
                    }
                    // What remains is the last function and it has no explicit length.
                    EitherOrBoth::Left(a) => (a, None),
                    // It is guaranteed that the right iterator will always be at most 1 shorter than left.
                    EitherOrBoth::Right(_) => unreachable!(),
                }
            })
            .scan(exec_copy.slice_mut(), |state, (f, len)| {
                //Split the diversified region into a slice for each function
                let remain = core::mem::take(state);
                let len = len.unwrap_or(remain.len());
                let (slice, remain) = remain.split_at_mut(len);
                *state = remain;
                Some((f, slice))
            })
            .for_each(|(func, slice)| {
                debugln!(
                    3,
                    "Moving {:?}[{:?}]=>{:?}@{:x?}",
                    func.undiv_start,
                    func.size,
                    func.div_start,
                    slice.as_ptr(),
                );

                let sz = func.size.unwrap();
                let (div_code, nops) = slice.split_at_mut(sz);
                // Write undiversified function to diversified location.
                let undiv_offset = TargetAddr::offset(&func.undiv_start, &self.exec_section.start);
                div_code.copy_from_slice(&undiv_code[undiv_offset..][..sz]);
                // Fill remaining region (if any) with nops
                arch::TargetArch::fill_nops(nops);
            });

        if self.in_place {
            debugln!(
                3,
                "Copy code back {:x?}[{}]=>{:x?}",
                exec_copy.slice().as_ptr(),
                exec_copy.slice().len(),
                undiv_code.as_ptr(),
            );
            let (copy_to, excess) = undiv_code.split_at_mut(exec_copy.slice().len());

            copy_to.copy_from_slice(exec_copy.slice());
            // if we have extra space to fill
            excess.fill(0xcc);

            // Revert the div_start addresses to the original section
            // self.functions.funcs.iter_mut().for_each(|f| {
            //     f.div_start = f.div_start.wrapping_sub(copy_delta);
            // });
        } else {
            self.patch_data_refs();
            self.exec_section_copy = Some(unsafe {
                self.module
                    .os_mod
                    .guard_exec_mem(exec_copy.persist(), context)
            });
        }
    }

    /// Patch data refs (if neccesary).
    fn patch_data_refs(&mut self) {
        self.trap_info.patch_data_refs(
            self.module,
            self.mem,
            &self.exec_section,
            &mut self.functions,
        )
    }
    /// Fixup OS dependent relocations.
    fn fixup_relocations(&mut self, got: &BTreeSet<TargetAddr<usize>>) {
        Posix::fixup_relocations(
            self.mem,
            self.module.info,
            self.module.os_mod,
            &self.functions,
            got,
            self.cache.as_deref_mut(),
        );
    }
    /// Fixup the trampoline section
    fn fixup_exports(&mut self) {
        debugln!(5, "Export Section: {:x?}", self.module.info.xptramp);
        arch::TargetArch::fixup_export_trampoline(
            self.mem,
            self.module.info.xptramp.into(),
            &self.functions,
        );
    }
    /// Build the BTree representation of the functions.
    #[cfg(feature = "btree")]
    fn build_function_btree(&mut self) {
        self.functions.build_tree();
    }
    /// Optionally process the Exception Handler Frames. This is not explicitly
    /// required to run the application but may aid in using other applications like
    /// pstack which require the information to be valid.
    /// This is optional for our GDB support because we do our own stack unwinding.
    /// GDB itself does not actually read this memory when updated anyway, preferring to
    /// only reference the on-disk form.
    fn process_eh_frame(&mut self) {
        if let Err(e) = Posix::update_eh_frame(self.mem, self.module.os_mod, |addr| {
            let addr = TargetAddr::from_raw(addr);
            let (_, new_addr) = self.functions.adjust_target(addr, None);
            debugln!(
                5,
                "Relocating eh_frame address {:x?} -> {:x?}",
                addr,
                new_addr
            );
            new_addr.absolute_addr()
        }) {
            debugln!(1, "ERROR: Processing EH_FRAMES {}", e)
        }
    }
    /// All cleanup is done by drops now.
    fn cleanup(&mut self) {
        debugln!(3, "Finishing cleanup");
    }

    /// Notify GDB at the end of randomization with all metadata required to
    /// run a debugging session.
    fn notify_gdb(&mut self, shuffled_order: &[usize]) {
        debugln!(3, "Notify GDB");

        if let Some(gdb) = &mut self.gdb {
            let gdb_data = self.module.gdb_data(
                self.exec_section.start.absolute_addr() as u64
                    ..self.exec_section.end.absolute_addr() as u64,
                self.exec_section_copy
                    .as_ref()
                    .map(|r| r.range().start as u64..r.range().end as u64),
            );
            let file_base = gdb_data.map_base;
            let mlf = Mlf {
                version: 0x101,
                seed: Some(self.rand.seed().to_vec()),
                file_base,
                func_base: Some(gdb_data.exec_region.start),
                func_size: gdb_data.exec_region.end - gdb_data.exec_region.start,
                mod_name: alloc::string::String::new(),
                funcs: (0..self.functions.funcs.len())
                    .map(|i| shuffled_order[i])
                    .map(|i| &self.functions.funcs[i])
                    .map(|f| lfr_meta::mlf::Func {
                        undiv_start: f.undiv_start.absolute_addr() as u64 - file_base.unwrap_or(0),
                        div_start: f.div_start.absolute_addr() as u64,
                        size: (f.size.unwrap() as u64),
                    })
                    .collect(),
            };

            gdb.trim().iter_mut().for_each(|t| {
                t.undiv_start -= file_base.unwrap_or(0);
            });
            let _ = gdb
                .notify(
                    mlf,
                    gdb_data,
                    self.rand,
                    self.cache.as_deref_mut(),
                    env::ProcessEhFrame::get(),
                )
                .map_err(|_| {
                    debugln!(1, "Failed to notify GDB Debug Information");
                });
        }
    }

    /// Write a layout file (MLF) for debugging or visualization.
    #[cfg(feature = "write_layouts")]
    fn write_layout_file(&self) -> Result<(), bionic::io::Error> {
        use bionic::io::Write;

        if !env::WriteLayoutFile::get() {
            return Ok(());
        }
        let version: u32 = 0x101;
        let func_base = self
            .functions
            .funcs
            .first()
            .map(|f| f.undiv_start)
            .unwrap_or_default();
        let func_end = self
            .functions
            .funcs
            .iter()
            .last()
            .and_then(|f| f.undiv_end())
            .unwrap_or_default();
        let func_size = TargetAddr::offset(&func_end, &func_base);
        let module_name = &self.module.os_mod.module_name();
        let filename = alloc::format!("/tmp/{}.mlf", bionic::getpid().0);
        let mut f = bionic::fs::File::create(filename)?;
        f.write_all(&version.to_le_bytes())?;
        f.write_all(&(self.rand.seed().len() as u32).to_le_bytes())?;
        for s in self.rand.seed() {
            f.write_all(&s.to_le_bytes())?;
        }
        f.write_all(&(usize::from(self.module.os_mod.image_base()) as u64).to_le_bytes())?;
        f.write_all(&func_base.absolute_addr().to_le_bytes())?;
        f.write_all(&func_size.to_le_bytes())?;
        f.write_all(
            module_name
                .map(|c| c.to_bytes_with_nul())
                .unwrap_or(&[b'\0']),
        )?;

        for func in &self.functions.funcs {
            f.write_all(&func.undiv_start.absolute_addr().to_le_bytes())?;
            f.write_all(&func.div_start.absolute_addr().to_le_bytes())?;
            f.write_all(&(func.size.unwrap_or(0) as u32).to_le_bytes())?;
        }
        f.write_all(&0usize.to_le_bytes())?;

        Ok(())
    }

    /// Compute all trap relocations            
    fn process_trap_relocations(&mut self) -> BTreeSet<TargetAddr<usize>> {
        self.trap_info.process_relocations(
            self.module,
            self.cache.as_deref_mut(),
            self.mem,
            &mut self.functions,
        )
    }

    /// Runs the entire flow of the diversification process.
    fn run(&mut self, context: &'context SeLinuxContext) -> bool {
        #[cfg(feature = "count_functions")]
        timeit!("count_functions", self.count_functions());

        timeit!("build_functions", self.build_functions());
        timeit!("prepare_functions", self.prepare_functions());
        let shuffle_order = timeit!("shuffle_functions", self.shuffle_functions());
        let exec_code_size = timeit!("layout_code", self.layout_code(&shuffle_order));
        timeit!(
            "shuffle_code",
            self.shuffle_code(&shuffle_order, exec_code_size, context)
        );

        #[cfg(feature = "btree")]
        timeit!("build_function_btree", self.build_function_btree());

        let got: BTreeSet<TargetAddr<usize>> =
            timeit!("process_trap_relocations", self.process_trap_relocations());
        debugln!(1, "GOT Relocations found {}", got.len());

        timeit!("fixup_relocations", self.fixup_relocations(&got));
        timeit!("fixup_exports", self.fixup_exports());

        #[cfg(feature = "write_layouts")]
        timeit!(
            "write_layout_file",
            if let Err(e) = self.write_layout_file() {
                debugln!(1, "ERROR: Unable to write layout file {:?}", e);
            }
        );

        if env::ProcessEhFrame::get() {
            timeit!("process_eh_frame", self.process_eh_frame());
        }

        timeit!("cleanup", self.cleanup());
        timeit!("notify_gdb", self.notify_gdb(&shuffle_order));

        self.in_place
    }
}

struct RngError;

/// Initializes a ChaCha RNG from environment, hardcoded key or random entropy.
#[allow(unused)]
fn init_chacha_rng() -> Result<LfrChaCha, RngError> {
    fn chacha_key(s: &str) -> Result<[u32; 10], RngError> {
        let chacha_key = s
            .split_whitespace()
            // Remove Optional Hex Prefix
            .map(|x| x.strip_prefix("0x").unwrap_or(x))
            // Remove any trailing commas
            .map(|x| x.trim_end_matches(','))
            .map(|x| u32::from_str_radix(x, 16))
            .take(10)
            .collect::<Result<Vec<_>, _>>()
            .map_err(|e| {
                debugln!(1, "Unable to convert to chacha key. {}", s);
                RngError
            })?;

        chacha_key.try_into().map_err(|_| {
            debugln!(1, "Specified chacha key ({}) is not 10 elements", s);
            RngError
        })
    }
    let mut env_key = env::ChaChaKey::get();

    let chacha_key = if let Some(key) = env_key {
        key
    } else {
        use bionic::io::Read;
        let mut random = bionic::fs::File::open("/dev/urandom").map_err(|e| {
            debugln!(1, "Unable to read /dev/urandom to seed RNG");
            RngError
        })?;
        let mut bytes = [0u32; 10];
        random
            .read_exact(bytemuck::cast_slice_mut(&mut bytes))
            .map_err(|e| {
                debugln!(1, "Unable to read /dev/urandom to seed RNG");
                RngError
            })?;
        bytes
    };

    debugln!(1, "Using Key {:x?}", chacha_key);
    LfrChaCha::new(chacha_key).map_err(|_| {
        debugln!(1, "Unable to initialize LfrChaCha");
        RngError
    })
}

/// Initializes a Rand_R RNG from environment, hardcoded key or random entropy.
#[allow(unused)]
fn init_rand_r_rng() -> Result<LfrRandR, RngError> {
    let seed = if let Some(seed) = env::DebugSeed::get() {
        seed
    } else {
        let now = bionic::time::UNIX_EPOCH.elapsed();
        now.as_secs() as u32 ^ now.subsec_nanos()
    };
    Ok(LfrRandR::new(seed))
}

/// Apply rnaomdizations to a module.
fn randomize_module<Rand: LfrRng>(
    module: &Module,
    context: &SeLinuxContext,
    mem: &mut MemRegion<'_>,
    mut cache: Option<&mut Cache>,
    rand: &mut Rand,
    mut gdb: Option<&mut Gdb>,
    in_place: bool,
) -> Result<(), ()> {
    module.for_all_exec_sections(mem, |mem, exec, trap| match trap {
        TrapInfo::TrapV1(trap) => {
            debugln!(3, "Processing TrapV1");
            let mut esp = ExecSectionProcessor::new(
                trap,
                rand,
                gdb.as_deref_mut(),
                cache.as_deref_mut(),
                mem,
                exec,
                module,
                in_place,
            );
            esp.run(context);
        }
        TrapInfo::TrapV2(trap) => {
            debugln!(3, "Processing TrapV2");
            let mut esp = ExecSectionProcessor::new(
                trap,
                rand,
                gdb.as_deref_mut(),
                cache.as_deref_mut(),
                mem,
                exec,
                module,
                in_place,
            );
            esp.run(context);
        }
    })?;
    Ok(())
}

/// Do the full randomization of a given module.        
fn do_randomization<Rand: LfrRng>(
    module: &Module,
    manager: &mut MemState<memstate::Context>,
    rng: &mut Rand,
    gdb: Option<&mut Gdb>,
) -> Result<(), RandoError> {
    fn do_randomization_impl<Rand: LfrRng>(
        module: &Module,
        manager: &mut MemState<memstate::Context>,
        rng: &mut Rand,
        mut gdb: Option<&mut Gdb>,
    ) -> Result<(), RandoError> {
        // TODO: check env var to see if caching should be disabled for sensitive
        // process
        let mut cd: Option<Cache> = if cfg!(feature = "cache_daemon") {
            Some(Cache::new(module.os_mod.image_base()))
        } else {
            None
        };
        if let Some(cd) = &mut cd {
            {
                debugln!(1, "Verifying that randolib has write access to memory prior to loading from cache");
                let (_context, mut manager) = manager.access()?;
                let mut manager = manager.target();
                let mut _mem = manager.regions()?;
                debugln!(1, "Memory mapped successfully");
            }
            match cd.load_from_cache(
                module,
                module.config.as_ref().map(|c| &c.cachedaemon),
                manager,
            ) {
                Err(e) => {
                    debugln!(1, "Error loading from cache {:?}", e);
                }
                Ok(false) => {
                    debugln!(1, "No Cache for module found");
                }
                Ok(true) => {
                    debugln!(1, "Successfully Accessed Cache");
                    let (_context, mut manager) = manager.access()?;
                    let mut manager = manager.target();
                    let mut mem = manager.regions()?;
                    cd.apply_cached_relocs(module, &mut mem);

                    let sections: &[_] = module.info.sections.into();

                    sections.iter().for_each(|sec| {
                        let exec: Range<_> = sec.exec.into();
                        let exec =
                            exec.start.absolute_addr() as u64..exec.end.absolute_addr() as u64;

                        let data = module.gdb_data(exec, None);
                        cd.apply_debug_info(data, gdb.as_deref_mut());
                    });

                    return Ok(());
                }
            }
        }

        let (context, mut manager) = manager.access()?;
        let mut target = manager.target();

        let in_place = true;
        let mut mem = target.regions()?;
        // For every section in the current program...
        randomize_module(
            module,
            context,
            &mut mem,
            cd.as_mut(),
            rng,
            gdb.as_deref_mut(),
            in_place,
        )
        .map_err(
            // Any failure that occurs during `randomize_module` is considered to be unrecoverable
            // and may leave the system in an undefined state.
            |()| RandoError::HardFail,
        )?;

        // FIXME: This does nothing, commenting out for now
        // os::Module::for_all_modules(randomize_module, nullptr);

        // FIXME: we could make .rndtext non-executable here
        if let Some(cd) = &mut cd {
            // If we fail to save to the cache, we don't stop randomizing.
            let saved = cd.save_to_cache(module, &mem).map_err(|e| {
                debugln!(1, "Failed to save cache {:?}", e);
            });
            debugln!(1, "Cache Saved : {}", saved.is_ok());
        }
        // GDB Must be notified AFTER the caching has been performed.
        // Otherwise breakpoints or other modifications that GDB makes will be in the cache.
        if let Some(gdb) = &mut gdb {
            gdb.commit().unwrap();
        }
        Ok(())
    }
    let r = do_randomization_impl(module, manager, rng, gdb);

    let condvar = &core::sync::atomic::AtomicU32::from_mut(unsafe {
        module.info.trampoline_condvar_ptr.as_mut().unwrap()
    });
    condvar.store(1, core::sync::atomic::Ordering::Release);
    bionic::futex_wake(condvar);
    r
}

impl From<MemFail> for RandoError {
    fn from(e: MemFail) -> RandoError {
        RandoError::Mem(e)
    }
}

impl From<ContextError> for RandoError {
    fn from(e: ContextError) -> RandoError {
        RandoError::Context(e)
    }
}

impl From<MemRegionErr> for RandoError {
    fn from(e: MemRegionErr) -> RandoError {
        RandoError::MemRegion(e)
    }
}

#[derive(Debug)]
pub enum RandoError {
    NoRng,
    ExplicitSkip,
    Mem(MemFail),
    MemRegion(MemRegionErr),
    Context(ContextError),
    HardFail,
}

/// Whether the rust implementation should notify GDB.
///
/// GDB should not be communicated with when the old LFR is doing
/// the communication. Having both enabled would add more communication
/// that confuses GDB.
fn gdb_enabled() -> bool {
    !env::GdbDisable::get()
}

#[derive(Debug)]
pub struct RandoNullErr;

pub fn rando_main(mod_info: &ModuleInfo<'_>) -> Result<(), RandoNullErr> {
    timeit! {"RandoMain", {
        debugln!(1, "Hello from Rust RandoLib");
        debugln!(
            1,
            "NOTICE: CacheDaemon support enabled: {}",
            cfg!(feature = "cache_daemon")
        );
        debugln!(10, "{:#x?}", mod_info);

        let mut gdb = Gdb::new(mod_info.orig_entry.absolute_addr() as u64);
        let gdb_enable = gdb_enabled();
        let os_mod = Posix::collect_os_info(mod_info, None);

        let config_path = env::ConfigFile::get();
        let config = crate::config::Config::new(&config_path, true);
        if let Err(e) =  &config {
            debugln!(
                1,
                "Unable to read config file {:?} due to: {}",
                config_path,
                e
            );
        };
        let config = config.ok();
        let module = Module::new(mod_info,&os_mod,  config);

        debugln!(1, "Module@{:x}", os_mod.image_base());
        debugln!(5, "{:#x?}", module);

        match do_rando(&module, &mut gdb, gdb_enable) {
            Ok(()) => {
                debugln!(1, "Finished randomizing");
                Ok(())
            }
            Err(RandoError::HardFail) => {
                debugln!(1, "Randomization failed");
                panic!("Randomization failed");
            }
            Err(e) => {
                debugln!(1, "Unable to randomize: {:?}", e);
                let sections: &[_] = mod_info.sections.into();
                let mut gdb = if gdb_enable { Some(gdb) } else { None };
                sections.iter().for_each(|sec| {
                    if let Some(gdb) = &mut gdb {
                        let exec: Range<_> = sec.exec.into();
                        let exec = exec.start.absolute_addr() as u64..exec.end.absolute_addr() as u64;
                        let data = module.gdb_data(exec, None);
                        let _ = gdb.skip(data).map_err(|_| {
                            debugln!(1, "Failed to send Skip Notification to GDB");
                        });
                    }
                });
                debugln!(1, "Finished NOT randomizing");
                debugln!(3, "Finishing cleanup");
                Ok(())
            }
        }
    }}
}

/// The Rust LFR Entrypoint
fn do_rando(module: &Module, gdb: &mut Gdb, gdb_enable: bool) -> Result<(), RandoError> {
    let mut manager = unsafe { MemState::uninit(module) };

    let mut manager = manager.request_context()?;
    let mut skip_rando = module
        .config
        .as_ref()
        .map(|c| {
            c.skip_rando(
                module.module_name().map(|c| c.to_bytes()),
                manager.context(),
            )
        })
        .unwrap_or(false);

    skip_rando = skip_rando || env::SkipRando::get();

    let mut rng = {
        cfg_if::cfg_if! {
            if #[cfg(feature = "rng_chacha")] {
                init_chacha_rng()
            } else if #[cfg(feature = "rng_rand_r")] {
                init_rand_r_rng()
            } else {
                compile_error!{"No RNG specified"}
            }
        }
    }.map_err(|_| {
        debugln!(1, "WARNING: Random number generator could not be initialized, randomization will not occur!");
        RandoError::NoRng
    })?;

    if skip_rando {
        return Err(RandoError::ExplicitSkip);
    }

    do_randomization(
        module,
        &mut manager,
        &mut rng,
        if gdb_enable { Some(gdb) } else { None },
    )?;

    Ok(())
}

/// Waits for the condvar to become available before returning.
///
/// # Safety
/// Must have exlusive access to `condvar`.
#[allow(unused_unsafe)]
pub unsafe fn trampoline_wait(condvar: *mut u32) {
    let condvar = &core::sync::atomic::AtomicU32::from_mut(unsafe { condvar.as_mut().unwrap() });
    while condvar.load(core::sync::atomic::Ordering::Relaxed) == 0 {
        bionic::futex_wait(condvar);
    }
}

/// The Rust LFR Entrypoint when unloading the library/application.                
pub fn rando_finish(orig_entry: TargetAddr<usize>) {
    debugln!(4, "LFR Finish Start");
    let mut gdb = Gdb::new(orig_entry.absolute_addr() as u64);

    if gdb_enabled() {
        gdb.action(GdbAction::Deregister);
    }
    debugln!(4, "LFR Finish End")
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_function_list() {
        let v = [10..20usize, 20..30, 40..50]
            .into_iter()
            .map(|r| Function {
                undiv_start: TargetAddr::from_raw(r.start),
                size: Some(r.len()),
                div_start: TargetAddr::from_raw(0usize),
                undiv_p2align: 0,
                skip_copy: false,
                from_trap: false,
                typ: FunctionType::Function,
            })
            .collect();

        let mut func_list = FunctionList {
            funcs: v,
            exec_section: TargetAddr::from_raw(0usize)..TargetAddr::from_raw(100usize),
            ..Default::default()
        };
        for addr in 0..100usize {
            let target = TargetAddr::from_raw(addr);
            println!("{:?}", func_list);
            let found = func_list.find_function(target, None).map(|f| f.undiv_start);
            let found2 = func_list
                .funcs
                .iter()
                .find(|f| f.undiv_contains(target))
                .map(|f| f.undiv_start);
            assert_eq!(found, found2);
        }

        #[cfg(feature = "btree")]
        {
            func_list.build_tree();
            for addr in 0..100usize {
                let target = TargetAddr::from_raw(addr);
                let found = func_list.find_function(target, None).map(|f| f.undiv_start);
                let found2 = func_list
                    .funcs
                    .iter()
                    .find(|f| f.undiv_contains(target))
                    .map(|f| f.undiv_start);
                assert_eq!(found, found2);
            }
        }
    }
}
