/* Copyright (c) 2022 RunSafe Security Inc. */

//! Relocation Caching.
//!
//! # How this Works
//!
//! Caching is made up of two parts, this LFR Cache (Client) and the Cache Daemon (Server).
//!
//! ## Cache Daemon
//! We have a cache daemon that is running on the local machine that keeps track of which files
//! have a corresponding cache entry. If it exists, when requested by LFR it will provide a File Descriptor
//! so that we can access it. It also prevents two processes from accessing the cache for the same file
//! simultaneously to maintain cache consistency.
//!
//! ## LFR Cache
//!
//! This all kicks off with a simple request to the Cache Daemon to determine whether an entry exists
//! for the current module. Fairly straightforward.
//!
//! ### Cached Entry Does Not Exist
//!
//! In this case, the cache daemon will create a new cache entry and provide us a reference to it.
//! We will then store all relocations that we see during randomization that map to writable segments within
//! memory that have a corresponding physical location in the original ELF file.
//!
//! When we have collected all the entries and randomization is complete we will then save the modified memory segments
//! to the same physical addresses they originally came from but to the Cached File. At an aligned location after all the
//! written segments we will write the relocations that we have seen. Notifying the cached daemon that we are now done.
//!
//! ### Cached Entry Exists
//!
//! In this case, we will have access to the cached file. This cached file being made up of all writable segments and relocations,
//! we will map in each writable segment into memory, which will include a diversified executable region.
//! After that, we simply re-apply the relocations to account segments that are not found in the cached file.

use crate::debugln;
use crate::env;
use crate::gdb::Gdb;
use crate::gdb::GdbData;
use crate::memregion::MemRegion;
use crate::os::{OsModExt, OsModule};
use crate::randolib::Relocation;
use crate::randolib::{memstate, Module};
use alloc::collections::BTreeMap;
use alloc::{vec, vec::Vec};
use arch::{Architecture, RelocationIdx};
use bionic::fs::File;
use bionic::io::OwnedFd;
use bionic::io::{AsFd, AsRawFd};
use bionic::io::{BufRead, BufReader};
use bionic::mem::{MemBuilder, MemPerms};
use bionic::net::CMsg;
use bionic::path::{Path, PathBuf};
use bionic::phdr::SegmentType;
use bionic::posix::{Posix, OS};
use bytemuck::{Pod, Zeroable};
use core::ops::Range;
use core::ptr::NonNull;
use goblin::elf::program_header as GPhdr;
use lfr_meta::cache::CacheFileMeta;
use lfr_meta::cache::{
    CMsgConnection, CacheDaemonMessage, MessageErr, StructuredMessageConnection,
};
use modinfo::TargetAddr;
use scroll::Pread;

/// Relocation Type representation
type RelocType = u64;

/// A relocation that is almost ready to be cached, it just needs the module
/// offset to be converted into the cacheable form.
pub struct CacheableRelocation {
    /// Type of Relocation
    typ: RelocType,
    /// Original Address
    source: TargetAddr<usize>,
    /// Original Target
    target: TargetAddr<usize>,
}

impl CacheableRelocation {
    /// Create a new CachedRelocation from a Relocation.
    pub fn new(reloc: &Relocation, target: TargetAddr<usize>) -> Self {
        Self {
            typ: reloc.typ().into(),
            source: reloc.get_source_ptr(),
            target,
        }
    }
    fn into_cache(self, image_base: TargetAddr<usize>) -> CachedRelocation {
        CachedRelocation {
            typ: self.typ,
            source_offset: self.source.wrapping_sub(image_base).into(),
            target_offset: self.target.wrapping_sub(image_base).into(),
        }
    }
}

/// A Cached Relocation
///
/// Will be persisted to disk.
#[repr(C)]
#[derive(Copy, Clone, Pod, Zeroable, Debug)]
struct CachedRelocation {
    /// Type of relocation [`goblin::elf::reloc`]
    typ: RelocType,
    /// The original offset.
    source_offset: usize,
    /// The new target.
    target_offset: usize,
}

impl CachedRelocation {
    /// Convert a CacheRelocation into a regular relocation.
    fn into_relocation(self, module: &OsModule) -> Relocation {
        Relocation::new(
            RelocationIdx(self.typ.try_into().unwrap()),
            module.rva2address(self.source_offset),
            None,
            None,
        )
    }
}

/// Find the path of the module from the `/proc/self/maps`.
fn find_path<W>(addr: TargetAddr<usize>, file: W) -> Result<Option<PathBuf>, bionic::io::Error>
where
    W: bionic::io::Read,
{
    use winnow::prelude::*;
    use winnow::{
        ascii::{hex_digit1, newline, space1},
        token::take_till,
    };

    struct Entry<'a> {
        addr: Range<TargetAddr<usize>>,
        path: Option<&'a Path>,
    }
    fn hex_value(input: &mut &[u8]) -> PResult<usize> {
        hex_digit1
            .try_map(core::str::from_utf8)
            .try_map(|out: &str| usize::from_str_radix(out, 16))
            .parse_next(input)
    }
    fn word<'i>(input: &mut &'i [u8]) -> PResult<&'i [u8]> {
        take_till(1.., |c| c == b' ' || c == b'\t')
            .recognize()
            .parse_next(input)
    }
    fn read_line<'i>(input: &mut &'i [u8]) -> PResult<Entry<'i>> {
        let (start, _, end) = (hex_value, "-", hex_value).parse_next(input)?;
        let _ = space1(input)?;

        //Permissions
        let _ = (word, space1).parse_next(input)?;
        //Offset
        let _ = (word, space1).parse_next(input)?;
        //Dev
        let _ = (word, space1).parse_next(input)?;
        //Inode
        let _ = (word, space1).parse_next(input)?;

        let (path, _) = (take_till(0.., |c| c == b'\n'), newline).parse_next(input)?;

        Ok(Entry {
            addr: start.into()..end.into(),
            path: if path.is_empty() {
                None
            } else {
                Some(Path::new(path))
            },
        })
    }
    // Keep a buffer of the file we have read to prevent excessive reads. Will only
    // resize of the buffer is completely full. Will auto rotate values to front if the end
    // is reached.
    let mut bufread = BufReader::new(file);
    let mut buf = vec![];
    loop {
        buf.clear();
        let count = bufread.read_until(b'\n', &mut buf)?;
        if count == 0 {
            break;
        }
        match read_line.parse(&*buf) {
            Ok(entry) if entry.addr.contains(&addr) => {
                return Ok(entry.path.map(|p| p.to_path_buf()));
            }
            Ok(_) => {}
            Err(_) => {
                return Err(bionic::io::Error::Unknown);
            }
        }
    }

    Ok(None)
}

/// Representation fo Cached File
struct CacheFile {
    /// Communicate with the CacheDaemon via this stream.
    stream: DaemonConnection,
    /// The File that is the actual cache.
    cache: File,
    /// The module that is referenced to determine the cache.
    module: File,
}

/// The Actual Cache
pub struct Cache {
    image_base: TargetAddr<usize>,
    /// Set of Relocations
    relocs: Vec<CachedRelocation>,
    /// The Debug Information
    debug: Option<Vec<u8>>,
    /// The Secions which are known to be writable.
    writable_segments: BTreeMap<TargetAddr<usize>, Range<TargetAddr<usize>>>,
    /// Whether this cache infomration is going to be persisted to disk.
    save: Option<CacheFile>,
}

impl Cache {
    pub fn new(image_base: TargetAddr<usize>) -> Self {
        Self {
            image_base,
            relocs: vec![],
            debug: None,
            writable_segments: BTreeMap::default(),
            save: None,
        }
    }
    /// Add a relocation to the cache.
    pub fn add_reloc(&mut self, reloc: Option<CacheableRelocation>) {
        if let Some(cached) = reloc {
            let should_cache = self
                .writable_segments
                .range(..cached.source)
                .next_back()
                .filter(|(_, r)| r.contains(&cached.source))
                .is_some();
            if should_cache {
                let cached = cached.into_cache(self.image_base);
                self.relocs.push(cached);
            }
        }
    }

    /// Add serialized debug information
    pub fn add_debug(&mut self, debug: Vec<u8>) {
        self.debug = Some(debug);
    }

    /// Map in an existing cache file into the the memory space.
    fn map_cached_file(
        &mut self,
        module: &Module,
        // This is unused but we require that no else is modifying memory.
        _mem: &mut memstate::MemState<memstate::Context>,
        f: File,
        meta: &CacheFileMeta,
    ) {
        // Important: we share cache_fd with the daemon and any other clients
        // connecting concurrently. This includes the current file offset, set by
        // lseek, so we can't use lseek+read and friends. Instead we have to file
        // backed memory mappings (with mmap).

        let md = f.metadata();
        let file_size = usize::try_from(md.len()).unwrap();
        debugln!(3, "File size: {}", file_size);
        let elf_hdr_map_size = Posix::PAGE_SIZE;

        let fd: OwnedFd = f.into();
        let elf_hdr: goblin::elf::header::Header;
        let phdr_end;
        {
            // Safety: We are referencing a file that must have an elf header and
            // is not going to change outside of this process.
            let elf_hdr_map = unsafe {
                MemBuilder::default()
                    .with_fd(&fd, 0)
                    .build(elf_hdr_map_size)
                    .unwrap()
            };

            elf_hdr = elf_hdr_map.slice().pread(0).unwrap();

            phdr_end =
                elf_hdr.e_phoff + u64::from(elf_hdr.e_phnum) * u64::from(elf_hdr.e_phentsize);
        }

        // Safety: We are referencing a file that must have phdr and
        // is not going to change outside of this process.
        let elf_hdr_map = unsafe {
            MemBuilder::default()
                .with_fd(&fd, 0)
                .build(phdr_end.try_into().unwrap())
                .unwrap()
        };

        debugln!(3, "Processing {} Phdr Entries", elf_hdr.e_phnum);

        (0..usize::from(elf_hdr.e_phnum))
            .scan(usize::try_from(elf_hdr.e_phoff).unwrap(), |offset, _i| {
                let header: goblin::elf::ProgramHeader = elf_hdr_map.slice().gread(offset).unwrap();
                //debugln!(3, "{:#x?}", header);
                Some(header)
            })
            .filter(|h| h.p_type == GPhdr::PT_LOAD && h.p_flags & GPhdr::PF_W == 0)
            .for_each(|h| {
                let map_start = Posix::page_align(
                    module
                        .os_mod
                        .rva2address_raw(usize::try_from(h.p_vaddr).unwrap()),
                );
                let memsz = usize::try_from(h.p_memsz).unwrap();
                let map_end = Posix::page_end(
                    module
                        .os_mod
                        .rva2address_raw(usize::try_from(h.p_vaddr).unwrap())
                        + memsz,
                );
                let map_offset = Posix::page_align(h.p_offset.try_into().unwrap());
                // Safety: We are referencing a file that is not going to change outside of this process.
                // These addresses are guaranteed to be non-overlapping since they are separate sections.
                unsafe {
                    MemBuilder::default()
                        .with_fd(&fd, map_offset)
                        .with_addr(NonNull::new(map_start as *mut u8).unwrap())
                        .with_exec(MemPerms::from_phdr_flags(h.p_flags).execute())
                        .build(map_end - map_start)
                        .unwrap()
                        .persist();
                }
            });

        if let Some(reloc_cache) = &meta.reloc_list {
            if !reloc_cache.is_empty() {
                // Safety: We are referencing a file that is not going to change outside of this process.
                let relocs = unsafe {
                    MemBuilder::default()
                        .with_fd(&fd, reloc_cache.start)
                        .build(reloc_cache.len())
                        .unwrap()
                };
                let relocs: &[CachedRelocation] = bytemuck::cast_slice(relocs.slice());
                self.relocs.extend(relocs.iter().cloned());
            }
        }
        if let Some(debug_cache) = &meta.debug_info {
            // Safety: We are referencing a file that is not going to change outside of this process.
            let debug = unsafe {
                MemBuilder::default()
                    .with_fd(&fd, debug_cache.start)
                    .build(debug_cache.len())
                    .unwrap()
            };

            self.add_debug(debug.slice().to_vec());
        }
    }

    /// Apply all the relcoation found in the cache file.
    pub fn apply_cached_relocs(&self, module: &Module, mem: &mut MemRegion) {
        debugln!(5, "Applying {} relocs from cache", self.relocs.len());

        self.relocs.iter().for_each(|r| {
            let reloc = r.into_relocation(module.os_mod);
            let target = module.os_mod.rva2address(r.target_offset);
            debugln!(9, "Applying cached reloc {:x?} -> {:x?}", reloc, target);
            arch::TargetArch::set_target_ptr(&reloc, mem, target)
                .or_else(crate::randolib::ignore_unimplemented_reloc)
                .unwrap();
        });
    }

    /// Apply any available debug info
    pub fn apply_debug_info(&self, data: GdbData, gdb: Option<&mut Gdb>) {
        if let (Some(gdb), Some(debug)) = (gdb, &self.debug) {
            debugln!(5, "Restoring {} bytes debug info from cache", debug.len());
            gdb.commit_cached(data, debug).unwrap();
        }
    }

    /// Internal method to save the memory regions/relocs to cache file on disk.
    fn save_impl(
        &mut self,
        save: &mut CacheFile,
        module: &Module,
        mem: &MemRegion,
    ) -> Result<CacheFileMeta, bionic::io::Error> {
        let file_size = mem
            .slices()
            .flat_map(|s| s.phys_range())
            .map(|r| r.end)
            .max()
            .unwrap_or(0);
        save.cache.set_len(file_size.try_into().unwrap())?;

        {
            // Safety: We are referencing a file that is not going to change outside of this process.
            let mut dest_mapping = unsafe {
                MemBuilder::default()
                    .with_fd(&save.cache.as_fd(), 0)
                    .with_shared(true)
                    .build_mut(file_size)
                    .unwrap()
            };

            let copied_elf_header = mem
                .slices()
                .any(|slice| slice.addr() == self.image_base.absolute_addr());

            mem.slices()
                .filter(|s| s.phys_range().is_some())
                .for_each(|slice| {
                    dest_mapping.slice_mut()[slice.phys_range().unwrap()]
                        .copy_from_slice(slice.slice())
                });

            if !copied_elf_header {
                // We need to copy the ELF header from the source file, since it wasn't mapped in memory
                use bionic::io::Read;
                #[cfg(target_pointer_width = "32")]
                let elf_header_len = goblin::elf::header::header32::SIZEOF_EHDR;
                #[cfg(target_pointer_width = "64")]
                let elf_header_len = goblin::elf::header::header64::SIZEOF_EHDR;
                save.module
                    .read_exact(&mut dest_mapping.slice_mut()[..elf_header_len])?;
            }
        }

        // Remap cached pages as MAP_PRIVATE so we can't modify the shared cache
        // file later, and save some memory by discarding the old randomized pages.
        debugln!(7, "CacheDaemon: Remapping Private");
        module
            .os_mod
            .phdr_info
            .iter()
            .filter(|phdr| {
                phdr.segment_type == SegmentType::Load && (phdr.flags & GPhdr::PF_W) == 0
            })
            .for_each(|phdr| {
                // This seems really sketchy...
                //I think I should be setting the segments in the outer scope rather than here.
                let map_start = Posix::page_align(module.os_mod.rva2address_raw(phdr.vaddr));
                let map_end =
                    Posix::page_end(module.os_mod.rva2address_raw(phdr.vaddr + phdr.memsz));
                debugln!(
                    7,
                    "CacheDaemon: Remapping to private {:?}:{:x?} {:x?}-{:x?} => {:x?}-{:x?}",
                    phdr.segment_type,
                    phdr.flags,
                    phdr.vaddr,
                    phdr.vaddr + phdr.memsz,
                    map_start,
                    map_end
                );
                // Safety: We are referencing a file that is not going to change outside of this process.
                // These address ranges are guaranteed to be non-overlapping ELF sections.
                unsafe {
                    MemBuilder::default()
                        .with_addr(NonNull::new(map_start as *mut _).unwrap())
                        .with_fd(&save.cache.as_fd(), Posix::page_align(phdr.offset))
                        .with_exec(MemPerms::from_phdr_flags(phdr.flags).execute())
                        .build(map_end - map_start)
                        .unwrap()
                        .persist();
                }
            });

        fn save_page_aligned_data(
            save: &mut CacheFile,
            name: &str,
            file_size: usize,
            bytes: Option<&[u8]>,
        ) -> (usize, Option<Range<usize>>) {
            let Some(bytes) = bytes else {
                return (file_size, None);
            };
            let page_begin = if Posix::page_align(file_size) == file_size {
                file_size
            } else {
                Posix::next_page_align(file_size)
            };
            debugln!(
                3,
                "Saving {} bytes of {} to cache @{:x}[{}]",
                bytes.len(),
                name,
                page_begin,
                bytes.len()
            );
            let new_file_size = Posix::next_page_align(page_begin + bytes.len());
            save.cache
                .set_len(new_file_size.try_into().unwrap())
                .unwrap();

            // Safety: We are referencing a file that is not going to change outside of this process.
            // These regions within the file do not exist prior this function.
            let mut target = unsafe {
                MemBuilder::default()
                    .with_fd(&save.cache.as_fd(), page_begin)
                    .with_shared(true)
                    .build_mut(bytes.len())
                    .unwrap()
            };
            target.slice_mut().copy_from_slice(bytes);
            (new_file_size, Some(page_begin..page_begin + bytes.len()))
        }

        let reloc_bytes: &[u8] = bytemuck::cast_slice(&self.relocs);
        let reloc_bytes = if reloc_bytes.is_empty() {
            None
        } else {
            Some(reloc_bytes)
        };
        let (file_size, reloc_list) =
            save_page_aligned_data(save, "relocs", file_size, reloc_bytes);
        let (file_size, debug_info) =
            save_page_aligned_data(save, "debug info", file_size, self.debug.as_deref());

        debugln!(3, "Reported File size: {:x?}", file_size);
        Ok(CacheFileMeta {
            reloc_list,
            debug_info,
        })
    }

    /// Save this module to the cache.
    pub fn save_to_cache(
        &mut self,
        module: &Module,
        mem: &MemRegion,
    ) -> Result<(), bionic::io::Error> {
        fn msg_err(m: MessageErr<bionic::io::Error>) -> bionic::io::Error {
            match m {
                MessageErr::IoError(io) => io,
                _ => bionic::io::Error::Unknown,
            }
        }
        let mut save = if let Some(save) = self.save.take() {
            save
        } else {
            debugln!(1, "No Cache to Save: Skipping");
            return Ok(());
        };
        match self.save_impl(&mut save, module, mem) {
            Ok(meta) => {
                save.stream
                    .send_item(CacheDaemonMessage::InitSuccessful { meta })
                    .map_err(|e| {
                        debugln!(
                            1,
                            "Failed to notify cache daemon that randomization is complete. {:?}",
                            e
                        );
                        msg_err(e)
                    })?;
            }
            Err(e) => {
                debugln!(1, "Failed to save to cache. {:?}", e);

                save.stream
                    .send_item(CacheDaemonMessage::InitFailed)
                    .map_err(|e| {
                        debugln!(
                            1,
                            "Failed to notify cache daemon that randomization failed : {:?}",
                            e
                        );
                        msg_err(e)
                    })?;
            }
        }
        Ok(())
    }

    /// Load this module from the cache.
    pub fn load_from_cache(
        &mut self,
        module: &Module,
        config: Option<&crate::config::CacheDaemon>,
        mem: &mut memstate::MemState<memstate::Context>,
    ) -> Result<bool, bionic::io::Error> {
        let addr = module.info.got_start;
        debugln!(1, "Attempting to load from Cache. Looking for {:x?}", addr);

        let file = File::open("/proc/self/maps")?;
        let path = match find_path(addr, file) {
            Ok(Some(path)) => path,
            Ok(None) => {
                debugln!(1, "Unable to determine module path from /proc/self/maps");
                return Err(bionic::io::Error::Unknown);
            }
            Err(e) => {
                debugln!(1, "Parse error for /proc/self/maps");
                return Err(e);
            }
        };
        debugln!(1, "Found module path: {}", path);

        let module_fd = File::open(path)?;

        let key = match env::CacheKey::cache_key() {
            Ok(key) => key,
            Err(_) => {
                debugln!(1, "Skipping caching due to cache key");
                return Ok(false);
            }
        };
        let uid = bionic::getuid();

        let socket_name = if let Some(path) = env::CacheDaemonSocket::get() {
            debugln!(
                1,
                "Using socket path from environment variable {} => {}",
                env::CacheDaemonSocket::name(),
                path
            );
            path
        } else if let Some(path) = config
            .as_ref()
            .and_then(|config| {
                let uid = alloc::format!("{}", uid.0);
                config.uids.get(&uid)
            })
            .cloned()
        {
            debugln!(
                1,
                "Using socket path from configuration for uid {}, {}",
                uid.0,
                path
            );

            path
        } else {
            debugln!(
                1,
                "Using default socket path {}",
                lfr_meta::cache::LFR_CACHE_SOCKET
            );

            Path::new(lfr_meta::cache::LFR_CACHE_SOCKET).to_path_buf()
        };

        let mut socket = bionic::net::UnixStream::connect(&socket_name).map_err(|e| {
            debugln!(
                1,
                "Unable to connect to socket {:?}. Is CacheDaemon running?",
                socket_name
            );
            e
        })?;
        socket.set_nonblocking(true)?;
        let lfr_cache_timeout = Some(core::time::Duration::from_secs(
            option_env!("LFR_CACHE_TIMEOUT")
                .unwrap_or("5")
                .parse()
                .expect("Invalid Timeout"),
        ));
        socket.set_read_timeout(lfr_cache_timeout)?;
        socket.set_write_timeout(lfr_cache_timeout)?;

        let cred = socket.cred()?;

        if uid != cred.uid {
            debugln!(
                1,
                "Remote socket uid does not match current uid {:?} != {:?}",
                cred.uid,
                uid,
            );
            return Err(bionic::io::Error::InvalidData);
        }
        let mut dc = DaemonConnection { stream: socket };

        dc.send_item(CacheDaemonMessage::Request(module_fd.as_raw_fd(), key))
            .map_err(|e| {
                debugln!(1, "Sending Request for cached module failed {:?}", e);
                bionic::io::Error::InvalidData
            })?;
        let msg = dc
            .receive_item()
            .map_err(|e| {
                debugln!(
                    1,
                    "Failed to receive valid response from cache daemon {:?}",
                    e
                );
                bionic::io::Error::InvalidData
            })
            .map(|msg| {
                let msg = msg.map_fd(|fd| unsafe { OwnedFd::from_raw_fd(fd) });
                debugln!(3, "response: {:?}", msg);
                msg
            })?;

        let mapping_successful = match msg {
            CacheDaemonMessage::NewFile(cached_fd) => {
                debugln!(1, "Randomizing new module for cache daemon");
                self.writable_segments = module
                    .os_mod
                    .phdr_info
                    .iter()
                    .filter(|phdr| {
                        phdr.segment_type == SegmentType::Load && (phdr.flags & GPhdr::PF_W) != 0
                    })
                    .map(|phdr| {
                        // This seems really sketchy...
                        //I think I should be setting the segments in the outer scope rather than here.
                        let map_start =
                            Posix::page_align(module.os_mod.rva2address_raw(phdr.vaddr));
                        let map_end =
                            Posix::page_end(module.os_mod.rva2address_raw(phdr.vaddr + phdr.memsz));
                        (
                            TargetAddr::from(map_start),
                            TargetAddr::from(map_start)..TargetAddr::from(map_end),
                        )
                    })
                    .collect();

                self.save = Some(CacheFile {
                    cache: cached_fd.into(),
                    module: module_fd,
                    stream: dc,
                });
                false
            }
            CacheDaemonMessage::FoundFile {
                fd: cached_fd,
                meta,
            } => {
                debugln!(1, "Received cached module from cache daemon");
                self.map_cached_file(module, mem, cached_fd.into(), &meta);
                true
            }
            CacheDaemonMessage::NoFile => {
                debugln!(1, "Cache Daemon did not return a module");
                false
            }
            response => {
                debugln!(
                    1,
                    "Unknown/unexpected or malformed response from cache daemon {:?}",
                    response
                );
                false
            }
        };
        Ok(mapping_successful)
    }
}

/// The Connection to the CacheDaemon
///
/// We build this abstraction because there is another protocol layer on top of a normal socket
/// that allows us to convert File Descriptors into our process so that we can access them.
/// <https://man7.org/linux/man-pages/man3/cmsg.3.html>

struct DaemonConnection {
    /// The underlying socket to the CacheDaemon
    stream: bionic::net::UnixStream,
}

impl CMsgConnection for DaemonConnection {
    type IoError = bionic::io::Error;
    type RawFd = bionic::io::RawFd;

    fn sendmsg(&mut self, msg: &libc::msghdr, flags: libc::c_int) -> Result<usize, Self::IoError> {
        self.stream.sendmsg(msg, flags)
    }
    fn recvmsg(
        &mut self,
        msg: &mut libc::msghdr,
        flags: libc::c_int,
    ) -> Result<usize, Self::IoError> {
        self.stream.recvmsg(msg, flags)
    }
}

impl StructuredMessageConnection for DaemonConnection {
    type Message = CacheDaemonMessage<i32>;
}

#[cfg(test)]
mod test {
    use super::*;
    #[cfg(target_pointer_width = "64")]
    #[test]
    fn test_proc_map_parsing() {
        let proc_map =
            "55af08bad000-55af08baf000 r--p 00000000 103:05 38404671                  /usr/bin/cat\n\
             55af08baf000-55af08bb3000 r-xp 00002000 103:05 38404671                  /usr/bin/cat\n\
             55af08bb3000-55af08bb5000 r--p 00006000 103:05 38404671                  /usr/bin/cat\n\
             7f2ad7931000-7f2ad7959000 r--p 00000000 103:05 38404157                  /usr/lib/x86_64-linux-gnu/libc.so.6\n\
             7f2ad7959000-7f2ad7aee000 r-xp 00028000 103:05 38404157                  /usr/lib/x86_64-linux-gnu/libc.so.6\n\
             7f2ad7aee000-7f2ad7b46000 r--p 001bd000 103:05 38404157                  /usr/lib/x86_64-linux-gnu/libc.so.6\n\
             7f2ad7b9f000-7f2ad7baa000 r--p 0002c000 103:05 38404153                  /usr/lib/x86_64-linux-gnu/ld-linux-x86-64.so.2\n\
             7f47293fb000-7f47293fd000 rw-p 00000000 00:00 0 \n\
             7f47293fd000-7f4729400000 rw-p 00000000 00:00 0 \n\
             7f2ad7bab000-7f2ad7bad000 r--p 00037000 103:05 38404153                  /usr/lib/x86_64-linux-gnu/ld-linux-x86-64.so.2\n\
             7f2ad7bad000-7f2ad7baf000 rw-p 00039000 103:05 38404153                  /usr/lib/x86_64-linux-gnu/ld-linux-x86-64.so.2\n\
             7ffe968a8000-7ffe968c9000 rw-p 00000000 00:00 0                          [stack]\n";

        let cat = Path::new("/usr/bin/cat").to_path_buf();
        let libc = Path::new("/usr/lib/x86_64-linux-gnu/libc.so.6").to_path_buf();
        let ld = Path::new("/usr/lib/x86_64-linux-gnu/ld-linux-x86-64.so.2").to_path_buf();
        let stack = Path::new("[stack]").to_path_buf();
        let test: [(usize, _); 8] = [
            (0x55af08bad000, Some(&cat)),
            (0x55af08bad001, Some(&cat)),
            (0x55af08bb4999, Some(&cat)),
            (0x7f47293fb000, None),
            (0x55af08bb5000, None),
            (0x7f2ad7959010, Some(&libc)),
            (0x7f2ad7bab000, Some(&ld)),
            (0x7ffe968c8999, Some(&stack)),
        ];

        for (addr, target) in test {
            println!("{:x?}", addr);
            assert_eq!(
                find_path(TargetAddr::from_raw(addr), proc_map.as_bytes())
                    .unwrap()
                    .as_ref(),
                target
            );
        }
    }
}
