/* Copyright (c) 2022 RunSafe Security Inc. */

//! Interface for communicating randomization to GDB.
use crate::log::debugln;
use crate::os::cache::Cache;
use alloc::{vec, vec::Vec};
use core::ffi::CStr;
use core::ops::Range;
use lfr_meta::mlf::Mlf;
use lfr_meta::symbols::{SymbolEntryV1, Trim};

///  GDB Communication Channel.
pub struct Gdb {
    /// Which functions have been trimmed, which cannot be determined during runtime without referencing on-disk binary.
    trim: Vec<Trim>,
    /// The key that GDB will use as a unique identifier. Usually the base address.
    key: u64,
}

/// Data provided to GDB.
pub struct GdbData<'a> {
    /// Executable Region
    pub exec_region: Range<u64>,
    /// TextRamp Region
    pub textramp_region: Range<u64>,
    /// Start of memory where file has been mapped.
    pub map_base: Option<u64>,
    /// Exception Frame Handler Addresss
    pub eh_frame_hdr: u64,
    /// Global Offset
    pub got: u64,
    /// Module path
    /// (stored in bytes for compatibility, would prefer PathBuf but not available in `no_std`)
    pub module_path: Option<&'a CStr>,
}

/// Avilable GDB Actions to notify GDB exactly what is happening in LFR>
pub enum GdbAction {
    /// Deregister the current randomization (at LFR finish)
    Deregister,
    /// Clear the Pseudo Symbols, so that randomization can occur without any breakpoints contained within.
    ClearPseudo,
    /// Not doing anything, effectively a "reset" signal to GDB.
    Nop,
}

#[derive(Debug)]
pub struct GdbSkipError;
#[derive(Debug)]
pub struct GdbError;

impl Gdb {
    /// Create new GDB interface with key.
    pub fn new(key: u64) -> Self {
        let mut gdb = Self { trim: vec![], key };
        gdb.action(GdbAction::ClearPseudo);
        gdb
    }
    /// Provide access to all trimmed functions.
    pub fn trim(&mut self) -> &mut Vec<Trim> {
        &mut self.trim
    }

    /// Notify GDB that we are skipping randomization.
    pub fn skip(&mut self, data: GdbData) -> Result<(), GdbSkipError> {
        let entry = SymbolEntryV1 {
            seed: None,
            key: self.key,
            exec_region: data.exec_region,
            textramp_region: data.textramp_region,
            module_path: data.module_path.map(|p| p.to_bytes().to_vec()),
            trim: core::mem::take(&mut self.trim),
            got: data.got,
            eh_frame_processed: false,
            eh_frame_hdr: data.eh_frame_hdr,
            mlf: None,
            map_base: data.map_base,
        };
        // Skipping randomization appears to cause the same library to repeatedly be entered
        // causing multiple entries to be added to the metadata. This prevents that.
        if crate::preserve_mem(entry).expect("Unable to preserve shuffle metadata") {
            lfr_jit::add_jit_entry(self.key);
        } else {
            lfr_jit::nop();
        }
        Ok(())
    }
    /// Notify GDB that randomization has occured.
    pub fn commit_cached(&mut self, data: GdbData, debug_info: &[u8]) -> Result<(), GdbError> {
        let entry: SymbolEntryV1 = ciborium::from_reader::<_, &[u8]>(debug_info).map_err(|_| {
            debugln!(1, "Unable to deserialize cached symbol data");
            GdbError
        })?;
        // These entries must be overridden from the cached debug information
        // to account for ASLR and other values that change from run to run.
        let entry = SymbolEntryV1 {
            key: self.key,
            exec_region: data.exec_region,
            textramp_region: data.textramp_region,
            got: data.got,
            map_base: data.map_base,
            eh_frame_hdr: data.eh_frame_hdr,
            module_path: data.module_path.map(|p| p.to_bytes().to_vec()),
            mlf: entry.mlf.map(|mlf| Mlf {
                funcs: mlf
                    .funcs
                    .iter()
                    .map(|f| lfr_meta::mlf::Func {
                        div_start: f.div_start - mlf.file_base.unwrap_or(0)
                            + data.map_base.unwrap_or(0),
                        ..*f
                    })
                    .collect(),
                ..mlf
            }),
            ..entry
        };
        crate::preserve_mem(entry).expect("Unable to preserve shuffle metadata");
        lfr_jit::add_jit_entry(self.key);
        Ok(())
    }
    /// Notify GDB that randomization has occured.
    pub fn notify<Rand>(
        &mut self,
        mlf: Mlf,
        data: GdbData,
        rand: &mut Rand,
        cache: Option<&mut Cache>,
        eh_frame_processed: bool,
    ) -> Result<(), GdbError>
    where
        Rand: lfr_meta::rand::LfrRng,
    {
        let entry = SymbolEntryV1 {
            seed: Some(rand.seed().to_vec()),
            key: self.key,
            exec_region: data.exec_region,
            textramp_region: data.textramp_region,
            module_path: data.module_path.map(|p| p.to_bytes().to_vec()),
            trim: core::mem::take(&mut self.trim),
            got: data.got,
            eh_frame_processed,
            eh_frame_hdr: data.eh_frame_hdr,
            mlf: if cfg!(debug_assertions) {
                Some(mlf)
            } else {
                None
            },
            map_base: data.map_base,
        };
        if let Some(cache) = cache {
            let mut v = vec![];
            ciborium::into_writer(&entry, &mut v).unwrap();
            cache.add_debug(v);
        }
        crate::preserve_mem(entry).expect("Unable to preserve shuffle metadata");
        Ok(())
    }

    /// Notify GDB that randomization has occured.
    pub fn commit(&mut self) -> Result<(), GdbError> {
        lfr_jit::add_jit_entry(self.key);
        Ok(())
    }
    /// Reset the internal metadata (for debugging)
    pub fn reset() {
        *crate::SYMBOL_MEM.lock() = crate::Metadata::default();
    }
    /// Execute the given action immediately by notifying GDB.
    pub fn action(&mut self, action: GdbAction) {
        match action {
            GdbAction::ClearPseudo => {
                lfr_jit::clear_pseudo(self.key);
            }
            GdbAction::Deregister => {
                lfr_jit::remove_jit_entry(self.key);
            }
            GdbAction::Nop => {
                lfr_jit::nop();
            }
        }
    }
}

impl Drop for Gdb {
    fn drop(&mut self) {
        self.action(GdbAction::Nop);
    }
}
