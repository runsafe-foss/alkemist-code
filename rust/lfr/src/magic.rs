// Copyright (c) 2022 RunSafe Security Inc.

//! "Magic" methods and associated hacks relating to `no_std`.
//!
//! This is a collection of all materials required to build in `![no_std]` without
//! any issues. Including but not limited to: panic handling, memory allocators,
//! and other symbols that don't exist.
//!
//! The symbols we add don't appear to be used at all, it's not clear why the dependency exists.

use bionic::eprintln;
use core::panic::PanicInfo;

/// An Aborting Panic Handler
#[panic_handler]
fn panic(panic_info: &PanicInfo<'_>) -> ! {
    eprintln!("panic occurred: {}", panic_info);
    core::intrinsics::abort()
}

/// Global Mmap Memory Allocator.
#[global_allocator]
static GLOBAL: bionic::alloc::MmapAllocator = bionic::alloc::MmapAllocator;

/// Stub for EH Personality
#[lang = "eh_personality"]
fn rust_eh_personality() {}
