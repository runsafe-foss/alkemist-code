/* Copyright (c) 2022 RunSafe Security Inc. */

//! Secure Environment Variable reading
use crate::log::debugln;
use alloc::str::FromStr;
use alloc::string::String;
use alloc::{vec, vec::Vec};
use bionic::path::{Path, PathBuf};
struct ParseErr;

enum Secure {
    Default(Option<String>),
    Allow,
}

impl Default for Secure {
    fn default() -> Self {
        Self::Default(None)
    }
}
// Whether or not we should cache if the env is present.
enum UseCache {
    /// We should use the cache and store the value if True.
    CacheValue(bool),
    /// We should not use cache.
    Cancel,
}

pub struct SkipCache;

macro_rules! default {
    () => {
        Default::default()
    };
    ($default:expr) => {
        $default
    };
}

/// A helper trait to keep track of the type of a given Environment variable.
trait EnvType {
    type Output;
}

/// Determine whether the trait is cacheable or not.
trait Cacheable {
    fn cacheable(&self) -> UseCache;
}

/// Provides useful function `cache_key` to produce a key or stop caching.
pub struct CacheKey;

macro_rules! cached_values {
    ($($cache:expr => { $($n:ident),* $(,)? } ),* $(,)?) => {
        $(
            $(
                impl Cacheable for $n {
                    fn cacheable(&self) -> UseCache {
                        $cache(&self.0)
                    }
                }
            )*
        )*
    }
}

/// Provide a parsing function for a given Environment Variable
trait Parseable: EnvType {
    fn parse(env: Option<&str>) -> Result<<Self as EnvType>::Output, ParseErr>;
}

macro_rules! parse_values {
    ($($parse:expr => { $($n:ident),* $(,)? } ),* $(,)?) => {
        $(
            $(
                impl Parseable for $n {
                    fn parse(env: Option<&str>) -> Result<<Self as EnvType>::Output, ParseErr> {
                        $parse(env)
                    }
                }
            )*
        )*
    }
}

/// Create a a [`Env`] based on what env vars should be safely accessible in a secure build.
macro_rules! secure_env {
    ( $(($enum:ident($val:ty $( = $default:expr)?) $(: $secure:expr)? => $($str:literal)|+)),* ) => {
        /// All known Environment variables used by LFR.
        ///
        /// They each are classified by whether they are "secure" or not.
        /// Prevents access during secure builds.
        $(
            pub struct $enum($val);

            impl Default for $enum {
                fn default() -> Self {
                    Self(default!($($default)?))
                }
            }

            impl $enum {
                pub fn new() -> Self {
                    Self(Self::get())
                }
                pub fn get() -> $val {
                    <Self as SecureEnv>::get()
                }
                pub fn name() -> &'static str {
                    <Self as SecureEnv>::name()
                }
            }

            impl EnvType for $enum {
                type Output = $val;
            }

            impl SecureEnv for $enum {
                fn names() -> &'static [&'static str] {
                    &[
                        $($str,)*
                    ]
                }
                fn secure() -> Secure {
                    default!($($secure)?)
                }

                fn get() -> <Self as EnvType>::Output {
                    let raw = Self::raw_get();
                    let parsed = Self::parse(raw.as_deref());
                    match (raw, &parsed) {
                        (Some(_), Err(_)) if Self::name() == "LFR_debug_level" => {
                            // Cannot report log level misparse, otherwise we try to read the log level to report it...
                        }
                        (Some(raw), Err(_)) => {
                            debugln!(1, "WARN: Unable to parse env {} => {:?}", Self::name(), raw);
                        }
                        _ => {},
                    }
                    parsed.unwrap_or_else(|_| $enum::default().0)
                }

            }

            impl core::fmt::Display for $enum {
                fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> Result<(), core::fmt::Error> {
                    write!(f, "{}", Self::name())
                }
            }
        )*


        impl CacheKey {
            pub fn cache_key() -> Result<Vec<u8>, SkipCache> {
                let mut v = vec![];
                    $(
                        let env = $enum::new();
                        match env.cacheable() {
                            UseCache::Cancel => {
                                debugln!(1, "Skipping Cache due to Env {:?} => {:?}", $enum::name(), env.0);
                                return Err(SkipCache);
                            }
                            UseCache::CacheValue(false) => {},
                            UseCache::CacheValue(true) => {
                                debugln!(1, "Adding ENV Value to Key {:?} => {:?}", $enum::name(), env.0);
                                ciborium::into_writer(&($enum::name(), &env.0), &mut v).map_err(|e| {
                                    debugln!(1, "Unable to create cache key {:?}", e);
                                    SkipCache
                                })?;
                            }
                        }
                    )*
                debugln!(1, "Length of cache key {:?}", v.len());
                Ok(v)
            }
        }
    }
}

trait SecureEnv: Default + Cacheable + EnvType {
    fn raw_get() -> Option<String> {
        #[cfg(feature = "lfr_secure")]
        match Self::secure() {
            Secure::Default(v) => return v,
            Secure::Allow => {}
        }
        Self::names().iter().find_map(|n| {
            #[allow(deprecated)]
            bionic::env::var(n).ok()
        })
    }
    fn get() -> <Self as EnvType>::Output;
    fn name() -> &'static str {
        Self::names()[0]
    }
    fn names() -> &'static [&'static str];
    fn secure() -> Secure;
}

fn parse_is_present(v: Option<&str>) -> Result<bool, ParseErr> {
    Ok(v.is_some())
}

fn parse_num_specified<V>(v: Option<&str>) -> Result<Option<V>, ParseErr>
where
    V: FromStr,
{
    v.map(|n| n.parse().map_err(|_| ParseErr)).transpose()
}

fn parse_number<V>(v: Option<&str>) -> Result<V, ParseErr>
where
    V: FromStr,
{
    let s = v.ok_or(ParseErr)?;
    let num = s.parse().map_err(|_| ParseErr)?;
    Ok(num)
}

fn default_config_path() -> PathBuf {
    let sysconfdir = "etc/alkemist/lfr/";
    if cfg!(target_os = "android") {
        Path::new("/").join(sysconfdir).join("lfr.toml")
    } else {
        Path::new(cmake::CMAKE_INSTALL_PREFIX)
            .join(sysconfdir)
            .join("lfr.toml")
    }
}

fn parse_optional_path(v: Option<&str>) -> Result<Option<PathBuf>, ParseErr> {
    v.map(|p| Some(Path::new(p).to_path_buf())).ok_or(ParseErr)
}

fn parse_path(v: Option<&str>) -> Result<PathBuf, ParseErr> {
    v.map(|p| Path::new(p).to_path_buf()).ok_or(ParseErr)
}

fn always_cache_novalue<T>(_v: T) -> UseCache {
    UseCache::CacheValue(false)
}

fn always_cache_value<T>(_v: T) -> UseCache {
    UseCache::CacheValue(true)
}

fn skip_if_set(v: &bool) -> UseCache {
    if *v {
        UseCache::Cancel
    } else {
        UseCache::CacheValue(false)
    }
}

fn skip_if_some<T>(v: &Option<T>) -> UseCache {
    if v.is_some() {
        UseCache::Cancel
    } else {
        UseCache::CacheValue(false)
    }
}

secure_env! {
    // Does not shuffle the functions.
    (SkipShuffle(bool) => "LFR_skip_shuffle" | "LFR_SKIP_SHUFFLE"),
    // Reverses the order of the functions (only when unshuffled)
    (Reverse(bool) => "LFR_REVERSE"),
    // Provide the index to start layout such that functions prior to the index are at the same address
    // and those after have the layout algorithm applied (may result in new positions).
    (StartLayout(Option<usize>) => "LFR_START_LAYOUT"),
    // Skips randomization.
    (SkipRando(bool) => "LFR_skip_rando" | "LFR_SKIP_RANDO"),
    // The level of debug output that is to be provided.
    (DebugLevel(u32) => "LFR_debug_level" | "LFR_DEBUG_LEVEL"),
    // Whether to apply LFR to the .eh_frame section (may impact debuggability).
    (ProcessEhFrame(bool) : Secure::Allow => "LFR_process_eh_frame" | "LFR_PROCESS_EH_FRAME"),
    // Forces sync after each write syscall (DEPRECATED)
    (ForceSync(bool) => "LFR_FORCE_SYNC"),
    // Provide a cha cha key to be used for randomization, 10 values whitespace delimited in hex (no 0x prefix).
    // Example LFR_CHACHA_KEY="0 1 2 3 4 5 6 7 8 9"
    (ChaChaKey(Option<[u32;10]>) => "LFR_CHACHA_KEY"),
    // Provide a decimal number rng seed instead of using system entropy.
    (DebugSeed(Option<u32>) => "LFR_DEBUG_SEED"),
    // Provide an alternate configuration file rather than the system provided one.
    (ConfigFile(PathBuf = default_config_path()) => "ALKEMIST_CONFIG_FILE"),
    // Disable all LFR GDB metadata collection and pretend GDB does not exist.
    (GdbDisable(bool) : Secure::Allow => "LFR_GDB_DISABLE"),
    // Write a layout file to the specified location.
    (WriteLayoutFile(bool) => "LFR_write_layout_file" | "LFR_WRITE_LAYOUT_FILE"),
    // Cache Daemon Socket Location
    (CacheDaemonSocket(Option<PathBuf>) => "LFR_CACHEDAEMON_SOCK")
}

// Some of the environment variables influence the cache in different ways.
// We need to keep track of certain values so that we can decide which cache entry
// to use or even whether to cache at all.
cached_values! {
    always_cache_value => {
        // GDB influences cache by storing debug information
        GdbDisable,
        // The EH Frame being processed or unprocessed changes how GDB interacts with the process.
        ProcessEhFrame,
        // If we have reversed the order, we have influenced the randomization.
        Reverse,
        // If we have skip the shuffle, we have influenced the randomization.
        SkipShuffle,
        // If we have adjusted the starting offset, we have influenced the randomization.
        StartLayout,
    },
    // These do not impact randomization and can be safely ignored.
    always_cache_novalue => {
        CacheDaemonSocket,
        ConfigFile,
        ForceSync,
        WriteLayoutFile,
        DebugLevel,
    },
    skip_if_set => {
        // There is no point in Caching at all of SkipRando is set.
        SkipRando,
    },
    skip_if_some => {
        // We probably shouldn't cache specified values of seeds.
        ChaChaKey,
        DebugSeed
    }
}

parse_values! {
    parse_is_present => {
        ForceSync,
        GdbDisable,
        ProcessEhFrame,
        Reverse,
        SkipRando,
        WriteLayoutFile,
        SkipShuffle,
    },
    parse_num_specified => {
        DebugSeed,
        StartLayout,
    },
    parse_number => {
        DebugLevel
    },
    parse_optional_path => {
        CacheDaemonSocket,
    },
    parse_path => {
        ConfigFile,
    }
}

impl Parseable for ChaChaKey {
    fn parse(v: Option<&str>) -> Result<Option<[u32; 10]>, ParseErr> {
        fn chacha_key(s: &str) -> Result<[u32; 10], ParseErr> {
            let chacha_key = s
                .split_whitespace()
                // Remove Optional Hex Prefix
                .map(|x| x.strip_prefix("0x").unwrap_or(x))
                // Remove any trailing commas
                .map(|x| x.trim_end_matches(','))
                .map(|x| u32::from_str_radix(x, 16))
                .take(10)
                .collect::<Result<Vec<_>, _>>()
                .map_err(|_| {
                    debugln!(1, "Unable to convert to chacha key. {}", s);
                    ParseErr
                })?;

            chacha_key.try_into().map_err(|_| {
                debugln!(1, "Specified chacha key ({}) is not 10 elements", s);
                ParseErr
            })
        }
        match option_env!("SR_DEBUG_SEED") {
            Some("env") => v.as_ref().map(|k| chacha_key(k)).transpose(),
            Some("") => Ok(None),
            Some(key) => chacha_key(key).map(Some),
            None => Ok(None),
        }
    }
}
