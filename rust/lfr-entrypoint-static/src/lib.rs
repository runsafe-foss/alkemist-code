/* Copyright (c) 2022 RunSafe Security Inc. */
#![cfg_attr(not(feature = "std"), no_std)]
extern crate entrypoint;
