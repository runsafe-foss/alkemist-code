include(CMakeRandoLib.txt)
if ((EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/cachedaemon") AND BUILD_CACHE_DAEMON)
   add_subdirectory(cachedaemon)
endif()

include(Closed.cmake OPTIONAL)

cargo_test(
        NAME cargo_test
        COMMAND cargo test --all-targets --workspace
                --features std --exclude trap_lto
                "--target-dir=${CMAKE_CURRENT_BINARY_DIR}/target-test"
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        )

cargo_test(
        NAME cargo_test_lfr
        COMMAND cargo test -p lfr-api
                --features std,btree,cache_fn_lookups
                "--target-dir=${CMAKE_CURRENT_BINARY_DIR}/target-test"
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        )

cargo_test(
        NAME cargo_doc
        COMMAND cargo test --doc --workspace
                --features std --exclude trap_lto
                "--target-dir=${CMAKE_CURRENT_BINARY_DIR}/target-test"
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        )
