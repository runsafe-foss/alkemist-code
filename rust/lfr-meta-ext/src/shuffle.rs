// Copyright (c) 2022 RunSafe Security Inc.

//! Functions relating to Shuffling Metadata and finding symbols

use fallible_iterator::FallibleIterator;
use itertools::Itertools;
use lfr_meta::mlf::{Func, Mlf};
use lfr_meta::rand::{Distribution, LfrChaCha, LfrRandom, Standard};
use lfr_meta::symbols::Trim;
use log::*;
use serde::Serialize;
use std::collections::HashMap;
use std::ops::Range;
use std::path::{Path, PathBuf};

use goblin::Object;
use std::io::{Error, ErrorKind, Read};
pub use trapdump::{Bits, Bits32, Bits64, NativeWidth, TrapPlatform};

use std::ffi::OsStr;
use std::os::unix::ffi::OsStrExt;

use num_traits;

/// Whether The Path is on the target or host system.
#[derive(Debug, Clone, Serialize, Eq, PartialEq, Hash)]
pub enum GdbPathBuf {
    /// Path is on the remote system
    Remote(PathBuf),
    /// Path is on the localhost
    Host(PathBuf),
}

impl GdbPathBuf {
    /// The underyling path
    pub fn path(&self) -> &Path {
        match self {
            Self::Remote(p) => p,
            Self::Host(p) => p,
        }
    }
}

/// Search info to find debug information
#[derive(Debug, Clone)]
pub struct SearchInfo {
    /// Path of binary to find debug info about.
    pub bin_path: GdbPathBuf,
    /// Path of SysRoot
    pub sysroot: GdbPathBuf,
    /// Search Lib Paths
    pub solib_search_paths: Vec<PathBuf>,
    /// Paths to search to find debug info (usually contains /usr/lib/debug)
    pub search_paths: Vec<PathBuf>,
}

impl SearchInfo {
    /// Append a path to the sysroot
    pub fn sysroot_path(&self, path: &GdbPathBuf) -> Result<PathBuf, std::io::Error> {
        match (&self.sysroot, path) {
            (_, GdbPathBuf::Host(path)) => Ok(path.clone()),
            (GdbPathBuf::Remote(_), _) => {
                // TODO: When we want to support this: gdb commands
                // remote get/remote put/remote delete should work.
                Err(Error::new(
                    ErrorKind::Other,
                    "LFR does not support remote debug file access.",
                ))
            }

            (GdbPathBuf::Host(sysroot), GdbPathBuf::Remote(path)) => {
                let newpath = if !path.is_absolute() || path.starts_with(sysroot) {
                    path.clone()
                } else if let Ok(suffix) = path.strip_prefix("/") {
                    sysroot.join(suffix)
                } else {
                    sysroot.join(path)
                };
                Ok(newpath)
            }
        }
    }
    /// Get the binary path with sysroot applied.
    pub fn sysroot_bin_path(&self) -> Result<PathBuf, std::io::Error> {
        self.sysroot_path(&self.bin_path)
    }
}

/// Symbol Search Type
#[derive(Debug, Clone)]
pub enum SymbolSearch {
    /// Only search exactly specified path
    Exact(PathBuf),
    /// Searches based on some heuristics to find path
    Standard(SearchInfo),
}

impl SymbolSearch {
    /// Searches using the description here
    /// (GDB - Seperate Debug Files)[https://sourceware.org/gdb/onlinedocs/gdb/Separate-Debug-Files.html].
    /// The only notable difference is that it looks at the sections `.lfr_debuglink` and `.note.lfr.build-id`.
    pub fn find(&self) -> std::io::Result<PathBuf> {
        match self {
            SymbolSearch::Standard(ref search) => {
                let bin_path = search.sysroot_bin_path()?;

                let (build_id, debug_link) = if let Ok(mut object) = std::fs::File::open(&bin_path)
                {
                    let mut object_data = Vec::new();
                    object.read_to_end(&mut object_data)?;
                    let object = Object::parse(&object_data)
                        .map_err(|_| Error::new(ErrorKind::Other, "unable to parse object file"))?;
                    match object {
                        Object::Elf(elf) => {
                            if elf
                                .section_headers
                                .iter()
                                .find(|phdr| {
                                    matches!(
                                        elf.shdr_strtab.get_at(phdr.sh_name),
                                        Some(".debug_info")
                                    )
                                })
                                .is_some()
                            {
                                return Ok(bin_path);
                            }

                            let build_id = elf
                                .iter_note_sections(&object_data, Some(".note.gnu.build-id"))
                                .into_iter()
                                .flatten()
                                .flatten()
                                .find_map(|note| {
                                    if note.name == "GNU"
                                        && note.n_type == goblin::elf::note::NT_GNU_BUILD_ID
                                    {
                                        Some(note.desc.to_vec())
                                    } else {
                                        None
                                    }
                                });

                            let debug_link = elf.section_headers.iter().find_map(|phdr| {
                                if let Some(".gnu_debuglink") = elf.shdr_strtab.get_at(phdr.sh_name)
                                {
                                    phdr.file_range().and_then(|range| {
                                        object_data[range.clone()].iter().position(|x| *x == 0).map(
                                            |pos| {
                                                PathBuf::from(OsStr::from_bytes(
                                                    &object_data[range][..pos],
                                                ))
                                            },
                                        )
                                    })
                                } else {
                                    None
                                }
                            });

                            (build_id, debug_link)
                        }
                        _ => (None, None),
                    }
                } else {
                    (None, None)
                };
                info!(
                    "Path: {:?} Build Id: {:?} Debug Link {:?}",
                    bin_path, build_id, debug_link
                );
                info!("Search Paths {:?}", search.search_paths);
                if let Some(build_id) = build_id {
                    let str_id: String = build_id.iter().map(|v| format!("{:02x}", v)).collect();
                    let mut chars = str_id.chars();
                    let split_id = (
                        chars.by_ref().take(2).collect::<String>(),
                        chars.by_ref().collect::<String>(),
                    );
                    for search in &search.search_paths {
                        let mut search = search.clone();
                        search.push(".build-id");
                        search.push(&split_id.0);
                        search.push(&split_id.1);
                        search.set_extension("lfr.debug");

                        debug!("Searching {}", search.display());
                        if search.exists() {
                            return Ok(search);
                        }
                    }
                }
                if let Some(filename) = debug_link {
                    let full_path = bin_path.canonicalize().ok();
                    if let Some(parent) = full_path.as_ref().and_then(|p| p.parent()) {
                        for s in &["", ".debug"] {
                            let search = parent.join(s).join(&filename);
                            debug!("Searching {}", search.display());
                            if search.exists() {
                                return Ok(search);
                            }
                        }
                        for s in &search.search_paths {
                            let mut search = parent.components().fold(s.clone(), |mut s, c| {
                                use std::path::Component;
                                if let Component::Normal(p) = c {
                                    s.push(p);
                                }
                                s
                            });
                            search.push(&filename);
                            debug!("Searching {}", search.display());
                            if search.exists() {
                                return Ok(search);
                            }
                        }
                    }
                }
            }
            SymbolSearch::Exact(path) => {
                return Ok(path.clone());
            }
        };
        Err(Error::new(ErrorKind::Other, "Unable to find debug info"))
    }
}

/// A simple means of comparing two results against each and printing an
/// error if they do not match. Basically an assertion that doesn't stop
/// the application
macro_rules! diff_it {
    ($left:expr, $right:expr, $($arg:tt)+) => {
        if !($left == $right) {
            error!("{}", format_args!($($arg)+));
            true
        } else {
            false
        }
    }
}

/// Diffs the functions contains in an Mlf and the newly computed
/// functions. This is a useful sanity check when computing
/// verifying accuracy of two different implementations of
/// shuffling.
fn diff(mlf: &Mlf, funcs: &[NewFunc]) -> bool {
    let mut diff = false;
    let mut sorted_funcs = funcs.to_vec();

    sorted_funcs.sort_by_key(|f| f.undiv_start);
    let mut mlf_funcs = mlf.funcs.clone();
    mlf_funcs.sort_by_key(|f| f.undiv_start);

    info!("Running Sanity Diff against known MLF");
    diff |= diff_it!(
        mlf.funcs.len(),
        sorted_funcs.len(),
        "Mismatched lengths {} != {}",
        mlf.funcs.len(),
        sorted_funcs.len()
    );

    //Same Set Check
    for (f1, f2) in mlf_funcs.iter().zip(sorted_funcs.iter()) {
        diff |= diff_it!(
            f1.undiv_start,
            f2.undiv_start,
            "symbol not located at correct undiv address {:x?} != {:x?}",
            f1,
            f2
        );
        diff |= diff_it!(
            f1.size,
            f2.undiv_end.unwrap() - f2.undiv_start,
            "symbol does not have matching size {:x?} != {:x?}",
            f1,
            f2
        );
    }

    //Order Check
    for (f1, f2) in mlf_funcs.iter().zip(sorted_funcs.iter()) {
        diff |= diff_it!(
            f1.undiv_start,
            f2.undiv_start,
            "copied symbol not in correct order {:x?} != {:x?}",
            f1,
            f2
        );
    }

    //Relocation Address Check
    for (f1, f2) in mlf_funcs.iter().zip(sorted_funcs.iter()) {
        if !f2.skip_copy {
            diff |= diff_it!(
                f1.div_start,
                f2.div_start.unwrap(),
                "symbol not relocated to correct div address {:x?} != {:x?}",
                f1,
                f2
            );
        }
    }

    info!("Diff result: {}", diff);
    if diff {
        info!("Sorted by div order");
        for z in mlf_funcs.iter().zip_longest(sorted_funcs.iter()) {
            use itertools::EitherOrBoth;
            let (f1, f2) = match z {
                EitherOrBoth::Both(f1, f2) => (Some(f1), Some(f2)),
                EitherOrBoth::Left(f1) => (Some(f1), None),
                EitherOrBoth::Right(f2) => (None, Some(f2)),
            };
            info!("{:x?},{:x?}", f1, f2)
        }
        mlf_funcs.sort_by_key(|x| x.undiv_start);
        sorted_funcs.sort_by_key(|x| x.undiv_start);
        info!("Sorted by undiv order");
        for z in mlf_funcs.iter().zip_longest(sorted_funcs.iter()) {
            use itertools::EitherOrBoth;
            let (f1, f2) = match z {
                EitherOrBoth::Both(f1, f2) => (Some(f1), Some(f2)),
                EitherOrBoth::Left(f1) => (Some(f1), None),
                EitherOrBoth::Right(f2) => (None, Some(f2)),
            };
            info!("{:x?},{:x?}", f1, f2)
        }
    }
    diff
}

/// A new Function, derived from TrapData
#[derive(Debug, Ord, PartialOrd, PartialEq, Eq, Clone)]
struct NewFunc {
    /// Undiversified Starting Address
    undiv_start: u64,
    /// Undiversified ending address
    undiv_end: Option<u64>,
    /// Diversified Starting Address
    div_start: Option<u64>,
    /// Whether to skip copying this entry
    skip_copy: bool,
    /// Alignment
    align: u64,
    /// Whether it's a gap function
    is_gap: bool,
}

impl NewFunc {
    /// Sorts based on a ranking as specified in RandoLib
    fn sort_rank(&self) -> u32 {
        match self.undiv_end {
            None => {
                if self.is_gap {
                    1
                } else {
                    2
                }
            }
            Some(end) if end - self.undiv_start == 0 => 3,
            Some(_) => 4,
        }
    }
}

/// A function taken from TrapDump information
#[derive(Debug)]
enum BuildFunc {
    /// Symbol and whether it's a gap function
    Symbol(trapdump::TrapSymbol, bool),
    /// Padding at address with corresponding size
    Padding(trapdump::TrapAddress, u64),
}
impl BuildFunc {
    /// Address of BuildFunc function based on offset
    fn address(&self, delta: u64) -> u64 {
        match self {
            BuildFunc::Symbol(s, _) => s.address.absolute(delta),
            BuildFunc::Padding(addr, _) => addr.absolute(delta),
        }
    }
    /// Size of Build Func
    fn size(&self) -> Option<u64> {
        match self {
            BuildFunc::Symbol(s, _) => s.size,
            BuildFunc::Padding(_, size) => Some(*size),
        }
    }
    /// Alignment of BuildFunc
    fn align(&self, config: &ShuffleOpts) -> u64 {
        match self {
            BuildFunc::Symbol(s, _) => s.align.unwrap_or(config.function_p2_align),
            BuildFunc::Padding(..) => 0,
        }
    }
    /// Whether this should be skipped
    fn skip(&self) -> bool {
        match self {
            BuildFunc::Symbol(_, _) => false,
            BuildFunc::Padding(..) => true,
        }
    }
    /// Whether this is a gap function
    fn is_gap(&self) -> bool {
        match self {
            BuildFunc::Symbol(_, gap) => *gap,
            BuildFunc::Padding(..) => false,
        }
    }
}

/// Sorting function based on implementation in RandoLib.cpp
fn rank_sort_funcs(a: &NewFunc, b: &NewFunc) -> std::cmp::Ordering {
    if a.undiv_start == b.undiv_start {
        a.sort_rank().cmp(&b.sort_rank())
    } else {
        a.undiv_start.cmp(&b.undiv_start)
    }
}

fn layout_trapv2_records<I>(
    records: I,
    header: &trapdump::TrapHeader,
    opts: &ShuffleOpts,
) -> Vec<NewFunc>
where
    I: FallibleIterator<Item = trapdump::TrapRecordV2, Error = trapdump::GenericParseError>,
{
    use trapdump::TrapRecordType;
    let mut funcs = records
        .flat_map(|r| {
            let bf = match r.typ {
                TrapRecordType::Padding | TrapRecordType::NonExecRegion => {
                    Some(BuildFunc::Padding(r.address, r.size.try_into().unwrap()))
                }
                TrapRecordType::Gap | TrapRecordType::Function => Some(BuildFunc::Symbol(
                    trapdump::TrapSymbol {
                        address: r.address,
                        size: Some(r.size.try_into().unwrap()),
                        align: Some(r.align.into()),
                    },
                    matches!(r.typ, TrapRecordType::Gap),
                )),
                TrapRecordType::Got => None,
                TrapRecordType::Unknown => None,
            };

            Ok(fallible_iterator::convert(bf.into_iter().map(Ok)))
        })
        .map(|b| {
            let start = b.address(0);
            Ok(NewFunc {
                undiv_start: start,
                undiv_end: b.size().map(|sz| sz + start),
                align: b.align(opts),
                skip_copy: b.skip(),
                div_start: None,
                is_gap: b.is_gap(),
            })
        })
        .collect::<Vec<_>>()
        .expect("valid records");
    if !header.flags.pre_sorted() {
        funcs.sort_unstable_by(rank_sort_funcs);
    }
    funcs
}

/// Lays out the functions based on the algorithm in RandoLib.cpp
fn layout_trapv2<T>(trap: &trapdump::TrapV2<'_, T>, opts: &ShuffleOpts) -> Vec<NewFunc>
where
    T: trapdump::ParserArch,
    trapdump::TrapRelocV2: trapdump::SliceRead<T>,
    trapdump::TrapRecordV2: trapdump::SliceRead<T>,
{
    layout_trapv2_records(trap.records(), trap.header(), opts)
}

/// Lays out Trapv1 based on approach in RandoLib.cpp
fn layout_trapv1<T>(
    trap: &trapdump::TrapV1<'_, T>,
    exec_region: Range<u64>,
    offset: u64,
    info: &trapdump::ObjectInfo,
    opts: &ShuffleOpts,
) -> Vec<NewFunc>
where
    T: trapdump::ParserArch,
{
    let mut funcs = trap
        .records()
        .filter(|r| Ok(exec_region.contains(&(r.address.absolute(offset)))))
        .flat_map(|r| {
            let pad = r
                .padding
                .as_ref()
                .filter(|p| p.size > 0)
                .map(|p| Ok(BuildFunc::Padding(p.address(&r), p.size)));
            Ok(r.symbols()
                .map(|s| Ok(BuildFunc::Symbol(s, false)))
                .chain(fallible_iterator::convert(pad.into_iter())))
        })
        .flat_map(|b| {
            let start = b.address(0);
            let new = NewFunc {
                undiv_start: start,
                undiv_end: b.size().map(|sz| sz + start),
                align: b.align(opts),
                skip_copy: b.skip(),
                div_start: None,
                is_gap: false,
            };
            let vals = if let Some(sz) = b.size() {
                let gap = NewFunc {
                    undiv_start: new.undiv_start + sz,
                    undiv_end: None,
                    align: 0,
                    skip_copy: false,
                    div_start: None,
                    is_gap: true,
                };
                vec![new, gap]
            } else {
                vec![new]
            };
            Ok(fallible_iterator::convert(vals.into_iter().map(Ok)))
        })
        .collect::<Vec<_>>()
        .expect("valid trap data");
    if !trap.header().flags.pre_sorted() {
        funcs.sort_unstable_by(rank_sort_funcs);
    }
    let mut next = info.exec_region.clone().unwrap().end as u64;
    funcs.iter_mut().rev().for_each(|f| {
        f.undiv_end = Some(next);
        next = f.undiv_start;
    });
    funcs.retain(|x| x.undiv_end.unwrap() > x.undiv_start);
    funcs
}

/// Determines a shuffle order of indices
fn shuffle_order<S>(seed: &[u32], funcs: &[NewFunc]) -> Vec<usize>
where
    S: LfrRandom + num_traits::ToPrimitive + num_traits::FromPrimitive,
    Standard: Distribution<S>,
{
    let mut rng = LfrChaCha::new(seed.try_into().unwrap()).expect("valid chacha");
    let mut v = (0..funcs.len()).collect::<Vec<_>>();
    for i in 0..v.len() {
        let max = v.len() - i;
        let j: usize = rng
            .random::<S>(S::from_usize(max).expect("fits in usize"))
            .to_usize()
            .unwrap();
        if j > 0 {
            v.swap(i, i + j);
        }
    }
    v
}

/// Shuffling Options
pub struct ShuffleOpts {
    /// Whether to preserve function offsets
    pub preserve_function_offset: bool,
    /// The global p2_align variable
    pub function_p2_align: u64,
}

impl Default for ShuffleOpts {
    fn default() -> ShuffleOpts {
        ShuffleOpts {
            preserve_function_offset: false,
            function_p2_align: 2,
        }
    }
}

/// Shuffles based on the info read from a trap file.
/// This is neccesary to correctly compute shuffles on mismatched architectures (for example x86 on x86_64).
fn shuffle_trap(
    path: &Path,
    seed: Option<&[u32]>,
    exec_region: Range<u64>,
    file_base: u64,
    config: ShuffleOpts,
    mlf: Option<&Mlf>,
    trim: &[Trim],
    info: &trapdump::ObjectInfo,
    addr_offset: u64,
) -> Option<Mlf> {
    use trapdump::Parser;
    let bytes = info.data.as_ref();
    let trap_parse = trapdump::TrapParser::new(info.trap_start_addr, 0, false)
        .runtime_platform(info.platform)
        .parse(bytes)
        .ok()?;

    use trapdump::{PlatformTrapInfo, TrapInfo};
    let mut funcs = match &trap_parse {
        PlatformTrapInfo::X86(TrapInfo::TrapV1(v1)) => {
            layout_trapv1(v1, exec_region.clone(), addr_offset, info, &config)
        }
        PlatformTrapInfo::Arm(TrapInfo::TrapV1(v1)) => {
            layout_trapv1(v1, exec_region.clone(), addr_offset, info, &config)
        }
        PlatformTrapInfo::X86_64(TrapInfo::TrapV1(v1)) => {
            layout_trapv1(v1, exec_region.clone(), addr_offset, info, &config)
        }
        PlatformTrapInfo::Aarch64(TrapInfo::TrapV1(v1)) => {
            layout_trapv1(v1, exec_region.clone(), addr_offset, info, &config)
        }
        PlatformTrapInfo::X86(TrapInfo::TrapV2(v2)) => layout_trapv2(v2, &config),
        PlatformTrapInfo::Arm(TrapInfo::TrapV2(v2)) => layout_trapv2(v2, &config),
        PlatformTrapInfo::X86_64(TrapInfo::TrapV2(v2)) => layout_trapv2(v2, &config),
        PlatformTrapInfo::Aarch64(TrapInfo::TrapV2(v2)) => layout_trapv2(v2, &config),
    };

    let shuffle_fn = match &trap_parse {
        PlatformTrapInfo::X86(_) | PlatformTrapInfo::Arm(_) => {
            shuffle_order::<<Bits32 as Bits>::UIntPtr>
        }
        PlatformTrapInfo::X86_64(_) | PlatformTrapInfo::Aarch64(_) => {
            shuffle_order::<<Bits64 as Bits>::UIntPtr>
        }
    };
    let strict_skip_shuffle = std::env::var_os("LFR_STRICT_SKIP_SHUFFLE");

    if let Some(seed) = seed.filter(|_| strict_skip_shuffle.is_none()) {
        let trim = trim
            .iter()
            .map(|t| (t.undiv_start, t.new_size))
            .collect::<HashMap<_, _>>();
        funcs.iter_mut().for_each(|f| {
            if let Some(new_size) = trim.get(&f.undiv_start) {
                let diff = f.undiv_end.unwrap() - f.undiv_start - new_size;
                f.undiv_start += diff;
            }
        });

        funcs.retain(|f| f.undiv_end.unwrap() - f.undiv_start != 0);

        let shuffle = shuffle_fn(seed, &funcs);

        let start = info.exec_region.clone().unwrap().start as u64;
        let mut cur_addr = exec_region.start;

        shuffle.iter().for_each(|i| {
            if let Some(f) = funcs.get_mut(*i) {
                if !f.skip_copy {
                    let align_mask = (1 << f.align) - 1;
                    let old_ofs = (f.undiv_start - start) & align_mask;
                    let curr_ofs = (cur_addr - start) & align_mask;
                    let want_ofs = if config.preserve_function_offset {
                        old_ofs
                    } else {
                        0
                    };
                    if want_ofs != curr_ofs {
                        let padding = ((align_mask + 1) + want_ofs - curr_ofs) & align_mask;
                        cur_addr += padding;
                    }
                    f.div_start = Some(cur_addr);
                    cur_addr += f.undiv_end.unwrap() - f.undiv_start;
                } else {
                    f.div_start = None;
                }
            }
        });

        debug!("LFR Movements");
        let mut funcs_order = funcs.clone();
        funcs_order.sort_by_key(|f| f.div_start);
        for f in &funcs_order {
            if let Some(div) = f.div_start {
                debug!(
                    "Moving {:#x}[{}]=>{:#x}@0xXXXXXXXX",
                    f.undiv_start,
                    f.undiv_end.unwrap() - f.undiv_start,
                    div
                );
            }
        }

        assert!(!mlf.map(|mlf| diff(mlf, &funcs)).unwrap_or(false));
    } else {
        funcs.iter_mut().for_each(|f| {
            f.div_start = Some(f.undiv_start + file_base);
        });
    }

    let newmlf = Mlf {
        version: 1,
        seed: seed.map(|s| s.to_vec()),
        file_base: Some(file_base),
        func_base: Some(exec_region.start),
        func_size: 0,
        mod_name: path.to_string_lossy().into_owned(),
        funcs: funcs
            .into_iter()
            .filter(|new| !new.skip_copy)
            .map(|new| {
                let div_start = new.div_start.unwrap();
                Func {
                    div_start,
                    undiv_start: new.undiv_start,
                    size: new.undiv_end.unwrap() - new.undiv_start,
                }
            })
            .collect(),
    };
    debug!("New Mlf {:#x?}", newmlf);
    Some(newmlf)
}

/// Shuffles the symbols found at `path` based on a provided `seed` placed in a specific
/// `exec_region`. The `file_base` is optional and will be automatically determined based
/// on a .text section if not provided.
/// The optional mlf will be used to verify correctness if provided.
/// `trim` is a list of all trimmed functions because the shuffle cannot determine those
/// if looking at a randomized section of memory.
pub fn shuffle<T>(
    path: T,
    seed: Option<&[u32]>,
    exec_region: Range<u64>,
    file_base: Option<u64>,
    config: ShuffleOpts,
    mlf: Option<&Mlf>,
    trim: &[Trim],
) -> Option<Mlf>
where
    T: AsRef<Path>,
{
    let (file_base, addr_offset) = if let Some(base) = file_base {
        (base, base)
    } else {
        info!("Attempting to read path {}", path.as_ref().display());
        let file = std::fs::read(&path).ok()?;
        info!("Parsing Object {}", path.as_ref().display());
        match Object::parse(&file).ok()? {
            Object::Elf(elf) => match elf.header.e_type {
                goblin::elf::header::ET_EXEC => (0, 0),
                goblin::elf::header::ET_DYN => {
                    info!("Need offset for ET_DYN");
                    let s = elf
                        .section_headers
                        .iter()
                        .find(|s| elf.shdr_strtab.get_at(s.sh_name) == Some(".text"));

                    if let Some(section) = s {
                        let offset = exec_region.start - section.vm_range().start as u64;
                        info!("New offset DYN {:x}", offset);
                        (offset, offset)
                    } else {
                        (0, 0)
                    }
                }
                _ => return None,
            },
            _ => return None,
        }
    };
    info!("Attempting to read path {}", path.as_ref().display());
    let mut object = std::fs::File::open(&path).ok()?;
    info!("Reading trapdump from {}", path.as_ref().display());
    let info = trapdump::from_object(&mut object).ok()?;
    assert_eq!(info.len(), 1);
    shuffle_trap(
        path.as_ref(),
        seed,
        exec_region,
        file_base,
        config,
        mlf,
        trim,
        &info[0],
        addr_offset,
    )
}
