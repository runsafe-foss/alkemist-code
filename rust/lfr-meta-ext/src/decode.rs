// Copyright (c) 2022 RunSafe Security Inc.

//! A module dedicated to decoding symbols found in liblfr.so
//! based on a few key symbols and access to the target memory space.

use anyhow::Result;
use const_env::from_env;
pub use lfr_meta::endian;
use lfr_meta::endian::*;
use lfr_meta::symbols::{CoreSymbols, ReadError, SymbolEntryV1};
use serde::{Deserialize, Serialize};
use std::borrow::Cow;
use thiserror::Error;

/// The name of liblfr that can be found looking through module names.
#[from_env("LFR_LIB_NAME_FULL")]
pub const LIBLFR_NAME: &str = "liblfr.so";

/// Symbol lookups with a name and a given type hint which is useful
/// when debugging information cannot be found.
#[derive(Debug, Clone)]
pub struct SymbolLookup<'a> {
    /// Name of Symbol
    pub name: Cow<'a, str>,
    /// Suggested name of C type.
    pub type_hint: Cow<'a, str>,
}
/// The Lfr Symbol Table found in lfrgdbapi
pub const LIBLFR_SYMBOL_TABLE: SymbolLookup<'static> = SymbolLookup {
    name: Cow::Borrowed("__liblfr_symbol_table"),
    type_hint: Cow::Borrowed("void *"),
};
/// The Lfr Symbol Table Size found in lfrgdbapi
pub const LIBLFR_SYMBOL_TABLE_SIZE: SymbolLookup<'static> = SymbolLookup {
    name: Cow::Borrowed("__liblfr_symbol_table_size"),
    type_hint: Cow::Borrowed("long long int"),
};

/// Possible prefix of Symbols
pub const LIBLFR_SYMBOL_TABLE_PREFIX: [&str; 2] = ["lfrgdbapi::", "_TRaP_"];

/// Name of function that needs a breakpoint on it to catch descriptor changes.
pub const LIBLFR_JIT_DEBUG_REGISTER: SymbolLookup<'static> = SymbolLookup {
    name: Cow::Borrowed("__lfr_jit_debug_register_code"),
    type_hint: Cow::Borrowed("void *"),
};

/// Name of jit debug descriptor
pub const LIBLFR_JIT_DEBUG_DESCRIPTOR: SymbolLookup<'static> = SymbolLookup {
    name: Cow::Borrowed("__lfr_jit_debug_descriptor"),
    type_hint: Cow::Borrowed("void"),
};

/// Address representing an on-disk raddress
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub struct BinAddr(pub u64);
/// Address representing an in-memory process address
#[derive(Debug, Copy, Clone)]
pub struct VirtAddr(pub u64);

/// Catch-all LFR Erro Type
#[derive(Error, Debug)]
pub enum LfrError {
    /// Couldn't find any metadata
    #[error("No Lfr Metadata found")]
    MissingMetadata,
    /// Metadata could not be converted from bogus info
    #[error("Bad Lfr Metadata found")]
    InvalidMetadata,
    /// Wasn't able to read memory
    #[error("Couldn't read memory region {0:x}.")]
    InvalidMemoryRegion(u64),
    /// Unexpected pointer size
    #[error("Pointer Size is not 32/64")]
    InvalidPointerSize,
    /// Couldn't find a symbol we were looking for.
    #[error("Couldn't find symbol {0}")]
    MissingSymbol(String),
    /// Value was too large to fit in current usize type.
    #[error("Couldn't convert value into usize type.")]
    ValueTooLarge,
    /// Couldn't convert a value and unwilling to be more specific.
    #[error("Generic conversion issue")]
    Conversion,
    /// Couldn't write entire buffer
    #[error("Unable to write target value memory length.")]
    ShortWrite,
    /// Couldn't fill entire buffer
    #[error("Unable to read target value memory length.")]
    ShortRead,
    /// Couldn't find register
    #[error("Missing register")]
    MissingReg,
    /// Some sort of unwind gerror
    #[error("Older version of GDB cannot read local variables while unwinding.")]
    Unwinding,
    /// Passthrough error from anyhow
    #[error(transparent)]
    Other(#[from] anyhow::Error),
}

/// A symbol and it's address
#[derive(Debug, Clone)]
pub struct Symbol {
    value: u64,
    size: usize,
}

impl Symbol {
    /// Create a new symbol with `value` and length `size`.
    pub fn new(value: u64, size: usize) -> Self {
        Self { value, size }
    }
}

/// A fully fleshed out set of symbols, already decoded.
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ConcreteCoreSymbols(pub Vec<SymbolEntryV1>);

impl ConcreteCoreSymbols {
    /// Whether the set of libraries contains the addr in the text section.
    pub fn contains(&self, ip: u64) -> Option<&SymbolEntryV1> {
        self.0.iter().find(|e| e.exec_region.contains(&ip))
    }
    /// Whether the set of libraries contains the addr in the textramp section.
    pub fn textramp_contains(&self, ip: u64) -> Option<&SymbolEntryV1> {
        self.0.iter().find(|e| e.textramp_region.contains(&ip))
    }
}

/// Represents an LFR Target that may or may not have the desired symbols.
/// however if it does it will be able to find and read the desired metadata.
pub trait Lfr {
    /// Lookup a given symbol in a library based on possible prefixes.
    /// Returns first match.
    fn symbol_val_prefix(
        &self,
        name: &SymbolLookup,
        prefix: &[&str],
        lib: &str,
    ) -> Result<Option<Symbol>, LfrError> {
        if let Ok(Some(sym)) = self.symbol_val(name, lib) {
            return Ok(Some(sym));
        }
        if prefix.is_empty() {
            return Err(LfrError::MissingSymbol(name.name.to_string()));
        }
        let mut search = prefix
            .iter()
            .map(|p| {
                let prefix_name = Cow::Owned(format!("{}{}", p, name.name));
                let prefix_sym = SymbolLookup {
                    name: prefix_name,
                    ..name.clone()
                };
                self.symbol_val(&prefix_sym, lib)
            })
            .collect::<Vec<_>>();

        if let Some(pos) = search.iter().position(|x| matches!(x, Ok(Some(_)))) {
            search.remove(pos)
        } else {
            search.remove(0)
        }
    }
    /// Get the targets metadata
    fn metadata(&self) -> Result<Option<ConcreteCoreSymbols>, LfrError> {
        let bincode_sym = self
            .symbol_val_prefix(
                &LIBLFR_SYMBOL_TABLE,
                &LIBLFR_SYMBOL_TABLE_PREFIX,
                LIBLFR_NAME,
            )?
            .ok_or_else(|| LfrError::MissingSymbol(LIBLFR_SYMBOL_TABLE.name.to_string()))?;
        assert!(
            bincode_sym.size == std::mem::size_of::<u32>()
                || bincode_sym.size == std::mem::size_of::<u64>()
        );
        let bincode_start_ptr = self.read_ptr(bincode_sym.value)?;
        let bincode_size_sym = self
            .symbol_val_prefix(
                &LIBLFR_SYMBOL_TABLE_SIZE,
                &LIBLFR_SYMBOL_TABLE_PREFIX,
                LIBLFR_NAME,
            )?
            .ok_or_else(|| LfrError::MissingSymbol(LIBLFR_SYMBOL_TABLE_SIZE.name.to_string()))?;
        assert!(
            bincode_sym.size == std::mem::size_of::<u32>()
                || bincode_sym.size == std::mem::size_of::<u64>()
        );
        let size = self
            .read::<u64>(bincode_size_sym.value)
            .and_then(|x| usize::try_from(x).map_err(|_| LfrError::ValueTooLarge))?;
        let meta = if size == 0 {
            return Ok(None);
        } else {
            let mut bincode = vec![0; size];
            self.read_mem_exact(bincode_start_ptr, &mut bincode)?;
            ciborium::from_reader::<CoreSymbols, &[u8]>(bincode.as_ref())
                .map_err(|_| LfrError::InvalidMetadata)?
        };

        match meta {
            CoreSymbols::V1(v) => Ok(Some(ConcreteCoreSymbols(v))),
            CoreSymbols::V2(mut data) => {
                let reader = |addr, n| self.read_slice(addr, n).map_err(|_| ReadError::OutOfRange);
                let offsets = data
                    .offsets
                    .read_all(reader)
                    .map_err(|_| LfrError::InvalidMetadata)?;
                let reader = |addr, n| self.read_slice(addr, n).map_err(|_| ReadError::OutOfRange);
                let entries = offsets
                    .iter()
                    .scan(0, |state, end| {
                        let start = *state;
                        let end = usize::try_from(*end).unwrap();
                        *state = end;
                        Some((|| {
                            let entry = data
                                .entries
                                .read_range(reader, start..end)
                                .map_err(|_| LfrError::InvalidMetadata)?;
                            ciborium::from_reader::<SymbolEntryV1, _>(entry.as_ref())
                                .map_err(|_| LfrError::InvalidMetadata)
                        })())
                    })
                    .collect::<Result<Vec<_>, _>>()?;
                Ok(Some(ConcreteCoreSymbols(entries)))
            }
            _ => Err(LfrError::InvalidMetadata),
        }
    }
    /// Endianness of target
    fn endian(&self) -> Endian;
    /// Pointer Size of Target
    fn pointer_size(&self) -> u32;
    /// Symbol Value in target based on name and library lookup.
    fn symbol_val(&self, name: &SymbolLookup, lib: &str) -> Result<Option<Symbol>, LfrError>;
    /// Read a buffer sized chunk of memory starting at address, may not fill whole buffer.
    fn read_mem(&self, addr: u64, buf: &mut [u8]) -> Result<usize, LfrError>;
    /// Reads the exact size of the buffer from mem starting at address
    fn read_mem_exact(&self, addr: u64, buf: &mut [u8]) -> Result<(), LfrError> {
        let size = self.read_mem(addr, buf)?;
        if size == buf.len() {
            Ok(())
        } else {
            Err(LfrError::ShortRead)
        }
    }
    /// Reads a pointer at address
    fn read_ptr(&self, addr: u64) -> Result<u64, LfrError> {
        let v = match self.pointer_size() {
            64 => self.read::<u64>(addr)?,
            32 => u64::from(self.read::<u32>(addr)?),
            _ => return Err(LfrError::InvalidPointerSize),
        };
        Ok(v)
    }
    /// Reads starting at into target type.
    fn read<T>(&self, addr: u64) -> Result<T, LfrError>
    where
        T: Sized + EndianRead,
        <T as EndianRead>::Array: AsMut<[u8]>,
    {
        let mut v: <T as EndianRead>::Array = Default::default();

        self.read_mem_exact(addr, v.as_mut())?;
        let val = match self.endian() {
            Endian::Big => T::from_be_bytes(v),
            Endian::Little => T::from_le_bytes(v),
        };
        Ok(val)
    }

    /// Reads a slice of type T from an address with a given length.
    fn read_slice<T>(&self, addr: u64, n: usize) -> Result<Cow<'_, [T]>, LfrError>
    where
        T: Sized + EndianRead,
        [T]: ToOwned,
        <T as EndianRead>::Array: AsMut<[u8]>,
    {
        let size = std::mem::size_of::<T>();
        let mut v = vec![0; size * n];
        self.read_mem_exact(addr, &mut v)?;
        let endian = self.endian();
        let items = v
            .chunks_exact(size)
            .map(|bytes| {
                let mut arr: <T as EndianRead>::Array = Default::default();
                arr.as_mut().copy_from_slice(bytes);
                match endian {
                    Endian::Big => T::from_be_bytes(arr),
                    Endian::Little => T::from_le_bytes(arr),
                }
            })
            .collect::<Vec<_>>();
        assert_eq!(items.len(), n);
        Ok(Cow::Owned(items.to_owned()))
    }
}
