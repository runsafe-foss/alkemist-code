// Copyright (c) 2022 RunSafe Security Inc.

//! Library pertaining to extensions of the lfr-meta library that
//! lives within liblfr.so. These would be one big library but there
//! are limitations on what can be in liblfr.so to maintain no_std compatibility.
#![deny(missing_docs)]
pub mod decode;
pub mod shuffle;
pub use lfr_meta;
