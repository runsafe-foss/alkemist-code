// Copyright (c) 2022 RunSafe Security Inc.
use goblin::elf::reloc as Rel;
use modinfo::TargetAddr;
#[cfg(feature = "trap_relax")]
use trap_parser::TrapAddress;
use trap_parser::TrapRelocAddend;

#[cfg(target_pointer_width = "64")]
type LocalUsizePtrTy = usize;

#[cfg(not(target_pointer_width = "64"))]
type LocalUsizePtrTy = u64;

use super::SignExtend;
use crate::{
    assert_delta_size, ArchError, ArchUtil, Architecture, Parseable, Relocation, TargetError,
    TargetRead, TargetWrite,
};

macro_rules! match_mask {
    ($(($nm:ident, $mask:expr, $val:expr)),*) => {
        enum Match {
            $(
                #[allow(unused)]
                $nm
            ),*
        }

        impl Match {
            fn matches(&self, contents: u32) -> bool {
                match self {
                    $(
                        Self::$nm => (contents & $mask) == $val,
                    )*
                }
            }
        }
    }
}

// These values were derived from the Arm A64 Instruction Set Architecture Documentation
// https://developer.arm.com/downloads/-/exploration-tools
//
// Additionally some instructions can be found hardcoded in the actual relaxations
// binutils/bfd/elfnn-aarch64.c:elfNN_aarch64_tls_relax
match_mask! {
    (Nop, 0xffffffff, 0xd503201f),
    (Adr, 0xbe000000, 0x20000000),
    (Adrp, 0x9f000000, 0x90000000),
    (LdSt, 0x0a000000, 0x08000000),
    (LdStUimm, 0x3b000000, 0x39000000),
    (MrsThreadPointer, 0xffffffff, 0xd53bd041),
    (Movz,0xff800000, 0xd2800000),
    (MovzLsl16,0xffe00000 , 0xd2a00000),
    (MovzLsl32,0xffe00000 , 0xd2c00000),
    (Movk,0xff800000 , 0xf2800000),
    (MovkLsl16,0xffe00000 , 0xf2a00000),
    (MovkLsl32,0xffe00000 , 0xf2c00000),
    (Mrs, 0xfff00000, 0xd5300000),
    (AddR0R0, 0xffffffff, 0x91000000),
    // This could be more specific, it's just checking R0
    (LdrR0,   0x0000001f, 0x00000000)
}

#[cfg(feature = "trap_relax")]
impl<T> crate::Relax<T> for ArchArm64
where
    T: TargetRead<LocalUsizePtrTy>,
{
    type UsizePtrTy = LocalUsizePtrTy;
    fn apply_relax<F>(
        trapv1: &mut trap_parser::write::TRaPV1Builder,
        target: &T,
        f: F,
    ) -> Result<(), crate::RelaxError>
    where
        F: Fn(TrapAddress) -> TargetAddr<LocalUsizePtrTy>,
    {
        let mut next_split: Option<usize> = None;
        let read = |addr| target.read_vaddr::<u32>(f(addr)).unwrap();

        for record in &mut trapv1.records {
            record
                .relocs
                .split_inclusive_mut(|reloc| {
                    next_split = next_split.map(|s| s.saturating_sub(1));
                    if let Some(0) = &next_split {
                        next_split = None;
                        return true;
                    }
                    /* request the number of additional entries in the slice */
                    next_split = match reloc.typ as u32 {
                        Rel::R_AARCH64_TLSDESC_LD_PREL19 | Rel::R_AARCH64_TLSGD_MOVW_G1 => Some(2),
                        Rel::R_AARCH64_TLSGD_ADR_PREL21
                        | Rel::R_AARCH64_TLSGD_ADD_LO12_NC
                        | Rel::R_AARCH64_TLSLD_ADR_PREL21
                        | Rel::R_AARCH64_TLSLD_ADD_LO12_NC => Some(1),
                        _ => None,
                    };
                    next_split.is_none()
                })
                .for_each(|slice| {
                    if slice.is_empty() {
                        // This appears to be a bug in the old rust release we use. It's not present in 1.73.
                        return;
                    }
                    // The following code mirrors very closely the behavior found in the BFD linker.
                    // binutils/bfd/elfnn-aarch64.c:elfNN_aarch64_tls_relax
                    match slice[0].typ as u32 {
                        Rel::R_AARCH64_TLSDESC_ADR_PAGE21 | Rel::R_AARCH64_TLSGD_ADR_PAGE21 if Match::Movz.matches(read(slice[0].address)) => {
                            // GD->LE relaxation:
                            // adrp x0, :tlsgd:var     =>   movz R0, :tprel_g1:var
                            // or
                            // adrp x0, :tlsdesc:var   =>   movz R0, :tprel_g1:var
                            // Where R is x for LP64, and w for ILP32.
                            slice[0].typ = Rel::R_AARCH64_TLSLE_MOVW_TPREL_G1.into();
                        }
                        Rel::R_AARCH64_TLSDESC_ADR_PAGE21 | Rel::R_AARCH64_TLSGD_ADR_PAGE21 => {
                            // GD->IE relaxation:
                            // adrp x0, :tlsgd:var     =>   adrp x0, :gottprel:var
                            // or
                            // adrp x0, :tlsdesc:var   =>   adrp x0, :gottprel:var

                            //WARNING: We cannot distinguish the third case where it's unmodified.
                            slice[0].typ = Rel::R_AARCH64_TLSIE_ADR_GOTTPREL_PAGE21.into();
                        }
                        Rel::R_AARCH64_TLSDESC_LD_PREL19 if Match::Movz.matches(read(slice[0].address)) => {
                            // Tiny TLSDESC->LE relaxation:
                            // ldr   x1, :tlsdesc:var       =>  movz  R0, #:tprel_g1:var
                            // adr   x0, :tlsdesc:var       =>  movk  R0, #:tprel_g0_nc:var
                            // .tlsdesccall var
                            // blr   x1                     =>  nop
                            // Where R is x for LP64, and w for ILP32.  
                            slice[0].typ = Rel::R_AARCH64_TLSLE_MOVW_TPREL_G0_NC.into();
                            assert_eq!(slice[1].typ, Rel::R_AARCH64_TLSDESC_ADR_PREL21.into());
                            assert_eq!(slice[2].typ, Rel::R_AARCH64_TLSDESC_CALL.into());
                            slice[1].typ = Rel::R_AARCH64_TLSLE_MOVW_TPREL_G0_NC.into();
                            slice[2].typ = Rel::R_AARCH64_NONE.into();
                        }
                        Rel::R_AARCH64_TLSDESC_LD_PREL19 if Match::Nop.matches(read(slice[1].address)) => {
                            // Tiny TLSDESC->IE relaxation:
                            // ldr   x1, :tlsdesc:var       =>  ldr   x0, :gottprel:var
                            // adr   x0, :tlsdesc:var       =>  nop
                            // .tlsdesccall var
                            // blr   x1                     =>  nop
                            slice[0].typ = Rel::R_AARCH64_TLSLE_MOVW_TPREL_G0_NC.into();
                            assert_eq!(slice[1].typ, Rel::R_AARCH64_TLSDESC_ADR_PREL21.into());
                            assert_eq!(slice[2].typ, Rel::R_AARCH64_TLSDESC_CALL.into());
                            slice[1].typ = Rel::R_AARCH64_NONE.into();
                            slice[2].typ = Rel::R_AARCH64_NONE.into();
                        }
                        Rel::R_AARCH64_TLSGD_ADR_PREL21 if Match::Mrs.matches(read(slice[0].address)) => {
                            // Tiny GD->LE relaxation:
                            // adr x0, :tlsgd:var       =>   mrs  x1, tpidr_el0
                            // bl    __tls_get_addr     =>   add  R0, R1, #:tprel_hi12:x, lsl #12
                            // nop                      =>   add  R0, R0, #:tprel_lo12_nc:x

                            // Where R is x for LP64, and x for Ilp32. 
                            assert_eq!(slice[0].address.wrapping_add(4), slice[1].address);

                            slice[0].typ = Rel::R_AARCH64_TLSLE_ADD_TPREL_HI12.into();
                            slice[0].address = slice[0].address.wrapping_add(4);

                            slice[1].typ = Rel::R_AARCH64_TLSLE_ADD_TPREL_LO12_NC.into();
                            slice[1].address = slice[0].address.wrapping_add(4);
                        }
                        Rel::R_AARCH64_TLSGD_ADR_PREL21 if Match::LdrR0.matches(read(slice[0].address)) => {
                            // Tiny GD->IE relaxation:
                            // adr x0, :tlsgd:var       =>   ldr  R0, :gottprel:var
                            // bl    __tls_get_addr     =>   mrs  x1, tpidr_el0
                            // nop                      =>   add  R0, R0, R1
                            // Where R is x for LP64, and w for Ilp32.
                            assert_eq!(slice[0].address.wrapping_add(4), slice[1].address);
                            slice[0].typ = Rel::R_AARCH64_TLSIE_LD_GOTTPREL_PREL19.into();
                            slice[1].typ = Rel::R_AARCH64_NONE.into();
                        }
                        Rel::R_AARCH64_TLSGD_MOVW_G1 if Match::MovzLsl32.matches(read(slice[0].address)) => {
                            // Large GD->LE relaxation:
                            // movz x0, #:tlsgd_g1:var     => movz x0, #:tprel_g2:var, lsl #32
                            // movk x0, #:tlsgd_g0_nc:var  => movk x0, #:tprel_g1_nc:var, lsl #16
                            // add x0, gp, x0              => movk x0, #:tprel_g0_nc:var
                            // bl __tls_get_addr           => mrs x1, tpidr_el0
                            // nop                         => add x0, x0, x1
                            assert_eq!(slice[1].typ, Rel::R_AARCH64_TLSGD_MOVW_G0_NC.into());
                            assert_eq!(slice[0].address.wrapping_add(12), slice[2].address);
                            assert_eq!(slice[2].typ, Rel::R_AARCH64_CALL26.into());

                            slice[0].typ = Rel::R_AARCH64_TLSLE_MOVW_TPREL_G2.into();
                            slice[1].typ = Rel::R_AARCH64_TLSLE_MOVW_TPREL_G1_NC.into();
                            slice[2].typ = Rel::R_AARCH64_TLSLE_MOVW_TPREL_G0_NC.into();
                            slice[2].address = slice[2].address.wrapping_add(8);
                        }
                        Rel::R_AARCH64_TLSGD_MOVW_G1 if Match::MovzLsl16.matches(read(slice[0].address)) => {
                             // Large GD->IE relaxation:
                             // movz x0, #:tlsgd_g1:var    => movz x0, #:gottprel_g1:var, lsl #16
                             // movk x0, #:tlsgd_g0_nc:var => movk x0, #:gottprel_g0_nc:var
                             // add x0, gp, x0             => ldr x0, [gp, x0]
                             // bl __tls_get_addr          => mrs x1, tpidr_el0
                             // nop                        => add x0, x0, x1

                            assert_eq!(slice[1].typ, Rel::R_AARCH64_TLSGD_MOVW_G0_NC.into());
                            assert_eq!(slice[0].address.wrapping_add(12), slice[2].address);
                            assert_eq!(slice[2].typ, Rel::R_AARCH64_CALL26.into());

                            slice[0].typ = Rel::R_AARCH64_TLSIE_MOVW_GOTTPREL_G1.into();
                            slice[1].typ = Rel::R_AARCH64_TLSIE_MOVW_GOTTPREL_G0_NC.into();
                            slice[2].typ = Rel::R_AARCH64_NONE.into();
                        }
                        // WARNING: We cannot determine which relocation was applied here.
                        // So we leave it alone. Luckily neither require relocation.
                        // Rel::R_AARCH64_TLSGD_MOVW_G0_NC => {
                        //     // GD -> LE
                        //     slice[0].typ = Rel::R_AARCH64_TLSLE_MOVW_TPREL_G1_NC.into();
                        // }
                        // Rel::R_AARCH64_TLSGD_MOVW_G0_NC => {
                        //     // GD -> IE
                        //     slice[0].typ = Rel::R_AARCH64_TLSIE_MOVW_GOTTPREL_G0_NC.into();
                        // }
                        Rel::R_AARCH64_TLSDESC_LD64_LO12 /*_NC*/ if Match::Movk.matches(read(slice[0].address)) => {
                            // GD->LE relaxation:
                            // ldr xd, [x0, #:tlsdesc_lo12:var]   =>   movk x0, :tprel_g0_nc:var
                            // Where R is x for lp64 mode, and w for ILP32 mode. 
                            slice[0].typ = Rel::R_AARCH64_TLSLE_MOVW_TPREL_G0_NC.into();
                        }
                        Rel::R_AARCH64_TLSDESC_LD64_LO12 /*_NC*/ => {
                            // GD->IE relaxation:
                            // ldr xd, [x0, #:tlsdesc_lo12:var] => ldr R0, [x0, #:gottprel_lo12:var]
                            // Where R is x for lp64 mode, and w for ILP32 mode.  
                            slice[0].typ = Rel::R_AARCH64_TLSIE_LD64_GOTTPREL_LO12_NC.into();
                        }
                        Rel::R_AARCH64_TLSGD_ADD_LO12_NC if Match::Movk.matches(read(slice[0].address)) => {
                            // GD->LE relaxation
                            // add  x0, #:tlsgd_lo12:var   => movk R0, :tprel_g0_nc:var
                            // bl    __tls_get_addr        => mrs  x1, tpidr_el0
                            // nop                         => add  R0, R1, R0
                            // Where R is x for lp64 mode, and w for ILP32 mode.  
                            assert_eq!(slice[0].address.wrapping_add(4), slice[1].address);
                            slice[0].typ = Rel::R_AARCH64_TLSLE_MOVW_TPREL_G0_NC.into();
                            slice[1].typ = Rel::R_AARCH64_NONE.into();
                        }
                        Rel::R_AARCH64_TLSGD_ADD_LO12_NC if Match::LdrR0.matches(read(slice[0].address)) => {
                            // GD->IE relaxation
                            // ADD  x0, #:tlsgd_lo12:var   => ldr  R0, [x0, #:gottprel_lo12:var]
                            // BL    __tls_get_addr        => mrs  x1, tpidr_el0
                            //    R_AARCH64_CALL26
                            // NOP                         => add  R0, R1, R0
                            // Where R is x for lp64 mode, and w for ilp32 mode.
                            assert_eq!(slice[1].typ, Rel::R_AARCH64_CALL26.into());
                            slice[0].typ = Rel::R_AARCH64_TLSIE_LD64_GOTTPREL_LO12_NC.into();
                            slice[1].typ = Rel::R_AARCH64_NONE.into();
                        }
                        Rel::R_AARCH64_TLSDESC_ADD
                        | Rel::R_AARCH64_TLSDESC_ADD_LO12
                        | Rel::R_AARCH64_TLSDESC_CALL if Match::Nop.matches(read(slice[0].address)) => {
                            // GD->IE/LE relaxation:
                            // add x0, x0, #:tlsdesc_lo12:var        =>   nop
                            // blr xd                                =>   nop
                            slice[0].typ = Rel::R_AARCH64_NONE.into();
                        }
                        Rel::R_AARCH64_TLSDESC_LDR if Match::Movk.matches(read(slice[0].address)) => {
                            // GD->LE relaxation:
                            // ldr xd, [gp, xn]   =>   movk R0, #:tprel_g0_nc:var
                            // Where R is x for lp64 mode, and w for ILP32 mode. 
                            slice[0].typ = Rel::R_AARCH64_TLSLE_MOVW_TPREL_G0_NC.into();
                        }
                        Rel::R_AARCH64_TLSDESC_LDR => {
                            // GD->IE relaxation:
                            // ldr xd, [gp, xn]   =>   ldr R0, [gp, xn]
                            // Where R is x for lp64 mode, and w for ILP32 mode. 
                            slice[0].typ = Rel::R_AARCH64_NONE.into();
                        }
                        Rel::R_AARCH64_TLSDESC_OFF_G0_NC if Match::MovkLsl16.matches(read(slice[0].address))  => {
                            // GD->LE relaxation:
                            // movk xd, #:tlsdesc_off_g0_nc:var => movk R0, #:tprel_g1_nc:var, lsl #16
                            // Where R is x for lp64 mode, and w for ILP32 mode. 
                            slice[0].typ = Rel::R_AARCH64_TLSLE_MOVW_TPREL_G1_NC.into();
                        }
                        Rel::R_AARCH64_TLSDESC_OFF_G0_NC => {
                            // GD->IE relaxation:
                            // movk xd, #:tlsdesc_off_g0_nc:var => movk Rd, #:gottprel_g0_nc:var
                            // Where R is x for lp64 mode, and w for ILP32 mode.
                            slice[0].typ = Rel::R_AARCH64_TLSIE_MOVW_GOTTPREL_G0_NC.into();
                        }
                        Rel::R_AARCH64_TLSDESC_OFF_G1 if Match::MovzLsl32.matches(read(slice[0].address))  => {
                            // GD->LE relaxation:
                            // movz xd, #:tlsdesc_off_g1:var => movz R0, #:tprel_g2:var, lsl #32
                            // Where R is x for lp64 mode, and w for ILP32 mode. 
                            slice[0].typ = Rel::R_AARCH64_TLSLE_MOVW_TPREL_G2.into();
                        }
                        Rel::R_AARCH64_TLSDESC_OFF_G1 if Match::MovzLsl16.matches(read(slice[0].address))   => {
                            // GD->IE relaxation:
                            // movz xd, #:tlsdesc_off_g1:var => movz Rd, #:gottprel_g1:var, lsl #16
                            // Where R is x for lp64 mode, and w for ILP32 mode. 
                            slice[0].typ = Rel::R_AARCH64_TLSIE_MOVW_GOTTPREL_G1.into();
                        }
                        Rel::R_AARCH64_TLSIE_ADR_GOTTPREL_PAGE21 if  Match::Movz.matches(read(slice[0].address))  => {
                            // IE->LE relaxation:
                            // adrp xd, :gottprel:var   =>   movz Rd, :tprel_g1:var
                            // Where R is x for lp64 mode, and w for ILP32 mode. 
                            slice[0].typ = Rel::R_AARCH64_TLSLE_MOVW_TPREL_G1.into();
                        }
                        Rel::R_AARCH64_TLSIE_ADR_GOTTPREL_PAGE21 => {
                            assert!(Match::Adrp.matches(read(slice[0].address)));
                            // no changes
                        }
                        Rel::R_AARCH64_TLSIE_LD64_GOTTPREL_LO12_NC if Match::Movk.matches(read(slice[0].address))  => {
                            // IE->LE relaxation:
                            // ldr  xd, [xm, #:gottprel_lo12:var]   =>   movk Rd, :tprel_g0_nc:var
                            // Where R is x for lp64 mode, and w for ILP32 mode. 
                            slice[0].typ = Rel::R_AARCH64_TLSLE_MOVW_TPREL_G0_NC.into();
                        }
                        Rel::R_AARCH64_TLSIE_LD64_GOTTPREL_LO12_NC => {
                            // do nothing
                            assert!(!Match::Movk.matches(read(slice[0].address)));
                        }
                        Rel::R_AARCH64_TLSLD_ADR_PREL21 if Match::Mrs.matches(read(slice[0].address)) => {
                            // LD->LE relaxation (tiny):
                            // adr  x0, :tlsldm:x  => mrs x0, tpidr_el0
                            // bl   __tls_get_addr => add R0, R0, TCB_SIZE
                            // Where R is x for lp64 mode, and w for ilp32 mode.
                            assert_eq!(slice[0].address.wrapping_add(4), slice[1].address);
                            assert_eq!(slice[1].typ, Rel::R_AARCH64_CALL26.into());
                            slice[0].typ = Rel::R_AARCH64_NONE.into();
                            slice[1].typ = Rel::R_AARCH64_NONE.into();
                        }
                        Rel::R_AARCH64_TLSLD_ADR_PREL21 => {
                            // do nothing
                            assert!(Match::Adr.matches(read(slice[0].address)));
                        }
                        Rel::R_AARCH64_TLSLD_ADR_PAGE21 if  Match::Mrs.matches(read(slice[0].address))  => {
                            // LD->LE relaxation (small):
                            // adrp  x0, :tlsldm:x       => mrs x0, tpidr_el0
                            slice[0].typ = Rel::R_AARCH64_NONE.into();
                        }
                        Rel::R_AARCH64_TLSLD_ADR_PAGE21 => {
                            // do nothing
                        }
                        Rel::R_AARCH64_TLSLD_ADD_LO12_NC if Match::AddR0R0.matches(read(slice[0].address)) => {
                            // LD->LE relaxation (small):
                            // add   x0, #:tlsldm_lo12:x => add R0, R0, TCB_SIZE
                            // bl   __tls_get_addr       => nop
                            // Where R is x for lp64 mode, and w for ilp32 mode. 
                            assert_eq!(slice[0].address.wrapping_add(4), slice[1].address);
                            assert_eq!(slice[1].typ, Rel::R_AARCH64_CALL26.into());
                            slice[0].typ = Rel::R_AARCH64_NONE.into();
                            slice[1].typ = Rel::R_AARCH64_NONE.into();
                        }
                        Rel::R_AARCH64_TLSLD_ADD_LO12_NC => {
                            // do nothing
                        }
                        _ => {
                            // not impacted by relaxations
                        }
                    }
                });

            record
                .relocs
                .retain(|r| r.typ != Rel::R_AARCH64_NONE.into());
        }
        Ok(())
    }
}

/// Arm64 Architecture
#[derive(Debug)]
pub struct ArchArm64;

#[allow(unused)]
#[allow(clippy::upper_case_acronyms)]
#[derive(Copy, Clone, Debug)]
enum Instruction {
    MOVW,
    MOVNZ,
    ADR,
    LDST,
    ADD,
    LDLIT,
    TSTBR,
    CONDBR,
    JUMPCALL,
}

impl Instruction {
    #[cfg(test)]
    fn default_sign_extend(&self) -> u32 {
        match self {
            Instruction::ADR => 21,
            Instruction::MOVW => 16,
            Instruction::LDST | Instruction::ADD => 12,
            Instruction::TSTBR => 14,
            Instruction::LDLIT | Instruction::CONDBR => 19,
            Instruction::JUMPCALL => 26,
            i => panic!("Unhandled instruction {:?}", i),
        }
    }
    fn read_operand(&self, contents: u32) -> u32 {
        match self {
            Instruction::ADR => {
                // ADR is really weird: the lowest two bits of the result
                // are encoded in bits 29-30 of the instruction
                ((contents & 0xffffe0) >> 3) | ((contents >> 29) & 3)
            }
            Instruction::MOVW => (contents & 0x1fffe0) >> 5, // 16/5
            Instruction::LDST | Instruction::ADD => (contents & 0x3ffc00) >> 10, // 12/10
            Instruction::TSTBR => (contents & 0x7ffe0) >> 5, // 14/5
            Instruction::LDLIT | Instruction::CONDBR => (contents & 0xffffe0) >> 5, // 19/5
            Instruction::JUMPCALL => contents & 0x3ffffff,   // 26/0
            i => panic!("Unhandled read operand {:?}", i),
        }
    }

    fn write_operand(&self, mut contents: u32, mut new_value: i64, shift: i64) -> u32 {
        new_value >>= shift;
        match self {
            Self::MOVW => {
                contents &= !0x1fffe0;
                contents |= ((new_value & 0xffff) as u32) << 5;
            }
            Self::MOVNZ => {
                // FIXME: implement this correctly
                if false {
                    contents &= !0x601fffe0;
                    if new_value < 0 {
                        // X < 0 => MOVN instruction, negate bits
                        new_value = !new_value;
                    } else {
                        // X >= 0 => MOVZ instruction
                        contents |= 0x2 << 29;
                    }
                    contents |= (((new_value >> shift) & 0xffff) as u32) << 5;
                } else {
                    panic!("MOVNX unimplemented!");
                }
            }
            Self::ADR => {
                assert_delta_size(21, new_value);
                contents &= !0x60ffffe0;
                contents |= (((new_value & 0x3) << 29) | ((new_value & 0x1ffffc) << 3)) as u32;
            }
            Self::LDST | Self::ADD => {
                contents &= !0x3ffc00;
                contents |= ((new_value & 0xfff) << 10) as u32;
            }
            Self::TSTBR => {
                assert_delta_size(14, new_value);
                contents &= !0x7ffe0;
                contents |= ((new_value & 0x3fff) << 5) as u32;
            }
            Self::LDLIT | Self::CONDBR => {
                assert_delta_size(19, new_value);
                contents &= !0xffffe0;
                contents |= ((new_value & 0x7ffff) << 5) as u32;
            }
            Self::JUMPCALL => {
                assert_delta_size(26, new_value);
                contents &= !0x3ffffff;
                contents |= (new_value & 0x3ffffff) as u32;
            }
        }

        contents
    }

    fn write_insn_operand<T>(
        &self,
        builder: &mut T,
        addr: TargetAddr<LocalUsizePtrTy>,
        new_value: i64,
        shift: i64,
    ) -> Result<(), TargetError>
    where
        T: TargetRead<LocalUsizePtrTy> + TargetWrite<LocalUsizePtrTy>,
    {
        #[derive(Copy, Clone)]
        struct Insns([u32; 4]);
        impl<'a> scroll::ctx::TryFromCtx<'a, scroll::Endian, [u8]> for Insns {
            type Error = scroll::Error;
            fn try_from_ctx(
                src: &'a [u8],
                le: scroll::Endian,
            ) -> Result<(Self, usize), Self::Error> {
                use scroll::Pread;
                let offset = &mut 0;
                let u0 = src.gread_with(offset, le)?;
                let u1 = src.gread_with(offset, le)?;
                let u2 = src.gread_with(offset, le)?;
                let u3 = src.gread_with(offset, le)?;
                Ok((Insns([u0, u1, u2, u3]), *offset))
            }
        }

        let contents = builder.read_vaddr(addr)?;
        builder.write_vaddr(addr, self.write_operand(contents, new_value, shift))?;

        let ptr = builder.read_vaddr::<Insns>(addr)?.0;

        if addr.mask(0xfff).absolute_addr() >= 0xff8
            && Match::Adrp.matches(ptr[0])
            && Match::LdSt.matches(ptr[1])
            && (Match::LdStUimm.matches(ptr[2]) || Match::LdStUimm.matches(ptr[3]))
        {
            // TODO: We may have an 843419 erratum
            //log::debug(1, "Warning: erratum 843419 violation at %p\n", at_ptr);
        }
        Ok(())
    }
}

const PAGE_MASK: LocalUsizePtrTy = !0xfff;

fn page_address(val: u64) -> u64 {
    val & !0xfff
}

fn force_adrp(mut contents: u32) -> u32 {
    contents &= 0x60ffffff;
    contents |= 0x90000000;
    contents
}

impl ArchUtil for ArchArm64 {
    const POINTER_RELOC: u32 = goblin::elf::reloc::R_AARCH64_ABS64;
    const COPY_RELOC: u32 = goblin::elf::reloc::R_AARCH64_COPY;

    fn is_one_byte_nop(_maybe_nop: u8) -> bool {
        false
    }

    fn fill_nops(fill: &mut [u8]) {
        fill.fill(0);
    }
}

impl Parseable for ArchArm64 {
    type ParserArch = trap_parser::Aarch64;
}

impl<T, R> Architecture<T, R> for ArchArm64
where
    T: TargetRead<LocalUsizePtrTy> + TargetWrite<LocalUsizePtrTy>,
    R: Relocation<LocalUsizePtrTy>,
{
    type UsizePtrTy = LocalUsizePtrTy;
    type UIntPtrTy = u64;
    type IntPtrTy = i64;

    fn reloc_type_bits() -> usize {
        11
    }

    fn get_target_ptr(reloc: &R, builder: &T) -> Option<TargetAddr<Self::UsizePtrTy>> {
        let get_addend = || reloc.addend().map(|a| a.generic()).unwrap_or(0);
        let contents = builder.read_vaddr::<u32>(reloc.address()).unwrap();

        match u32::from(reloc.typ()) {
            Rel::R_AARCH64_ABS32 => Some(contents.into()),

            Rel::R_AARCH64_ABS64 => {
                Some(builder.read_vaddr::<u64>(reloc.address()).unwrap().into())
            }

            Rel::R_AARCH64_PREL32 => Some(
                reloc
                    .orig_address()
                    .wrapping_add(i64::from(contents as i32) as u64)
                    .wrapping_sub(get_addend() as u64),
            ),

            Rel::R_AARCH64_PREL64 => Some(
                reloc
                    .orig_address()
                    .wrapping_add(builder.read_vaddr::<i64>(reloc.address()).unwrap() as u64)
                    .wrapping_sub(get_addend() as u64),
            ),

            Rel::R_AARCH64_MOVW_UABS_G0
            | Rel::R_AARCH64_MOVW_UABS_G0_NC
            | Rel::R_AARCH64_MOVW_UABS_G1
            | Rel::R_AARCH64_MOVW_UABS_G1_NC
            | Rel::R_AARCH64_MOVW_UABS_G2
            | Rel::R_AARCH64_MOVW_UABS_G2_NC
            | Rel::R_AARCH64_MOVW_UABS_G3
            | Rel::R_AARCH64_ADR_PREL_PG_HI21
            | Rel::R_AARCH64_ADR_PREL_PG_HI21_NC
            | Rel::R_AARCH64_ADD_ABS_LO12_NC
            | Rel::R_AARCH64_LDST8_ABS_LO12_NC
            | Rel::R_AARCH64_LDST16_ABS_LO12_NC
            | Rel::R_AARCH64_LDST32_ABS_LO12_NC
            | Rel::R_AARCH64_LDST64_ABS_LO12_NC
            | Rel::R_AARCH64_LDST128_ABS_LO12_NC => Some(
                reloc
                    .symbol_address()
                    .expect("MOVW, ADR, and LDST must have a symbol pointer"),
            ),

            Rel::R_AARCH64_LD_PREL_LO19
            | Rel::R_AARCH64_GOT_LD_PREL19
            | Rel::R_AARCH64_TLSLD_LD_PREL19
            | Rel::R_AARCH64_TLSIE_LD_GOTTPREL_PREL19
            | Rel::R_AARCH64_TLSDESC_LD_PREL19 => {
                let imm = Instruction::LDLIT.read_operand(contents) << 2;
                Some(
                    reloc
                        .orig_address()
                        .wrapping_add((imm).sign_extend(21) as u64),
                )
            }

            Rel::R_AARCH64_ADR_PREL_LO21
            | Rel::R_AARCH64_TLSGD_ADR_PREL21
            | Rel::R_AARCH64_TLSLD_ADR_PREL21
            | Rel::R_AARCH64_TLSDESC_ADR_PREL21 => {
                let imm = Instruction::ADR.read_operand(contents);
                Some(
                    reloc
                        .orig_address()
                        .wrapping_add(imm.sign_extend(21) as u64),
                )
            }

            Rel::R_AARCH64_TSTBR14 => {
                let imm = Instruction::TSTBR.read_operand(contents) << 2;
                Some(
                    reloc
                        .orig_address()
                        .wrapping_add(imm.sign_extend(16) as u64),
                )
            }

            Rel::R_AARCH64_CONDBR19 => {
                let imm = Instruction::CONDBR.read_operand(contents) << 2;
                Some(
                    reloc
                        .orig_address()
                        .wrapping_add(imm.sign_extend(21) as u64),
                )
            }
            Rel::R_AARCH64_JUMP26 | Rel::R_AARCH64_CALL26 => {
                let imm = Instruction::JUMPCALL.read_operand(contents) << 2;
                Some(
                    reloc
                        .orig_address()
                        .wrapping_add(imm.sign_extend(28) as u64),
                )
            }

            Rel::R_AARCH64_GOTREL64 => Some(
                builder
                    .got_address()?
                    .wrapping_add(builder.read_vaddr::<i64>(reloc.address()).unwrap() as u64),
            ),

            Rel::R_AARCH64_GOTREL32 => {
                // We need to use the original address as the source here (not
                // the diversified one) to keep in consistent with the original
                // relocation entry (before shuffling)
                Some(
                    builder
                        .got_address()?
                        .wrapping_add(i64::from(contents as i32) as u64),
                )
            }

            Rel::R_AARCH64_ADR_GOT_PAGE
            | Rel::R_AARCH64_TLSGD_ADR_PAGE21
            | Rel::R_AARCH64_TLSLD_ADR_PAGE21
            | Rel::R_AARCH64_TLSIE_ADR_GOTTPREL_PAGE21
            | Rel::R_AARCH64_TLSDESC_ADR_PAGE21 => {
                // We have to account for bfd&gold's fixes for erratum 843419:
                // when an ADRP instruction falls on the last few bytes of a page,
                // the linker replaces it with an ADR
                assert!(Match::Adrp.matches(contents));
                let delta = Instruction::ADR.read_operand(contents).sign_extend(21) << 12;
                let base = page_address(reloc.orig_address().into());
                Some(base.wrapping_add(delta as i64 as u64).into())
            }

            // We don't care about these here, but we need them for the GOT
            Rel::R_AARCH64_MOVW_GOTOFF_G0
            | Rel::R_AARCH64_MOVW_GOTOFF_G0_NC
            | Rel::R_AARCH64_MOVW_GOTOFF_G1
            | Rel::R_AARCH64_MOVW_GOTOFF_G1_NC
            | Rel::R_AARCH64_MOVW_GOTOFF_G2
            | Rel::R_AARCH64_MOVW_GOTOFF_G2_NC
            | Rel::R_AARCH64_MOVW_GOTOFF_G3
            | Rel::R_AARCH64_LD64_GOTOFF_LO15
            | Rel::R_AARCH64_LD64_GOT_LO12_NC
            | Rel::R_AARCH64_LD64_GOTPAGE_LO15 => None,

            _ => {
                //info!("Unimplemented relocation | {}", reloc.typ);
                None
            }
        }
    }

    fn set_target_ptr(
        reloc: &R,
        builder: &mut T,
        new_target: TargetAddr<Self::UsizePtrTy>,
    ) -> Result<(), ArchError> {
        let addend: i64 = generic!(reloc.addend());
        let pcrel_delta: i64 = TargetAddr::offset(&new_target, &reloc.address()) as i64;
        let addend_pcrel_delta = pcrel_delta.wrapping_add(addend);
        let pcrel_page_delta = (page_address(new_target.into())
            .wrapping_sub(page_address(reloc.address().into())))
            as i64;
        let addend_pcrel_page_delta = (page_address(new_target.wrapping_add(addend as u64).into())
            as i64)
            .wrapping_sub(page_address(reloc.address().into()) as i64);

        match u32::from(reloc.typ()) {
            Rel::R_AARCH64_ABS32 => {
                builder.write_vaddr::<u32>(reloc.address(), u64::from(new_target) as u32)
            }
            Rel::R_AARCH64_ABS64 => builder.write_vaddr::<u64>(reloc.address(), new_target.into()),
            Rel::R_AARCH64_PREL32 => {
                assert_delta_size(32, addend_pcrel_delta);
                builder.write_vaddr::<i32>(reloc.address(), addend_pcrel_delta as i32)
            }
            Rel::R_AARCH64_PREL64 => {
                builder.write_vaddr::<i64>(reloc.address(), addend_pcrel_delta)
            }
            Rel::R_AARCH64_MOVW_UABS_G0 | Rel::R_AARCH64_MOVW_UABS_G0_NC => Instruction::MOVW
                .write_insn_operand(builder, reloc.address(), u64::from(new_target) as i64, 0),
            Rel::R_AARCH64_MOVW_UABS_G2 | Rel::R_AARCH64_MOVW_UABS_G2_NC => Instruction::MOVW
                .write_insn_operand(builder, reloc.address(), u64::from(new_target) as i64, 32),
            Rel::R_AARCH64_MOVW_UABS_G3 => Instruction::MOVW.write_insn_operand(
                builder,
                reloc.address(),
                u64::from(new_target) as i64,
                48,
            ),
            Rel::R_AARCH64_LD_PREL_LO19
            | Rel::R_AARCH64_GOT_LD_PREL19
            | Rel::R_AARCH64_TLSLD_LD_PREL19
            | Rel::R_AARCH64_TLSIE_LD_GOTTPREL_PREL19
            | Rel::R_AARCH64_TLSDESC_LD_PREL19 => {
                let write =
                    Instruction::LDLIT.write_insn_operand(builder, reloc.address(), pcrel_delta, 2);
                assert_eq!(
                    pcrel_delta,
                    ((Instruction::LDLIT.read_operand(builder.read_vaddr(reloc.address())?) << 2)
                        as u64)
                        .sign_extend(21)
                );

                write
            }

            Rel::R_AARCH64_ADR_PREL_LO21
            | Rel::R_AARCH64_TLSGD_ADR_PREL21
            | Rel::R_AARCH64_TLSLD_ADR_PREL21
            | Rel::R_AARCH64_TLSDESC_ADR_PREL21 => {
                let write =
                    Instruction::ADR.write_insn_operand(builder, reloc.address(), pcrel_delta, 0);
                assert_eq!(
                    pcrel_delta,
                    ((Instruction::ADR.read_operand(builder.read_vaddr(reloc.address())?)) as u64)
                        .sign_extend(21)
                );

                write
            }
            Rel::R_AARCH64_ADR_PREL_PG_HI21 | Rel::R_AARCH64_ADR_PREL_PG_HI21_NC => {
                assert!(reloc.symbol_address().is_some());
                let write = Instruction::ADR.write_insn_operand(
                    builder,
                    reloc.address(),
                    addend_pcrel_page_delta,
                    12,
                );
                assert_eq!(
                    addend_pcrel_page_delta,
                    (Instruction::ADR.read_operand(builder.read_vaddr(reloc.address())?) as u64)
                        .sign_extend(21)
                        << 12
                );

                write
            }

            Rel::R_AARCH64_ADD_ABS_LO12_NC => {
                let val = u64::from(new_target.wrapping_add(addend as u64)) as u32 & 0xfff;
                let write =
                    Instruction::ADD.write_insn_operand(builder, reloc.address(), val as i64, 0);
                assert_eq!(
                    val,
                    Instruction::ADD.read_operand(builder.read_vaddr(reloc.address())?)
                );

                write
            }
            Rel::R_AARCH64_LDST8_ABS_LO12_NC => {
                assert!(reloc.symbol_address().is_some());
                let val = u64::from(new_target.wrapping_add(addend as u64)) as u32 & 0xfff;
                let write =
                    Instruction::LDST.write_insn_operand(builder, reloc.address(), val as i64, 0);
                assert_eq!(
                    val,
                    Instruction::LDST.read_operand(builder.read_vaddr(reloc.address())?)
                );

                write
            }
            Rel::R_AARCH64_LDST16_ABS_LO12_NC => {
                assert!(reloc.symbol_address().is_some());
                let val = u64::from(new_target.wrapping_add(addend as u64)) as u32 & 0xfff;
                let write =
                    Instruction::LDST.write_insn_operand(builder, reloc.address(), val as i64, 1);
                assert_eq!(
                    val,
                    (Instruction::LDST.read_operand(builder.read_vaddr(reloc.address())?) << 1)
                );

                write
            }

            Rel::R_AARCH64_LDST32_ABS_LO12_NC => {
                assert!(reloc.symbol_address().is_some());
                let val = u64::from(new_target.wrapping_add(addend as u64)) as u32 & 0xfff;
                let write =
                    Instruction::LDST.write_insn_operand(builder, reloc.address(), val as i64, 2);
                assert_eq!(
                    val,
                    (Instruction::LDST.read_operand(builder.read_vaddr(reloc.address())?) << 2)
                );

                write
            }
            Rel::R_AARCH64_LDST64_ABS_LO12_NC => {
                assert!(reloc.symbol_address().is_some());
                let val = u64::from(new_target.wrapping_add(addend as u64)) as u32 & 0xfff;
                let write =
                    Instruction::LDST.write_insn_operand(builder, reloc.address(), val as i64, 3);
                assert_eq!(
                    val,
                    (Instruction::LDST.read_operand(builder.read_vaddr(reloc.address())?) << 3)
                );

                write
            }
            Rel::R_AARCH64_LDST128_ABS_LO12_NC => {
                assert!(reloc.symbol_address().is_some());
                let val = u64::from(new_target.wrapping_add(addend as u64)) as u32 & 0xfff;
                let write =
                    Instruction::LDST.write_insn_operand(builder, reloc.address(), val as i64, 4);
                assert_eq!(
                    val,
                    (Instruction::LDST.read_operand(builder.read_vaddr(reloc.address())?) << 4)
                );

                write
            }
            Rel::R_AARCH64_TSTBR14 => {
                // FIXME: handle overflow (see below)
                let write =
                    Instruction::TSTBR.write_insn_operand(builder, reloc.address(), pcrel_delta, 2);
                assert_eq!(
                    pcrel_delta,
                    ((Instruction::TSTBR.read_operand(builder.read_vaddr(reloc.address())?) << 2)
                        as u64)
                        .sign_extend(16)
                );

                write
            }
            Rel::R_AARCH64_CONDBR19 => {
                // FIXME: sometimes, our function shuffling pushes functions
                // out of each other's CONDBR19 range
                let write = Instruction::CONDBR.write_insn_operand(
                    builder,
                    reloc.address(),
                    pcrel_delta,
                    2,
                );
                assert_eq!(
                    pcrel_delta,
                    ((Instruction::CONDBR.read_operand(builder.read_vaddr(reloc.address())?) << 2)
                        as u64)
                        .sign_extend(21)
                );
                write
            }

            Rel::R_AARCH64_JUMP26 | Rel::R_AARCH64_CALL26 => {
                // FIXME: sometimes, our function shuffling pushes functions
                // out of each other's CONDBR19 range
                let write = Instruction::JUMPCALL.write_insn_operand(
                    builder,
                    reloc.address(),
                    pcrel_delta,
                    2,
                );
                assert_eq!(
                    pcrel_delta,
                    ((Instruction::JUMPCALL.read_operand(builder.read_vaddr(reloc.address())?) << 2)
                        as u64)
                        .sign_extend(28)
                );
                write
            }
            Rel::R_AARCH64_GOTREL64 => builder.write_vaddr::<u64>(
                reloc.address(),
                new_target
                    .wrapping_sub(builder.got_address().ok_or(ArchError::NoGOT)?)
                    .into(),
            ),
            Rel::R_AARCH64_GOTREL32 => {
                let val = u64::from(
                    new_target.wrapping_sub(builder.got_address().ok_or(ArchError::NoGOT)?),
                ) as i64;
                assert_delta_size(32, val);

                builder.write_vaddr::<u32>(reloc.address(), val as u32)
            }
            Rel::R_AARCH64_ADR_GOT_PAGE
            | Rel::R_AARCH64_TLSGD_ADR_PAGE21
            | Rel::R_AARCH64_TLSLD_ADR_PAGE21
            | Rel::R_AARCH64_TLSIE_ADR_GOTTPREL_PAGE21
            | Rel::R_AARCH64_TLSDESC_ADR_PAGE21 => {
                // FIXME: should we implement our own fix for 843419???
                // FIXME: for now, we force every ADR into an ADRP
                let contents = builder.read_vaddr::<u32>(reloc.address())?;
                builder.write_vaddr(reloc.address(), force_adrp(contents))?;
                let write = Instruction::ADR.write_insn_operand(
                    builder,
                    reloc.address(),
                    pcrel_page_delta as i64,
                    12,
                );
                assert_eq!(
                    pcrel_page_delta,
                    ((Instruction::ADR.read_operand(builder.read_vaddr(reloc.address())?)) as u64)
                        .sign_extend(21)
                        << 12
                );

                write
            }
            Rel::R_AARCH64_LD64_GOTOFF_LO15
            | Rel::R_AARCH64_LD64_GOT_LO12_NC
            | Rel::R_AARCH64_LD64_GOTPAGE_LO15
            | Rel::R_AARCH64_TLSIE_LD64_GOTTPREL_LO12_NC => {
                // These are safe to ignore
                Ok(())
            }
            _ => return Err(ArchError::Unimplemented),
        }?;

        Ok(())
    }

    fn get_got_entry(
        reloc: &R,
        build: &T,
    ) -> Result<Option<TargetAddr<Self::UsizePtrTy>>, ArchError> {
        let entry = match u32::from(reloc.typ()) {
            Rel::R_AARCH64_ADR_GOT_PAGE => {
                assert!(reloc.symbol_data().is_some());
                // We have to account for bfd&gold's fixes for erratum 843419:
                // when an ADRP instruction falls on the last few bytes of a page,
                // the linker replaces it with an ADR
                let mut base = reloc.orig_address();
                let contents = build.read_vaddr(reloc.address())?;
                let mut delta = Instruction::ADR.read_operand(contents).sign_extend(21);
                if Match::Adrp.matches(contents) {
                    base = base.mask(PAGE_MASK);
                    delta <<= 12;
                }
                assert_eq!(
                    (base.wrapping_add(i64::from(delta) as u64)).mask(0xfff),
                    TargetAddr::from(0u64)
                );
                //FIXME: assumes little endian
                let sym_val = reloc.symbol_data().unwrap();
                delta += (Instruction::LDST.read_operand(sym_val) << 3) as i32;

                Some(base.wrapping_add(i64::from(delta) as u64))
            }
            Rel::R_AARCH64_MOVW_GOTOFF_G0 | Rel::R_AARCH64_MOVW_GOTOFF_G0_NC => {
                assert!(reloc.symbol_data().is_some());
                // Decode the GOT entry from the G0 encoded inside this instruction,
                // plus the G1-G3 values encoded inside Trap info
                let contents = build.read_vaddr(reloc.address())?;
                let g0: u64 = Instruction::MOVW.read_operand(contents).into();
                // G1 is encoded inside m_symbol_ptr
                // FIXME: assumes little-endian
                let g1: u64 = Instruction::MOVW
                    .read_operand(reloc.symbol_data().unwrap())
                    .into();

                // G2 and G3 are encoded inside m_addend as 2 32-bit values
                let addend = reloc.addend();
                let (lo, hi) = match addend {
                    Some(TrapRelocAddend::Instruction(lo, hi)) => (lo, hi),
                    _ => panic!("Bad trap entry for R_AARCH64_MOVW_GOTOFF_G0"),
                };
                let g2: u64 = Instruction::MOVW.read_operand(lo).into();

                let g3: u64 = Instruction::MOVW.read_operand(hi).into();

                Some((g0 | (g1 << 16) | (g2 << 32) | (g3 << 48)).into())
            }
            Rel::R_AARCH64_LD64_GOT_LO12_NC
            | Rel::R_AARCH64_MOVW_GOTOFF_G1
            | Rel::R_AARCH64_MOVW_GOTOFF_G1_NC
            | Rel::R_AARCH64_MOVW_GOTOFF_G2
            | Rel::R_AARCH64_MOVW_GOTOFF_G2_NC
            | Rel::R_AARCH64_MOVW_GOTOFF_G3 => None,

            Rel::R_AARCH64_GOT_LD_PREL19 => {
                let contents = build.read_vaddr(reloc.address())?;
                Some(reloc.orig_address().wrapping_add(
                    (Instruction::LDLIT.read_operand(contents) << 2).sign_extend(21) as u64,
                ))
            }
            Rel::R_AARCH64_LD64_GOTOFF_LO15 => {
                let contents = build.read_vaddr(reloc.address())?;
                Some(
                    build
                        .got_address()
                        .ok_or(ArchError::NoGOT)?
                        .wrapping_add(Instruction::LDST.read_operand(contents) << 3),
                )
            }
            Rel::R_AARCH64_LD64_GOTPAGE_LO15 => {
                let contents = build.read_vaddr(reloc.address())?;
                let delta = Instruction::LDST.read_operand(contents) << 3;
                let addr = build
                    .got_address()
                    .ok_or(ArchError::NoGOT)?
                    .mask(PAGE_MASK)
                    .wrapping_add(i64::from(delta) as u64);
                Some(addr)
            }
            Rel::R_AARCH64_TLSIE_LD64_GOTTPREL_LO12_NC => {
                //Warning: R_AARCH64_TLSIE_LD64_GOTTPREL_LO12_NC"
                None
            }
            _ => None,
        };

        Ok(entry)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::test::{fake_mem_strategy, verify_get_set_target_ptr, TestReloc};
    use crate::{RelocationIdx, TrapRelocAddend};
    use proptest::prelude::*;

    fn arm64_reloc_num() -> impl Strategy<Value = u32> {
        use goblin::elf::reloc as R;
        prop_oneof!(
            Just(R::R_AARCH64_P32_ABS32),
            R::R_AARCH64_P32_COPY..=R::R_AARCH64_P32_IRELATIVE,
            R::R_AARCH64_ABS64..=R::R_AARCH64_LD64_GOTPAGE_LO15,
            R::R_AARCH64_TLSGD_ADR_PREL21..=R::R_AARCH64_TLSLD_LDST128_DTPREL_LO12_NC,
            R::R_AARCH64_COPY..=R::R_AARCH64_IRELATIVE
        )
        .prop_filter("Needs symbol", |v| {
            !matches!(
                *v,
                R::R_AARCH64_MOVW_UABS_G0
                    | R::R_AARCH64_MOVW_UABS_G0_NC
                    | R::R_AARCH64_MOVW_UABS_G1
                    | R::R_AARCH64_MOVW_UABS_G1_NC
                    | R::R_AARCH64_MOVW_UABS_G2
                    | R::R_AARCH64_MOVW_UABS_G2_NC
                    | R::R_AARCH64_MOVW_UABS_G3
                    | R::R_AARCH64_ADR_PREL_PG_HI21
                    | R::R_AARCH64_ADR_PREL_PG_HI21_NC
                    | R::R_AARCH64_ADD_ABS_LO12_NC
                    | R::R_AARCH64_LDST8_ABS_LO12_NC
                    | R::R_AARCH64_LDST16_ABS_LO12_NC
                    | R::R_AARCH64_LDST32_ABS_LO12_NC
                    | R::R_AARCH64_LDST64_ABS_LO12_NC
                    | R::R_AARCH64_LDST128_ABS_LO12_NC
            )
        })
        .prop_filter("Forces adrp", |v| {
            !matches!(
                *v,
                R::R_AARCH64_ADR_GOT_PAGE
                    | R::R_AARCH64_TLSGD_ADR_PAGE21
                    | R::R_AARCH64_TLSLD_ADR_PAGE21
                    | R::R_AARCH64_TLSIE_ADR_GOTTPREL_PAGE21
                    | R::R_AARCH64_TLSDESC_ADR_PAGE21
            )
        })
    }

    fn interesting_reloc_prefix() -> impl Strategy<Value = Vec<u8>> {
        prop_oneof!(prop::collection::vec(any::<u8>(), 10))
    }

    fn instrs() -> impl Strategy<Value = Instruction> {
        use Instruction as I;

        prop_oneof!(
            Just(I::MOVW),
            //Just(I::MOVNZ), //unimplemented instruction
            Just(I::ADR),
            Just(I::LDST),
            Just(I::ADD),
            Just(I::LDLIT),
            Just(I::TSTBR),
            Just(I::CONDBR),
            Just(I::JUMPCALL),
        )
    }
    proptest! {
        #[test]
        fn insn_operand_roundtrip(instr in instrs(),
                        contents in any::<u32>(),

        ) {
            let read = instr.read_operand(contents).sign_extend(instr.default_sign_extend());
            let written = instr.write_operand(contents, read.into(), 0);
            assert_eq!(contents, written);
        }

        #[test]
        fn test_reloc(
            reloc in arm64_reloc_num(),
            mem in fake_mem_strategy(interesting_reloc_prefix()),
            addend in any::<i32>(),
        ) {
            let reloc = TestReloc::new(
                RelocationIdx(reloc),
                mem.reloc_addr(),
                None,
                Some(TrapRelocAddend::Generic(addend as i64)),
            );

            verify_get_set_target_ptr::<ArchArm64, _>(reloc, mem);
        }
    }
}
