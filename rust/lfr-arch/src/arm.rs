// Copyright (c) 2022 RunSafe Security Inc.

use super::{bit, SignExtend};
use crate::{
    assert_delta_size, ArchError, ArchUtil, Architecture, Parseable, Relocation, TargetRead,
    TargetWrite,
};
use core::convert::TryInto;
use modinfo::TargetAddr;
#[cfg(feature = "trap_relax")]
use trap_parser::TrapAddress;

use goblin::elf::reloc as Rel;

#[cfg(target_pointer_width = "32")]
type LocalUsizePtrTy = usize;

#[cfg(not(target_pointer_width = "32"))]
type LocalUsizePtrTy = u32;

/// Arm Architecture
#[derive(Debug)]
pub struct ArchArm;

#[cfg(feature = "trap_relax")]
impl<T> crate::Relax<T> for ArchArm
where
    T: TargetRead<LocalUsizePtrTy>,
{
    type UsizePtrTy = LocalUsizePtrTy;
    fn apply_relax<F>(
        _trapv1: &mut trap_parser::write::TRaPV1Builder,
        _target: &T,
        _f: F,
    ) -> Result<(), crate::RelaxError>
    where
        F: Fn(TrapAddress) -> TargetAddr<LocalUsizePtrTy>,
    {
        unimplemented!()
    }
}

fn bit_select<X, Y, Z, T>(mask: Z, x: X, y: Y) -> T
where
    X: Into<T>,
    Y: Into<T>,
    Z: Into<T> + Copy,
    T: core::ops::BitAnd<Output = T>
        + core::ops::BitAnd<Output = T>
        + core::ops::Not<Output = T>
        + core::ops::BitOr<Output = T>
        + Copy,
{
    (x.into() & !mask.into()) | (y.into() & mask.into())
}

fn arm_bl_is_x(ins: u32) -> bool {
    (ins & 0xfe000000) == 0xfa000000
}

fn thm_bl_is_x(ins: u32) -> bool {
    !bit(ins, 28)
}

fn imm24_get(ins: u32) -> i32 {
    ((ins & 0xffffff) << 2).sign_extend(26)
}

fn imm24_set(ins: u32, dest: i32) -> u32 {
    bit_select(0xffffffu32, ins, ((dest & 0x3ffffff) >> 2) as u32)
}

/// Get the immediate operand from an Branch Encoding T1 instruction
fn b_t1_get(ins: u16) -> i32 {
    ((ins as u32 & 0xff) << 1).sign_extend(9)
}

fn b_t1_set(ins: u16, dest: i32) -> u16 {
    bit_select(0xffu16, ins, ((dest & 0x1fe) >> 1) as u16)
}

/// Get the immediate operand from an Branch Encoding T2 instruction
fn b_t2_get(ins: u16) -> i32 {
    ((ins as u32 & 0x7ff) << 1).sign_extend(12)
}

fn b_t2_set(ins: u16, dest: i32) -> u16 {
    bit_select(0x7ffu16, ins, ((dest & 0xffe) >> 1) as u16)
}

fn thm_imm24_get(ins: u32) -> i32 {
    let s = bit(ins, 10);
    let i1 = !(bit(ins, 29) ^ s);
    let i2 = !(bit(ins, 27) ^ s);
    let value = (u32::from(s) << 24)
        | (u32::from(i1) << 23)
        | (u32::from(i2) << 22)
        | ((ins & 0x3ff) << 12)
        | ((ins & 0x7ff0000) >> 15);
    (value).sign_extend(25)
}

fn thm_imm24_set(ins: u32, dest: i32) -> u32 {
    let imm11 = (dest >> 1) & 0x7ff;
    let imm10 = (dest >> 12) & 0x3ff;
    let i2 = bit(dest, 22);
    let i1 = bit(dest, 23);
    let s = bit(dest, 24);
    let j1 = !i1 ^ s;
    let j2 = !i2 ^ s;
    bit_select(
        0x2fff07ffu32,
        ins,
        (u32::from(s) << 10)
            | (u32::from(j1) << 29)
            | (u32::from(j2) << 27)
            | imm10 as u32
            | ((imm11 as u32) << 16),
    )
}

fn thm_imm19_get(ins: u32) -> i32 {
    let s = bit(ins, 10);
    let j1 = bit(ins, 29);
    let j2 = bit(ins, 27);
    let value = (u32::from(s) << 20)
        | (u32::from(j2) << 19)
        | (u32::from(j1) << 18)
        | ((ins & 0x3f) << 12)
        | ((ins & 0x7ff0000) >> 15);
    (value).sign_extend(21)
}

fn thm_imm19_set(ins: u32, dest: i32) -> u32 {
    let imm11 = (dest >> 1) & 0x7ff;
    let imm6 = (dest >> 12) & 0x3f;
    let s = bit(dest, 20);
    let j2 = bit(dest, 19);
    let j1 = bit(dest, 18);
    bit_select(
        0x2fff043fu32,
        ins,
        (u32::from(s) << 10)
            | (u32::from(j1) << 29)
            | (u32::from(j2) << 27)
            | imm6 as u32
            | ((imm11 as u32) << 16),
    )
}

fn movwt_set(mut ins: u32, mut imm: u32) -> u32 {
    imm &= 0xffff;
    ins &= 0xfff0f000;
    ins |= imm & 0x0fff;
    ins |= (imm & 0xf000) << 4;
    ins
}

fn thm_movwt_set(mut ins: u32, mut imm: u32) -> u32 {
    // FIXME: this assumes little-endian
    imm &= 0xffff;
    ins &= 0x8f00fbf0;
    ins |= (imm & 0xf000) >> 12;
    ins |= (imm & 0x0800) >> 1;
    ins |= (imm & 0x0700) << 20;
    ins |= (imm & 0x00ff) << 16;
    ins
}

impl ArchUtil for ArchArm {
    const POINTER_RELOC: u32 = goblin::elf::reloc::R_ARM_ABS32;
    const COPY_RELOC: u32 = goblin::elf::reloc::R_ARM_COPY;

    fn is_one_byte_nop(_maybe_nop: u8) -> bool {
        false
    }

    fn fill_nops(fill: &mut [u8]) {
        fill.fill(0);
    }
}

impl Parseable for ArchArm {
    type ParserArch = trap_parser::Arm;
}

impl<T, R> Architecture<T, R> for ArchArm
where
    T: TargetRead<LocalUsizePtrTy> + TargetWrite<LocalUsizePtrTy>,
    R: Relocation<LocalUsizePtrTy>,
{
    type UsizePtrTy = LocalUsizePtrTy;
    type UIntPtrTy = u32;
    type IntPtrTy = i32;

    fn get_target_ptr(reloc: &R, builder: &T) -> Option<TargetAddr<Self::UsizePtrTy>> {
        let reloc_contents: u32 = builder.read_vaddr(reloc.address()).ok()?;
        let addend: i32 = generic!(reloc.addend()).try_into().unwrap();
        match u32::from(reloc.typ()) {
            // Data relocs
            Rel::R_ARM_ABS32 | Rel::R_ARM_TARGET1 => {
                // The ARM exception handling ABI says Rel::R_ARM_TARGET1 is equivalent to Rel::R_ARM_ABS32
                Some((reloc_contents & 0xfffffffe).into())
            }

            Rel::R_ARM_REL32
            | Rel::R_ARM_GOT_PREL
            | Rel::R_ARM_TARGET2
            | Rel::R_ARM_TLS_GD32
            | Rel::R_ARM_TLS_LDM32
            | Rel::R_ARM_TLS_IE32
            | Rel::R_ARM_GOTPC => {
                // Rel::R_ARM_GOTPC aka Rel::R_ARM_BASE_PREL
                Some(
                    reloc
                        .orig_address()
                        .wrapping_add(reloc_contents)
                        .wrapping_sub(addend as u32),
                )
            }

            Rel::R_ARM_PREL31 => Some(
                reloc
                    .orig_address()
                    .wrapping_add((reloc_contents & 0x7fffffff).sign_extend(31) as u32)
                    .wrapping_sub(addend as u32),
            ),
            Rel::R_ARM_GOTOFF => Some(
                builder
                    .got_address()?
                    .wrapping_add(reloc_contents)
                    .wrapping_sub(addend as u32),
            ),
            // Instruction relocs
            Rel::R_ARM_CALL if arm_bl_is_x(reloc_contents) => Some(
                reloc
                    .orig_address()
                    .wrapping_add(8u32)
                    .wrapping_add(imm24_get(reloc_contents) as u32)
                    .wrapping_add((reloc_contents >> 23) & 0x2),
            ),

            Rel::R_ARM_CALL | Rel::R_ARM_JUMP24 | Rel::R_ARM_PLT32 => Some(
                reloc
                    .orig_address()
                    .wrapping_add(8u32)
                    .wrapping_add(imm24_get(reloc_contents) as u32),
            ),

            // The linker will translate the instruction at this relocation
            // to either B <veneer> or MOV PC, <reg>
            Rel::R_ARM_V4BX if (reloc_contents & 0x0f000000) == 0x0a000000 => Some(
                reloc
                    .orig_address()
                    .wrapping_add(8u32)
                    .wrapping_add(imm24_get(reloc_contents) as u32),
            ),
            Rel::R_ARM_V4BX => None,

            Rel::R_ARM_THM_PC22 if thm_bl_is_x(reloc_contents) => Some(
                reloc
                    .orig_address()
                    .wrapping_add(4u32)
                    .align_val_mask(4)
                    .wrapping_add(thm_imm24_get(reloc_contents) as u32),
            ),

            Rel::R_ARM_THM_PC22 | Rel::R_ARM_THM_JUMP24 => Some(
                reloc
                    .orig_address()
                    .wrapping_add(4u32)
                    .wrapping_add(thm_imm24_get(reloc_contents) as u32),
            ),

            Rel::R_ARM_THM_JUMP19 => Some(
                reloc
                    .orig_address()
                    .wrapping_add(4u32)
                    .wrapping_add(thm_imm19_get(reloc_contents) as u32),
            ),

            Rel::R_ARM_THM_PC11 => Some(
                reloc
                    .orig_address()
                    .wrapping_add(4u32)
                    .wrapping_add(b_t2_get(builder.read_vaddr(reloc.address()).unwrap()) as u32),
            ),

            Rel::R_ARM_THM_PC9 => Some(
                reloc
                    .orig_address()
                    .wrapping_add(4u32)
                    .wrapping_add(b_t1_get(builder.read_vaddr(reloc.address()).unwrap()) as u32),
            ),

            Rel::R_ARM_MOVW_ABS_NC
            | Rel::R_ARM_MOVT_ABS
            | Rel::R_ARM_THM_MOVW_ABS_NC
            | Rel::R_ARM_THM_MOVT_ABS => {
                let symbol = reloc
                    .symbol_address()
                    .expect("MOVW and MOVT must have a symbol pointer");
                Some(symbol.wrapping_add(addend as u32))
            }

            Rel::R_ARM_MOVW_PREL_NC
            | Rel::R_ARM_MOVT_PREL
            | Rel::R_ARM_THM_MOVW_PREL_NC
            | Rel::R_ARM_THM_MOVT_PREL => {
                let symbol = reloc
                    .symbol_address()
                    .expect("MOVW and MOVT must have a symbol pointer");
                Some(symbol)
            }

            Rel::R_ARM_GOT32 => None,

            _ => {
                //info!("Unimplemented relocation: {}", reloc.typ);
                None
            }
        }
    }

    fn set_target_ptr(
        reloc: &R,
        builder: &mut T,
        new_target: TargetAddr<Self::UsizePtrTy>,
    ) -> Result<(), ArchError> {
        let reloc_contents: u32 = builder.read_vaddr(reloc.address()).unwrap();
        let pcrel_delta: i32 = TargetAddr::offset(&new_target, &reloc.address()) as i32;
        let addend: i32 = generic!(reloc.addend()).try_into().unwrap();

        let addend_pcrel_delta = pcrel_delta.wrapping_add(addend);

        match u32::from(reloc.typ()) {
            Rel::R_ARM_REL32
            | Rel::R_ARM_GOT_PREL
            | Rel::R_ARM_TARGET2
            | Rel::R_ARM_TLS_GD32
            | Rel::R_ARM_TLS_LDM32
            | Rel::R_ARM_TLS_IE32
            | Rel::R_ARM_GOTPC => builder.write_vaddr::<i32>(reloc.address(), addend_pcrel_delta),

            Rel::R_ARM_PREL31 => {
                assert_delta_size(31, addend_pcrel_delta);

                builder.write_vaddr::<u32>(
                    reloc.address(),
                    bit_select(0x7fffffffu32, reloc_contents, addend_pcrel_delta as u32),
                )
            }
            Rel::R_ARM_GOTOFF => builder.write_vaddr::<u32>(
                reloc.address(),
                new_target
                    .wrapping_add(addend as u32)
                    .wrapping_sub(builder.got_address().ok_or(ArchError::NoGOT)?)
                    .into(),
            ),

            Rel::R_ARM_THM_PC11 => {
                let pcrel_delta = pcrel_delta - 4;
                assert_delta_size(12, pcrel_delta);
                builder.write_vaddr::<u16>(
                    reloc.address(),
                    b_t2_set(builder.read_vaddr::<u16>(reloc.address())?, pcrel_delta),
                )
            }
            Rel::R_ARM_THM_PC9 => {
                let pcrel_delta = pcrel_delta - 4;
                assert_delta_size(9, pcrel_delta);
                builder.write_vaddr::<u16>(
                    reloc.address(),
                    b_t1_set(builder.read_vaddr::<u16>(reloc.address())?, pcrel_delta),
                )
            }
            Rel::R_ARM_THM_PC22 if thm_bl_is_x(reloc_contents) => {
                let pcrel_delta =
                    new_target.wrapping_sub(reloc.address().wrapping_add(4u32).align_val_mask(4));
                assert_delta_size(25, u32::from(pcrel_delta) as i32);
                builder.write_vaddr::<u32>(
                    reloc.address(),
                    thm_imm24_set(reloc_contents, u32::from(pcrel_delta) as i32 & 0x01fffffe),
                )
            }

            Rel::R_ARM_THM_PC22 | Rel::R_ARM_THM_JUMP24 => {
                let pcrel_delta = pcrel_delta - 4;
                assert_delta_size(25, pcrel_delta);
                builder.write_vaddr::<u32>(
                    reloc.address(),
                    thm_imm24_set(reloc_contents, pcrel_delta & 0x01fffffe),
                )
            }

            Rel::R_ARM_THM_JUMP19 => {
                let pcrel_delta = pcrel_delta - 4;
                assert_delta_size(21, pcrel_delta);
                builder.write_vaddr::<u32>(
                    reloc.address(),
                    thm_imm19_set(reloc_contents, pcrel_delta & 0x1ffffe),
                )
            }
            // Instruction relocs
            Rel::R_ARM_CALL | Rel::R_ARM_JUMP24 | Rel::R_ARM_PLT32
                if arm_bl_is_x(reloc_contents) =>
            {
                let pcrel_delta = pcrel_delta - 8;
                assert_delta_size(26, pcrel_delta);
                let mut val = imm24_set(reloc_contents, pcrel_delta & 0x03fffffe);
                if u32::from(reloc.typ()) == Rel::R_ARM_CALL {
                    // If the instruction is a BLX direct call, bit 24 of the
                    // instruction encodes bit 1 of the offset
                    if bit(pcrel_delta, 1) {
                        val |= 1 << 24;
                    } else {
                        val &= !(1 << 24);
                    }
                }

                builder.write_vaddr::<u32>(reloc.address(), val)
            }

            Rel::R_ARM_CALL | Rel::R_ARM_JUMP24 | Rel::R_ARM_PLT32 => {
                let pcrel_delta = pcrel_delta - 8;
                assert_delta_size(26, pcrel_delta);
                let val = imm24_set(reloc_contents, pcrel_delta & 0x03fffffe);
                builder.write_vaddr::<u32>(reloc.address(), val)
            }
            Rel::R_ARM_V4BX if (reloc_contents & 0x0f000000) == 0x0a000000 => {
                // B <veneer> instruction => rewrite it
                let pcrel_delta = pcrel_delta - 8;
                assert_delta_size(26, pcrel_delta);
                builder.write_vaddr::<u32>(
                    reloc.address(),
                    imm24_set(reloc_contents, pcrel_delta & 0x03fffffe),
                )
            }
            // Data relocs
            Rel::R_ARM_ABS32 | Rel::R_ARM_TARGET1 => builder.write_vaddr::<u32>(
                reloc.address(),
                bit_select(0xfffffffeu32, reloc_contents, new_target),
            ),
            Rel::R_ARM_JUMP_SLOT => Ok(()),
            Rel::R_ARM_MOVW_ABS_NC if reloc.symbol_address().is_some() => builder
                .write_vaddr::<u32>(
                    reloc.address(),
                    movwt_set(
                        builder.read_vaddr::<u32>(reloc.address())?,
                        new_target.into(),
                    ),
                ),

            Rel::R_ARM_MOVT_ABS if reloc.symbol_address().is_some() => builder.write_vaddr::<u32>(
                reloc.address(),
                movwt_set(
                    builder.read_vaddr::<u32>(reloc.address())?,
                    u32::from(new_target) >> 16,
                ),
            ),

            Rel::R_ARM_MOVW_PREL_NC if reloc.symbol_address().is_some() => builder
                .write_vaddr::<u32>(
                    reloc.address(),
                    movwt_set(
                        builder.read_vaddr::<u32>(reloc.address())?,
                        addend_pcrel_delta as u32,
                    ),
                ),

            Rel::R_ARM_MOVT_PREL if reloc.symbol_address().is_some() => builder.write_vaddr::<u32>(
                reloc.address(),
                movwt_set(
                    builder.read_vaddr::<u32>(reloc.address())?,
                    (addend_pcrel_delta >> 16) as u32,
                ),
            ),

            Rel::R_ARM_THM_MOVW_ABS_NC if reloc.symbol_address().is_some() => builder
                .write_vaddr::<u32>(
                    reloc.address(),
                    thm_movwt_set(
                        builder.read_vaddr::<u32>(reloc.address())?,
                        new_target.into(),
                    ),
                ),

            Rel::R_ARM_THM_MOVT_ABS if reloc.symbol_address().is_some() => builder
                .write_vaddr::<u32>(
                    reloc.address(),
                    thm_movwt_set(
                        builder.read_vaddr::<u32>(reloc.address())?,
                        u32::from(new_target) >> 16,
                    ),
                ),

            Rel::R_ARM_THM_MOVW_PREL_NC if reloc.symbol_address().is_some() => builder
                .write_vaddr::<u32>(
                    reloc.address(),
                    thm_movwt_set(
                        builder.read_vaddr::<u32>(reloc.address())?,
                        addend_pcrel_delta as u32,
                    ),
                ),

            Rel::R_ARM_THM_MOVT_PREL if reloc.symbol_address().is_some() => builder
                .write_vaddr::<u32>(
                    reloc.address(),
                    thm_movwt_set(
                        builder.read_vaddr::<u32>(reloc.address())?,
                        addend_pcrel_delta as u32 >> 16,
                    ),
                ),
            _ => return Err(ArchError::Unimplemented),
        }?;
        Ok(())
    }

    fn get_got_entry(
        reloc: &R,
        build: &T,
    ) -> Result<Option<TargetAddr<Self::UsizePtrTy>>, ArchError> {
        let entry = match u32::from(reloc.typ()) {
            Rel::R_ARM_GOT32 => Some(
                build
                    .got_address()
                    .ok_or(ArchError::NoGOT)?
                    .wrapping_add(build.read_vaddr::<u32>(reloc.address())?)
                    .wrapping_sub(generic!(reloc.addend()) as u32),
            ),
            Rel::R_ARM_GOT_PREL => Some(
                reloc
                    .orig_address()
                    .wrapping_add(build.read_vaddr::<u32>(reloc.address())?)
                    .wrapping_sub(generic!(reloc.addend()) as u32),
            ),
            _ => None,
        };
        Ok(entry)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::test::{fake_mem_strategy, verify_get_set_target_ptr, TestReloc};
    use crate::{RelocationIdx, TrapRelocAddend};
    use proptest::prelude::*;

    fn arm_reloc_num() -> impl Strategy<Value = u32> {
        use goblin::elf::reloc as R;
        (1..R::R_ARM_NUM).prop_filter("Unnaccepted relocation type", |v| match *v {
            132..=159 | 161..=248 => false,
            // requires symbols
            Rel::R_ARM_MOVW_ABS_NC
            | Rel::R_ARM_MOVT_ABS
            | Rel::R_ARM_THM_MOVW_ABS_NC
            | Rel::R_ARM_THM_MOVT_ABS => false,
            Rel::R_ARM_MOVW_PREL_NC
            | Rel::R_ARM_MOVT_PREL
            | Rel::R_ARM_THM_MOVW_PREL_NC
            | Rel::R_ARM_THM_MOVT_PREL => false,
            _ => true,
        })
    }

    fn interesting_reloc_prefix() -> impl Strategy<Value = Vec<u8>> {
        prop_oneof!(prop::collection::vec(any::<u8>(), 10))
    }
    #[test]
    fn test_bit_select() {
        assert_eq!(bit_select::<u32, u32, u32, u32>(0x0f, 0xaa, 0xbb), 0xab);
    }

    proptest! {
        #[test]
        fn prop_bit_select(
            mask in any::<u32>(),
            a in any::<u32>(),
            b in any::<u32>()
        ) {
            let res : u32 = bit_select(mask, a,b);
            assert_eq!(res & mask, b & mask);
            assert_eq!(res & !mask, a & !mask);
        }

        #[test]
        fn b_t1_roundtrip(ins in any::<u16>()) {
            let got = b_t1_get(ins);
            let ins2 = b_t1_set(ins, got);
            assert_eq!(ins, ins2);
        }

        #[test]
        fn b_t2_roundtrip(ins in any::<u16>()) {
            let got = b_t2_get(ins);
            let ins2 = b_t2_set(ins, got);
            assert_eq!(ins, ins2);
        }

        #[test]
        fn imm24_roundtrip(ins in any::<u32>()) {
            let got = imm24_get(ins);
            let ins2 = imm24_set(ins, got);
            assert_eq!(ins, ins2);
        }
        #[test]
        fn thm_imm19_roundtrip(ins in any::<u32>()) {
            let got = thm_imm19_get(ins);
            let ins2 = thm_imm19_set(ins, got);
            assert_eq!(ins, ins2);
        }

        #[test]
        fn thm_imm24_roundtrip(ins in any::<u32>()) {
            let got = thm_imm24_get(ins);
            let ins2 = thm_imm24_set(ins, got);
            assert_eq!(ins, ins2);
        }
        #[test]
        fn test_reloc(
            reloc in arm_reloc_num(),
            mem in fake_mem_strategy(interesting_reloc_prefix()),
            addend in any::<i32>(),
        ) {
             let reloc = TestReloc::new(
                 RelocationIdx(reloc),
                mem.reloc_addr(),
                None,
                Some(TrapRelocAddend::Generic(addend as i64))
            );

            verify_get_set_target_ptr::<ArchArm, _>(reloc, mem);
        }
    }
}
