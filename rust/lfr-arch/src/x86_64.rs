// Copyright (c) 2022 RunSafe Security Inc.

use crate::ArchError;
use crate::{assert_cast, ArchUtil, Architecture, Parseable, Relocation, TargetRead, TargetWrite};
use goblin::elf::reloc as Rel;
use modinfo::TargetAddr;
#[cfg(feature = "trap_relax")]
use trap_parser::TrapAddress;
use trap_parser::TrapRelocAddend;

#[cfg(target_pointer_width = "64")]
type LocalUsizePtrTy = usize;

#[cfg(not(target_pointer_width = "64"))]
type LocalUsizePtrTy = u64;

/// X86_64 Architecture
#[derive(Debug)]
pub struct ArchX86_64;

#[cfg(feature = "trap_relax")]
impl<T> crate::Relax<T> for ArchX86_64
where
    T: TargetRead<LocalUsizePtrTy>,
{
    type UsizePtrTy = LocalUsizePtrTy;
    fn apply_relax<F>(
        _trapv1: &mut trap_parser::write::TRaPV1Builder,
        _target: &T,
        _f: F,
    ) -> Result<(), crate::RelaxError>
    where
        F: Fn(TrapAddress) -> TargetAddr<LocalUsizePtrTy>,
    {
        unimplemented!()
    }
}

impl ArchX86_64 {
    fn is_patched_gotpcrel<T, R>(reloc: &R, builder: &T) -> bool
    where
        T: TargetRead<LocalUsizePtrTy>,
        R: Relocation<LocalUsizePtrTy>,
    {
        // BFD-specific hack: BFD sometimes replaces instructions like
        // OP %reg, foo@GOTPCREL(%rip) with immediate address versions:
        // OP %reg, $foo
        let bytes = builder.read_vaddr_slice(reloc.address() - 2, 2);
        matches!(reloc.addend(), Some(TrapRelocAddend::Generic(-4)))
            && matches!(bytes.map(|v| v.into_slice()), Ok(&[0xf7 | 0xc7 | 0x81, b]) if b >> 6 == 0x3)
    }

    fn is_patched_tls_get_addr_call<T>(addr: TargetAddr<LocalUsizePtrTy>, builder: &T) -> bool
    where
        T: TargetRead<LocalUsizePtrTy>,
    {
        // TLS GD-IE or GD-LE transformation in gold:
        // replaces a call to __tls_get_addr with a
        // RAX-relative LEA instruction
        // Bytes are: 64 48 8B 04 25 00 00 00 00 48 8D 80
        let bytes = builder.read_vaddr_slice(addr - 12, 12);
        matches!(
            bytes.map(|v| v.into_slice()),
            Ok(&[0x64, 0x48, 0x8B, 0x04, 0x25, 0x00, 0x00, 0x00, 0x00, 0x48, 0x8D, 0x80])
        )
    }

    fn is_pcrel_tlsxd<T>(addr: TargetAddr<LocalUsizePtrTy>, builder: &T) -> bool
    where
        T: TargetRead<LocalUsizePtrTy>,
    {
        let bytes = builder.read_vaddr_slice(addr - 3, 3);
        matches!(bytes.map(|v| v.into_slice()), Ok(&[0x48, 0x8d, 0x3d]))
    }

    fn is_pcrel_gottpoff<T>(addr: TargetAddr<LocalUsizePtrTy>, builder: &T) -> bool
    where
        T: TargetRead<LocalUsizePtrTy>,
    {
        let bytes = builder.read_vaddr_slice(addr - 2, 2);
        matches!(bytes.map(|v| v.into_slice()), Ok(&[
            0x8b /* MOV */| 0x03 /* ADD */,
            rip
        ]) if rip & 0xc7 == 0x05 /* RIP-relative */)
    }

    fn is_pcrel_gotpc_tlsdesc<T>(addr: TargetAddr<LocalUsizePtrTy>, builder: &T) -> bool
    where
        T: TargetRead<LocalUsizePtrTy>,
    {
        let bytes = builder.read_vaddr_slice(addr - 3, 3);
        matches!(
            bytes.map(|v| v.into_slice()),
            Ok(&[0x48, 0x8d | 0x8b, 0x05])
        )
    }
}

impl Parseable for ArchX86_64 {
    type ParserArch = trap_parser::X86_64;
}

impl<T, R> Architecture<T, R> for ArchX86_64
where
    T: TargetRead<LocalUsizePtrTy> + TargetWrite<LocalUsizePtrTy>,
    R: Relocation<LocalUsizePtrTy>,
{
    type UsizePtrTy = LocalUsizePtrTy;
    type UIntPtrTy = u64;
    type IntPtrTy = i64;

    fn get_target_ptr(reloc: &R, builder: &T) -> Option<TargetAddr<Self::UsizePtrTy>> {
        match u32::from(reloc.typ()) {
            Rel::R_X86_64_32 | Rel::R_X86_64_32S => {
                Some(builder.read_vaddr::<u32>(reloc.address()).unwrap().into())
            }

            Rel::R_X86_64_64 => Some(builder.read_vaddr::<u64>(reloc.address()).unwrap().into()),

            Rel::R_X86_64_GOT64 | Rel::R_X86_64_GOTOFF64 => Some(
                builder
                    .got_address()?
                    .wrapping_add(builder.read_vaddr::<u64>(reloc.address()).unwrap()),
            ),

            Rel::R_X86_64_GOTPCREL | Rel::R_X86_64_GOTPCRELX | Rel::R_X86_64_REX_GOTPCRELX
                if Self::is_patched_gotpcrel(reloc, builder) =>
            {
                Some(builder.read_vaddr::<u32>(reloc.address()).unwrap().into())
            }

            Rel::R_X86_64_GOTPCREL | Rel::R_X86_64_GOTPCRELX | Rel::R_X86_64_REX_GOTPCRELX
                if matches!(
                    builder
                        .read_vaddr_slice(reloc.address() - 2, 6)
                        .map(|v| v.into_slice()),
                    Ok(&[0xe9, _, _, _, _, 0x90])
                ) =>
            {
                Some(
                    (reloc.orig_address() - 1)
                        .wrapping_sub(generic!(reloc.addend()) as u64)
                        .wrapping_add(
                            builder.read_vaddr::<i32>(reloc.address() - 1).unwrap() as i64 as u64,
                        ),
                )
            }

            Rel::R_X86_64_PC32 | Rel::R_X86_64_PLT32 | Rel::R_X86_64_GOTPC32
                if Self::is_patched_tls_get_addr_call(reloc.address(), builder) =>
            {
                None
            }

            Rel::R_X86_64_PC64 | Rel::R_X86_64_GOTPCREL64 | Rel::R_X86_64_GOTPC64 => Some(
                reloc
                    .orig_address()
                    .wrapping_sub(generic!(reloc.addend()) as u64)
                    .wrapping_add(builder.read_vaddr::<i64>(reloc.address()).unwrap() as u64),
            ),

            Rel::R_X86_64_TLSGD | Rel::R_X86_64_TLSLD
                if !Self::is_pcrel_tlsxd(reloc.address(), builder) =>
            {
                None
            }

            Rel::R_X86_64_GOTTPOFF if !Self::is_pcrel_gottpoff(reloc.address(), builder) => None,

            Rel::R_X86_64_GOTPC32_TLSDESC
                if !Self::is_pcrel_gotpc_tlsdesc(reloc.address(), builder) =>
            {
                None
            }

            // pcrel_reloc:
            Rel::R_X86_64_PC32
            | Rel::R_X86_64_PLT32
            | Rel::R_X86_64_GOTPC32
            | Rel::R_X86_64_GOTPCREL
            | Rel::R_X86_64_GOTPCRELX
            | Rel::R_X86_64_REX_GOTPCRELX
            | Rel::R_X86_64_TLSGD
            | Rel::R_X86_64_TLSLD
            | Rel::R_X86_64_GOTTPOFF
            | Rel::R_X86_64_GOTPC32_TLSDESC => {
                Some(
                    reloc
                        .orig_address()
                        .wrapping_sub(generic!(reloc.addend()) as u64)
                        .wrapping_add(
                            builder.read_vaddr::<i32>(reloc.address()).unwrap() as i64 as u64
                        ),
                )
            }

            _ => {
                //info!("Unimplemented relocation: {}", reloc.typ());
                None
            }
        }
    }

    fn set_target_ptr(
        reloc: &R,
        builder: &mut T,
        new_target: TargetAddr<Self::UsizePtrTy>,
    ) -> Result<(), ArchError> {
        match u32::from(reloc.typ()) {
            Rel::R_X86_64_32 | Rel::R_X86_64_32S => {
                builder.write_vaddr::<u32>(reloc.address(), assert_cast::<u64, _, _>(new_target))
            }

            Rel::R_X86_64_64 => {
                builder.write_vaddr::<u64>(reloc.address(), assert_cast::<u64, _, _>(new_target))
            }

            Rel::R_X86_64_GOT64 | Rel::R_X86_64_GOTOFF64 => {
                let write = new_target.wrapping_sub(builder.got_address().ok_or(ArchError::NoGOT)?);

                builder.write_vaddr::<u64>(reloc.address(), assert_cast::<u64, _, _>(write))
            }

            Rel::R_X86_64_GOTPCREL | Rel::R_X86_64_GOTPCRELX | Rel::R_X86_64_REX_GOTPCRELX
                if Self::is_patched_gotpcrel(reloc, builder) =>
            {
                builder.write_vaddr::<u32>(reloc.address(), assert_cast::<u64, _, _>(new_target))
            }

            Rel::R_X86_64_GOTPCREL | Rel::R_X86_64_GOTPCRELX | Rel::R_X86_64_REX_GOTPCRELX
                if matches!(
                    builder
                        .read_vaddr_slice(reloc.address() - 2, 6)
                        .map(|v| v.into_slice()),
                    Ok(&[0xe9, _, _, _, _, 0x90])
                ) =>
            {
                let adj_src_ptr = reloc.address() - 1;
                let new_offset = new_target
                    .wrapping_add(generic!(reloc.addend()) as u64)
                    .wrapping_sub(adj_src_ptr);
                builder.write_vaddr::<i32>(adj_src_ptr, assert_cast::<i64, _, _>(new_offset))
            }

            Rel::R_X86_64_PC32 | Rel::R_X86_64_PLT32 | Rel::R_X86_64_GOTPC32
                if Self::is_patched_tls_get_addr_call(reloc.address(), builder) =>
            {
                Ok(())
            }

            Rel::R_X86_64_PC64 | Rel::R_X86_64_GOTPCREL64 | Rel::R_X86_64_GOTPC64 => {
                let write = new_target
                    .wrapping_add(generic!(reloc.addend()) as Self::UsizePtrTy)
                    .wrapping_sub(reloc.address());
                builder.write_vaddr::<i64>(reloc.address(), assert_cast::<i64, _, _>(write))
            }

            Rel::R_X86_64_TLSGD | Rel::R_X86_64_TLSLD
                if !Self::is_pcrel_tlsxd(reloc.address(), builder) =>
            {
                Ok(())
            }

            Rel::R_X86_64_GOTTPOFF if !Self::is_pcrel_gottpoff(reloc.address(), builder) => Ok(()),

            Rel::R_X86_64_GOTPC32_TLSDESC
                if !Self::is_pcrel_gotpc_tlsdesc(reloc.address(), builder) =>
            {
                Ok(())
            }

            // pcrel_reloc:
            Rel::R_X86_64_PC32
            | Rel::R_X86_64_PLT32
            | Rel::R_X86_64_GOTPC32
            | Rel::R_X86_64_GOTPCREL
            | Rel::R_X86_64_GOTPCRELX
            | Rel::R_X86_64_REX_GOTPCRELX
            | Rel::R_X86_64_TLSGD
            | Rel::R_X86_64_TLSLD
            | Rel::R_X86_64_GOTTPOFF
            | Rel::R_X86_64_GOTPC32_TLSDESC => {
                let write = new_target
                    .wrapping_add(generic!(reloc.addend()) as Self::UsizePtrTy)
                    .wrapping_sub(reloc.address());
                builder.write_vaddr::<i32>(reloc.address(), assert_cast::<i64, _, _>(write))
            }

            _ => return Err(ArchError::Unimplemented),
        }?;

        Ok(())
    }

    fn get_got_entry(
        reloc: &R,
        target: &T,
    ) -> Result<Option<TargetAddr<Self::UsizePtrTy>>, ArchError> {
        match u32::from(reloc.typ()) {
            Rel::R_X86_64_GOT32 => Ok(Some(
                target
                    .got_address()
                    .ok_or(ArchError::NoGOT)?
                    .wrapping_add(target.read_vaddr::<i32>(reloc.address())? as i64 as u64)
                    .wrapping_sub(generic!(reloc.addend()) as u64),
            )),
            Rel::R_X86_64_GOT64 | Rel::R_X86_64_GOTPLT64 => Ok(Some(
                target
                    .got_address()
                    .ok_or(ArchError::NoGOT)?
                    .wrapping_add(target.read_vaddr::<i64>(reloc.address())? as u64)
                    .wrapping_sub(generic!(reloc.addend()) as u64),
            )),

            Rel::R_X86_64_GOTPCREL | Rel::R_X86_64_GOTPCRELX | Rel::R_X86_64_REX_GOTPCRELX => {
                if Self::is_patched_gotpcrel(reloc, target) {
                    return Ok(None);
                }
                if matches!(
                    target
                        .read_vaddr_slice(reloc.address() - 2, 5)
                        .as_ref()
                        .map(|v| v.as_ref()),
                    Ok(
                        &[0x8d, _, _, _, _]  // MOV-to-LEA conversion
                            | &[0x67, 0xe8, _, _, _]  // callq-to-addr32-callq conversion
                            | &[0xe9, _, _, _, 0x90] // jmpq-to-jmpq-nop conversion
                    )
                ) {
                    return Ok(None);
                }
                Ok(Some(
                    reloc
                        .orig_address()
                        .wrapping_add(target.read_vaddr::<i32>(reloc.address())? as i64 as u64)
                        .wrapping_sub(generic!(reloc.addend()) as u64),
                ))
            }
            Rel::R_X86_64_GOTPC32 => {
                let maybe_got = reloc
                    .orig_address()
                    .wrapping_add(target.read_vaddr::<i32>(reloc.address())? as i64 as u64)
                    .wrapping_sub(generic!(reloc.addend()) as u64);
                debug_assert_eq!(Some(maybe_got), target.got_address());
                Ok(Some(maybe_got))
            }
            Rel::R_X86_64_GOTPC64 => {
                let maybe_got = reloc
                    .orig_address()
                    .wrapping_add(target.read_vaddr::<i64>(reloc.address())? as u64)
                    .wrapping_sub(generic!(reloc.addend()) as u64);
                debug_assert_eq!(Some(maybe_got), target.got_address());
                Ok(Some(maybe_got))
            }
            Rel::R_X86_64_GOTPCREL64 => {
                let maybe_got = reloc
                    .orig_address()
                    .wrapping_add(target.read_vaddr::<i64>(reloc.address())? as u64)
                    .wrapping_sub(generic!(reloc.addend()) as u64);
                Ok(Some(maybe_got))
            }
            _ => Ok(None),
        }
    }
}

impl ArchUtil for ArchX86_64 {
    const POINTER_RELOC: u32 = goblin::elf::reloc::R_X86_64_64;
    const COPY_RELOC: u32 = goblin::elf::reloc::R_X86_64_COPY;

    fn fill_nops(fill: &mut [u8]) {
        match fill {
            [] => {}
            [b0] => {
                *b0 = 0x90;
            }
            [b0, b1] => {
                *b0 = 0x66;
                *b1 = 0x90;
            }
            [b0, b1, b2] => {
                *b0 = 0x0f;
                *b1 = 0x1f;
                *b2 = 0x00;
            }
            all => {
                all.fill(0x90);
            }
        }
    }
    fn is_one_byte_nop(maybe_nop: u8) -> bool {
        matches!(maybe_nop, 0xcc | 0x90)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::test::{fake_mem_strategy, verify_get_set_target_ptr, TestReloc};
    use crate::{RelocationIdx, TrapRelocAddend};
    use proptest::prelude::*;

    fn x86_64_reloc_num() -> impl Strategy<Value = u32> {
        use goblin::elf::reloc as R;
        1..R::R_X86_64_NUM
    }

    fn interesting_reloc_prefix() -> impl Strategy<Value = Vec<u8>> {
        prop_oneof!(
            (
                proptest::sample::select([0xf7, 0xc7, 0x81, 0x8b, 0x03].as_slice()),
                any::<u8>()
            )
                .prop_map(|(v, b)| vec![v, b]),
            Just(vec![
                0x64, 0x48, 0x8B, 0x04, 0x25, 0x00, 0x00, 0x00, 0x00, 0x48, 0x8D, 0x80
            ]),
            proptest::sample::select([0x8d, 0x8d].as_slice()).prop_map(|b| vec![0x48, b, 0x3d]),
            prop::collection::vec(any::<u8>(), 10)
        )
    }

    proptest! {
        #[test]
        fn test_reloc(
            reloc in x86_64_reloc_num(),
            mem in fake_mem_strategy(interesting_reloc_prefix()),
            addend in any::<i32>(),
        ) {
            let reloc = TestReloc::new(
                RelocationIdx(reloc),
                mem.reloc_addr(),
                None,
                Some(TrapRelocAddend::Generic(addend as i64)),
            );

            verify_get_set_target_ptr::<ArchX86_64, _>(reloc, mem);
        }
    }
}
