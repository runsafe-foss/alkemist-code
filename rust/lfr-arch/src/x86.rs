// Copyright (c) 2022 RunSafe Security Inc.

use crate::{
    assert_cast, ArchError, ArchUtil, Architecture, Parseable, Relocation, TargetRead, TargetWrite,
};
use core::convert::TryInto;
use goblin::elf::reloc as Rel;
use modinfo::TargetAddr;
#[cfg(feature = "trap_relax")]
use trap_parser::TrapAddress;

#[cfg(target_pointer_width = "32")]
type LocalUsizePtrTy = usize;

#[cfg(not(target_pointer_width = "32"))]
type LocalUsizePtrTy = u32;

/// X86 Architecture
#[derive(Debug)]
pub struct ArchX86;

#[cfg(feature = "trap_relax")]
impl<T> crate::Relax<T> for ArchX86
where
    T: TargetRead<LocalUsizePtrTy>,
{
    type UsizePtrTy = LocalUsizePtrTy;
    fn apply_relax<F>(
        _trapv1: &mut trap_parser::write::TRaPV1Builder,
        _builder: &T,
        _f: F,
    ) -> Result<(), crate::RelaxError>
    where
        F: Fn(TrapAddress) -> TargetAddr<LocalUsizePtrTy>,
    {
        unimplemented!()
    }
}

#[derive(Debug)]
enum Got32Patch {
    /// Unpatched R_GOT32[X] relocation: G+A-GOT
    Unpatched,
    /// Unpatched baseless (no base register, absolute address), or some other
    /// kind of direct GOT entry reference: G+A
    UnpatchedGotEntryAddress,
    /// Patched direct address access: S+A
    PatchedSymbolAddress,
    /// Patched LEA with base register: S+A-GOT
    PatchedLea,
    /// Replaced call instruction with prefix (see comments below)
    PatchedBranchPrefix,
    /// Replaced call instruction with suffix
    PatchedBranchSuffix,
}

impl ArchX86 {
    fn got32_patch_kind<T>(address: TargetAddr<LocalUsizePtrTy>, builder: &T) -> Got32Patch
    where
        T: TargetRead<LocalUsizePtrTy>,
    {
        let bytes = builder.read_vaddr_slice(address - 2, 2).unwrap();
        let baseless = |b| b & 0xc7 == 0x05;

        match *bytes.as_ref() {
            // mov foo@GOT, ... => lea foo[@GOTOFF], ...
            [0x8d, base] if baseless(base) => Got32Patch::PatchedSymbolAddress,
            [0x8d, _] => Got32Patch::PatchedLea,
            // mov   foo@GOT, ... => mov $foo, ...
            // test  ..., foo@GOT => test ..., $foo
            // binop foo@GOT, ... => binop $foo, ...
            [0xc7 | 0xf7 | 0x81, _] => Got32Patch::PatchedSymbolAddress,
            // gold doesn't always convert 0x8b to 0x8d; even without the
            // patch, the instruction still takes an absolute operand
            [0x8b, base] if baseless(base) => Got32Patch::UnpatchedGotEntryAddress,
            // gold also leaves in some indirect calls that reference
            // a GOT entry by absolute address
            [0xff, 0x15] => Got32Patch::UnpatchedGotEntryAddress,
            // FIXME: for the replaced call instructions, ld provides the
            // -z call-nop option which lets users specify both the position
            // and byte of the NOP prefix/suffix; we can't handle the general
            // case (any byte before or after the relocation), only the most
            // common
            //
            // call foo@GOT => addr16 call foo
            [0x67, 0xe8] => Got32Patch::PatchedBranchPrefix,
            // call foo@GOT => call foo ; nop
            // jmp foo@GOT  => jmp foo ; nop
            [0xe8 | 0xe9, _] if builder.read_vaddr::<u8>(address + 3).unwrap() == 0x90 => {
                Got32Patch::PatchedBranchSuffix
            }
            _ => Got32Patch::Unpatched,
        }
    }

    /// Determines whether targeted relocation has been patched.
    /// TLS GD-IE or GD-LE transformation in gold:
    /// replaces a call to __tls_get_addr with a
    /// `MOV from GS:0 to EAX`
    /// Format is: `65 A1 00 00 00 00 MM NN ...`
    /// the total length is either 11 or 12 bytes

    /// # Gold
    /// * `gold/i386.cc`
    ///    * `Target_i386::Relocate::tls_gd_to_le`
    ///    * `Target_i386::Relocate::tls_gd_to_ie`
    ///    * `Target_i386::Relocate::tls_ld_to_le`
    /// # BFD
    ///  * `bfd/elf32-i386.c`
    ///    * `elf_i386_tls_transition`
    /// # LLD
    /// * `lld/ELF/Arch/X86.cpp`
    ///    * `X86::relaxTlsGdToLe`
    ///    * `X86::relaxTlsGdToIe`
    ///    * `X86::relaxTlsLdToLe`
    fn is_patched_tls_get_addr_call<T>(address: TargetAddr<LocalUsizePtrTy>, builder: &T) -> bool
    where
        T: TargetRead<LocalUsizePtrTy>,
    {
        let prefix = [0x65, 0xa1, 0x00, 0x00, 0x00, 0x00];
        const X86_REG_MASK: u8 = 0x7;

        if let Ok(mem) = builder.read_vaddr_slice(address - 8, 8) {
            let (mem_prefix, suffix) = mem.as_ref().split_at(prefix.len());
            if mem_prefix == prefix {
                match suffix {
                    // Gold/LLD/BFD
                    // Linker Converted
                    //   leal x@tlsgd(, %ebx, 1),
                    //   call __tls_get_addr@plt
                    // to
                    //   movl %gs:0,%eax
                    //   subl $x@ntpoff,%eax
                    [0x81, 0xe8] => return true,
                    // Gold/BFD/LLD
                    // Linker Converted
                    //   leal x@tlsgd(, %ebx, 1),
                    //   call __tls_get_addr@plt
                    // to
                    //   movl %gs:0, %eax
                    //   addl/subl x@gotntpoff(%reg), %eax
                    [0x03 | 0x2b, reg] if reg & !X86_REG_MASK == 0x80 => return true,
                    // Gold
                    // Linker Converted
                    //   leal foo(%reg), %eax; call call *___tls_get_addr@GOT(%reg)
                    // to
                    //   movl %gs:0,%eax; leal (%esi),%esi
                    [0x8d, 0xb6] => return true,
                    _ => {}
                }
            }
        }

        if let Ok(mem) = builder.read_vaddr_slice(address - 7, 7) {
            let (mem_prefix, suffix) = mem.as_ref().split_at(prefix.len());
            if mem_prefix == prefix {
                if let [0x2d | 0x90] = suffix {
                    // Gold/LLD
                    // Linker Converted
                    //   leal foo(%reg),%eax
                    //   call ___tls_get_addr
                    // to
                    //   movl %gs:0,%eax
                    //   nop
                    //   leal 0(%esi,1),%esi
                    // or
                    //   movl %gs:0,%eax
                    //   subl ...
                    return true;
                }
            }
        }
        false
    }
}

impl ArchUtil for ArchX86 {
    const POINTER_RELOC: u32 = goblin::elf::reloc::R_386_32;
    const COPY_RELOC: u32 = goblin::elf::reloc::R_386_COPY;

    fn fill_nops(fill: &mut [u8]) {
        match fill {
            [] => {}
            [b0] => {
                *b0 = 0x90;
            }
            [b0, b1] => {
                *b0 = 0x66;
                *b1 = 0x90;
            }
            [b0, b1, b2] => {
                *b0 = 0x0f;
                *b1 = 0x1f;
                *b2 = 0x00;
            }
            all => {
                all.fill(0x90);
            }
        }
    }
    fn is_one_byte_nop(maybe_nop: u8) -> bool {
        matches!(maybe_nop, 0xcc | 0x90)
    }
}

impl Parseable for ArchX86 {
    type ParserArch = trap_parser::X86;
}

impl<T, R> Architecture<T, R> for ArchX86
where
    T: TargetRead<LocalUsizePtrTy> + TargetWrite<LocalUsizePtrTy>,
    R: Relocation<LocalUsizePtrTy>,
{
    type UsizePtrTy = LocalUsizePtrTy;
    type UIntPtrTy = u32;
    type IntPtrTy = i32;

    fn get_target_ptr(reloc: &R, builder: &T) -> Option<TargetAddr<Self::UsizePtrTy>> {
        let addend: i32 = generic!(reloc.addend()).try_into().unwrap();

        let abs32_reloc = || -> Option<TargetAddr<Self::UsizePtrTy>> {
            Some(builder.read_vaddr::<u32>(reloc.address()).unwrap().into())
        };
        let pcrel_reloc = |offset: u32| -> Option<TargetAddr<Self::UsizePtrTy>> {
            // We need to use the original address as the source here (not the diversified one)
            // to keep in consistent with the original relocation entry (before shuffling)
            Some(
                reloc
                    .orig_address()
                    .wrapping_add(
                        builder
                            .read_vaddr::<u32>(reloc.address().wrapping_sub(offset))
                            .unwrap(),
                    )
                    .wrapping_sub(addend as u32)
                    .wrapping_sub(offset),
            )
        };

        let gotoff_reloc = || -> Option<TargetAddr<Self::UsizePtrTy>> {
            let got = builder.got_address()?;
            Some(
                got.wrapping_add(builder.read_vaddr::<u32>(reloc.address()).unwrap())
                    .wrapping_sub(addend as u32),
            )
        };

        match u32::from(reloc.typ()) {
            Rel::R_386_32 => abs32_reloc(),

            Rel::R_386_GOT32 | Rel::R_386_GOT32X => {
                match Self::got32_patch_kind(reloc.address(), builder) {
                    Got32Patch::UnpatchedGotEntryAddress | Got32Patch::PatchedSymbolAddress => {
                        abs32_reloc()
                    }
                    Got32Patch::PatchedBranchPrefix => pcrel_reloc(0),
                    // This patch shifts the relocation back by 1 byte
                    Got32Patch::PatchedBranchSuffix => pcrel_reloc(1),
                    // Compilers may try to indirectly call __tls_get_addr through the
                    // GOT, which would be encoded as an indirect call with a
                    // Rel::R_386_GOT32X relocation
                    _ if u32::from(reloc.typ()) == Rel::R_386_GOT32X
                        && Self::is_patched_tls_get_addr_call(reloc.address(), builder) =>
                    {
                        None
                    }
                    _ => gotoff_reloc(),
                }
            }

            Rel::R_386_GOTOFF => gotoff_reloc(),

            Rel::R_386_PC32 | Rel::R_386_PLT32 | Rel::R_386_GOTPC
                if !Self::is_patched_tls_get_addr_call(reloc.address(), builder) =>
            {
                pcrel_reloc(0)
            }
            Rel::R_386_PC32 | Rel::R_386_PLT32 | Rel::R_386_GOTPC => None,
            _ => None,
        }
    }

    fn set_target_ptr(
        reloc: &R,
        builder: &mut T,
        new_target: TargetAddr<Self::UsizePtrTy>,
    ) -> Result<(), ArchError> {
        #[derive(Debug)]
        enum SetType {
            Abs32,
            PcRel(u32),
            GotOff,
        }

        let addend: i32 = generic!(reloc.addend()).try_into().unwrap();

        let set = match u32::from(reloc.typ()) {
            Rel::R_386_32 => Some(SetType::Abs32),

            Rel::R_386_GOT32 | Rel::R_386_GOT32X => {
                match Self::got32_patch_kind(reloc.address(), builder) {
                    Got32Patch::UnpatchedGotEntryAddress | Got32Patch::PatchedSymbolAddress => {
                        Some(SetType::Abs32)
                    }
                    Got32Patch::PatchedBranchPrefix => Some(SetType::PcRel(0)),
                    // This patch shifts the relocation back by 1 byte
                    Got32Patch::PatchedBranchSuffix => Some(SetType::PcRel(1)),
                    // Compilers may try to indirectly call __tls_get_addr through the
                    // GOT, which would be encoded as an indirect call with a
                    // Rel::R_386_GOT32X relocation
                    _ if u32::from(reloc.typ()) == Rel::R_386_GOT32X
                        && Self::is_patched_tls_get_addr_call(reloc.address(), builder) =>
                    {
                        None
                    }
                    _ => Some(SetType::GotOff),
                }
            }

            Rel::R_386_GOTOFF => Some(SetType::GotOff),

            Rel::R_386_PC32 | Rel::R_386_PLT32 | Rel::R_386_GOTPC
                if !Self::is_patched_tls_get_addr_call(reloc.address(), builder) =>
            {
                Some(SetType::PcRel(0))
            }
            Rel::R_386_PC32 | Rel::R_386_PLT32 | Rel::R_386_GOTPC => None,
            _ => return Err(ArchError::Unimplemented),
        };

        match set {
            Some(SetType::Abs32) => {
                builder.write_vaddr::<u32>(reloc.address(), assert_cast::<u32, _, _>(new_target))?
            }
            Some(SetType::PcRel(offset)) => {
                let val = new_target
                    .wrapping_add(addend as u32)
                    .wrapping_sub(reloc.address().wrapping_sub(offset));
                builder.write_vaddr::<i32>(
                    reloc.address().wrapping_sub(offset),
                    assert_cast::<i32, _, _>(val),
                )?
            }
            Some(SetType::GotOff) => {
                let val = new_target
                    .wrapping_add(addend as u32)
                    .wrapping_sub(builder.got_address().ok_or(ArchError::NoGOT)?);

                builder.write_vaddr::<i32>(reloc.address(), assert_cast::<i32, _, _>(val))?
            }
            None => {}
        }
        Ok(())
    }
    fn get_got_entry(
        reloc: &R,
        target: &T,
    ) -> Result<Option<TargetAddr<Self::UsizePtrTy>>, ArchError> {
        match u32::from(reloc.typ()) {
            Rel::R_386_GOT32 | Rel::R_386_GOT32X => {
                match Self::got32_patch_kind(reloc.address(), target) {
                    Got32Patch::Unpatched => Ok(Some(
                        target
                            .got_address()
                            .ok_or(ArchError::NoGOT)?
                            .wrapping_add(target.read_vaddr::<u32>(reloc.address())?)
                            .wrapping_sub(generic!(reloc.addend()) as u32),
                    )),
                    Got32Patch::UnpatchedGotEntryAddress => {
                        Ok(Some(target.read_vaddr::<u32>(reloc.address())?.into()))
                    }
                    _ => Ok(None),
                }
            }
            _ => Ok(None),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::test::{fake_mem_strategy, verify_get_set_target_ptr, TestReloc};
    use crate::{RelocationIdx, TrapRelocAddend};
    use proptest::prelude::*;

    fn x86_reloc_num() -> impl Strategy<Value = u32> {
        use goblin::elf::reloc as R;
        (1..R::R_386_NUM).prop_filter("Unnaccepted relocation type", |v| match *v {
            R::R_386_TLS_LDM
            | R::R_386_TLS_LDM_PUSH
            | R::R_386_TLS_LDM_32
            | R::R_386_TLS_LDM_CALL
            | R::R_386_TLS_LDM_POP
            | R::R_386_TLS_GD
            | R::R_386_TLS_GD_POP
            | R::R_386_TLS_GD_PUSH
            | R::R_386_TLS_GD_32
            | R::R_386_TLS_GD_CALL
            | R::R_386_32PLT
            | R::R_386_JMP_SLOT
            | 12
            | 13 => false,

            //These may need to be added for embedded systems
            R::R_386_PC8 | R::R_386_8 => false,
            R::R_386_PC16 | R::R_386_16 => false,
            _ => true,
        })
    }

    fn interesting_reloc_prefix() -> impl Strategy<Value = Vec<u8>> {
        const PATCHED_TLS: [u8; 6] = [0x65, 0xa1, 0x00, 0x00, 0x00, 0x00];
        prop_oneof!(
            any::<u8>().prop_map(|v| vec![0x8d, v]),
            (
                proptest::sample::select([0xe8, 0xe9, 0x8b, 0xc7, 0xf7, 0x81].as_slice()),
                any::<u8>()
            )
                .prop_map(|(v, v2)| vec![v, v2]),
            Just(vec![0xff, 0x15]),
            Just(vec![0x67, 0xe8]),
            (1..=2usize)
                .prop_flat_map(|len| { prop::collection::vec(any::<u8>(), len) })
                .prop_map(|v| {
                    let mut tls = PATCHED_TLS.to_vec();
                    tls.extend(v);
                    tls
                }),
            prop::collection::vec(any::<u8>(), 10)
        )
    }

    proptest! {
        #[test]
        fn test_reloc(
            reloc in x86_reloc_num(),
            mem in fake_mem_strategy(interesting_reloc_prefix()),
            addend in any::<i32>(),
        ) {
            let reloc = TestReloc::new(
                RelocationIdx(reloc),
                mem.reloc_addr(),
                None,
                Some(TrapRelocAddend::Generic(addend as i64))
            );

            verify_get_set_target_ptr::<ArchX86, _>(reloc, mem);
        }
    }
}
