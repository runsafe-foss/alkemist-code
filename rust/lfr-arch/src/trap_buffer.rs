// Copyright (c) 2023 RunSafe Security Inc.
use crate as arch;
use crate::{Target, TargetError, TargetSlice, TargetSliceMut, TargetWrite};
use modinfo::{TargetAddr, TargetAddrPtr};
use trap_parser::{RangeMap, TrapAddress};
extern crate alloc;
use alloc::vec::Vec;

pub struct TRaPBuffer {
    got_address: Option<TrapAddress>,
    buffer: Vec<u8>,
    mem_ranges: Vec<RangeMap>,
}

impl TRaPBuffer {
    pub fn new(
        got_address: Option<TrapAddress>,
        buffer: Vec<u8>,
        mem_ranges: Vec<RangeMap>,
    ) -> Self {
        Self {
            got_address,
            buffer,
            mem_ranges,
        }
    }
    fn find_region<T>(&self, addr: TargetAddr<T>) -> Option<RangeMap>
    where
        T: TargetAddrPtr,
    {
        self.mem_ranges
            .iter()
            .find(|mem_range| {
                mem_range
                    .vm
                    .contains(&addr.absolute_addr().try_into().map_err(|_| ()).unwrap())
            })
            .cloned()
    }
    fn find_file_offset<T>(&self, addr: TargetAddr<T>) -> Option<usize>
    where
        T: TargetAddrPtr,
    {
        self.find_region(addr).map(|mem_range| {
            (TryInto::<usize>::try_into(addr.absolute_addr())
                .map_err(|_| ())
                .unwrap()
                - mem_range.vm.start)
                + mem_range.file.start
        })
    }
}

impl<T> arch::Target<T> for TRaPBuffer
where
    T: TargetAddrPtr,
{
    fn got_address(&self) -> Option<TargetAddr<T>> {
        let got = self.got_address?;
        let got: T = got.absolute(0).try_into().map_err(|_| ()).unwrap();
        Some(TargetAddr::from_raw(got))
    }
    fn contains(&self, vaddr: TargetAddr<T>) -> bool {
        self.find_region(vaddr).is_some()
    }

    fn contains_range(&self, vaddr: TargetAddr<T>, len: usize) -> bool {
        self.find_region(vaddr)
            .filter(|range| {
                let start = vaddr.absolute_addr().try_into().map_err(|_| ()).unwrap();
                let end = start + len;
                range.vm.contains(&start) && (end == range.vm.end || range.vm.contains(&end))
            })
            .is_some()
    }
}

impl<T> arch::TargetRead<T> for TRaPBuffer
where
    T: TargetAddrPtr,
{
    fn read_vaddr_max_slice(
        &self,
        vaddr: TargetAddr<T>,
    ) -> core::result::Result<TargetSlice<'_, T>, TargetError> {
        let got = self.got_address();
        let offset = self
            .find_file_offset(vaddr)
            .ok_or(TargetError::OutOfRange)?;
        Ok(TargetSlice::new(
            self.buffer.get(offset..).ok_or(TargetError::OutOfRange)?,
            vaddr,
            got,
        ))
    }
}

impl<T> TargetWrite<T> for TRaPBuffer
where
    T: TargetAddrPtr,
{
    fn write_vaddr_slice(
        &mut self,
        vaddr: TargetAddr<T>,
        len: usize,
    ) -> core::result::Result<TargetSliceMut<'_, T>, TargetError> {
        let got = self.got_address();
        let offset = self
            .find_file_offset(vaddr)
            .ok_or(TargetError::OutOfRange)?;
        if offset + len < self.buffer.len() {
            Ok(TargetSliceMut::new(
                &mut self.buffer[offset..][..len],
                vaddr,
                got,
            ))
        } else {
            Err(TargetError::OutOfRange)?
        }
    }
    fn contiguous_slice_containing_mut(
        &mut self,
        vaddr: TargetAddr<T>,
    ) -> core::result::Result<TargetSliceMut<'_, T>, TargetError> {
        let got = self.got_address();
        let region = self.find_region(vaddr).ok_or(TargetError::OutOfRange)?;
        let start: T = region.vm.start.try_into().map_err(|_| ()).unwrap();
        let start = TargetAddr::from_raw(start);
        Ok(TargetSliceMut::new(
            &mut self.buffer[region.file],
            start,
            got,
        ))
    }
}
