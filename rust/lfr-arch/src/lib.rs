// Copyright (c) 2022 RunSafe Security Inc.
#![cfg_attr(not(feature = "std"), no_std)]

use core::fmt::Debug;
use modinfo::{TargetAddr, TargetAddrPtr};
use num_traits::cast::AsPrimitive;
use scroll::ctx::{IntoCtx, SizeWith, TryFromCtx, TryIntoCtx};
#[cfg(feature = "std")]
use scroll::IOwrite;
use scroll::{Endian, Pread, Pwrite};
#[cfg(feature = "name_relocs")]
use trap_parser::ParserArch;
#[cfg(feature = "trap_relax")]
use trap_parser::TrapAddress;
use trap_parser::{TrapRelocAddend, TrapRelocInfo};

pub mod trap_buffer;

macro_rules! generic {
    ($addend:expr) => {
        $addend.map(|x| x.generic()).unwrap_or(0)
    };
}

mod arm;
mod arm64;
mod x86;
mod x86_64;

pub use arm::ArchArm;
pub use arm64::ArchArm64;
pub use x86::ArchX86;
pub use x86_64::ArchX86_64;

cfg_if::cfg_if! {
    if #[cfg(target_arch="x86")] {
        /// Current Target Architecture
        pub type TargetArch = x86::ArchX86;
    } else if #[cfg(target_arch="x86_64")] {
        /// Current Target Architecture
        pub type TargetArch = x86_64::ArchX86_64;
    } else if #[cfg(target_arch="arm")] {
        /// Current Target Architecture
        pub type TargetArch = arm::ArchArm;
    } else if #[cfg(target_arch="aarch64")] {
        /// Current Target Architecture
        pub type TargetArch = arm64::ArchArm64;
    } else {
        compile_error!{"Unknown target arch"}
    }
}

#[inline]
fn bit<T, S>(x: T, bit: S) -> bool
where
    T: core::ops::Shr<S, Output = T>
        + core::ops::BitAnd<Output = T>
        + num_traits::identities::One
        + core::cmp::PartialEq,
{
    (x >> bit) & T::one() == T::one()
}

/// # Generic Sign Extension
/// Trait for converting unsigned values into signed values, specifying the bit width  of the integer
/// to expand from.
/// ```rust
/// # use arch::SignExtend;
/// assert_eq!(5u32.sign_extend(32), 5i32);
/// assert_eq!(0xffu32.sign_extend(3), -1i32);
/// assert_eq!(0b1101u32.sign_extend(4), -3i32);
/// assert_eq!(0b1111_1101u32.sign_extend(8), -3i32);
/// assert_eq!((-1i32 as u32).sign_extend(32), -1i32);
/// assert_eq!((-10i32 as u64).sign_extend(32), -10i64);
/// ```
pub trait SignExtend {
    /// Output target type (usually the signed equivalent of the implementor type.
    type Output;
    /// Extends the sign of the given value by the position specified in `bits`.
    fn sign_extend(self, width: u32) -> Self::Output;
}

impl SignExtend for u32 {
    type Output = i32;
    fn sign_extend(self, width: u32) -> Self::Output {
        debug_assert!((1..=Self::BITS).contains(&width));
        let invbits = u32::BITS - width;
        (self as Self::Output)
            .wrapping_shl(invbits)
            .wrapping_shr(invbits)
    }
}

impl SignExtend for u64 {
    type Output = i64;
    fn sign_extend(self, width: u32) -> Self::Output {
        debug_assert!((1..=Self::BITS).contains(&width));
        let invbits = Self::BITS - width;
        (self as Self::Output)
            .wrapping_shl(invbits)
            .wrapping_shr(invbits)
    }
}

/// Asserts if the given value cannot be converted to the target value.
/// ```should_panic
/// # use modinfo::TargetAddr;
/// # use arch::assert_cast;
/// // Value cannot be coerced into a smaller value
/// assert_cast::<i32, i16, _>(TargetAddr::<u64>::from_raw(0x0fff_ffffu32));
/// ```
/// ```rust
/// # use modinfo::TargetAddr;
/// # use arch::assert_cast;
/// // Value fits in smaller representation.
/// assert_eq!(10, assert_cast::<i64, i32, _>(TargetAddr::<u64>::from_raw(10u64)));
/// ```

pub fn assert_cast<F, T, RawPtr>(addr: TargetAddr<RawPtr>) -> T
where
    F: core::fmt::Display + core::marker::Copy + AsPrimitive<T> + 'static,
    RawPtr: TargetAddrPtr + AsPrimitive<F>,
    T: TryFrom<F> + 'static + core::marker::Copy,
{
    let f: F = addr.absolute_addr().as_();
    let t: T = if cfg!(debug_assertions) {
        f.try_into().map_err(|_| ()).unwrap_or_else(|_| {
            panic!(
                "Couldn't coerce value to {}:{} TO {}",
                f,
                core::any::type_name::<F>(),
                core::any::type_name::<T>(),
            )
        })
    } else {
        f.as_()
    };
    t
}

#[cfg(feature = "trap_relax")]
#[derive(Debug)]
pub struct RelaxError;

#[cfg(feature = "trap_relax")]
pub trait Relax<T>
where
    T: Target<Self::UsizePtrTy>,
{
    type UsizePtrTy;
    fn apply_relax<F>(
        trapv1: &mut trap_parser::write::TRaPV1Builder,
        builder: &T,
        f: F,
    ) -> Result<(), RelaxError>
    where
        F: Fn(TrapAddress) -> TargetAddr<Self::UsizePtrTy>;
}

/// Errors that are returned by [`Architecture`] and [`ArchUtil`] trait methods.
#[derive(Debug, PartialEq)]
pub enum ArchError {
    /// Specific operation is unimplemented. Leaves it to caller to determine if this matters.
    Unimplemented,
    /// No Global Offset Table pointer was provided.
    NoGOT,
    /// Value provided does not match system pointer size.
    InvalidPointerSize,
    /// Was unable to write to specified location.
    BadWrite,
    /// Received a [TargetError] when attempting to interact with [Target].
    Target(TargetError),
}

/// Errors returned by [`Target`] trait methods.
#[derive(Debug, PartialEq)]
pub enum TargetError {
    /// Address specified was out of range.
    OutOfRange,
    /// Unable to convert target memory to the specicied type.
    BadConversion,
}

impl From<TargetError> for ArchError {
    fn from(e: TargetError) -> Self {
        Self::Target(e)
    }
}

pub trait Target<RawPtr> {
    // Does the target contain the address
    fn contains(&self, vaddr: TargetAddr<RawPtr>) -> bool;
    // Does the target contain entire the address range
    fn contains_range(&self, vaddr: TargetAddr<RawPtr>, len: usize) -> bool;
    /// The provided Global Offset Table address for the [`Target`].
    fn got_address(&self) -> Option<TargetAddr<RawPtr>>;
}

/// An abstration around a block of memory to more easily allow reading
/// and writing byte slices and other values directly to the memory, with a given `RawPtr` address width.
pub trait TargetWrite<RawPtr>: Target<RawPtr> {
    /// Writes a given value to the given virtual address.
    fn write_vaddr<T>(&mut self, vaddr: TargetAddr<RawPtr>, val: T) -> Result<(), TargetError>
    where
        T: TryIntoCtx<Endian, Error = scroll::Error> + Copy,
    {
        let mut slice = self.write_vaddr_slice(vaddr, core::mem::size_of::<T>())?;
        slice
            .as_mut_slice()
            .pwrite(val, 0)
            .map_err(|_| TargetError::BadConversion)
            .map(|_| ())
    }

    /// Retreives a mutable slices of bytes starting at `vaddr` of length `len`.
    fn write_vaddr_slice(
        &mut self,
        vaddr: TargetAddr<RawPtr>,
        len: usize,
    ) -> Result<TargetSliceMut<'_, RawPtr>, TargetError>;

    /// Reads the largesst contiguous slice containing `vaddr`.
    fn contiguous_slice_containing_mut(
        &mut self,
        vaddr: TargetAddr<RawPtr>,
    ) -> Result<TargetSliceMut<'_, RawPtr>, TargetError>;
}

/// An abstration around a block of memory to more easily allow reading
/// and writing byte slices and other values directly to the memory, with a given `RawPtr` address width.
pub trait TargetRead<RawPtr>: Target<RawPtr>
where
    RawPtr: TargetAddrPtr,
{
    /// Reads the desired value from a given virtual address.
    fn read_vaddr<'b, T>(&'b self, vaddr: TargetAddr<RawPtr>) -> Result<T, TargetError>
    where
        T: TryFromCtx<'b, Endian, Error = scroll::Error> + Copy,
    {
        let slice = self.read_vaddr_slice(vaddr, core::mem::size_of::<T>())?;
        slice
            .into_slice()
            .pread(0)
            .map_err(|_| TargetError::BadConversion)
    }
    /// Reads a slice starting at `vaddr` of `len` bytes.
    fn read_vaddr_slice(
        &self,
        vaddr: TargetAddr<RawPtr>,
        len: usize,
    ) -> Result<TargetSlice<'_, RawPtr>, TargetError> {
        let max_slice = self.read_vaddr_max_slice(vaddr)?;
        Ok(TargetSlice {
            offset: vaddr,
            mem: max_slice
                .into_slice()
                .get(..len)
                .ok_or(TargetError::OutOfRange)?,
            got_address: self.got_address(),
        })
    }
    /// Reads the longest possible slice starting at `vaddr`.
    /// Useful for reading through memory of objects of unspecified lengths (e.g. Symbol Tables).
    fn read_vaddr_max_slice(
        &self,
        vaddr: TargetAddr<RawPtr>,
    ) -> Result<TargetSlice<'_, RawPtr>, TargetError>;
}

macro_rules! target_slices {
    (mut $name:ident) => {
        target_slices!(@ [mut] $name);
        impl<RawPtr> AsMut<[u8]> for $name<'_, RawPtr> {
            fn as_mut(&mut self) -> &mut [u8] {
                self.mem.as_mut()
            }
        }

        impl<'a, RawPtr> $name<'a, RawPtr> {
            pub fn as_mut_slice(&mut self) -> &mut [u8] {
                self.mem
            }
            pub fn into_mut_slice(self) -> &'a mut [u8] {
                self.mem
            }

        }

        impl<'a, RawPtr> TargetWrite<RawPtr> for $name<'a, RawPtr>
        where
            RawPtr: TargetAddrPtr,
        {
            /// Retreives a mutable slices of bytes starting at `vaddr` of length `len`.
            fn write_vaddr_slice(
                &mut self,
                vaddr: TargetAddr<RawPtr>,
                len: usize,
            ) -> Result<TargetSliceMut<'_, RawPtr>, TargetError> {
                if vaddr < self.offset {
                    return Err(TargetError::OutOfRange);
                }
                let new_offset = vaddr
                    .wrapping_sub(self.offset);
                let idx = new_offset
                    .absolute_addr()
                    .try_into()
                    .map_err(|_| ())
                    .unwrap();

                let slice = self.mem
                    .get_mut(idx..idx + len)
                    .ok_or_else(|| TargetError::OutOfRange)?;

                Ok(TargetSliceMut::new(slice, new_offset, self.got_address))
            }

            /// Reads the largesst contiguous slice containing `vaddr`.
            fn contiguous_slice_containing_mut(
                &mut self,
                vaddr: TargetAddr<RawPtr>,
            ) -> Result<TargetSliceMut<'_, RawPtr>, TargetError> {
                if self.contains(vaddr) {
                    Ok(TargetSliceMut::new(self.mem, self.offset, self.got_address))
                } else {
                    Err(TargetError::OutOfRange)
                }
            }
        }
    };
    ($name:ident) => {
        target_slices!(@ [] $name);

    };
    (@ [$($mut:tt)*] $name:ident) => {
        pub struct $name<'a, RawPtr> {
            mem: &'a $($mut)* [u8],
            offset: TargetAddr<RawPtr>,
            got_address: Option<TargetAddr<RawPtr>>,
        }

        impl<'a, RawPtr> $name<'a, RawPtr> {
            pub fn new(mem: &'a $($mut)* [u8], offset: TargetAddr<RawPtr>, got_address: Option<TargetAddr<RawPtr>>) -> Self{
                Self {
                    mem,
                    offset,
                    got_address
                }
            }
            pub fn as_slice(&self) -> &[u8] {
                self.mem
            }
            pub fn into_slice(self) -> &'a [u8] {
                self.mem
            }
        }

        impl<RawPtr> Target<RawPtr> for $name<'_, RawPtr>
        where
            RawPtr: TargetAddrPtr,
        {
            // Does the target contain the address
            fn contains(&self, vaddr: TargetAddr<RawPtr>) -> bool {
                let idx = vaddr.wrapping_sub(self.offset);
                self.mem
                    .get(idx.absolute_addr().try_into().map_err(|_| ()).unwrap()..)
                    .is_some()
            }
            // Does the target contain entire the address range
            fn contains_range(&self, vaddr: TargetAddr<RawPtr>, len: usize) -> bool {
                let idx = vaddr.wrapping_sub(self.offset);
                let start = idx.absolute_addr().try_into().map_err(|_| ()).unwrap();
                self.mem.get(start..start + len).is_some()
            }
            /// The provided Global Offset Table address for the [`Target`].
            fn got_address(&self) -> Option<TargetAddr<RawPtr>> {
                self.got_address
            }
        }

        impl<'a, RawPtr> TargetRead<RawPtr> for $name<'a, RawPtr>
        where
            RawPtr: TargetAddrPtr,
        {
            /// Reads the longest possible slice starting at `vaddr`.
            /// Useful for reading through memory of objects of unspecified lengths (e.g. Symbol Tables).
            fn read_vaddr_max_slice(
                &self,
                vaddr: TargetAddr<RawPtr>,
            ) -> Result<TargetSlice<'_, RawPtr>, TargetError> {
                if vaddr < self.offset {
                    return Err(TargetError::OutOfRange);
                }
                let idx = vaddr.wrapping_sub(self.offset);
                Ok(TargetSlice {
                    offset: idx,
                    mem: self
                        .mem
                        .get(idx.absolute_addr().try_into().map_err(|_| ()).unwrap()..)
                        .ok_or_else(|| TargetError::OutOfRange)?,
                    got_address: self.got_address(),
                })
            }
        }

        impl<RawPtr> AsRef<[u8]> for $name<'_, RawPtr> {
            fn as_ref(&self) -> &[u8] {
                self.mem.as_ref()
            }
        }
    }
}

target_slices!(mut TargetSliceMut);
target_slices!(TargetSlice);

#[derive(Copy, Clone, PartialEq)]
pub struct RelocationIdx(pub u32);

impl core::fmt::Debug for RelocationIdx {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let mut f = f.debug_struct("RelocationIdx");
        f.field("typ", &self.0);

        #[cfg(feature = "name_relocs")]
        f.field(
            "name",
            &goblin::elf::reloc::r_to_str(
                self.0,
                <TargetArch as Parseable>::ParserArch::MACHINE_TYPE,
            ),
        );

        f.finish()
    }
}

impl core::fmt::Display for RelocationIdx {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{}", self.0)?;
        #[cfg(feature = "name_relocs")]
        write!(
            f,
            "::{}",
            goblin::elf::reloc::r_to_str(
                self.0,
                <TargetArch as Parseable>::ParserArch::MACHINE_TYPE
            )
        )?;
        Ok(())
    }
}

impl From<RelocationIdx> for u32 {
    fn from(r: RelocationIdx) -> u32 {
        r.0
    }
}

impl From<RelocationIdx> for u64 {
    fn from(r: RelocationIdx) -> u64 {
        r.0.into()
    }
}

/// Abstraction layer around potential relocation types for [`Architecture`] and [`ArchUtil`] to interact with
/// relocations.
pub trait Relocation<RawPtr> {
    /// `type` of the relocation. Usually a value found in [`goblin::elf::reloc`].
    fn typ(&self) -> RelocationIdx;
    /// Current address of the relocation.
    fn address(&self) -> TargetAddr<RawPtr>;
    /// Address before any shuffling of the relocation.
    fn orig_address(&self) -> TargetAddr<RawPtr>;
    /// Addend for relocation.
    fn addend(&self) -> Option<TrapRelocAddend>;
    /// Address of a symbol (mutually exclusive with `symbol_data`).
    fn symbol_address(&self) -> Option<TargetAddr<RawPtr>>;
    /// Data of a symbol (mutually exclusive with `symbol_address`).
    fn symbol_data(&self) -> Option<u32>;
    /// Creates a new relocation.
    fn new(
        typ: RelocationIdx,
        ptr: TargetAddr<RawPtr>,
        symbol: Option<TargetAddr<RawPtr>>,
        addend: Option<TrapRelocAddend>,
    ) -> Self;
}

/// Provides architecture specific data/methods unrelated to relocations.
pub trait ArchUtil: Debug {
    /// Pointer relocation type from [`goblin::elf::reloc`].
    const POINTER_RELOC: u32;
    /// Copy relocation type from [`goblin::elf::reloc`].
    const COPY_RELOC: u32;

    /// Whether the given byte is a "No Op" from the given target.
    fn is_one_byte_nop(maybe_nop: u8) -> bool;

    /// Fill entire slice with a "No Ops".
    fn fill_nops(fill: &mut [u8]);
}

pub trait Parseable {
    /// The associated ParserArch
    type ParserArch: trap_parser::ParserArch;
}

/// Architecture specific methods relating to [`Relocation`] specific manipulations of a given [`Target`] memory space.
pub trait Architecture<T, R>: Debug + Parseable
where
    T: Target<Self::UsizePtrTy>,
    R: Relocation<Self::UsizePtrTy>,
{
    // This will either by the same as UIntPtrTy or `usize` depending on platform;
    type UsizePtrTy: TargetAddrPtr;
    type UIntPtrTy: IntoCtx<Endian> + SizeWith<Endian> + TargetAddrPtr;
    type IntPtrTy: TryFrom<i64> + IntoCtx<Endian> + SizeWith<Endian>;

    /// Attempt to return the target address of the given reloc.
    fn get_target_ptr(reloc: &R, target: &T) -> Option<TargetAddr<Self::UsizePtrTy>>;

    #[cfg(feature = "std")]
    /// Write a value as a uintptr for this architecture
    fn write_uintptr<V, W>(value: V, out: &mut W) -> Result<(), ArchError>
    where
        V: TryInto<Self::UIntPtrTy> + Copy,
        W: IOwrite<Endian>,
    {
        let ptr: Self::UIntPtrTy = value
            .try_into()
            .map_err(|_| ArchError::InvalidPointerSize)?;
        out.iowrite_with(ptr, scroll::LE)
            .map_err(|_| ArchError::BadWrite)?;
        Ok(())
    }

    #[cfg(feature = "std")]
    /// Write a value as an intptr for this architecture
    fn write_intptr<V, W>(value: V, out: &mut W) -> Result<(), ArchError>
    where
        V: TryInto<Self::IntPtrTy> + Copy,
        W: IOwrite<Endian>,
    {
        let ptr: Self::IntPtrTy = value
            .try_into()
            .map_err(|_| ArchError::InvalidPointerSize)?;
        out.iowrite_with(ptr, scroll::LE)
            .map_err(|_| ArchError::BadWrite)?;
        Ok(())
    }

    /// Number of bits used for relocation type.
    fn reloc_type_bits() -> usize {
        8
    }
    /// [`TrapRelocInfo`] for a given Relocation.
    fn trap_reloc_info(typ: u64) -> TrapRelocInfo {
        <Self::ParserArch as trap_parser::ParserArch>::trap_reloc_info(typ)
    }

    /// Sets the value `addr` in the given `target` specified by the [`Relocation`] `reloc`.
    fn set_target_ptr(
        reloc: &R,
        target: &mut T,
        addr: TargetAddr<Self::UsizePtrTy>,
    ) -> Result<(), ArchError>;

    /// Gets the GOT entry for a given Relocation.
    fn get_got_entry(
        reloc: &R,
        build: &T,
    ) -> Result<Option<TargetAddr<Self::UsizePtrTy>>, ArchError>;
}

/// Assert that given `delta` fits within the `bits` size spcified.
pub(crate) fn assert_delta_size<T>(bits: u32, delta: T)
where
    T: PartialOrd<T>
        + num_traits::identities::One
        + core::ops::Shl<u32, Output = T>
        + core::ops::Sub<Output = T>
        + core::ops::Neg<Output = T>,
{
    let max = (T::one() << (bits - 1)) - T::one();
    let min = -(T::one() << (bits - 1));
    debug_assert!((min..=max).contains(&delta))
}

#[cfg(test)]
mod test {
    use super::*;
    use modinfo::TargetAddrPtr;
    use proptest::prelude::*;

    use crate::TrapRelocAddend;

    pub struct TestReloc<RawPtr> {
        reloc: RelocationIdx,
        src_addr: TargetAddr<RawPtr>,
        symbol_addr: Option<TargetAddr<RawPtr>>,
        addend: Option<TrapRelocAddend>,
    }

    impl<RawPtr: TargetAddrPtr> Relocation<RawPtr> for TestReloc<RawPtr> {
        /// `type` of the relocation. Usually a value found in [`goblin::elf::reloc`].
        fn typ(&self) -> RelocationIdx {
            self.reloc
        }
        /// Current address of the relocation.
        fn address(&self) -> TargetAddr<RawPtr> {
            self.src_addr
        }
        /// Address before any shuffling of the relocation.
        fn orig_address(&self) -> TargetAddr<RawPtr> {
            self.src_addr
        }
        /// Addend for relocation.
        fn addend(&self) -> Option<TrapRelocAddend> {
            self.addend
        }
        /// Address of a symbol (mutually exclusive with `symbol_data`).
        fn symbol_address(&self) -> Option<TargetAddr<RawPtr>> {
            self.symbol_addr
        }
        /// Data of a symbol (mutually exclusive with `symbol_address`).
        fn symbol_data(&self) -> Option<u32> {
            unimplemented!()
        }
        /// Creates a new relocation.
        fn new(
            typ: RelocationIdx,
            ptr: TargetAddr<RawPtr>,
            symbol: Option<TargetAddr<RawPtr>>,
            addend: Option<TrapRelocAddend>,
        ) -> Self {
            TestReloc {
                reloc: typ,
                src_addr: ptr,
                symbol_addr: symbol,
                addend,
            }
        }
    }

    #[derive(Clone, PartialEq, Debug)]
    pub struct FakeMem<RawPtr>
    where
        RawPtr: core::fmt::LowerHex,
    {
        offset: TargetAddr<RawPtr>,
        got_address: Option<TargetAddr<RawPtr>>,
        mem: Vec<u8>,
        reloc_addr: TargetAddr<RawPtr>,
    }

    impl<RawPtr: TargetAddrPtr> FakeMem<RawPtr> {
        pub fn reloc_addr(&self) -> TargetAddr<RawPtr> {
            self.reloc_addr
        }
        pub fn target_slice_mut(&mut self) -> TargetSliceMut<'_, RawPtr> {
            TargetSliceMut::new(&mut self.mem, self.offset, self.got_address)
        }
    }

    pub fn fake_mem_strategy<RawPtr: TargetAddrPtr + Arbitrary>(
        prefix: impl Strategy<Value = Vec<u8>>,
    ) -> impl Strategy<Value = FakeMem<RawPtr>> {
        (
            prefix,
            any::<RawPtr>(),
            any::<RawPtr>(),
            prop::collection::vec(any::<u8>(), 30),
        )
            .prop_map(|(prefix, mem_offset, got_address, mem)| {
                let mem = prefix.iter().chain(mem.iter()).cloned().collect();

                FakeMem {
                    reloc_addr: TargetAddr::from_raw(
                        mem_offset + prefix.len().try_into().map_err(|_| ()).unwrap(),
                    ),
                    offset: TargetAddr::from_raw(mem_offset),
                    got_address: Some(TargetAddr::from_raw(got_address)),
                    mem,
                }
            })
    }

    pub fn verify_get_set_target_ptr<Arch, RawPtr>(
        reloc: TestReloc<
            <Arch as Architecture<TargetSliceMut<'_, RawPtr>, TestReloc<RawPtr>>>::UsizePtrTy,
        >,
        mut mem: FakeMem<
            <Arch as Architecture<TargetSliceMut<'_, RawPtr>, TestReloc<RawPtr>>>::UsizePtrTy,
        >,
    ) where
        for<'a> Arch:
            Architecture<TargetSliceMut<'a, RawPtr>, TestReloc<RawPtr>, UsizePtrTy = RawPtr>,
        RawPtr: TargetAddrPtr,
    {
        pub fn inner<'a, Arch, RawPtr>(
            reloc: TestReloc<Arch::UsizePtrTy>,
            mut mem: TargetSliceMut<'a, RawPtr>,
        ) where
            Arch: Architecture<TargetSliceMut<'a, RawPtr>, TestReloc<RawPtr>, UsizePtrTy = RawPtr>,
            RawPtr: TargetAddrPtr,
        {
            {
                let read_value = Arch::get_target_ptr(&reloc, &mem);
                let set = Arch::set_target_ptr(
                    &reloc,
                    &mut mem,
                    read_value.unwrap_or(TargetAddr::from_raw(
                        RawPtr::try_from(0u32).map_err(|_| ()).unwrap(),
                    )),
                );

                match (read_value, set) {
                    (Some(_) | None, Ok(_)) => {}
                    (None, Err(_)) => {}
                    (read, set) => {
                        panic!("Unexpected value:  Read: {:?} Set: {:?}", read, set);
                    }
                }
            }
        }
        let copy_mem = mem.clone();
        inner::<Arch, RawPtr>(reloc, mem.target_slice_mut());
        assert_eq!(copy_mem, mem);
    }

    proptest! {
       #[test]
        fn test_bits(
            value in any::<u32>(),
            offset in 0..32u32
        ) {
            let bit_val = 1 << offset;
            let on = value | bit_val;
            assert!(bit(on, offset));
            let off = value & !bit_val;
            assert!(!bit(off, offset));
        }

        #[test]
        fn test_sign_extend_u32(
            value in any::<u32>(),
            width in 1..=32u32
        ) {
            let result = value.sign_extend(width);
            assert_eq!(result < 0, bit(value, width - 1));
            assert_eq!((result << (u32::BITS - width)) as u32, value << (u32::BITS - width));
        }

        #[test]
        fn test_sign_extend_u64(
            value in any::<u64>(),
            width in 1..=64u32
        ) {
            let result = value.sign_extend(width);
            assert_eq!(result < 0, bit(value, width - 1));
            assert_eq!((result << (u64::BITS - width)) as u64, value << (u64::BITS - width));
        }
    }
}
