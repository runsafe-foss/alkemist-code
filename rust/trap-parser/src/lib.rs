// Copyright (c) 2022 RunSafe Security Inc.
#![feature(is_sorted)]
#![cfg_attr(not(feature = "std"), no_std)]
#[cfg(feature = "std")]
pub mod write;

use bitfield_struct::bitfield;
use core::convert::{TryFrom, TryInto};
use core::fmt;
use core::marker::PhantomData;
use core::ops::{Add, AddAssign, Range, Sub, SubAssign};
use fallible_iterator::{DoubleEndedFallibleIterator, FallibleIterator};
use goblin::elf;
use goblin::elf::reloc;
#[cfg(feature = "std")]
use goblin::elf::ProgramHeader;
#[cfg(feature = "std")]
use goblin::Object;
use modinfo::{TargetAddr, TargetAddrPtr};
#[cfg(feature = "std")]
use serde::{
    ser::{SerializeSeq, SerializeStruct, Serializer},
    Serialize,
};
use static_assertions::const_assert_eq;
#[cfg(feature = "std")]
use std::io::Read;
use winnow::binary::{le_i32, le_i64, le_u32, le_u64, le_u8, length_take};
use winnow::combinator::{cond, fold_repeat};
use winnow::error::ErrorKind;
use winnow::error::{ContextError, ParserError};
use winnow::prelude::*;
use winnow::stream::Offset;
use winnow::token::take;
pub use winnow::Parser;

pub const TRAP_RECORD_V2_SIZE_32: usize = 17;
pub const TRAP_RECORD_V2_SIZE_64: usize = 25;
pub const TRAP_RELOC_V2_SIZE_32: usize = 12;
pub const TRAP_RELOC_V2_SIZE_64: usize = 24;

const_assert_eq!(
    core::mem::size_of::<C_TrapRelocV2_32>(),
    TRAP_RELOC_V2_SIZE_32
);
const_assert_eq!(
    core::mem::size_of::<C_TrapRelocV2_64>(),
    TRAP_RELOC_V2_SIZE_64
);
const_assert_eq!(
    core::mem::size_of::<C_TrapRecordV2_32>(),
    TRAP_RECORD_V2_SIZE_32
);
const_assert_eq!(
    core::mem::size_of::<C_TrapRecordV2_64>(),
    TRAP_RECORD_V2_SIZE_64
);

#[derive(Copy, Clone, Debug)]
pub struct GenericParseError;

impl From<GenericParseError> for core::fmt::Error {
    fn from(_e: GenericParseError) -> Self {
        core::fmt::Error
    }
}

impl<E> From<ErrMode<E>> for GenericParseError {
    fn from(_e: ErrMode<E>) -> Self {
        GenericParseError
    }
}

#[cfg(target_pointer_width = "32")]
pub type NativeWidth = Bits32;
#[cfg(target_pointer_width = "64")]
pub type NativeWidth = Bits64;

pub struct DumpContext {
    machine: u16,
    addr_delta: u64,
}

pub trait TrapDump {
    fn dump(&self, ctx: &DumpContext, f: &mut fmt::Formatter<'_>) -> fmt::Result;
}

pub struct TrapDumper<T: TrapDump> {
    info: T,
    machine: u16,
    addr_delta: u64,
}

impl<T: TrapDump> TrapDumper<T> {
    pub fn new(info: T, machine: u16, addr_delta: u64) -> Self {
        Self {
            info,
            machine,
            addr_delta,
        }
    }
}

impl TrapDump for &dyn TrapDump {
    fn dump(&self, ctx: &DumpContext, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        (*self).dump(ctx, f)
    }
}

impl<T: TrapDump> fmt::Display for TrapDumper<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.info.dump(
            &DumpContext {
                machine: self.machine,
                addr_delta: self.addr_delta,
            },
            f,
        )
    }
}

use winnow::error::ErrMode;

trait WinnowResult<V> {
    fn map_err_winnow(self, input: &[u8]) -> Result<V, ErrMode<ContextError>>;
}

impl<V, E> WinnowResult<V> for Result<V, E> {
    fn map_err_winnow(self, input: &[u8]) -> Result<V, ErrMode<ContextError>> {
        self.map_err(|_e| ErrMode::from_error_kind(&input, ErrorKind::Verify))
    }
}

pub enum PlatformTrapInfo<'i> {
    X86(TrapInfo<'i, X86>),
    Aarch64(TrapInfo<'i, Aarch64>),
    X86_64(TrapInfo<'i, X86_64>),
    Arm(TrapInfo<'i, Arm>),
}

cfg_if::cfg_if! {
    if #[cfg(target_arch="x86")] {
        pub type TargetArch = X86;
    } else if #[cfg(target_arch="x86_64")] {
        pub type TargetArch = X86_64;
    } else if #[cfg(target_arch="arm")] {
        pub type TargetArch = Arm;
    } else if #[cfg(target_arch="aarch64")] {
        pub type TargetArch = Aarch64;
    } else {
        compile_error!{"Unknown target arch"}
    }
}

pub struct TrapParser {
    trap_file_offset: u64,
    virtual_base_address: u64,
    trapv2_padding: bool,
}

impl TrapParser {
    pub fn new(trap_file_offset: u64, virtual_base_address: u64, trapv2_padding: bool) -> Self {
        Self {
            trap_file_offset,
            virtual_base_address,
            trapv2_padding,
        }
    }

    pub fn platform<T: ParserArch>(
        &self,
    ) -> impl for<'i> FnMut(&mut &'i [u8]) -> PResult<TrapInfo<'i, T>> + '_ {
        |bytes: &mut &[u8]| {
            TrapInfo::new(
                bytes,
                T::PLATFORM,
                self.trap_file_offset,
                self.virtual_base_address,
                self.trapv2_padding,
            )
        }
    }
    pub fn target(
        &self,
    ) -> impl for<'i> FnMut(&mut &'i [u8]) -> PResult<TrapInfo<'i, TargetArch>> + '_ {
        self.platform::<TargetArch>()
    }

    pub fn runtime_platform(
        &self,
        platform: TrapPlatform,
    ) -> impl for<'i> FnMut(&mut &'i [u8]) -> PResult<PlatformTrapInfo<'i>> + '_ {
        move |bytes: &mut &[u8]| {
            let found = match platform {
                TrapPlatform::PosixX86 => {
                    PlatformTrapInfo::X86(self.platform::<X86>().parse_next(bytes)?)
                }
                TrapPlatform::PosixARM64 => {
                    PlatformTrapInfo::Aarch64(self.platform::<Aarch64>().parse_next(bytes)?)
                }
                TrapPlatform::PosixX86_64 => {
                    PlatformTrapInfo::X86_64(self.platform::<X86_64>().parse_next(bytes)?)
                }
                TrapPlatform::PosixARM => {
                    PlatformTrapInfo::Arm(self.platform::<Arm>().parse_next(bytes)?)
                }
                a => unimplemented!("Unsupported Arch {:?}", a),
            };
            Ok(found)
        }
    }
}

#[cfg(feature = "std")]
#[derive(Debug)]
pub struct ObjectInfo {
    pub data: Vec<u8>,
    pub headers: Vec<ProgramHeader>,
    pub platform: TrapPlatform,
    pub machine: u16,
    pub trap_region: Range<usize>,
    pub trap_start_addr: u64,
    pub exec_region: Option<Range<usize>>,
    pub exec_start_addr: Option<u64>,
    pub is_relocatable: bool,
}

#[derive(Debug, Clone)]
pub struct RangeMap {
    pub vm: Range<usize>,
    pub file: Range<usize>,
}

#[cfg(feature = "std")]
use thiserror::Error;

#[cfg(feature = "std")]
#[derive(Error, Debug)]
pub enum ObjectError {
    #[error("Not LFR protected binary")]
    NotLfr,
    #[error("Unsupported object")]
    InvalidObject,
    #[error("Elf Parsing Error: {0}")]
    Elf(#[from] goblin::error::Error),
    #[error("IoError: {0}")]
    Io(#[from] std::io::Error),
}

#[cfg(feature = "std")]
impl ObjectInfo {
    pub fn mem_ranges(&self) -> Vec<RangeMap> {
        self.headers
            .iter()
            .map(|header| RangeMap {
                vm: header.vm_range(),
                file: header.file_range(),
            })
            .collect()
    }
}

#[cfg(feature = "std")]
pub fn from_object<F: Read>(file: &mut F) -> Result<Vec<ObjectInfo>, ObjectError> {
    let mut buffer = Vec::new();
    file.read_to_end(&mut buffer)?;
    from_object_bytes(&buffer)
}

#[cfg(feature = "std")]
pub fn from_object_bytes(buffer: &[u8]) -> Result<Vec<ObjectInfo>, ObjectError> {
    fn find_section<'a>(
        elf: &'a elf::Elf,
        find_name: &'a str,
    ) -> impl Iterator<Item = &'a elf::SectionHeader> + 'a {
        elf.section_headers
            .iter()
            .filter(move |x| matches!(elf.shdr_strtab.get_at(x.sh_name), Some(name) if name == find_name && (x.sh_type == elf::section_header::SHT_PROGBITS)))
    }

    match Object::parse(buffer)? {
        Object::Elf(elf) => {
            let text = find_section(&elf, ".text").next();
            let platform = match elf.header.e_machine {
                elf::header::EM_386 => TrapPlatform::PosixX86,
                elf::header::EM_X86_64 => TrapPlatform::PosixX86_64,
                elf::header::EM_ARM => TrapPlatform::PosixARM,
                elf::header::EM_AARCH64 => TrapPlatform::PosixARM64,
                _ => panic!("Unhandled Elf Platform"),
            };
            let is_relocatable = elf.header.e_type == elf::header::ET_REL;

            let found: Vec<_> = find_section(&elf, ".txtrp")
                .map(|txtrp| {
                    let trap_region = txtrp.file_range().ok_or_else(|| {
                        std::io::Error::new(std::io::ErrorKind::Other, "txtrp section missing")
                    })?;
                    Ok(ObjectInfo {
                        data: buffer[trap_region.clone()].to_vec(),
                        headers: elf.program_headers.clone(),
                        machine: elf.header.e_machine,
                        platform,
                        trap_region,
                        trap_start_addr: txtrp.sh_addr,
                        exec_region: text.and_then(|t| t.file_range()),
                        exec_start_addr: text.map(|t| t.sh_addr),
                        is_relocatable,
                    })
                })
                .collect::<std::io::Result<_>>()?;

            if found.is_empty() {
                Err(ObjectError::NotLfr)
            } else {
                Ok(found)
            }
        }
        _ => Err(ObjectError::InvalidObject),
    }
}

#[derive(Debug, Clone)]
pub struct TrapV1<'i, T> {
    context: Context<'i>,
    nonexec_relocs: &'i [u8],
    records: &'i [u8],
    _phantom: PhantomData<T>,
}

#[cfg(feature = "std")]
impl<T> Serialize for TrapV1<'_, T>
where
    T: ParserArch,
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut state = serializer.serialize_struct("TrapV1", 2)?;
        state.serialize_field("version", &self.context.header.version)?;
        state.serialize_field("flags", &self.context.header.flags)?;
        //state.serialize_field("nonexec_relocs", &self.nonexec_relocs())?;
        state.serialize_field("records", &self.records())?;
        state.end()
    }
}

impl<'i, T> TrapV1<'i, T>
where
    T: ParserArch,
{
    pub fn nonexec_relocs(&self) -> TrapRelocIterator<'i, '_, T> {
        TrapRelocIterator {
            ctx: &self.context,
            data: self.nonexec_relocs,
            addr: TrapAddress(0),
            _phantom: PhantomData,
        }
    }

    pub fn records(&self) -> TrapRecordIterator<'i, '_, T> {
        TrapRecordIterator {
            ctx: &self.context,
            data: self.records,
            _phantom: PhantomData,
        }
    }

    pub fn header(&self) -> &TrapHeader {
        &self.context.header
    }
}

type TrapRelocV2Result = Result<(u16, Option<u64>), core::array::TryFromSliceError>;

pub trait ParserArch {
    type ArchBits: Bits;
    const PLATFORM: TrapPlatform;
    const MACHINE_TYPE: u16;
    type CTrapV2RelocType;
    type CTrapV2RecordType;
    fn trapv2_reloc_type_symbol(bytes: &[u8]) -> TrapRelocV2Result;
    fn trap_reloc_info(typ: u64) -> TrapRelocInfo;
}

#[derive(Debug)]
pub struct X86 {}
#[repr(C, packed(1))]
pub struct C_TrapRelocV2_32 {
    addr: u32,
    typ_symbol_index: u32,
    addend: i32,
}

#[repr(C, packed(1))]
pub struct C_TrapRecordV2_32 {
    addr: u32,
    size: u32,
    align_typ: u8,
    reloc_count: u32,
    reloc_start_index: u32,
}

pub struct Bits32 {}
pub struct Bits64 {}

pub trait Bits {
    type UIntPtr: TryInto<u64> + TryInto<u32> + Into<TrapAddress> + TryInto<usize>;
    type IntPtr: TryInto<i64>;
    fn pointer_size_bits() -> usize {
        core::mem::size_of::<Self::UIntPtr>() * 8
    }
    fn read_uintptr(input: &mut &[u8]) -> PResult<Self::UIntPtr>;
    fn read_intptr(input: &mut &[u8]) -> PResult<Self::IntPtr>;
}

impl Bits for Bits32 {
    type UIntPtr = u32;
    type IntPtr = i32;
    fn read_uintptr(input: &mut &[u8]) -> PResult<Self::UIntPtr> {
        le_u32(input)
    }
    fn read_intptr(input: &mut &[u8]) -> PResult<Self::IntPtr> {
        le_i32(input)
    }
}

impl Bits for Bits64 {
    type UIntPtr = u64;
    type IntPtr = i64;
    fn read_uintptr(input: &mut &[u8]) -> PResult<Self::UIntPtr> {
        le_u64(input)
    }
    fn read_intptr(input: &mut &[u8]) -> PResult<Self::IntPtr> {
        le_i64(input)
    }
}

impl ParserArch for X86 {
    type ArchBits = Bits32;
    type CTrapV2RelocType = C_TrapRelocV2_32;
    type CTrapV2RecordType = C_TrapRecordV2_32;
    const PLATFORM: TrapPlatform = TrapPlatform::PosixX86;
    const MACHINE_TYPE: u16 = elf::header::EM_386;
    fn trapv2_reloc_type_symbol(bytes: &[u8]) -> TrapRelocV2Result {
        #[bitfield(u32)]
        struct TrapRelocV2_32Bits {
            pub typ: u8,
            #[bits(24)]
            pub symbol_idx: u32,
        }
        let v = TrapRelocV2_32Bits::from(u32::from_le_bytes(bytes.try_into().unwrap()));
        Ok((
            v.typ().into(),
            if v.symbol_idx() == (1 << 24) - 1 {
                None
            } else {
                Some(v.symbol_idx().into())
            },
        ))
    }
    fn trap_reloc_info(typ: u64) -> TrapRelocInfo {
        match typ as u32 {
            reloc::R_386_PC32
            | reloc::R_386_GOT32
            | reloc::R_386_PLT32
            | reloc::R_386_GOTOFF
            | reloc::R_386_GOTPC
            | reloc::R_386_GOT32X => TrapRelocInfo::new().with_addend(true),
            reloc::R_386_TLS_TPOFF
            | reloc::R_386_TLS_IE
            | reloc::R_386_TLS_GOTIE
            | reloc::R_386_TLS_LE
            | reloc::R_386_TLS_GD
            | reloc::R_386_TLS_LDM
            | reloc::R_386_TLS_LDO_32
            | reloc::R_386_TLS_IE_32
            | reloc::R_386_TLS_LE_32
            | reloc::R_386_TLS_DTPMOD32
            | reloc::R_386_TLS_DTPOFF32
            | reloc::R_386_TLS_TPOFF32
            | reloc::R_386_TLS_GOTDESC
            | reloc::R_386_TLS_DESC_CALL
            | reloc::R_386_TLS_DESC => TrapRelocInfo::new().with_ignore(true),
            _ => TrapRelocInfo::new(),
        }
    }
}
#[derive(Debug)]
pub struct Arm {}
impl ParserArch for Arm {
    type ArchBits = Bits32;
    type CTrapV2RelocType = C_TrapRelocV2_32;
    type CTrapV2RecordType = C_TrapRecordV2_32;
    const PLATFORM: TrapPlatform = TrapPlatform::PosixARM;
    const MACHINE_TYPE: u16 = elf::header::EM_ARM;
    fn trapv2_reloc_type_symbol(bytes: &[u8]) -> TrapRelocV2Result {
        #[bitfield(u32)]
        struct TrapRelocV2_32Bits {
            pub typ: u8,
            #[bits(24)]
            pub symbol_idx: u32,
        }
        let v = TrapRelocV2_32Bits::from(u32::from_le_bytes(bytes.try_into().unwrap()));
        Ok((
            v.typ().into(),
            if v.symbol_idx() == (1 << 24) - 1 {
                None
            } else {
                Some(v.symbol_idx().into())
            },
        ))
    }
    fn trap_reloc_info(typ: u64) -> TrapRelocInfo {
        match typ as u32 {
            reloc::R_ARM_CALL | reloc::R_ARM_ABS32 => TrapRelocInfo::new(),
            reloc::R_ARM_REL32 => TrapRelocInfo::new().with_addend(true),
            reloc::R_ARM_GOTOFF
            | reloc::R_ARM_GOTPC
            | reloc::R_ARM_GOT32
            | reloc::R_ARM_GOT_PREL
            | reloc::R_ARM_TLS_GD32
            | reloc::R_ARM_TLS_LDM32
            | reloc::R_ARM_TLS_IE32
            | reloc::R_ARM_TARGET2
            | reloc::R_ARM_PREL31 => TrapRelocInfo::new().with_addend(true),
            reloc::R_ARM_MOVW_ABS_NC
            | reloc::R_ARM_MOVT_ABS
            | reloc::R_ARM_MOVW_PREL_NC
            | reloc::R_ARM_MOVT_PREL
            | reloc::R_ARM_THM_MOVW_ABS_NC
            | reloc::R_ARM_THM_MOVT_ABS
            | reloc::R_ARM_THM_MOVW_PREL_NC
            | reloc::R_ARM_THM_MOVT_PREL => {
                // All R_ARM_MOV[TW] variants (excluding BREL)
                TrapRelocInfo::new().with_symbol(true).with_addend(true)
            }
            reloc::R_ARM_TLS_LDO32
            | reloc::R_ARM_TLS_LE32
            | reloc::R_ARM_TLS_LDO12
            | reloc::R_ARM_TLS_LE12
            | reloc::R_ARM_TLS_IE12GP => TrapRelocInfo::new().with_ignore(true),
            _ => TrapRelocInfo::new(),
        }
    }
}

#[repr(C, packed(1))]
pub struct C_TrapRelocV2_64 {
    addr: u64,
    typ_symbol_index: u64,
    addend: i64,
}

#[repr(C, packed(1))]
pub struct C_TrapRecordV2_64 {
    addr: u64,
    size: u64,
    align_typ: u8,
    reloc_count: u32,
    reloc_start_index: u32,
}
#[derive(Debug)]
pub struct Aarch64 {}
impl ParserArch for Aarch64 {
    type ArchBits = Bits64;
    type CTrapV2RelocType = C_TrapRelocV2_64;
    type CTrapV2RecordType = C_TrapRecordV2_64;
    const PLATFORM: TrapPlatform = TrapPlatform::PosixARM64;
    const MACHINE_TYPE: u16 = elf::header::EM_AARCH64;
    fn trapv2_reloc_type_symbol(bytes: &[u8]) -> TrapRelocV2Result {
        #[bitfield(u64)]
        struct TrapRelocV2Aarch64Bits {
            #[bits(11)]
            pub typ: u16,
            #[bits(53)]
            pub symbol_idx: u64,
        }

        let v = TrapRelocV2Aarch64Bits::from(u64::from_le_bytes(bytes.try_into().unwrap()));
        Ok((
            v.typ(),
            if v.symbol_idx() == (1 << 53) - 1 {
                None
            } else {
                Some(v.symbol_idx())
            },
        ))
    }

    fn trap_reloc_info(typ: u64) -> TrapRelocInfo {
        match typ as u32 {
            reloc::R_AARCH64_PREL64 | reloc::R_AARCH64_PREL32 => {
                TrapRelocInfo::new().with_addend(true)
            }
            // Relocations that contain some subset of the address
            // of this symbol's GOT entry; we collect all such relocations
            // for each symbol, and merge them together to obtain the full address
            reloc::R_AARCH64_MOVW_GOTOFF_G0 | reloc::R_AARCH64_MOVW_GOTOFF_G0_NC => {
                TrapRelocInfo::new().with_arm64_got_group(true)
            }
            reloc::R_AARCH64_ADR_GOT_PAGE => TrapRelocInfo::new().with_arm64_got_page(true),
            reloc::R_AARCH64_MOVW_UABS_G0
            | reloc::R_AARCH64_MOVW_UABS_G0_NC
            | reloc::R_AARCH64_MOVW_UABS_G1
            | reloc::R_AARCH64_MOVW_UABS_G1_NC
            | reloc::R_AARCH64_MOVW_UABS_G2
            | reloc::R_AARCH64_MOVW_UABS_G2_NC
            | reloc::R_AARCH64_MOVW_UABS_G3
            | reloc::R_AARCH64_ADR_PREL_PG_HI21
            | reloc::R_AARCH64_ADR_PREL_PG_HI21_NC
            | reloc::R_AARCH64_ADD_ABS_LO12_NC
            | reloc::R_AARCH64_LDST8_ABS_LO12_NC
            | reloc::R_AARCH64_LDST16_ABS_LO12_NC
            | reloc::R_AARCH64_LDST32_ABS_LO12_NC
            | reloc::R_AARCH64_LDST64_ABS_LO12_NC
            | reloc::R_AARCH64_LDST128_ABS_LO12_NC => {
                TrapRelocInfo::new().with_symbol(true).with_addend(true)
            }
            _ => TrapRelocInfo::new(),
        }
    }
}

#[derive(Debug)]
pub struct X86_64 {}
impl ParserArch for X86_64 {
    type ArchBits = Bits64;
    type CTrapV2RelocType = C_TrapRelocV2_64;
    type CTrapV2RecordType = C_TrapRecordV2_64;
    const PLATFORM: TrapPlatform = TrapPlatform::PosixX86_64;
    const MACHINE_TYPE: u16 = elf::header::EM_X86_64;
    fn trapv2_reloc_type_symbol(bytes: &[u8]) -> TrapRelocV2Result {
        #[bitfield(u64)]
        struct TrapRelocV264Bits {
            pub typ: u8,
            #[bits(56)]
            pub symbol_idx: u64,
        }

        let v = TrapRelocV264Bits::from(u64::from_le_bytes(bytes.try_into().unwrap()));
        Ok((
            v.typ() as u16,
            if v.symbol_idx() == (1 << 56) - 1 {
                None
            } else {
                Some(v.symbol_idx())
            },
        ))
    }
    fn trap_reloc_info(typ: u64) -> TrapRelocInfo {
        match typ as u32 {
            reloc::R_X86_64_PC32
            | reloc::R_X86_64_GOT32
            | reloc::R_X86_64_PLT32
            | reloc::R_X86_64_GOTPCREL
            | reloc::R_X86_64_TLSGD
            | reloc::R_X86_64_TLSLD
            | reloc::R_X86_64_GOTTPOFF
            | reloc::R_X86_64_PC64
            | reloc::R_X86_64_GOTPC32
            | reloc::R_X86_64_GOT64
            | reloc::R_X86_64_GOTPCREL64
            | reloc::R_X86_64_GOTPC64
            | reloc::R_X86_64_GOTPLT64
            | reloc::R_X86_64_GOTPC32_TLSDESC
            | reloc::R_X86_64_GOTPCRELX
            | reloc::R_X86_64_REX_GOTPCRELX => TrapRelocInfo::new().with_addend(true),
            reloc::R_X86_64_DTPOFF64
            | reloc::R_X86_64_TPOFF64
            | reloc::R_X86_64_DTPOFF32
            | reloc::R_X86_64_TPOFF32 => TrapRelocInfo::new().with_ignore(true),
            _ => TrapRelocInfo::new(),
        }
    }
}

#[derive(Debug)]
pub struct TrapV2<'i, T> {
    context: Context<'i>,
    records: &'i [u8],
    relocs: &'i [u8],
    _phantom: core::marker::PhantomData<T>,
}

impl<'i, T> TrapV2<'i, T>
where
    T: ParserArch,
    TrapRecordV2: SliceRead<T>,
    TrapRelocV2: SliceRead<T>,
{
    fn try_from_context(input: &mut &'i [u8], context: Context<'i>) -> PResult<Self> {
        if context.header.version == 2
            && T::ArchBits::pointer_size_bits() == context.header.pointer_size
        {
            *input = context.trap_data;
            const TRAP_HEADER_V2_SIZE: usize = 8;
            let _header = take(TRAP_HEADER_V2_SIZE).parse_next(input)?;
            let records = V2SliceIter::<T, TrapRecordV2>::from_input(&context, input)?;
            let relocs = V2SliceIter::<T, TrapRelocV2>::from_input(&context, input)?;
            //Anything left after TrapV2 is unused;
            *input = &[];
            Ok(TrapV2 {
                records: records.remain(),
                relocs: relocs.remain(),
                context,
                _phantom: PhantomData,
            })
        } else {
            Err(ErrMode::from_error_kind(
                &context.trap_data,
                winnow::error::ErrorKind::Tag,
            ))
        }
    }
}
impl<'i, T> TrapV2<'i, T>
where
    T: ParserArch,
    TrapRecordV2: SliceRead<T>,
    TrapRelocV2: SliceRead<T>,
{
    pub fn records(
        &self,
    ) -> impl FallibleIterator<Item = TrapRecordV2, Error = GenericParseError> + '_ {
        V2SliceIter::<T, TrapRecordV2>::from_slice(&self.context, self.records).unwrap()
    }

    pub fn relocs(
        &self,
    ) -> impl FallibleIterator<Item = TrapRelocV2, Error = GenericParseError> + '_ {
        V2SliceIter::<T, TrapRelocV2>::from_slice(&self.context, self.relocs).unwrap()
    }
}

impl<T> TrapV2<'_, T> {
    pub fn header(&self) -> &TrapHeader {
        &self.context.header
    }
}

struct V2SliceIter<'i, 'c, P, T> {
    ctx: &'c Context<'i>,
    data: &'i [u8],
    _phantom: PhantomData<(P, T)>,
}

impl<'i, 'c, P, T: SliceRead<P>> V2SliceIter<'i, 'c, P, T> {
    pub fn from_input(ctx: &'c Context<'i>, input: &mut &'i [u8]) -> PResult<Self> {
        let count = le_u32.try_map(usize::try_from).parse_next(input)?;
        let slice = take(<T as SliceRead<P>>::size_bytes() * count).parse_next(input)?;
        Ok(Self {
            ctx,
            data: slice,
            _phantom: PhantomData,
        })
    }
    pub fn from_slice(ctx: &'c Context<'i>, slice: &'i [u8]) -> PResult<Self> {
        Ok(Self {
            ctx,
            data: slice,
            _phantom: PhantomData,
        })
    }
    pub fn remain(&self) -> &'i [u8] {
        self.data
    }
}

impl<'i, 'c, P, T: SliceRead<P>> DoubleEndedFallibleIterator for V2SliceIter<'i, 'c, P, T> {
    fn next_back(&mut self) -> Result<Option<Self::Item>, Self::Error> {
        if self.data.is_empty() {
            return Ok(None);
        }
        let (before, mut entry_bytes) = self.data.split_at(self.data.len() - T::size_bytes());
        let entry = T::read(&mut entry_bytes, self.ctx)?;
        assert_eq!(entry_bytes, &[][..]);
        self.data = before;
        Ok(Some(entry))
    }
}

impl<'i, 'c, P, T: SliceRead<P>> FallibleIterator for V2SliceIter<'i, 'c, P, T> {
    type Item = T;
    type Error = GenericParseError;
    fn next(&mut self) -> Result<Option<Self::Item>, Self::Error> {
        if self.data.is_empty() {
            return Ok(None);
        }
        let (mut entry_bytes, after) = self.data.split_at(T::size_bytes());
        let entry = T::read(&mut entry_bytes, self.ctx).map_err(|_| GenericParseError)?;
        assert_eq!(entry_bytes, &[][..]);
        self.data = after;
        Ok(Some(entry))
    }
    fn nth(&mut self, n: usize) -> Result<Option<Self::Item>, Self::Error> {
        let target = <T as SliceRead<P>>::size_bytes() * n;
        self.data = if target < self.data.len() {
            &self.data[target..]
        } else {
            &[]
        };
        self.next()
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        let size = self.data.len() / <T as SliceRead<P>>::size_bytes();
        (size, Some(size))
    }
}

impl<'i, T> TrapInfo<'i, T>
where
    T: ParserArch,
    TrapRelocV2: SliceRead<T>,
    TrapRecordV2: SliceRead<T>,
{
    pub fn new(
        input: &mut &'i [u8],
        platform: TrapPlatform,
        trap_start_addr: u64,
        base_address: u64,
        trapv2_padding: bool,
    ) -> PResult<TrapInfo<'i, T>> {
        let trap_data = *input;
        let version = le_u8.parse_next(input)?;
        let flags = take(3usize)
            .map(|bytes: &[u8]| TrapHeaderFlags::from_le_bytes(bytes.try_into().unwrap()))
            .parse_next(input)?;
        let header = TrapHeader {
            flags,
            platform,
            version,
            pointer_size: platform.pointer_size(),
        };
        let mut context = Context {
            header,
            trap_data,
            trap_start_addr,
            base_address,
            trapv2_padding,
        };
        let nonexec_relocs = cond(flags.has_nonexec_relocs(), |input: &mut _| {
            RelocReader::<T>::skip(input, &context)
        })
        .parse_next(input)?;

        let pointer_size = cond(flags.has_pointer_size(), |input: &mut _| {
            if context.header.version == 1 {
                read_uleb64.parse_next(input)
            } else {
                le_u32.map(u64::from).parse_next(input)
            }
        })
        .parse_next(input)?;
        if let Some(pointer_size) = pointer_size {
            assert!(pointer_size == 32 || pointer_size == 64);
            context.header.pointer_size = pointer_size as usize;
        }

        if context.header.pointer_size != T::ArchBits::pointer_size_bits() {
            return Err(ErrMode::from_error_kind(
                input,
                winnow::error::ErrorKind::Fail,
            ));
        }

        let info = match context.header.version {
            1 => {
                let trapv1_padding = if flags.has_known_post_padding() {
                    read_padding_from_end(input).unwrap()
                } else {
                    // These values are from TrapFooterFiller.S and must be kept in sync
                    match platform {
                        TrapPlatform::PosixARM | TrapPlatform::PosixX86 => 34,
                        TrapPlatform::PosixARM64 | TrapPlatform::PosixX86_64 => 50,
                        _ => panic!("Unsupported platform"),
                    }
                };

                let records_len = if input.len() < trapv1_padding {
                    input.len()
                } else {
                    let (trimmed, maybe_pad) = input.split_at(input.len() - trapv1_padding);
                    if flags.has_known_post_padding() || maybe_pad.iter().all(|&c| c == 0xcc) {
                        trimmed.len()
                    } else {
                        input.len()
                    }
                };
                let padding_len = input.len() - records_len;

                let records = take(records_len).parse_next(input)?;
                let _padding = take(padding_len).parse_next(input)?;

                TrapInfo::TrapV1(TrapV1 {
                    context,
                    nonexec_relocs: nonexec_relocs.unwrap_or(&[]),
                    records,
                    _phantom: PhantomData,
                })
            }
            2 => TrapInfo::TrapV2(TrapV2::<T>::try_from_context(input, context)?),
            v => unimplemented!("Unsupported Trap Version {}", v),
        };

        Ok(info)
    }

    pub fn header(&self) -> &TrapHeader {
        let ctx = match self {
            TrapInfo::TrapV1(v1) => &v1.context,
            TrapInfo::TrapV2(v2) => &v2.context,
        };
        &ctx.header
    }
}

impl<'i, T> TrapDump for TrapV2<'i, T>
where
    T: ParserArch,
    TrapRelocV2: SliceRead<T>,
    TrapRecordV2: SliceRead<T>,
{
    fn dump(&self, ctx: &DumpContext, f: &mut fmt::Formatter) -> fmt::Result {
        self.context.header.dump(ctx, f)?;
        let mut records = self.records();
        while let Some(r) = records.next()? {
            r.dump(ctx, f)?;
            f.write_str("\n")?;
            let mut relocs = r.relocs(self);
            while let Some(reloc) = relocs.next()? {
                f.write_str("\t")?;
                reloc.dump(ctx, f)?;
                f.write_str("\n")?;
            }
        }
        Ok(())
    }
}

impl<'i, T> TrapDump for TrapV1<'i, T>
where
    T: ParserArch,
{
    fn dump(&self, ctx: &DumpContext, f: &mut fmt::Formatter) -> fmt::Result {
        self.context.header.dump(ctx, f)?;
        if self.nonexec_relocs().next()?.is_some() {
            write!(f, "NonExec Relocs:\n\n")?;
            let mut relocs = self.nonexec_relocs();
            while let Some(nonexec) = relocs.next()? {
                nonexec.dump(ctx, f)?;
            }
        }
        let mut records = self.records();
        while let Some(r) = records.next()? {
            r.dump(ctx, f)?;
            f.write_str("\n")?;
            let mut syms = r.symbols();
            while let Some(s) = syms.next()? {
                f.write_str("\t")?;
                s.dump(ctx, f)?;
                f.write_str("\n")?;
            }
            let mut relocs = r.relocs();
            while let Some(reloc) = relocs.next()? {
                f.write_str("\t")?;
                reloc.dump(ctx, f)?;
                f.write_str("\n")?;
            }
            let mut data_refs = r.data_refs();
            while let Some(data) = data_refs.next()? {
                f.write_str("\t")?;
                write!(f, "\t0x{:08x}", data)?;
                f.write_str("\n")?;
            }
            if let Some(p) = r.padding {
                f.write_str("\t")?;
                p.dump(ctx, f)?;
                f.write_str("\n")?;
            }
        }
        Ok(())
    }
}

#[derive(Debug)]
pub enum TrapInfo<'i, T> {
    TrapV1(TrapV1<'i, T>),
    TrapV2(TrapV2<'i, T>),
}

impl TrapDump for TrapHeader {
    fn dump(&self, _ctx: &DumpContext, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "{:?}", self.flags)?;
        let mut flag_bytes = self.flags.to_le_bytes();
        flag_bytes.reverse();
        writeln!(
            f,
            "Self: {:02x?}{:02x} Version: {:02x} Flags: {:02x?} PtrSize {:02}",
            flag_bytes, self.version, self.version, flag_bytes, self.pointer_size
        )?;
        Ok(())
    }
}

impl<'i, T> TrapDump for TrapInfo<'i, T>
where
    T: ParserArch,
    TrapRelocV2: SliceRead<T>,
    TrapRecordV2: SliceRead<T>,
{
    fn dump(&self, ctx: &DumpContext, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            TrapInfo::TrapV1(v1) => v1.dump(ctx, f),
            TrapInfo::TrapV2(v2) => v2.dump(ctx, f),
        }?;
        Ok(())
    }
}

#[derive(Debug, Clone)]
pub struct Context<'i> {
    header: TrapHeader,
    trap_data: &'i [u8],

    /// Address of the start of the .txtrp section
    trap_start_addr: u64,

    /// Virtual base address of the file
    base_address: u64,

    /// Whether this file has trapv2 padding or not.
    trapv2_padding: bool,
}

impl<'i> Context<'i> {
    fn trap_info_address(&self, data: &mut &[u8]) -> TrapAddress {
        TrapAddress(self.trap_start_addr + data.offset_from(&self.trap_data) as u64)
    }
}

#[derive(Debug, Copy)]
pub struct TrapRelocIterator<'i, 'c, T> {
    ctx: &'c Context<'i>,
    data: &'i [u8],
    addr: TrapAddress,
    _phantom: PhantomData<T>,
}

impl<T> Clone for TrapRelocIterator<'_, '_, T> {
    fn clone(&self) -> Self {
        Self {
            ctx: self.ctx,
            data: self.data,
            addr: self.addr,
            _phantom: PhantomData,
        }
    }
}

impl<'i, 'c, T> FallibleIterator for TrapRelocIterator<'i, 'c, T>
where
    T: ParserArch,
{
    type Item = TrapReloc;
    type Error = GenericParseError;
    fn next(&mut self) -> Result<Option<Self::Item>, Self::Error> {
        if self.data.is_empty() {
            return Ok(None);
        }
        let mut reloc =
            RelocReader::<T>::read(&mut self.data, self.ctx).map_err(|_| GenericParseError)?;
        reloc.address = TrapAddress::new_from_delta(&mut self.addr, reloc.address);
        Ok(Some(reloc))
    }
}

#[cfg(feature = "std")]
impl<T> Serialize for TrapRelocIterator<'_, '_, T>
where
    T: ParserArch,
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut seq = serializer.serialize_seq(None)?;
        let mut cloned = (*self).clone();
        while let Some(reloc) = cloned
            .next()
            .map_err(|_| serde::ser::Error::custom("invalid reloc"))?
        {
            seq.serialize_element(&WithContext::new(self.ctx, &reloc))?;
        }
        seq.end()
    }
}

pub struct TrapRecordIterator<'i, 'c, T> {
    ctx: &'c Context<'i>,
    data: &'i [u8],
    _phantom: PhantomData<T>,
}
impl<T> Clone for TrapRecordIterator<'_, '_, T> {
    fn clone(&self) -> Self {
        Self {
            ctx: self.ctx,
            data: self.data,
            _phantom: PhantomData,
        }
    }
}

#[cfg(feature = "std")]
impl<T> Serialize for TrapRecordIterator<'_, '_, T>
where
    T: ParserArch,
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut seq = serializer.serialize_seq(None)?;
        let mut cloned = (*self).clone();
        while let Some(record) = cloned
            .next()
            .map_err(|_| serde::ser::Error::custom("invalid record"))?
        {
            seq.serialize_element(&record)?;
        }
        seq.end()
    }
}

impl<'i, 'c, T> FallibleIterator for TrapRecordIterator<'i, 'c, T>
where
    T: ParserArch,
{
    type Item = TrapRecord<'i, 'c, T>;
    type Error = GenericParseError;
    fn next(&mut self) -> Result<Option<Self::Item>, Self::Error> {
        if self.data.is_empty() {
            return Ok(None);
        }
        let record = read_record(&mut self.data, self.ctx).map_err(|_| GenericParseError)?;
        Ok(Some(record))
    }
}

#[derive(Copy, Clone)]
pub struct TrapSymbolIterator<'i, 'c> {
    ctx: &'c Context<'i>,
    data: &'i [u8],
    addr: TrapAddress,
}

#[cfg(feature = "std")]
impl Serialize for TrapSymbolIterator<'_, '_> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut seq = serializer.serialize_seq(None)?;
        let mut cloned = *self;
        while let Some(symbol) = cloned
            .next()
            .map_err(|_| serde::ser::Error::custom("invalid symbol"))?
        {
            seq.serialize_element(&symbol)?;
        }
        seq.end()
    }
}

impl<'i, 'c> FallibleIterator for TrapSymbolIterator<'i, 'c> {
    type Item = TrapSymbol;
    type Error = GenericParseError;

    fn next(&mut self) -> Result<Option<Self::Item>, Self::Error> {
        if self.data.is_empty() {
            return Ok(None);
        }
        let mut symbol =
            SymbolReader::read(&mut self.data, self.ctx).map_err(|_| GenericParseError)?;
        // Symbol addresses are encoded as offset from last symbol
        symbol.address = TrapAddress::new_from_delta(&mut self.addr, symbol.address);
        Ok(Some(symbol))
    }
}

pub struct TrapDataRefIterator<'i> {
    data: Option<&'i [u8]>,
}

impl<'i> FallibleIterator for TrapDataRefIterator<'i> {
    type Item = u64;
    type Error = GenericParseError;
    fn next(&mut self) -> Result<Option<Self::Item>, Self::Error> {
        let data = match self.data.as_mut() {
            Some(d) => d,
            None => return Ok(None),
        };
        let record = read_uleb64
            .parse_next(data)
            .map_err(|_| GenericParseError)?;
        Ok(Some(record))
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
#[repr(u32)]
pub enum TrapPlatform {
    Unknown = 0,
    PosixX86,
    PosixX86_64,
    PosixARM,
    PosixARM64,
    Win32,
    Win64,
}

impl TrapPlatform {
    pub fn pointer_size(&self) -> usize {
        match self {
            TrapPlatform::PosixX86 | TrapPlatform::PosixARM | TrapPlatform::Win32 => 32,
            TrapPlatform::PosixX86_64 | TrapPlatform::PosixARM64 | TrapPlatform::Win64 => 64,
            TrapPlatform::Unknown => 8 * core::mem::size_of::<usize>(),
        }
    }
    pub fn machine(&self) -> u16 {
        match self {
            TrapPlatform::PosixX86 => X86::MACHINE_TYPE,
            TrapPlatform::PosixARM => Arm::MACHINE_TYPE,
            TrapPlatform::PosixX86_64 => X86_64::MACHINE_TYPE,
            TrapPlatform::PosixARM64 => Aarch64::MACHINE_TYPE,
            TrapPlatform::Win32 | TrapPlatform::Win64 | TrapPlatform::Unknown => {
                todo!("Unsupported platform")
            }
        }
    }
}

impl From<u32> for TrapPlatform {
    fn from(v: u32) -> TrapPlatform {
        match v {
            1 => TrapPlatform::PosixX86,
            2 => TrapPlatform::PosixX86_64,
            3 => TrapPlatform::PosixARM,
            4 => TrapPlatform::PosixARM64,
            5 => TrapPlatform::Win32,
            6 => TrapPlatform::Win64,
            _ => TrapPlatform::Unknown,
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum TrapRecordType {
    Function = 0,
    Padding = 1,
    Gap = 2,
    NonExecRegion = 3,
    Got = 4,
    Unknown,
}

impl From<TrapRecordType> for u8 {
    fn from(ty: TrapRecordType) -> u8 {
        match ty {
            TrapRecordType::Function => 0,
            TrapRecordType::Padding => 1,
            TrapRecordType::Gap => 2,
            TrapRecordType::NonExecRegion => 3,
            TrapRecordType::Got => 4,
            TrapRecordType::Unknown => panic!("Unknown TrapRecordType"),
        }
    }
}

impl From<u32> for TrapRecordType {
    fn from(v: u32) -> TrapRecordType {
        match v {
            0 => TrapRecordType::Function,
            1 => TrapRecordType::Padding,
            2 => TrapRecordType::Gap,
            3 => TrapRecordType::NonExecRegion,
            4 => TrapRecordType::Got,
            _ => TrapRecordType::Unknown,
        }
    }
}
impl TryFrom<TrapRecordType> for u32 {
    type Error = GenericParseError;
    fn try_from(v: TrapRecordType) -> Result<u32, GenericParseError> {
        match v {
            TrapRecordType::Function => Ok(0),
            TrapRecordType::Padding => Ok(1),
            TrapRecordType::Gap => Ok(2),
            TrapRecordType::NonExecRegion => Ok(3),
            TrapRecordType::Got => Ok(4),
            _ => Err(GenericParseError),
        }
    }
}

//Note: TrapHeaderFlags are actually 24 bits, this has been extended to match u32.
#[bitfield(u32)]
#[derive(Eq, PartialEq)]
#[cfg_attr(feature = "std", derive(Serialize))]
pub struct TrapHeaderFlags {
    pub functions_marked: bool,
    pub pre_sorted: bool,
    pub has_symbol_size: bool,
    pub has_data_refs: bool,
    pub has_record_relocs: bool,
    pub has_nonexec_relocs: bool,
    pub has_record_padding: bool,
    pub pc_relative_addresses: bool,
    pub has_symbol_p2align: bool,
    pub has_pointer_size: bool,
    pub base_relative_addresses: bool,
    pub has_vector_lengths: bool,
    pub has_trapv2_metadata: bool,
    pub has_known_post_padding: bool,
    pub pre_relaxed: bool,
    #[bits(9)]
    _unused: u16,
    #[bits(8)]
    _unused2: u8,
}

impl TrapHeaderFlags {
    pub fn from_le_bytes(bytes: [u8; 3]) -> Self {
        let mut new = [0u8; 4];
        new[0..3].copy_from_slice(&bytes);
        TrapHeaderFlags::from(u32::from_le_bytes(new))
    }
    pub fn to_le_bytes(&self) -> [u8; 3] {
        u32::from(*self).to_le_bytes()[0..3].try_into().unwrap()
    }
}

// The definition of TrapRelocInfoC must match this bitfield.
#[bitfield(u32)]
pub struct TrapRelocInfo {
    pub symbol: bool,
    pub addend: bool,
    pub ignore: bool,
    #[bits(13)]
    _unused: u16,
    pub arm64_got_page: bool,
    pub arm64_got_group: bool,
    #[bits(14)]
    _unused2: u16,
}

const_assert_eq!(
    TrapRelocInfo::new().with_symbol(true).0,
    TrapRelocInfoC::Symbol as u32
);

const_assert_eq!(
    TrapRelocInfo::new().with_addend(true).0,
    TrapRelocInfoC::Addend as u32
);

const_assert_eq!(
    TrapRelocInfo::new().with_ignore(true).0,
    TrapRelocInfoC::Ignore as u32
);

const_assert_eq!(
    TrapRelocInfo::new().with_arm64_got_page(true).0,
    TrapRelocInfoC::Arm64GotPage as u32
);

const_assert_eq!(
    TrapRelocInfo::new().with_arm64_got_group(true).0,
    TrapRelocInfoC::Arm64GotGroup as u32
);

#[repr(u32)]
pub enum TrapRelocInfoC {
    None = 0,
    Symbol = 0x1,
    Addend = 0x2,
    Ignore = 0x4,
    // ARM64-specific relocs
    Arm64GotPage = 0x10000,
    Arm64GotGroup = 0x20000,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct TrapHeader {
    pub flags: TrapHeaderFlags,
    pub version: u8,
    pub platform: TrapPlatform,
    pub pointer_size: usize,
}

fn read_leb64<const SIGN_EXTEND: bool>(input: &mut &[u8]) -> PResult<u64> {
    const MAX_64_LEB_BYTES: usize = 10;
    let mut shift = 0;
    let mut seen_term = false;
    let mut v = fold_repeat(
        1..=MAX_64_LEB_BYTES,
        |input: &mut _| {
            if seen_term {
                return Err(ErrMode::from_error_kind(
                    input,
                    winnow::error::ErrorKind::Many,
                ));
            }
            let b = le_u8(input)?;
            seen_term |= b & 0x80 == 0;
            Ok(b)
        },
        || 0u64,
        |mut acc, b| {
            acc |= u64::from(b & 0x7f) << shift;
            shift += 7;
            acc
        },
    )
    .parse_next(input)?;
    if SIGN_EXTEND {
        let sign = (v & (1u64.checked_shl(shift - 1).unwrap_or(0))) != 0;
        if sign {
            v |= (-1i64).checked_shl(shift).unwrap_or(0) as u64;
        }
    }
    Ok(v)
}
fn read_uleb64(input: &mut &[u8]) -> PResult<u64> {
    read_leb64::<false>.parse_next(input)
}

fn read_padding_from_end(input: &mut &[u8]) -> Result<usize, ()> {
    const MAX_64_LEB_BYTES: usize = 10;
    let mut uleb_bytes = [0u8; MAX_64_LEB_BYTES + 1];
    if let Some(slice) = input.get(input.len().saturating_sub(uleb_bytes.len())..) {
        let uleb_bytes = &mut uleb_bytes[..slice.len()];
        uleb_bytes.copy_from_slice(slice);
        uleb_bytes.reverse();

        read_uleb64
            .map(|count| usize::try_from(count).unwrap())
            .parse_next(&mut &*uleb_bytes)
            .map_err(|_| ())
    } else {
        Err(())
    }
}

fn read_sleb64(input: &mut &[u8]) -> PResult<i64> {
    read_leb64::<true>.map(|v| v as i64).parse_next(input)
}

#[derive(Debug)]
pub enum TrapRelocV2Symbol {
    Symbol(u64),
    Index(usize),
    Data(u64),
}

#[derive(Debug)]
pub struct TrapRelocV2 {
    pub address: TrapAddress,
    pub typ: u16,
    pub symbol: Option<TrapRelocV2Symbol>,
    pub addend: i64,
}

impl TrapDump for TrapRelocV2 {
    fn dump(&self, ctx: &DumpContext, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Rel[{:?}:{}]@{:x}={:x?}={}",
            self.typ,
            if cfg!(feature = "name-relocs") {
                goblin::elf::reloc::r_to_str(self.typ.into(), ctx.machine)
            } else {
                ""
            },
            self.address.absolute(ctx.addr_delta),
            self.symbol,
            self.addend
        )
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
#[cfg_attr(feature = "std", derive(Serialize))]
pub enum TrapRelocSymbol {
    Instruction(u32),
    Address(TrapAddress),
}

#[derive(Debug, Copy, Clone, PartialEq)]
#[cfg_attr(feature = "std", derive(Serialize))]
pub enum TrapRelocAddend {
    Instruction(u32, u32),
    Generic(i64),
}

impl TrapRelocAddend {
    pub fn generic(&self) -> i64 {
        match self {
            Self::Generic(g) => *g,
            _ => panic!("TrapRelocAddend is not generic"),
        }
    }
    pub fn instruction(&self) -> (u32, u32) {
        match self {
            Self::Instruction(low, high) => (*low, *high),
            _ => panic!("TrapRelocAddend is not instruction"),
        }
    }
}

impl From<TrapRelocAddend> for i64 {
    fn from(reloc: TrapRelocAddend) -> Self {
        match reloc {
            TrapRelocAddend::Instruction(low, high) => {
                (u64::from(low) | (u64::from(high) << 32)) as i64
            }
            TrapRelocAddend::Generic(addend) => addend,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct TrapReloc {
    pub address: TrapAddress,
    pub typ: u64,
    pub symbol: Option<TrapRelocSymbol>,
    pub addend: Option<TrapRelocAddend>,
}

#[cfg(feature = "std")]
struct WithContext<'a, T> {
    ctx: &'a Context<'a>,
    inner: &'a T,
}
#[cfg(feature = "std")]
impl<'a, T> WithContext<'a, T> {
    fn new(ctx: &'a Context, inner: &'a T) -> Self {
        Self { ctx, inner }
    }
}
#[cfg(feature = "std")]
impl Serialize for WithContext<'_, TrapReloc> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        // 3 is the number of fields in the struct.
        let mut state = serializer.serialize_struct("TrapReloc", 3)?;
        state.serialize_field("address", &self.inner.address)?;
        state.serialize_field("typ", &self.inner.typ)?;
        if cfg!(feature = "name-relocs") {
            let name = goblin::elf::reloc::r_to_str(
                self.inner.typ.try_into().unwrap(),
                self.ctx.header.platform.machine(),
            );
            state.serialize_field("typ_name", &name)?
        }
        state.serialize_field("symbol", &self.inner.symbol)?;
        state.serialize_field("addend", &self.inner.addend)?;
        state.end()
    }
}

impl TrapRelocSymbol {
    pub fn address(&self) -> TrapAddress {
        match self {
            Self::Address(addr) => *addr,
            _ => panic!("TrapRelocSymbol not address"),
        }
    }
    pub fn instruction(&self) -> u32 {
        match self {
            Self::Instruction(d) => *d,
            _ => panic!("TrapRelocSymbol not instruction"),
        }
    }
}

impl TrapDump for TrapReloc {
    fn dump(&self, ctx: &DumpContext, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Rel[{}:{}]@{:x}=",
            self.typ,
            if cfg!(feature = "name-relocs") {
                goblin::elf::reloc::r_to_str(self.typ.try_into().unwrap(), ctx.machine)
            } else {
                ""
            },
            self.address.absolute(ctx.addr_delta),
        )?;
        match self.symbol {
            Some(TrapRelocSymbol::Address(a)) => {
                write!(f, "Addr:{:x}", a.absolute(ctx.addr_delta))?
            }
            Some(TrapRelocSymbol::Instruction(i)) => write!(f, "Instr:{:x}", i)?,
            _ => write!(f, "None")?,
        }
        write!(f, "={:?}", self.addend)
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
#[cfg_attr(feature = "std", derive(Serialize))]
pub struct TrapAddress(pub u64);

impl TrapAddress {
    pub fn normalize<P: ParserArch>(&self) -> TrapAddress {
        let v = if P::ArchBits::pointer_size_bits() == 32 {
            u64::from(self.0 as u32)
        } else {
            self.0
        };
        TrapAddress(v)
    }

    pub fn absolute(&self, addr_delta: u64) -> u64 {
        self.0.overflowing_add(addr_delta).0
    }

    pub fn absolute_addr<T>(&self, addr_delta: T) -> TargetAddr<T>
    where
        T: num_traits::CheckedAdd + TryFrom<u64> + TargetAddrPtr,
    {
        TargetAddr::from_raw(
            T::try_from(self.0)
                .map_err(|_| ())
                .unwrap()
                .checked_add(&addr_delta)
                .unwrap(),
        )
    }
    /// Construct a new TrapAddress from the last address plus a delta.
    ///
    /// After computing the new address, reset the previous address to the new
    /// address and return it.
    fn new_from_delta(previous: &mut TrapAddress, delta: Self) -> Self {
        *previous = Self(previous.0.wrapping_add(delta.0));
        *previous
    }

    pub fn offset(self, other: Self) -> i64 {
        other.0.wrapping_sub(self.0) as i64
    }

    #[cfg(feature = "std")]
    pub fn into_file_offset(self, object: &ObjectInfo) -> Option<usize> {
        let addr: usize = self.0.try_into().unwrap();
        object
            .headers
            .iter()
            .find(|header| header.vm_range().contains(&addr))
            .map(|header| (addr - header.vm_range().start) + header.file_range().start)
    }

    #[must_use]
    pub fn wrapping_add(self, other: u64) -> Self {
        Self(self.0.wrapping_add(other))
    }

    #[must_use]
    pub fn wrapping_sub(self, other: u64) -> Self {
        Self(self.0.wrapping_sub(other))
    }
}

impl fmt::Display for TrapAddress {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "TrapAddress({})", self.0)
    }
}

impl Add<u64> for TrapAddress {
    type Output = Self;

    fn add(self, other: u64) -> Self {
        Self(self.0 + other)
    }
}

impl Sub<u64> for TrapAddress {
    type Output = Self;

    fn sub(self, other: u64) -> Self {
        Self(self.0 - other)
    }
}

impl AddAssign<u64> for TrapAddress {
    fn add_assign(&mut self, other: u64) {
        self.0 += other
    }
}

impl SubAssign<u64> for TrapAddress {
    fn sub_assign(&mut self, other: u64) {
        self.0 -= other
    }
}

impl From<u32> for TrapAddress {
    fn from(addr: u32) -> TrapAddress {
        TrapAddress(addr as u64)
    }
}

impl From<u64> for TrapAddress {
    fn from(addr: u64) -> TrapAddress {
        TrapAddress(addr)
    }
}

impl TryFrom<TrapAddress> for i64 {
    type Error = <i64 as TryFrom<u64>>::Error;

    fn try_from(addr: TrapAddress) -> Result<i64, Self::Error> {
        addr.0.try_into()
    }
}

impl From<TrapAddress> for u64 {
    fn from(addr: TrapAddress) -> u64 {
        addr.0
    }
}

impl TryFrom<i64> for TrapAddress {
    type Error = <u64 as TryFrom<i64>>::Error;

    fn try_from(addr: i64) -> Result<TrapAddress, Self::Error> {
        addr.try_into().map(TrapAddress)
    }
}

impl TryFrom<i32> for TrapAddress {
    type Error = <u64 as TryFrom<i32>>::Error;

    fn try_from(addr: i32) -> Result<TrapAddress, Self::Error> {
        addr.try_into().map(TrapAddress)
    }
}

impl TryFrom<TrapAddress> for u32 {
    type Error = <u32 as TryFrom<u64>>::Error;

    fn try_from(addr: TrapAddress) -> Result<u32, Self::Error> {
        addr.0.try_into()
    }
}

impl TryFrom<TrapAddress> for i32 {
    type Error = <i32 as TryFrom<u64>>::Error;

    fn try_from(addr: TrapAddress) -> Result<i32, Self::Error> {
        addr.0.try_into()
    }
}

fn read_address<T>(input: &mut &[u8], ctx: &Context) -> PResult<TrapAddress>
where
    T: ParserArch,
{
    let mut start = *input;
    let delta = if T::ArchBits::pointer_size_bits() == 32 {
        le_u32.map(u64::from).parse_next(input)
    } else {
        le_u64(input)
    }?;
    let addr = if ctx.header.flags.pc_relative_addresses() {
        ctx.trap_info_address(&mut start).wrapping_add(delta)
    } else if ctx.header.flags.base_relative_addresses() {
        TrapAddress(ctx.base_address).wrapping_add(delta)
    } else {
        TrapAddress(delta)
    };

    Ok(addr.normalize::<T>())
}

trait SkipReader {
    type MaybeTerm;
    type Entry;
    fn is_term(maybe_term: &Self::MaybeTerm) -> bool;
    fn read_maybe_term<'i>(input: &mut &'i [u8], info: &Context<'i>) -> PResult<Self::MaybeTerm>;

    fn read_with_term<'i>(
        prefix: Self::MaybeTerm,
        input: &mut &'i [u8],
        info: &Context<'i>,
    ) -> PResult<Self::Entry>;

    fn skip_term_read<'i>(input: &mut &'i [u8], info: &Context<'i>) -> PResult<&'i [u8]> {
        let orig_input = *input;
        let mut loop_input = *input;
        loop {
            let input = &mut loop_input;
            if input.is_empty() {
                return Ok(orig_input);
            }
            let maybe_term = Self::read_maybe_term(input, info)?;
            if Self::is_term(&maybe_term) {
                // Yield the slice of all entries, without including the terminal sequence.
                return Ok(&orig_input[..loop_input.offset_from(&orig_input)]);
            }
            let _ = Self::read_with_term(maybe_term, input, info)?;
        }
    }
    fn read<'i>(input: &mut &'i [u8], info: &Context<'i>) -> PResult<Self::Entry> {
        let maybe_term = Self::read_maybe_term(input, info)?;
        Self::read_with_term(maybe_term, input, info)
    }

    fn skip<'i>(input: &mut &'i [u8], info: &Context<'i>) -> PResult<&'i [u8]> {
        if info.header.flags.has_vector_lengths() {
            let vector_length = read_uleb64.try_map(usize::try_from).parse_next(input)?;
            take(vector_length).parse_next(input)
        } else {
            Self::skip_term_read(input, info)
        }
    }
}

struct RelocReader<T> {
    _phantom: PhantomData<T>,
}
struct RelocTerm {
    curr_delta: u64,
    curr_type: u64,
}

impl<T> SkipReader for RelocReader<T>
where
    T: ParserArch,
{
    type Entry = TrapReloc;
    type MaybeTerm = RelocTerm;

    fn is_term(maybe_term: &Self::MaybeTerm) -> bool {
        maybe_term.curr_delta == 0 && maybe_term.curr_type == 0
    }
    fn read_maybe_term<'i>(input: &mut &'i [u8], _info: &Context<'i>) -> PResult<Self::MaybeTerm> {
        let curr_delta = read_uleb64.parse_next(input)?;
        let curr_type = read_uleb64.parse_next(input)?;
        Ok(RelocTerm {
            curr_delta,
            curr_type,
        })
    }
    fn read_with_term(
        prefix: Self::MaybeTerm,
        input: &mut &[u8],
        ctx: &Context,
    ) -> PResult<TrapReloc>
    where
        T: ParserArch,
    {
        let RelocTerm {
            curr_delta,
            curr_type,
        } = prefix;
        let extra_info = T::trap_reloc_info(curr_type);

        let mut symbol = cond(
            extra_info.symbol(),
            ({ |x: &mut _| read_address::<T>(x, ctx) }).map(TrapRelocSymbol::Address),
        )
        .parse_next(input)?;

        let mut addend = cond(
            extra_info.addend(),
            read_sleb64.map(TrapRelocAddend::Generic),
        )
        .parse_next(input)?;

        let got_sym = cond(
            extra_info.arm64_got_page(),
            // HACK: store the ARM64 instruction in curr_symbol,
            // since we should never have both TRAP_RELOC_SYMBOL
            // along with this one
            le_u32.map(TrapRelocSymbol::Instruction),
        )
        .parse_next(input)?;
        symbol = got_sym.or(symbol);

        let got_group = cond(
            extra_info.arm64_got_group(),
            // 3x bigger HACK: we have 3 32-bit values, so we store the first
            // one in curr_symbol and the other 2 inside addend
            (
                le_u32.map(|x| TrapRelocSymbol::Address(TrapAddress(x.into()))),
                (le_u32, le_u32).map(|(low, high)| TrapRelocAddend::Instruction(low, high)),
            ),
        )
        .parse_next(input)?;
        if let Some((new_sym, new_addend)) = got_group {
            symbol = Some(new_sym);
            addend = Some(new_addend);
        }

        Ok(TrapReloc {
            address: TrapAddress(curr_delta),
            typ: curr_type,
            symbol,
            addend,
        })
    }
}

struct SymbolReader {}
struct TrapSymbolTerm {
    delta: u64,
    align: Option<u64>,
    size: Option<u64>,
}

impl SkipReader for SymbolReader {
    type MaybeTerm = TrapSymbolTerm;
    type Entry = TrapSymbol;
    fn is_term(maybe_term: &Self::MaybeTerm) -> bool {
        maybe_term.delta == 0 && maybe_term.size.is_none() && maybe_term.align.is_none()
    }
    fn read_maybe_term<'i>(input: &mut &'i [u8], ctx: &Context<'i>) -> PResult<Self::MaybeTerm> {
        let delta = read_uleb64.parse_next(input)?;
        //println!("Curr Delta : {}", curr_delta);
        let size = cond(ctx.header.flags.has_symbol_size(), read_uleb64).parse_next(input)?;
        //println!("Curr size {:?}", curr_size);
        let align = cond(ctx.header.flags.has_symbol_p2align(), read_uleb64).parse_next(input)?;
        //println!("Curr p2 {:?}", curr_p2align);
        Ok(TrapSymbolTerm { delta, align, size })
    }

    fn read_with_term<'i>(
        prefix: Self::MaybeTerm,
        _input: &mut &'i [u8],
        _info: &Context<'i>,
    ) -> PResult<Self::Entry> {
        Ok(TrapSymbol {
            address: TrapAddress(prefix.delta),
            align: prefix.align,
            size: prefix.size,
        })
    }
}

struct NonNullBytesReader {}

impl SkipReader for NonNullBytesReader {
    type MaybeTerm = u8;
    type Entry = u8;
    fn is_term(maybe_term: &Self::MaybeTerm) -> bool {
        *maybe_term == 0
    }
    fn read_maybe_term<'i>(input: &mut &'i [u8], _info: &Context<'i>) -> PResult<Self::MaybeTerm> {
        le_u8(input)
    }
    fn read_with_term<'i>(
        prefix: Self::MaybeTerm,
        _input: &mut &'i [u8],
        _info: &Context<'i>,
    ) -> PResult<Self::Entry> {
        Ok(prefix)
    }
}

#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(feature = "std", derive(Serialize))]
pub struct TrapSymbol {
    pub address: TrapAddress,
    pub align: Option<u64>,
    pub size: Option<u64>,
}

impl TrapDump for TrapSymbol {
    fn dump(&self, ctx: &DumpContext, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Symbol@??/{:x}[{}] align:{}",
            self.address.absolute(ctx.addr_delta),
            self.size.unwrap_or(0),
            1 << self.align.unwrap_or(0),
        )
    }
}

#[derive(Debug, PartialEq)]
#[cfg_attr(feature = "std", derive(Serialize))]
pub struct Padding {
    pub size: u64,
    offset: u64,
}

impl Padding {
    pub fn address<T>(&self, record: &TrapRecord<'_, '_, T>) -> TrapAddress {
        record.address.wrapping_add(self.offset)
    }
}

impl TrapDump for Padding {
    fn dump(&self, _ctx: &DumpContext, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Padding[{}]{:x}/??", self.size, self.offset,)
    }
}

#[derive(Debug)]
pub struct TrapRecordV2 {
    pub address: TrapAddress,
    pub size: usize,
    pub align: u16,
    pub typ: TrapRecordType,
    relocs: Range<usize>,
}

impl TrapRecordV2 {
    pub fn relocs<'i, T>(
        &self,
        trap: &'i TrapV2<T>,
    ) -> impl FallibleIterator<Item = TrapRelocV2, Error = GenericParseError> + 'i
    where
        T: ParserArch,
        TrapRecordV2: SliceRead<T>,
        TrapRelocV2: SliceRead<T>,
    {
        trap.relocs()
            .skip(self.relocs.start)
            .take(self.relocs.len())
    }
}

pub trait SliceRead<T>: Sized {
    fn size_bytes() -> usize;
    fn read(input: &mut &[u8], ctx: &Context) -> PResult<Self>;
}

impl<T> SliceRead<T> for TrapRelocV2
where
    T: ParserArch,
{
    fn size_bytes() -> usize {
        core::mem::size_of::<T::CTrapV2RelocType>()
    }
    fn read(input: &mut &[u8], _ctx: &Context) -> PResult<Self> {
        let address = T::ArchBits::read_uintptr(input)?;
        let ptr_size = core::mem::size_of::<<<T as ParserArch>::ArchBits as Bits>::UIntPtr>();
        let (typ, symbol_idx) = take(ptr_size)
            .try_map(T::trapv2_reloc_type_symbol)
            .parse_next(input)?;
        let addend = T::ArchBits::read_intptr(input)?;

        let extra_info = T::trap_reloc_info(typ.into());

        let symbol = match symbol_idx {
            Some(data) if extra_info.symbol() => Some(TrapRelocV2Symbol::Symbol(data)),
            Some(data) if extra_info.arm64_got_page() || extra_info.arm64_got_group() => {
                Some(TrapRelocV2Symbol::Data(data))
            }
            Some(idx) => Some(TrapRelocV2Symbol::Index(
                idx.try_into().map_err_winnow(input)?,
            )),
            None => None,
        };

        Ok(TrapRelocV2 {
            address: address.into(),
            typ,
            symbol,
            addend: addend.try_into().map_err_winnow(input)?,
        })
    }
}

impl<T> SliceRead<T> for TrapRecordV2
where
    T: ParserArch,
{
    fn size_bytes() -> usize {
        core::mem::size_of::<T::CTrapV2RecordType>()
    }
    fn read(input: &mut &[u8], _ctx: &Context) -> PResult<Self> {
        let address = T::ArchBits::read_uintptr(input)?;
        let size = T::ArchBits::read_uintptr(input)?;
        let bits = take(1usize)
            .map(|byte: &[u8]| TrapRecordV2Bits::from(byte[0]))
            .parse_next(input)?;
        let reloc_count = le_u32(input)?;
        let reloc_start_index = le_u32(input)?;

        Ok(TrapRecordV2 {
            address: address.into(),
            size: size.try_into().map_err_winnow(input)?,
            align: bits.align() as u16,
            typ: TrapRecordType::from(u32::from(bits.typ())),
            relocs: reloc_start_index as usize..(reloc_count + reloc_start_index) as usize,
        })
    }
}

impl TrapDump for TrapRecordV2 {
    fn dump(&self, ctx: &DumpContext, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Record@{:x}|{:?}",
            self.address.absolute(ctx.addr_delta),
            self.typ
        )
    }
}

#[derive(Debug)]
pub struct TrapRecord<'i, 'c, T> {
    context: &'c Context<'i>,
    pub address: TrapAddress,
    symbols: &'i [u8],
    relocs: &'i [u8],
    data_refs: Option<&'i [u8]>,
    pub padding: Option<Padding>,
    #[allow(unused)]
    trapv2_padding: Option<usize>,
    _phantom: PhantomData<T>,
}

impl<'i, 'c: 'i, T> TrapRecord<'i, 'c, T>
where
    T: ParserArch,
{
    pub fn relocs(&self) -> TrapRelocIterator<'i, 'c, T> {
        TrapRelocIterator {
            ctx: self.context,
            data: self.relocs,
            addr: self.address,
            _phantom: PhantomData,
        }
    }

    pub fn symbols(&self) -> TrapSymbolIterator<'i, 'c> {
        TrapSymbolIterator {
            ctx: self.context,
            data: self.symbols,
            addr: self.address,
        }
    }

    pub fn data_refs(&self) -> impl FallibleIterator<Item = u64, Error = GenericParseError> + 'i {
        TrapDataRefIterator {
            data: self.data_refs,
        }
    }
}

#[cfg(feature = "std")]
impl<T> Serialize for TrapRecord<'_, '_, T>
where
    T: ParserArch,
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut state = serializer.serialize_struct("TrapRecord", 3)?;
        state.serialize_field("address", &self.address)?;
        state.serialize_field("padding", &self.padding)?;
        state.serialize_field("trapv2_padding", &self.trapv2_padding)?;
        state.serialize_field("relocs", &self.relocs())?;
        state.serialize_field("symbols", &self.symbols())?;
        //state.serialize_field("data_refs", &self.data_refs())?;

        state.end()
    }
}

impl<T> TrapDump for TrapRecord<'_, '_, T> {
    fn dump(&self, ctx: &DumpContext, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Record@{:x}", self.address.absolute(ctx.addr_delta))
    }
}

#[bitfield(u8)]
struct TrapRecordV2Bits {
    #[bits(4)]
    pub align: u8,
    #[bits(4)]
    pub typ: u8,
}

fn read_record<'i, 'c, T>(
    input: &mut &'i [u8],
    ctx: &'c Context<'i>,
) -> PResult<TrapRecord<'i, 'c, T>>
where
    T: ParserArch,
{
    let address = read_address::<T>(input, ctx)?;
    //println!("Address {:x?}", address);
    let symbols = SymbolReader::skip(input, ctx)?;
    //let (_, first_symbol) = read_symbol(&header, syms.entries)?;

    let relocs = cond(ctx.header.flags.has_record_relocs(), |input: &mut _| {
        RelocReader::<T>::skip(input, ctx)
    })
    .parse_next(input)?;

    let data_refs = cond(ctx.header.flags.has_data_refs(), |input: &mut _| {
        NonNullBytesReader::skip(input, ctx)
    })
    .parse_next(input)?;

    let padding = cond(
        ctx.header.flags.has_record_padding(),
        (read_uleb64, read_uleb64).map(|(offset, size)| Padding { size, offset }),
    )
    .parse_next(input)?;

    let trapv2_padding = cond(ctx.trapv2_padding, length_take(le_u32)).parse_next(input)?;

    Ok(TrapRecord {
        context: ctx,
        address,
        padding,
        symbols,
        trapv2_padding: trapv2_padding.map(|x| x.len()),
        relocs: relocs.unwrap_or(&[]),
        data_refs,
        _phantom: PhantomData,
    })
}

#[cfg(feature = "c-api")]
#[no_mangle]
extern "C" fn trap_reloc_info(reloc_type: u64, platform: u32) -> u32 {
    let info = match TrapPlatform::from(platform) {
        TrapPlatform::PosixARM => Arm::trap_reloc_info(reloc_type),
        TrapPlatform::PosixARM64 => Aarch64::trap_reloc_info(reloc_type),
        TrapPlatform::PosixX86_64 => X86_64::trap_reloc_info(reloc_type),
        TrapPlatform::PosixX86 => X86::trap_reloc_info(reloc_type),
        TrapPlatform::Unknown | TrapPlatform::Win32 | TrapPlatform::Win64 => {
            panic!("Unsupported platform")
        }
    };
    info.into()
}

#[cfg(test)]
mod test {
    use super::*;
    use proptest::prelude::*;
    proptest! {
       #[test]
        fn test_uleb64(
            value in any::<u64>(),
        ) {
            let mut leb :Vec<u8>= vec![];
            let mut x = value;
            while x >= 0x80 {
                leb.push(0x80 | (x as u8 & 0x7f));
                x >>= 7;
            }
            assert!(x > 0 && x < 0x80);
            leb.push(x as u8);

            let bytes = &leb;
            assert_eq!(read_uleb64.parse(bytes), Ok(value));
        }

        #[test]
        fn test_sleb64(
            value in any::<i64>(),
        ) {
            let mut sleb :Vec<u8>= vec![];
            let mut x = value;
            let mut more = true;
            while more {
                let mut byte = x as u8 & 0x7f;
                x >>= 7;
                more = match x {
                    0 if (byte & 0x40) == 0 => false,
                    -1 if (byte & 0x40) != 0 => false,
                    _ => true
                };
                if more {
                    byte |= 0x80;
                }
                sleb.push(byte);
            }
            let bytes = &sleb;
            assert_eq!(read_sleb64.parse(bytes), Ok(value));
        }
    }
}
