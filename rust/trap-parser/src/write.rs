// Copyright (c) 2023 RunSafe Security Inc.
use crate as read;
use crate::{GenericParseError, ParserArch, TrapAddress, TrapReloc, TrapSymbol};
use fallible_iterator::FallibleIterator;
use std::io::Result;
use std::io::Write;
use std::marker::PhantomData;

// This should match TrapHeaderCommon.inc
macro_rules! TRAP_HEADER_FLAGS {
    () => {
        read::TrapHeaderFlags::from_le_bytes([0xD1, 0x2B, 0x00])
    };
}

#[derive(Debug, PartialEq)]
pub struct TrapRecord {
    pub address: TrapAddress,
    pub symbols: Vec<TrapSymbol>,
    pub relocs: Vec<TrapReloc>,
    //data_refs: Vec<u64>,
    pub padding: read::Padding,
    pub v2_padding: Option<usize>,
}

impl<T> TryFrom<read::TrapRecord<'_, '_, T>> for TrapRecord
where
    T: ParserArch,
{
    type Error = GenericParseError;
    fn try_from(v: read::TrapRecord<'_, '_, T>) -> std::result::Result<Self, Self::Error> {
        Ok(Self {
            address: v.address,
            symbols: v.symbols().collect()?,
            relocs: v.relocs().collect()?,
            //data_refs: v.data_refs().collect()?,
            padding: v.padding.unwrap_or(read::Padding { size: 0, offset: 0 }),
            v2_padding: v.trapv2_padding,
        })
    }
}

#[derive(Debug, PartialEq)]
pub struct TRaPV1Builder {
    //nonexec_relocs: Vec<TrapReloc>,
    pub records: Vec<TrapRecord>,
}

impl<T> TryFrom<read::TrapV1<'_, T>> for TRaPV1Builder
where
    T: ParserArch,
{
    type Error = GenericParseError;
    fn try_from(trap: read::TrapV1<'_, T>) -> std::result::Result<Self, Self::Error> {
        Ok(Self {
            records: trap.records().map(TrapRecord::try_from).collect()?,
            //nonexec_relocs: trap.nonexec_relocs().collect()?,
        })
    }
}

impl TRaPV1Builder {
    pub fn write<P: ParserArch>(
        &self,
        out: &mut [u8],
        offset: u64,
        trapv2_padding: bool,
        relaxed: bool,
    ) -> Result<()> {
        let sorted = self.records.is_sorted_by_key(|r| r.address);

        let cxt = TRaPV1Context {
            header: TRAP_HEADER_FLAGS!()
                .with_pre_relaxed(relaxed)
                .with_pre_sorted(sorted),
            trap_start_addr: offset,
            trapv2_padding,
            write_region: out.len().try_into().unwrap(),
        };
        let mut cursor = std::io::Cursor::new(out);
        <Self as TRaPV1Writer<_, P>>::write_v1(self, &mut cursor, &cxt)?;
        Ok(())
    }
}

pub struct TRaPV1Context {
    /// Header Flags
    header: read::TrapHeaderFlags,
    /// Whether trapv2 padding is to be emitted (if included)
    trapv2_padding: bool,
    /// The start offset of the trap region
    trap_start_addr: u64,
    /// The length of trap write region.
    write_region: u64,
}

pub struct PcWrite<W, P> {
    start: u64,
    buf: W,
    count: u64,
    _pd: PhantomData<P>,
}

impl<W, P> PcWrite<W, P>
where
    P: ParserArch,
    Self: TRaPV1Write<P>,
{
    pub fn new(pc: u64, inner: W) -> Self {
        Self {
            start: pc,
            count: 0,
            buf: inner,
            _pd: PhantomData,
        }
    }
    pub fn into_inner(self) -> W {
        self.buf
    }
}

impl<P> TRaPV1Write<P> for std::io::Cursor<&mut [u8]>
where
    P: ParserArch,
{
    fn pc(&self, offset: u64) -> u64 {
        self.position() + offset
    }
}

impl<P, W> TRaPV1Write<P> for PcWrite<W, P>
where
    P: ParserArch,
    W: Write,
{
    fn pc(&self, offset: u64) -> u64 {
        self.count + offset + self.start
    }
}

impl<P, W> Write for PcWrite<W, P>
where
    W: Write,
{
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        let count = self.buf.write(buf)?;
        self.count += u64::try_from(count).unwrap();
        Ok(count)
    }
    fn flush(&mut self) -> Result<()> {
        self.buf.flush()
    }
}

impl<T, W, P> TRaPV1Writer<W, P> for &T
where
    T: TRaPV1Writer<W, P>,
{
    fn write_v1(&self, out: &mut W, cxt: &TRaPV1Context) -> Result<()> {
        <T as TRaPV1Writer<W, P>>::write_v1(&**self, out, cxt)
    }
}
pub trait TRaPV1Write<P>: Sized + Write
where
    P: ParserArch,
{
    fn pc(&self, offset: u64) -> u64;

    fn write_uleb64<V>(&mut self, v: V) -> Result<()>
    where
        V: Into<u64>,
    {
        let mut x: u64 = v.into();
        while x >= 0x80 {
            self.write_all(&[0x80 | (x as u8 & 0x7f)])?;
            x >>= 7;
        }
        assert!(x < 0x80);
        self.write_all(&[x as u8])?;
        Ok(())
    }
    fn write_sleb64<V>(&mut self, v: V) -> Result<()>
    where
        V: Into<i64>,
    {
        let mut x: i64 = v.into();
        let mut more = true;
        while more {
            let mut byte = x as u8 & 0x7f;
            x >>= 7;
            more = match x {
                0 if (byte & 0x40) == 0 => false,
                -1 if (byte & 0x40) != 0 => false,
                _ => true,
            };
            if more {
                byte |= 0x80;
            }
            self.write_all(&[byte])?;
        }
        Ok(())
    }
    fn write_vector<I, V>(&mut self, cxt: &TRaPV1Context, iter: I) -> Result<()>
    where
        V: TRaPV1Writer<PcWrite<std::io::Sink, P>, P> + TRaPV1Writer<Self, P>,
        I: IntoIterator<Item = V> + Clone,
    {
        // Do a fake write to determine the length of the vector.
        let mut pcbuf = PcWrite::new(self.pc(0), std::io::sink());
        for i in iter.clone() {
            i.write_v1(&mut pcbuf, cxt)?;
        }
        self.write_uleb64(pcbuf.count)?;
        // Actually write everything now that the length of the Uleb prefix is known,
        // to allow the pc relative offsets to be correct.
        for i in iter {
            i.write_v1(self, cxt)?;
        }

        Ok(())
    }
}

pub trait TRaPV1Writer<W, P> {
    fn write_v1(&self, out: &mut W, cxt: &TRaPV1Context) -> Result<()>;
}

impl<W, P> TRaPV1Writer<W, P> for TrapAddress
where
    W: TRaPV1Write<P>,
    P: ParserArch,
{
    fn write_v1(&self, out: &mut W, cxt: &TRaPV1Context) -> Result<()> {
        assert!(cxt.header.pc_relative_addresses());
        let relative = self.0.wrapping_sub(out.pc(cxt.trap_start_addr));
        if <<P as ParserArch>::ArchBits as read::Bits>::pointer_size_bits() == 32 {
            out.write_all(&(relative as i32).to_le_bytes())?;
        } else {
            out.write_all(&relative.to_le_bytes())?;
        }
        Ok(())
    }
}

// impl<W, P> TRaPV1Writer<W, P> for read::TrapHeader
// where
//     W: TRaPV1Write<P>,
//     P: ParserArch,
// {
//     fn write_v1(&self, out: &mut W, cxt: &TRaPV1Context) -> Result<()> {
//         out.write_all(&self.version.to_le_bytes())?;
//         out.write_all(&self.flags.into_bytes())?;

//         out.write_uleb64(
//             u64::try_from(<<P as ParserArch>::ArchBits as read::Bits>::pointer_size_bits())
//                 .unwrap(),
//         )?;
//         Ok(())
//     }
// }

impl<W, P> TRaPV1Writer<W, P> for TRaPV1Builder
where
    W: TRaPV1Write<P>,
    P: ParserArch,
{
    fn write_v1(&self, out: &mut W, cxt: &TRaPV1Context) -> Result<()> {
        // version
        out.write_all(&[0x01])?;
        out.write_all(&cxt.header.to_le_bytes())?;

        assert!(!cxt.header.has_nonexec_relocs());
        //out.write_vector(cxt, &self.nonexec_relocs)?;

        assert!(cxt.header.has_pointer_size());
        out.write_uleb64(
            u8::try_from(<<P as ParserArch>::ArchBits as read::Bits>::pointer_size_bits()).unwrap(),
        )?;

        for r in &self.records {
            r.write_v1(out, cxt)?;
        }

        assert!(cxt.header.has_known_post_padding());
        let pos = out.pc(0);
        let mut uleb64 = [0xccu8; 20];
        let mut cursor = std::io::Cursor::new(uleb64.as_mut());
        TRaPV1Write::<P>::write_uleb64(&mut cursor, cxt.write_region.saturating_sub(pos))?;
        let uleb64_len = usize::try_from(cursor.position()).unwrap();
        let (uleb64, _) = uleb64.split_at_mut(uleb64_len);
        uleb64.reverse();

        for _ in pos..cxt
            .write_region
            .saturating_sub(uleb64.len().try_into().unwrap())
        {
            out.write_all(&[0xcc])?;
        }
        out.write_all(uleb64)?;
        Ok(())
    }
}

impl<W, P> TRaPV1Writer<W, P> for u8
where
    W: TRaPV1Write<P>,
    P: ParserArch,
{
    fn write_v1(&self, out: &mut W, _cxt: &TRaPV1Context) -> Result<()> {
        out.write_all(&[*self])
    }
}

impl<W, P> TRaPV1Writer<W, P> for read::Padding
where
    W: TRaPV1Write<P>,
    P: ParserArch,
{
    fn write_v1(&self, out: &mut W, _cxt: &TRaPV1Context) -> Result<()> {
        out.write_uleb64(self.offset)?;
        out.write_uleb64(self.size)?;
        Ok(())
    }
}
impl<W, P> TRaPV1Writer<W, P> for TrapRecord
where
    W: TRaPV1Write<P>,
    P: ParserArch,
{
    fn write_v1(&self, out: &mut W, cxt: &TRaPV1Context) -> Result<()> {
        self.address.write_v1(out, cxt)?;
        let mut last_addr = self.address;
        out.write_vector(
            cxt,
            self.symbols.iter().map(move |s| {
                let write = TrapSymbol {
                    address: TrapAddress(s.address.0.wrapping_sub(last_addr.0)),
                    ..*s
                };
                last_addr = s.address;
                write
            }),
        )?;
        let mut last_addr = self.address;
        assert!(cxt.header.has_record_relocs());
        out.write_vector(
            cxt,
            self.relocs.iter().map(move |r| {
                let write = TrapReloc {
                    address: r.address.wrapping_sub(last_addr.0),
                    // symbol: match r.symbol {
                    //     Some(TrapRelocSymbol::Address(addr)) => {
                    //         Some(TrapRelocSymbol::Address(addr.wrapping_sub(last_addr.0)))
                    //     }
                    //     other => other,
                    // },
                    ..*r
                };
                last_addr = r.address;
                write
            }),
        )?;
        assert!(!cxt.header.has_data_refs());
        //out.write_vector(cxt, self.data_refs.iter().flat_map(|x| x.to_le_bytes()))?;
        assert!(cxt.header.has_record_padding());
        self.padding.write_v1(out, cxt)?;

        if cxt.trapv2_padding {
            let padding = self.v2_padding.unwrap_or(0);
            out.write_all(&u32::try_from(padding).unwrap().to_le_bytes())?;
            for _ in 0..padding {
                out.write_all(&[0])?;
            }
        }
        Ok(())
    }
}

impl<W, P> TRaPV1Writer<W, P> for TrapSymbol
where
    W: TRaPV1Write<P>,
    P: ParserArch,
{
    fn write_v1(&self, out: &mut W, cxt: &TRaPV1Context) -> Result<()> {
        out.write_uleb64(self.address.0)?;
        assert!(!cxt.header.has_symbol_size());
        // if let Some(size) = self.size {
        //     out.write_uleb64(size)?;
        // }
        assert!(cxt.header.has_symbol_p2align());
        out.write_uleb64(self.align.unwrap())?;
        Ok(())
    }
}

impl<W, P> TRaPV1Writer<W, P> for TrapReloc
where
    W: TRaPV1Write<P>,
    P: ParserArch,
{
    fn write_v1(&self, out: &mut W, cxt: &TRaPV1Context) -> Result<()> {
        out.write_uleb64(self.address.0)?;
        out.write_uleb64(self.typ)?;

        let extra_info = P::trap_reloc_info(self.typ);
        if extra_info.symbol() {
            self.symbol.unwrap().address().write_v1(out, cxt)?;
        }
        if extra_info.addend() {
            out.write_sleb64(self.addend.unwrap().generic())?;
        }
        if extra_info.arm64_got_page() {
            out.write_all(&self.symbol.unwrap().instruction().to_le_bytes())?;
        }
        if extra_info.arm64_got_group() {
            out.write_all(&self.symbol.unwrap().instruction().to_le_bytes())?;
            let (low, high) = self.addend.unwrap().instruction();
            out.write_all(&low.to_le_bytes())?;
            out.write_all(&high.to_le_bytes())?;
        }

        Ok(())
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use winnow::prelude::*;

    use proptest::prelude::*;
    fn gen_address<P: ParserArch>() -> impl Strategy<Value = TrapAddress> {
        (any::<u64>(),).prop_map(|(address,)| {
            use read::Bits;
            if P::ArchBits::pointer_size_bits() == 32 {
                TrapAddress(u64::from(address as u32))
            } else {
                TrapAddress(address)
            }
        })
    }
    fn gen_builder<P: ParserArch>() -> impl Strategy<Value = TRaPV1Builder> {
        use proptest::collection::vec;
        (vec(gen_record::<P>(), 0..10)).prop_map(|records| TRaPV1Builder { records })
    }
    fn gen_symbol<P: ParserArch>() -> impl Strategy<Value = TrapSymbol> {
        (gen_address::<P>(), any::<u64>()).prop_map(|(address, align)| TrapSymbol {
            address,
            align: Some(align),
            size: None,
        })
    }
    fn gen_padding() -> impl Strategy<Value = read::Padding> {
        (any::<u64>(), any::<u64>()).prop_map(|(size, offset)| read::Padding { size, offset })
    }

    fn gen_record<P: ParserArch>() -> impl Strategy<Value = TrapRecord> {
        use proptest::collection::vec;
        (
            gen_address::<P>(),
            vec(gen_symbol::<P>(), 0..10),
            vec(gen_reloc::<P>(), 0..10),
            gen_padding(),
        )
            .prop_map(|(address, mut symbols, mut relocs, padding)| {
                symbols.sort_by_key(|v| v.address.0);
                relocs.sort_by_key(|v| v.address.0);
                TrapRecord {
                    address,
                    symbols,
                    relocs,
                    padding,
                    v2_padding: None,
                }
            })
    }

    fn gen_reloc<P: ParserArch>() -> impl Strategy<Value = TrapReloc> {
        (
            gen_address::<P>(),
            any::<u64>(),
            gen_address::<P>(),
            any::<u32>(),
            any::<u32>(),
        )
            .prop_map(|(address, typ, addr, v1, v2)| {
                use read::{TrapRelocAddend, TrapRelocSymbol};
                let extra_info = P::trap_reloc_info(typ);
                let mut symbol = None;
                let mut addend = None;
                if extra_info.symbol() {
                    symbol = Some(TrapRelocSymbol::Address(addr));
                }
                if extra_info.addend() {
                    addend = Some(TrapRelocAddend::Generic(addr.0 as i64));
                }
                if extra_info.arm64_got_page() {
                    symbol = Some(TrapRelocSymbol::Instruction(v1));
                }

                if extra_info.arm64_got_group() {
                    symbol = Some(TrapRelocSymbol::Address(addr));
                    addend = Some(TrapRelocAddend::Instruction(v1, v2));
                }
                TrapReloc {
                    typ,
                    address,
                    symbol,
                    addend,
                }
            })
    }
    fn verify_roundtrip<P, V, R>(offset: u64, val: V, read: R)
    where
        P: ParserArch,
        R: for<'a, 'b, 'c> FnOnce(&'a mut &'b [u8], &'c read::Context<'b>) -> V,
        V: TRaPV1Writer<PcWrite<Vec<u8>, P>, P> + PartialEq + std::fmt::Debug,
    {
        let cxt = TRaPV1Context {
            trap_start_addr: offset,
            header: TRAP_HEADER_FLAGS!(),
            trapv2_padding: false,
            write_region: 1 << 20,
        };
        let mut v = PcWrite::new(0, vec![]);
        <V as TRaPV1Writer<_, P>>::write_v1(&val, &mut v, &cxt).unwrap();
        let v = v.into_inner();
        let bytes = v.as_ref();
        let read_cxt = read::Context {
            header: read::TrapHeader {
                version: 1,
                flags: TRAP_HEADER_FLAGS!(),
                platform: P::PLATFORM,
                pointer_size: <P::ArchBits as read::Bits>::pointer_size_bits(),
            },
            trap_data: bytes,
            trap_start_addr: offset,
            base_address: offset,
            trapv2_padding: false,
        };
        let mut bytes = bytes;
        let new = read(&mut bytes, &read_cxt);
        assert_eq!(val, new);
    }
    use paste::paste;
    macro_rules! test_arch {
        ($(($name:ident, $arch:ty)),*) => {
            paste! {
                proptest! {
                    $(
                        #[test]
                        fn  [< read_write_symbol_ $name >](offset in any::<u16>(), value in gen_symbol::<$arch>()) {
                            verify_roundtrip::<$arch, _, _>(u64::from(offset), value, |slice, cxt| {
                                use read::SkipReader;
                                read::SymbolReader::read(slice, cxt).unwrap()
                            })
                        }

                        #[test]
                        fn [< read_write_reloc_ $name >](offset in any::<u16>(), value in gen_reloc::<$arch>()) {
                            verify_roundtrip::<$arch, _, _>(u64::from(offset), value, |slice, cxt| {
                                use read::SkipReader;
                                read::RelocReader::<$arch>::read(slice, cxt).unwrap()
                            })
                        }

                        #[test]
                        fn [<read_write_record_ $name>] (offset in any::<u16>(), value in gen_record::<$arch>()) {
                            verify_roundtrip::<$arch, _, _>(u64::from(offset), value, |slice, cxt| {
                                TrapRecord::try_from(read::read_record::<$arch>(slice, cxt).unwrap()).unwrap()

                            })
                        }

                        #[test]
                        fn  [<read_write_address_ $name >] (offset in any::<u16>(), value in gen_address::<$arch>()) {
                            verify_roundtrip::<$arch, _, _>(u64::from(offset), value, |slice, cxt| {
                                read::read_address::<$arch>(slice, cxt).unwrap()
                            })
                        }

                        #[test]
                        fn [< read_write_trapv1_ $name >] (offset in any::<u16>(), value in gen_builder::<$arch>()) {
                            verify_roundtrip::<$arch, _, _>(u64::from(offset), value, |slice, cxt| {
                                let trap = read::TrapInfo::<$arch>::new(
                                    slice, cxt.header.platform,
                                    u64::from(offset), u64::from(offset), false).unwrap();
                                let trapv1 = match trap {
                                    read::TrapInfo::TrapV1(trap_v1) => trap_v1,
                                    _ => panic!()
                                };

                                trapv1.try_into().unwrap()
                            })
                        }
                    )*
                }
            }
        }
    }

    test_arch! {
        (x86, crate::X86),
        (x86_64, crate::X86_64),
        (arm, crate::Arm),
        (arm64, crate::Aarch64)
    }

    proptest! {
        #[test]
        fn test_uleb64(
            value in any::<u64>(),
        ) {
            let mut leb = PcWrite::new(0, vec![]);
            TRaPV1Write::<crate::X86>::write_uleb64(&mut leb, value).unwrap();
            let leb = leb.into_inner();
            let bytes = &leb;
            assert_eq!(read::read_uleb64.parse(bytes), Ok(value));
        }

        #[test]
        fn test_sleb64(
            value in any::<i64>(),
        ) {
            let mut sleb = PcWrite::new(0, vec![]);
            TRaPV1Write::<crate::X86>::write_sleb64(&mut sleb, value).unwrap();
            let sleb = sleb.into_inner();
            let bytes = &sleb;
            assert_eq!(read::read_sleb64.parse(bytes), Ok(value));
        }
    }
}
