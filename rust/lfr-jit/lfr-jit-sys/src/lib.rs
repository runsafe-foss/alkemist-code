// Copyright (c) 2022 RunSafe Security Inc.

#![no_std]

/// A function that a GDB process can put a breakpoint on and watch for changes.
/// This appears to need to be in an entirely separate crate to not be inadvertantly
/// optimized away.
#[inline(never)]
#[no_mangle]
#[cfg(feature = "interface")]
pub extern "C" fn __lfr_jit_debug_register_code() {
    // Even with inlining disable if this function is empty, the call might be optimized away.
    // The following nonsense no-op read/write prevents the call from being optimized out
    // by intraprocedural analysis.
    unsafe {
        let read = core::ptr::read_volatile(&__lfr_jit_debug_descriptor);
        core::ptr::write_volatile(&mut __lfr_jit_debug_descriptor, read);
    }
}

/// This represents the "message" that is being sent to GDB.
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct LfrJitDescriptor {
    /// Version  of the message
    pub version: u32,
    /// The action to take
    pub action_flag: u32,
    /// A "key" that goes along with the message.
    pub objfile_start_addr: u64,
}

/// The memory that must be set to be read by GDB
#[no_mangle]
#[allow(non_upper_case_globals)]
#[cfg(feature = "interface")]
#[used]
pub static mut __lfr_jit_debug_descriptor: LfrJitDescriptor = LfrJitDescriptor {
    version: 1,
    action_flag: 0,
    objfile_start_addr: 0,
};
