// Copyright (c) 2022 RunSafe Security Inc.

//! # LFR JIT SYS
//!
//! A simple interface which is similar to
//! [gdb-jit-interface](https://sourceware.org/gdb/current/onlinedocs/gdb/JIT-Interface.html)
//! which allows something like GDB to watch for changes within a process and react accordingly.
#![no_std]
use core::convert::TryFrom;
use core::fmt::Debug;

#[cfg(not(feature = "interface"))]
pub use lfr_jit_sys::LfrJitDescriptor;
#[cfg(feature = "interface")]
use lfr_jit_sys::{LfrJitDescriptor, __lfr_jit_debug_descriptor, __lfr_jit_debug_register_code};

/// The action for the JIT interface to take.
#[derive(Debug)]
#[repr(u32)]
pub enum LfrJitAction {
    NoAction = 0,
    RegisterFn = 1,
    UnregisterFn = 2,
    ClearPseudo = 3,
}

impl TryFrom<u32> for LfrJitAction {
    type Error = ();
    fn try_from(val: u32) -> Result<LfrJitAction, ()> {
        match val {
            0 => Ok(LfrJitAction::NoAction),
            1 => Ok(LfrJitAction::RegisterFn),
            2 => Ok(LfrJitAction::UnregisterFn),
            3 => Ok(LfrJitAction::ClearPseudo),
            _ => Err(()),
        }
    }
}

/// Removes a given JITed entry.
#[cfg(feature = "interface")]
pub fn remove_jit_entry(objfile_start_addr: u64) {
    send_action(LfrJitAction::UnregisterFn, objfile_start_addr)
}

/// Adds given JITed entry.
#[cfg(feature = "interface")]
pub fn add_jit_entry(objfile_start_addr: u64) {
    send_action(LfrJitAction::RegisterFn, objfile_start_addr)
}

/// Clears any pseudo symbols out.
/// This must be done prior to randomization, otherwise GDB
/// might have modified the memory with breakpoints or whatnot
/// which will be copied during randomization.
#[cfg(feature = "interface")]
pub fn clear_pseudo(objfile_start_addr: u64) {
    send_action(LfrJitAction::ClearPseudo, objfile_start_addr)
}

/// Just sends a dummy signal when no other action is coming
/// but might be expected. Useful for clearing state.
#[cfg(feature = "interface")]
pub fn nop() {
    send_action(LfrJitAction::NoAction, 0)
}

/// "Sends" the desired message to GDB.
#[cfg(feature = "interface")]
fn send_action(action: LfrJitAction, objfile_start_addr: u64) {
    let descriptor = LfrJitDescriptor {
        version: 1,
        action_flag: action as u32,
        objfile_start_addr,
    };

    unsafe {
        __lfr_jit_debug_descriptor = descriptor;
        __lfr_jit_debug_register_code();
        __lfr_jit_debug_descriptor.action_flag = LfrJitAction::NoAction as u32;
    }
}
