/* Copyright (c) 2022 RunSafe Security Inc. */
//! # Entrypoint
//!
//! This is the entrypoint for LFR. It will be compiled into each shared library and
//! executable and will invoke [`lfr_run_rust`] as the effective new entrypoint of the
//! application library.
//!
//! Anything that needs to be on a per-application/library must be defined here and passed
//! to LFR library. The LFR library is intended to be stateless and relies on these values
//! provided by the entrypoint.

#![cfg_attr(not(feature = "std"), no_std)]
#![feature(linkage)]
#![allow(internal_features)]
#![feature(core_intrinsics)]
use modinfo::{LfrAddr, ModuleInfo, TargetAddr, TrapSectionInfoTable};

// The following `extern`ed values are expected to be provided by most of the Assembly entrypoints.
// This gives the entrypoint knowledge of the "shape" of binary to be randomized.
// These should almost always be limited visiblity and not exposed to the rest of the application.
// Many of these are placed in `hidden.c` to be compiled separately with the correct visibility options.
extern "C" {
    // Values provided by Assembly entrypoint or other linker specific values.
    static orig_init: libc::c_char;
    static orig_entry: libc::c_char;
    static lfr_preinit: libc::c_char;
    static lfr_init: libc::c_char;
    static lfr_entry: libc::c_char;
    static lfr_return: libc::c_char;
    static lfr_remove_call: libc::c_char;
    #[linkage = "extern_weak"]
    static xptramp_begin: *const libc::c_char;
    static trap_begin: libc::c_char;
    static trap_end: libc::c_char;
    #[linkage = "extern_weak"]
    static trap_end_page: *const libc::c_char; // FIXME: this might not be available under -Bsymbolic
    static mut lfr_trampoline_counter: i32;
    static mut lfr_trampoline_condvar: u32;

    /// Helper methods to keep certain symbol visibility contained in C symbols.
    fn get_dynamic() -> libc::uintptr_t;
    fn get_got() -> libc::uintptr_t;
    fn get_lfr_addrs() -> *const libc::uintptr_t;

    // LFR Entrypoints
    fn RandoFinish(orig_entry: TargetAddr<usize>);
    fn RandoMain(mod_info: &ModuleInfo<'_>);
}

macro_rules! uintptr_t {
    ($e:expr) => {
        TargetAddr::<usize>::from_raw($e as *const _ as usize)
    }
}

/// Whether randomization has already been run for this binary.
static mut RUN_ONCE: bool = false;

/// The actual entry point that requires the application arguments.
///
/// It collects all the information regarding the target application/library and
/// provides it to the LFR library, keeping a clean seperation between the two.
///
/// # Safety
/// Expected to be run from an entrypoint, single threaded.
#[no_mangle]
#[link_section = ".lfr.entry"]
pub unsafe extern "C" fn lfr_run_rust(args: *const libc::uintptr_t) {
    if RUN_ONCE {
        return;
    }
    RUN_ONCE = true;

    let lfr_addr_ptr = get_lfr_addrs();
    let lfr_addrs: [libc::uintptr_t; LfrAddr::Num as usize] = core::array::from_fn(|i| {
        // This is volatilely read because otherwise the values would be considered constant and folded.
        // The values are set independently of the compiler and instead are set during traplinker.
        // We will always read the "correct" values via this method.
        core::ptr::read_volatile(lfr_addr_ptr.add(i))
    });

    let trap_load_bias = TargetAddr::offset(
        &uintptr_t!(&trap_begin),
        &TargetAddr::from_raw(lfr_addrs[LfrAddr::TxtRpBegin as usize]),
    );

    let offset = uintptr_t!(xptramp_begin).wrapping_sub(lfr_addrs[LfrAddr::TextRampBegin as usize]);
    let sections: [TrapSectionInfoTable; 1] = [TrapSectionInfoTable {
        exec: (offset.wrapping_add(lfr_addrs[LfrAddr::TextBegin as usize])
            ..offset.wrapping_add(lfr_addrs[LfrAddr::TextEnd as usize]))
            .into(),
        trap: (offset.wrapping_add(lfr_addrs[LfrAddr::TxtRpBegin as usize])
            ..offset.wrapping_add(lfr_addrs[LfrAddr::TxtRpEnd as usize]))
            .into(),
    }];
    let modinfo = ModuleInfo {
        args,
        orig_dt_init: uintptr_t!(&orig_init),
        orig_entry: uintptr_t!(&orig_entry),
        lfr_preinit: uintptr_t!(&lfr_preinit),
        lfr_init: uintptr_t!(&lfr_init),
        lfr_entry: uintptr_t!(&lfr_entry),
        lfr_remove_call: uintptr_t!(&lfr_remove_call),
        lfr_return: uintptr_t!(&lfr_return),
        xptramp: (uintptr_t!(xptramp_begin)
            ..uintptr_t!(xptramp_begin).wrapping_add(lfr_addrs[LfrAddr::TextRampSize as usize]))
            .into(),
        got_start: TargetAddr::from_raw(get_got()),
        dynamic: TargetAddr::from_raw(get_dynamic()),
        trap_end_page: if uintptr_t!(trap_end_page) > uintptr_t!(&trap_end) {
            uintptr_t!(trap_end_page)
        } else {
            TargetAddr::from_raw(0usize)
        },
        trap_load_bias,
        trampoline_counter_ptr: (&lfr_trampoline_counter) as *const _ as *mut _,
        trampoline_condvar_ptr: (&lfr_trampoline_condvar) as *const _ as *mut _,

        linker_stubs: TargetAddr::from_raw(0usize),
        sections: sections.as_slice().into(),
    };

    RandoMain(&modinfo);
}

/// The `fini` entrypoint that is triggered at the conclusion of the application/binary.
/// # Safety
/// Expected to be run from an entrypoint, single threaded.
#[no_mangle]
#[link_section = ".lfr.entry"]
pub unsafe extern "C" fn lfr_fini_rust() {
    RandoFinish(uintptr_t!(&orig_entry));
}

/// An Aborting Panic Handler
#[cfg(all(not(feature = "std"), feature = "standalone"))]
#[panic_handler]
#[link_section = ".lfr.entry"]
fn panic(_panic_info: &core::panic::PanicInfo<'_>) -> ! {
    core::intrinsics::abort()
}
