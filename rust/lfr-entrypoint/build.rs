/* Copyright (c) 2022 RunSafe Security Inc. */
use lfr_target::{build::depend, Arch, BuildTarget};
use modinfo::LfrAddr;
use std::path::Path;

fn main() {
    let arch = Arch::build_script();
    let arch_dir = Path::new("c/").join(arch.to_string());

    let files = ["EntryPointWrapper.S", "Textramp.S"];

    let mut build = cc::Build::new_target(false);
    if cfg!(feature = "hook_preinit") {
        build.file(depend(arch_dir.join("PreinitEntryPoint.S")));
    }
    for f in files {
        build.file(depend(arch_dir.join(f)));
    }

    build
        .file(depend("hidden.c"))
        .define(
            "LFR_ADDR_COUNT",
            (LfrAddr::Num as usize).to_string().as_ref(),
        )
        .compile("foo");
}
