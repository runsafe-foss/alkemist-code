/* Copyright (c) 2022 RunSafe Security Inc. */
#include <stdint.h>

#pragma GCC visibility push(hidden)
// How is it possible there is no way to explicitly mark declarations as hidden in Rust?
int lfr_trampoline_counter = 0;
uint32_t lfr_trampoline_condvar = 0;

// Array containing a set of interesting addresses computed at
// link-time by TrapLinker (which patches the binary post-linking),
// like the addresses of .text, .textramp and .txtrp.
const uintptr_t lfr_addrs[LFR_ADDR_COUNT] __attribute__((section(".lfr.addrs")));

extern void lfr_run_rust(uintptr_t *args);
extern void lfr_fini_rust(void);

void lfr_run(uintptr_t *args) __attribute__((section(".lfr.entry")));
void lfr_run(uintptr_t *args) {
    lfr_run_rust(args);
}

void lfr_fini(void) __attribute__((section(".lfr.entry")));
void lfr_fini(void) {
    lfr_fini_rust();
}

#if RANDOLIB_IS_ANDROID
// lld doesn't seem to link the weak _DYNAMIC symbol correctly; it always
// prioritizes ours over its internal symbol, which isn't what we want
extern uintptr_t _DYNAMIC[];
#else
uintptr_t _DYNAMIC[] __attribute__((weak)) = {};
#endif

extern uintptr_t _GLOBAL_OFFSET_TABLE_[];

uintptr_t get_got() {
    return (uintptr_t)_GLOBAL_OFFSET_TABLE_;
}

uintptr_t get_dynamic() {
    return (uintptr_t)_DYNAMIC;
}

const uintptr_t *get_lfr_addrs() {
    return lfr_addrs;
}
#pragma GCC visibility pop
