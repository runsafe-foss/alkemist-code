/*
 * This file is part of selfrando.
 * Copyright (c) 2015-2022 RunSafe Security Inc.
 * For license information, see the LICENSE file
 * included with selfrando.
 *
 */

.section .lfr.entry, "ax", @progbits
// lfr_preinit implementation
// We need this even if we're not hooking preinit,
// since OSImpl.cpp needs something to patch
.globl lfr_preinit
.hidden lfr_preinit
.type lfr_preinit,@function
        // selfrando will patch this to lfr_return
.byte 0xE9, 0x00, 0x00, 0x00, 0x00
lfr_preinit:
.cfi_startproc
.byte 0xEB, 0x00

        xor %rdi, %rdi
        jmp lfr_run_and_remove
.cfi_endproc
.size lfr_preinit, . - lfr_preinit


// lfr_init implementation
.globl lfr_init
.hidden lfr_init
.type lfr_init,@function

1:
        // selfrando will patch this to the correct target
        // and the next 2-byte jump to come back here
        jmp orig_init
lfr_init:
.cfi_startproc
.byte 0xEB, 0x00

        // Save the argument registers
        push %rdi // argc
.cfi_adjust_cfa_offset 8
.cfi_rel_offset %rdi, 0
        push %rsi // argv
.cfi_adjust_cfa_offset 8
.cfi_rel_offset %rsi, 0
        push %rdx // envp
.cfi_adjust_cfa_offset 8
.cfi_rel_offset %rdx, 0
        xor %rdi, %rdi
        call lfr_run_and_remove
        pop %rdx
.cfi_adjust_cfa_offset -8
.cfi_restore %rdx
        pop %rsi
.cfi_adjust_cfa_offset -8
.cfi_restore %rsi
        pop %rdi
.cfi_adjust_cfa_offset -8
.cfi_restore %rdi
        jmp 1b
.cfi_endproc
.size lfr_init, . - lfr_init


// lfr_entry implementation
.globl lfr_entry
.hidden lfr_entry
.type lfr_entry,@function

1:
        jmp orig_entry
lfr_entry:
.cfi_startproc
        // selfrando will patch this to the correct target
.byte 0xEB, 0x00

        // We're at the entry point, which means we have
        // the arguments, environment and auxv at the top
        // of the stack
        push %rdi
.cfi_adjust_cfa_offset 8
.cfi_rel_offset %rdi, 0
        push %rdx
.cfi_adjust_cfa_offset 8
.cfi_rel_offset %rdx, 0

        lea 16(%rsp), %rdi
        call lfr_run_and_remove
        pop %rdx
.cfi_adjust_cfa_offset -8
.cfi_restore %rdx
        pop %rdi
.cfi_adjust_cfa_offset -8
.cfi_restore %rdi
        jmp 1b
.cfi_endproc
.size lfr_entry, . - lfr_entry


// lfr_run_and_remove implementation
.globl lfr_run_and_remove
.hidden lfr_run_and_remove
.type lfr_run_and_remove,@function
lfr_run_and_remove:
.cfi_startproc
        push %rbp
.cfi_adjust_cfa_offset 8
.cfi_rel_offset %rbp, 0
        mov %rsp, %rbp
.cfi_def_cfa_register %rbp

        and $-0x10, %rsp // need to align stack to 16 bytes
        call lfr_run

.globl lfr_remove_call
.hidden lfr_remove_call
.type lfr_remove_call, @object
lfr_remove_call:
        // selfrando will patch this to a jump to
        // the munmap code, if we have it
.byte 0x0F, 0x1F, 0x44, 0x00, 0x00

        mov %rbp, %rsp
.cfi_def_cfa_register %rsp
        pop %rbp
.cfi_adjust_cfa_offset -8
.cfi_restore %rbp
        ret
.cfi_endproc
.size lfr_run_and_remove, . - lfr_run_and_remove


// TrapLinker defines these as strong global symbols
// pointing to their actual implementations, but if
// either/both of those is missing, redirect to lfr_return
.weak orig_fini
.hidden orig_fini       
.type orig_fini,@function
.weak orig_init
.hidden orig_init
.type orig_init,@function

.weak orig_entry
.hidden orig_entry
.type orig_entry,@function

// lfr_return implementation
.globl lfr_return
.hidden lfr_return
.type lfr_return,@function
orig_fini:
orig_init:
orig_entry:
lfr_return:
.cfi_startproc
        ret
.cfi_endproc
.size lfr_return, . - lfr_return
        
.globl lfr_exit
.hidden lfr_exit
.type lfr_exit,@function
lfr_exit:       
.cfi_startproc
        push %rbp
        mov %rsp, %rbp
        and $-16, %rsp  
        call orig_fini
        call lfr_fini        
        leave
        ret
.cfi_endproc
.size lfr_exit, . - lfr_exit        

.globl lfr_increment_trampoline_counter
.hidden lfr_increment_trampoline_counter
.type lfr_increment_trampoline_counter,@function
lfr_increment_trampoline_counter:
.cfi_startproc
        lock incl lfr_trampoline_counter(%rip)
        ret
.cfi_endproc
.size lfr_increment_trampoline_counter, . - lfr_increment_trampoline_counter

.globl lfr_trampoline_wait_thunk
.hidden lfr_trampoline_wait_thunk
.type lfr_trampoline_wait_thunk,@function
lfr_trampoline_wait_thunk:
.cfi_startproc
        push %rax
.cfi_adjust_cfa_offset 8
.cfi_rel_offset %rax, 0
        push %rdi
.cfi_adjust_cfa_offset 8
.cfi_rel_offset %rdi, 0
        push %rsi
.cfi_adjust_cfa_offset 8
.cfi_rel_offset %rsi, 0
        push %rdx
.cfi_adjust_cfa_offset 8
.cfi_rel_offset %rdx, 0
        push %rcx
.cfi_adjust_cfa_offset 8
.cfi_rel_offset %rcx, 0
        push %r8
.cfi_adjust_cfa_offset 8
.cfi_rel_offset %r8, 0
        push %r9
.cfi_adjust_cfa_offset 8
.cfi_rel_offset %r9, 0
        lea lfr_trampoline_condvar(%rip), %rdi
        call lfr_trampoline_wait@PLT
        pop %r9
.cfi_adjust_cfa_offset -8
.cfi_restore %r9
        pop %r8
.cfi_adjust_cfa_offset -8
.cfi_restore %r9
        pop %rcx
.cfi_adjust_cfa_offset -8
.cfi_restore %rcx
        pop %rdx
.cfi_adjust_cfa_offset -8
.cfi_restore %rdx
        pop %rsi
.cfi_adjust_cfa_offset -8
.cfi_restore %rsi
        pop %rdi
.cfi_adjust_cfa_offset -8
.cfi_restore %rdi
        pop %rax
.cfi_adjust_cfa_offset -8
.cfi_restore %rax
        ret
.cfi_endproc
.size lfr_trampoline_wait_thunk, . - lfr_trampoline_wait_thunk
