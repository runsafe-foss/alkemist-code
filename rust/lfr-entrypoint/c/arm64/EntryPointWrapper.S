/*
 * This file is part of selfrando.
 * Copyright (c) 2015-2022 RunSafe Security Inc.
 * For license information, see the LICENSE file
 * included with selfrando.
 *
 */

.section .lfr.entry, "ax", %progbits
.globl lfr_preinit
.hidden lfr_preinit
.type lfr_preinit, %function
1:
    .long 0x14000001
lfr_preinit:
.cfi_startproc
    // selfrando will patch this to lfr_return
    .long 0x14000001
    stp x0, x30, [sp, #-16]!
.cfi_adjust_cfa_offset 16
.cfi_rel_offset x0, 0
.cfi_rel_offset x30, 8

    mov x0, #0
    bl lfr_run_and_remove

    // Restore the finalizer pointer and link register
    ldp x0, x30, [sp], #16
.cfi_adjust_cfa_offset -16
.cfi_restore x0
.cfi_restore x30
    ret
.cfi_endproc
.size lfr_preinit, . - lfr_preinit


.globl  lfr_init
.hidden lfr_init
.type   lfr_init,%function

1:
    .reloc ., R_AARCH64_JUMP26, orig_init
    .long 0x14000001
lfr_init:
.cfi_startproc
    // selfrando will patch this to the correct target
    .long 0x14000001

    stp x0, x30, [sp, #-16]!
.cfi_adjust_cfa_offset 16
.cfi_rel_offset x0, 0
.cfi_rel_offset x30, 8

    mov x0, #0
    bl lfr_run_and_remove

    // Restore the finalizer pointer and link register
    ldp x0, x30, [sp], #16
.cfi_adjust_cfa_offset -16
.cfi_restore x0
.cfi_restore x30

    // Jump to `orig_init`
    b 1b
.cfi_endproc
.size lfr_init, . - lfr_init


.globl  lfr_entry
.hidden lfr_entry
.type   lfr_entry,%function

1:
    .reloc ., R_AARCH64_JUMP26, orig_entry
    .long 0x14000001
lfr_entry:
.cfi_startproc
    // selfrando will patch this to the correct target
    .long 0x14000001

    stp x0, x30, [sp, #-16]!
.cfi_adjust_cfa_offset 16
.cfi_rel_offset x0, 0
.cfi_rel_offset x30, 8

    add x0, sp, #16
    bl lfr_run_and_remove

    // Restore the finalizer pointer and link register
    ldp x0, x30, [sp], #16
.cfi_adjust_cfa_offset -16
.cfi_restore x0
.cfi_restore x30

    b 1b
.cfi_endproc
.size lfr_entry, . - lfr_entry


.globl  lfr_run_and_remove, lfr_remove_call
.hidden lfr_run_and_remove, lfr_remove_call
.type   lfr_run_and_remove,%function
lfr_run_and_remove:
.cfi_startproc
    stp x0, x30, [sp, #-16]!
.cfi_adjust_cfa_offset 16
.cfi_rel_offset x0, 0
.cfi_rel_offset x30, 8

    bl lfr_run
    // Fall-through
lfr_remove_call:
    // TODO: add sequence that calls mprotect()

    ldp x0, x30, [sp], #16
.cfi_adjust_cfa_offset -16
.cfi_restore x0
.cfi_restore x30

    ret
.cfi_endproc
.size lfr_run_and_remove, . - lfr_run_and_remove


// TrapLinker defines these as strong global symbols
// pointing to their actual implementations, but if
// either/both of those is missing, redirect to lfr_return
        
.weak orig_fini
.hidden orig_fini       
.type orig_fini,%function
        
.weak orig_init
.hidden orig_init
.type orig_init,%function

.weak orig_entry
.hidden orig_entry
.type orig_entry,%function

.globl  lfr_return
.hidden lfr_return
.type   lfr_return,%function
        
orig_fini:      
orig_init:
orig_entry:
lfr_return:
.cfi_startproc
    ret
.cfi_endproc
.size lfr_return, . - lfr_return

.globl lfr_exit
.hidden lfr_exit
.type lfr_exit,%function
lfr_exit:       
.cfi_startproc
        stp x0, x30, [sp, #-16]!
.cfi_adjust_cfa_offset 16
.cfi_rel_offset x0, 0
.cfi_rel_offset x30, 8
        bl orig_fini
        bl lfr_fini
        
        ldp x0, x30, [sp], #16
.cfi_adjust_cfa_offset -16
.cfi_restore x0
.cfi_restore x30

        ret
.cfi_endproc
.size lfr_exit, . - lfr_exit        
