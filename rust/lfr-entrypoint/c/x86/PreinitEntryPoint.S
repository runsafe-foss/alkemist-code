/*
 * This file is part of selfrando.
 * Copyright (c) 2015-2022 RunSafe Security Inc.
 * For license information, see the LICENSE file
 * included with selfrando.
 *
 */

.section .preinit_array, "aw"
.p2align 4
.int lfr_preinit
