/* Copyright (c) 2022 RunSafe Security Inc. */
#![no_std]
use lfrgdbapi::modinfo::TargetAddr;
use lfrgdbapi::randolib;

#[no_mangle]
pub extern "C" fn _TRaP_RandoMain(module: &lfrgdbapi::modinfo::ModuleInfo) {
    let _ = randolib::rando_main(module);
}

#[no_mangle]
pub extern "C" fn _TRaP_RandoFinish(addr: TargetAddr<usize>) {
    randolib::rando_finish(addr);
}

/// # Safety
/// Expects a valid pointer to a conditional var.
#[no_mangle]
pub unsafe extern "C" fn _TRaP_lfr_trampoline_wait(condvar: *mut u32) {
    randolib::trampoline_wait(condvar)
}
