// Copyright (c) 2022 RunSafe Security Inc.

extern crate alloc;
use alloc::vec::Vec;
pub use rand::{distributions::Standard, prelude::Distribution, Rng};
pub use rand_chacha::rand_core::{RngCore, SeedableRng};
pub use rand_chacha::ChaCha20Rng as ChaCha;

/// A wrapper around a standard ChaCha RNG that has some additional modifications
/// to more closely match the behavior of the liblfr implementation.
#[derive(Clone, Debug)]
pub struct LfrChaCha {
    rng: ChaCha,
    seed: [u32; 10],
}

/// Chacha Errors
#[derive(Debug)]
pub enum ChaChaError {
    BadSeed,
}

pub trait LfrRng {
    fn rand<T>(&mut self) -> T
    where
        T: LfrRandom,
        Standard: Distribution<T>,

        LfrRandR: RandGen<T>;
    fn random<T>(&mut self, max: T) -> T
    where
        T: LfrRandom,
        Standard: Distribution<T>,
        LfrRandR: RandGen<T>,
    {
        if max == T::zero() {
            T::zero()
        } else {
            let mask = T::max_value() >> max.leading_zeros() as usize;
            loop {
                let unmasked = self.rand::<T>();
                let j = unmasked & mask;
                if j < max {
                    break j;
                }
            }
        }
    }

    fn seed(&self) -> &[u32];
}

pub trait LfrRandom:
    num_traits::PrimInt + num_traits::Zero + num_traits::Bounded + num_traits::Unsigned
{
}

impl LfrRandom for usize {}
impl LfrRandom for u32 {}
impl LfrRandom for u64 {}

impl LfrChaCha {
    /// Create a new Chacha based Rng, assuming the Seed is of the correct size (10 u32s).
    /// It is initialized to the same specification as the original liblfr rand_chacha
    /// but doesn't have the same rekeying behavior.
    pub fn new(seed: [u32; 10]) -> Result<LfrChaCha, ChaChaError> {
        let bytes = seed
            .iter()
            .flat_map(|x| x.to_le_bytes().to_vec())
            .collect::<Vec<_>>();
        let mut buf: [u8; 32] = [0u8; 32];
        let len = buf.len();
        buf.copy_from_slice(&bytes[..len]);
        let mut rng = ChaCha::from_seed(buf);
        let mut nonce_bytes: [u8; 8] = [0u8; 8];
        nonce_bytes.copy_from_slice(&bytes[len..]);
        assert_eq!(nonce_bytes.len() + buf.len(), bytes.len());
        rng.set_stream(u64::from_le_bytes(nonce_bytes));
        Ok(LfrChaCha { seed, rng })
    }
    /// Convert this into the interior Rng for use with the liblfr C implementation
    pub fn into_inner(self) -> ChaCha {
        self.rng
    }
    /// This method mirrors the corresponding function in RandoLib/OS.h:random
    /// allowing for a randomized number below a given maximum value.
    pub fn random<T>(&mut self, max: T) -> T
    where
        T: LfrRandom,
        Standard: Distribution<T>,
    {
        if max == T::zero() {
            T::zero()
        } else {
            let mask = T::max_value() >> max.leading_zeros() as usize;
            loop {
                let unmasked = self.rng.gen::<T>();
                let j = unmasked & mask;
                if j < max {
                    break j;
                }
            }
        }
    }
}

impl LfrRng for LfrChaCha {
    fn rand<T>(&mut self) -> T
    where
        T: LfrRandom,
        Standard: Distribution<T>,
        LfrRandR: RandGen<T>,
    {
        self.rng.gen()
    }

    fn seed(&self) -> &[u32] {
        &self.seed[..]
    }
}

pub struct LfrRandR {
    orig_seed: [u32; 1],
    seed: u32,
}
impl LfrRandR {
    pub fn new(seed: u32) -> Self {
        Self {
            seed,
            orig_seed: [seed],
        }
    }
}

pub trait RandGen<T>
where
    T: LfrRandom,
{
    fn gen(&mut self) -> T;
}

impl RandGen<u32> for LfrRandR {
    fn gen(&mut self) -> u32 {
        self.next_u32()
    }
}
impl RandGen<u64> for LfrRandR {
    fn gen(&mut self) -> u64 {
        ((self.next_u32() as u64) << 32) | (self.next_u32() as u64)
    }
}

impl RandGen<usize> for LfrRandR {
    fn gen(&mut self) -> usize {
        #[cfg(target_pointer_width = "32")]
        {
            <Self as RandGen<u32>>::gen(self) as usize
        }
        #[cfg(target_pointer_width = "64")]
        {
            <Self as RandGen<u64>>::gen(self) as usize
        }
    }
}

impl LfrRandR {
    fn next_u32(&mut self) -> u32 {
        self.seed = self.seed.wrapping_mul(1103515245).wrapping_add(12345);
        self.seed
    }
}

impl LfrRng for LfrRandR {
    fn rand<T>(&mut self) -> T
    where
        T: LfrRandom,
        Standard: Distribution<T>,
        LfrRandR: RandGen<T>,
    {
        self.gen()
    }
    fn seed(&self) -> &[u32] {
        &self.orig_seed
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_chacha() {
        let mut rng = LfrChaCha::new([100; 10]).expect("valid chacha");
        assert!(rng.random(0usize) == 0);
        for max in 1..100usize {
            for _ in 0..1000 {
                assert!(rng.random(max) < max);
            }
        }
    }
}
