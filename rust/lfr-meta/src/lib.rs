// Copyright (c) 2022 RunSafe Security Inc.

//! A collection of useful types related to the LFR Metadata
//! This crate will be used by both the `liblfr.so` at runtime
//! and by a gdb python process.
//! Due to the runtime requirements it is strictly `![no_std]` for
//! everything that is used by `liblfr.so`.
#![cfg_attr(not(feature = "std"), no_std)]

extern crate alloc;

pub mod cache;
pub mod endian;
pub mod mlf;
pub mod rand;
pub mod symbols;
