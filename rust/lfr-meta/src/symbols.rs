// Copyright (c) 2022 RunSafe Security Inc.

//! Symbol Representation of LFR Metadata
//!
//! This information will be transmitted through protected memory to an external
//! reader so that the locations of symbols are able to be deduced. The symbol
//! data must contain *all neccesary* information to deterministically shuffle a set of symbols
//! without any additional knowledge.
use crate::mlf::Mlf;
use cfg_if::cfg_if;
use core::convert::{TryFrom, TryInto};
use core::marker::PhantomData;
use core::num::TryFromIntError;
use core::ops::{Range, RangeBounds};
use serde::{Deserialize, Serialize};

cfg_if! {
    if #[cfg(not(feature="std"))] {
        extern crate alloc;
        use alloc::{vec::Vec};
        use alloc::borrow::Cow;
    } else {
        use std::borrow::Cow;
        use std::path::{PathBuf};
        use std::ffi::{OsString};
        use std::os::unix::ffi::OsStringExt;
        pub trait SymbolEntry {
            fn module_path(&self) -> Option<PathBuf>;
        }

        impl SymbolEntry for SymbolEntryV1 {
            fn module_path(&self) -> Option<PathBuf> {
                self.module_path.as_ref().map(|p| PathBuf::from(OsString::from_vec(p.to_vec())))
            }
        }
    }
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct StoredVector<T> {
    len: u64,
    addr: u64,
    _phantom: PhantomData<T>,
}

pub enum ReadError {
    OutOfRange,
    NegativeRange,
}

impl<T> StoredVector<T>
where
    T: Clone,
{
    pub fn new(len: usize, addr: *mut u8) -> Self {
        StoredVector {
            len: u64::try_from(len).unwrap(),
            addr: addr as u64,
            _phantom: PhantomData,
        }
    }
    pub fn read_range<'a, B, R>(
        &mut self,
        mut reader: R,
        range: B,
    ) -> Result<Cow<'a, [T]>, ReadError>
    where
        B: RangeBounds<usize>,
        R: FnMut(u64, usize) -> Result<Cow<'a, [T]>, ReadError>,
    {
        use core::ops::Bound;

        let len = usize::try_from(self.len).unwrap();
        let start = match range.start_bound() {
            Bound::Excluded(s) => s + 1,
            Bound::Included(s) => *s,
            Bound::Unbounded => 0,
        };

        let end = match range.end_bound() {
            Bound::Excluded(e) => *e,
            Bound::Included(e) => e + 1,
            Bound::Unbounded => len,
        };

        if end > len {
            return Err(ReadError::OutOfRange);
        } else if end < start {
            return Err(ReadError::NegativeRange);
        }
        let size = core::mem::size_of::<T>();
        let start_addr = self.addr + u64::try_from(start * size).unwrap();
        let vec = reader(start_addr, end - start)?;
        Ok(vec)
    }
    pub fn read_all<'a, R>(&mut self, reader: R) -> Result<Cow<'a, [T]>, ReadError>
    where
        R: FnMut(u64, usize) -> Result<Cow<'a, [T]>, ReadError>,
    {
        self.read_range(reader, ..)
    }
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct CoreSymbolData {
    pub entries: StoredVector<u8>,
    pub offsets: StoredVector<u64>,
}

/// The Metadata
///
/// If the data changes and we want to maintain backwards compatibility add a new version (e.g. `V2`).
#[derive(Serialize, Deserialize, Debug, Clone)]
#[non_exhaustive]
pub enum CoreSymbols {
    /// Version 1 Symbol Info
    V1(Vec<SymbolEntryV1>),
    /// Version 2 Symbol Info, each entry can be converted into a SymbolEntryV1
    V2(CoreSymbolData),
}

/// Trimmed Data represents functions that were shortened. This information cannot be derived later.
#[derive(Serialize, Deserialize, Debug, Hash, Eq, PartialEq, Copy, Clone)]
#[repr(C)]
pub struct Trim {
    /// Undiversified Start
    pub undiv_start: u64,
    /// New Size
    pub new_size: u64,
}

#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
#[repr(C)]
pub struct CopyRange<T> {
    pub start: T,
    pub end: T,
}

impl<T> From<CopyRange<T>> for Range<T> {
    fn from(copy: CopyRange<T>) -> Self {
        copy.start..copy.end
    }
}

impl<T> From<Range<T>> for CopyRange<T> {
    fn from(r: Range<T>) -> Self {
        CopyRange {
            start: r.start,
            end: r.end,
        }
    }
}

impl CopyRange<usize> {
    pub fn to_u64(&self) -> Result<CopyRange<u64>, TryFromIntError> {
        Ok(CopyRange {
            start: self.start.try_into()?,
            end: self.end.try_into()?,
        })
    }
}

impl CopyRange<u64> {
    pub fn to_usize(&self) -> Result<CopyRange<usize>, TryFromIntError> {
        Ok(CopyRange {
            start: self.start.try_into()?,
            end: self.end.try_into()?,
        })
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub struct StringRef(CopyRange<usize>);

#[derive(Debug)]
pub enum VecLikeErr {
    NoMem,
}

impl core::fmt::Display for VecLikeErr {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> Result<(), core::fmt::Error> {
        match self {
            VecLikeErr::NoMem => writeln!(f, "Out of memory")?,
        }
        Ok(())
    }
}

pub trait VecLike<T>: Default
where
    T: Clone,
{
    fn push(&mut self, val: T) -> Result<(), VecLikeErr>;
    fn extend<E>(&mut self, val: E) -> Result<(), VecLikeErr>
    where
        E: Iterator<Item = T>,
    {
        for v in val {
            self.push(v)?;
        }
        Ok(())
    }
    fn extend_range<E>(&mut self, val: E) -> Result<core::ops::Range<usize>, VecLikeErr>
    where
        E: Iterator<Item = T>,
    {
        let start = self.len();
        self.extend(val)?;
        let end = self.len();
        Ok(start..end)
    }
    fn get(&self, range: core::ops::Range<usize>) -> Option<Cow<[T]>>;
    fn len(&self) -> usize;
    fn is_empty(&self) -> bool {
        self.len() == 0
    }
}

impl<T> VecLike<T> for Vec<T>
where
    T: Clone,
{
    fn push(&mut self, val: T) -> Result<(), VecLikeErr> {
        self.push(val);
        Ok(())
    }
    fn get(&self, range: core::ops::Range<usize>) -> Option<Cow<[T]>> {
        self.as_slice().get(range).map(|v| v.into())
    }
    fn len(&self) -> usize {
        self.len()
    }
}

fn default_as_true() -> bool {
    true
}

/// An entry for a symbol file. Keeps all information to re-shuffle an unrandomized symbol file.
#[derive(Serialize, Deserialize, Debug, Hash, Eq, PartialEq, Clone)]
pub struct SymbolEntryV1 {
    /// Unique Identifier (usually an address)
    pub key: u64,
    /// Randomized Seed
    pub seed: Option<Vec<u32>>,
    /// Executable Region
    pub exec_region: Range<u64>,
    /// TextRamp Region
    pub textramp_region: Range<u64>,
    /// Start of memory where file has been mapped.
    pub map_base: Option<u64>,
    /// Exception Frame Handler Addresss
    pub eh_frame_hdr: u64,
    /// Global Offset
    pub got: u64,
    /// Module path
    /// (stored in bytes for compatibility, would prefer PathBuf but not available in `no_std`)
    pub module_path: Option<Vec<u8>>,
    /// All Trimed Functions
    pub trim: Vec<Trim>,
    /// Optional MLF of original randomization, for debugging,
    pub mlf: Option<Mlf>,
    /// Whether eh_frame_symbols were processed.
    /// In early versions, eh_frames were processed so long as an eh_frame_header existed.
    /// Now it's conditional based on an environment variable.
    #[serde(default = "default_as_true")]
    pub eh_frame_processed: bool,
}
