/* Copyright (c) 2022 RunSafe Security Inc. */
use alloc::borrow::ToOwned;
use alloc::vec;
use alloc::vec::Vec;
use core::marker::PhantomData;
use core::mem;
use libc::{c_int, c_void, cmsghdr, iovec, msghdr};
use libc::{SCM_RIGHTS, SOL_SOCKET};
use serde::{Deserialize, Serialize};

/// Path for Unix socket. This path should be in a directory only writable by
/// root but it's no longer required. The socket can be anywhere, we check the credentials
/// of the process attached to the socket.
/// Must be set via the LFR_CACHE_SOCKET environment variable.
pub const LFR_CACHE_SOCKET: &str = match option_env!("LFR_CACHE_SOCKET") {
    Some(v) => v,
    None => "/tmp/lfr_cache",
};

/// Private implementation of CMSG_* functions for accessing control message
/// data. The libc versions are marked unsafe, but don't need to be.
mod cmsg {
    use core::mem::size_of;
    use libc::cmsghdr;

    /// Offset to start of ancillary data payload from start of control header
    /// in bytes
    const CONTROL_DATA_OFFSET: usize = size_of::<cmsghdr>();

    /// Return `length` with the required alignment for use in a control message
    const fn align(length: usize) -> usize {
        (length + size_of::<usize>() - 1) & !(size_of::<usize>() - 1)
    }

    /// Return the total length in bytes of a control message with a payload of
    /// `length` bytes.
    pub(super) const fn space(length: usize) -> usize {
        align(length) + align(size_of::<cmsghdr>())
    }

    /// Value to store in `cmsg_len` field of a control message with a payload
    /// of `length` bytes.
    pub(super) const fn len(length: usize) -> usize {
        align(size_of::<cmsghdr>()) + length
    }

    /// Return the mutable data slice from the whole control message buffer for
    /// a given payload length.
    pub(super) fn data_mut(cmsg_buffer: &mut [u8], payload_len: usize) -> &mut [u8] {
        &mut cmsg_buffer[CONTROL_DATA_OFFSET..CONTROL_DATA_OFFSET + payload_len]
    }

    /// Return the data slice from the whole control message buffer for a given
    /// payload length.
    pub(super) fn data(cmsg_buffer: &[u8], payload_len: usize) -> &[u8] {
        &cmsg_buffer[CONTROL_DATA_OFFSET..CONTROL_DATA_OFFSET + payload_len]
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub struct CacheFileMeta {
    pub reloc_list: Option<core::ops::Range<usize>>,
    pub debug_info: Option<core::ops::Range<usize>>,
}

#[repr(u8)]
#[derive(Debug, PartialEq)]
pub enum CacheDaemonMessage<RawFd> {
    Request(RawFd, Vec<u8>),
    NewFile(RawFd),
    FoundFile { fd: RawFd, meta: CacheFileMeta },
    NoFile,
    InitSuccessful { meta: CacheFileMeta },
    InitFailed,
}

impl<RawFd> CacheDaemonMessage<RawFd> {
    pub fn map_fd<NewFd, F>(self, f: F) -> CacheDaemonMessage<NewFd>
    where
        F: Fn(RawFd) -> NewFd,
    {
        match self {
            CacheDaemonMessage::Request(fd, key) => CacheDaemonMessage::Request(f(fd), key),
            CacheDaemonMessage::FoundFile { fd, meta } => {
                CacheDaemonMessage::FoundFile { fd: f(fd), meta }
            }
            CacheDaemonMessage::NewFile(fd) => CacheDaemonMessage::NewFile(f(fd)),
            CacheDaemonMessage::NoFile => CacheDaemonMessage::NoFile,
            CacheDaemonMessage::InitSuccessful { meta } => {
                CacheDaemonMessage::InitSuccessful { meta }
            }
            CacheDaemonMessage::InitFailed => CacheDaemonMessage::InitFailed,
        }
    }
}

#[derive(Serialize, Deserialize)]
enum CacheDaemonBody {
    Request(Vec<u8>),
    NewFile,
    FoundFile(CacheFileMeta),
    NoFile,
    InitSuccessful(CacheFileMeta),
    InitFailed,
}

impl<RawFd: Copy + From<i32>, IoError> TryFrom<CacheDaemonMessage<RawFd>>
    for Message<RawFd, IoError>
{
    type Error = MessageErr<IoError>;
    fn try_from(msg: CacheDaemonMessage<RawFd>) -> Result<Message<RawFd, IoError>, Self::Error> {
        let (fds, body) = match msg {
            CacheDaemonMessage::Request(fd, key) => (vec![fd], CacheDaemonBody::Request(key)),
            CacheDaemonMessage::NewFile(fd) => (vec![fd], CacheDaemonBody::NewFile),
            CacheDaemonMessage::FoundFile { fd, meta } => {
                (vec![fd], CacheDaemonBody::FoundFile(meta))
            }
            CacheDaemonMessage::NoFile => (vec![], CacheDaemonBody::NoFile),
            CacheDaemonMessage::InitSuccessful { meta } => {
                (vec![], CacheDaemonBody::InitSuccessful(meta))
            }
            CacheDaemonMessage::InitFailed => (vec![], CacheDaemonBody::InitFailed),
        };

        let mut msg = vec![];
        ciborium::into_writer(&body, &mut msg).map_err(|_| MessageErr::BadParse)?;
        Ok(Message::<RawFd, IoError>::from_owned(msg, fds))
    }
}

impl<RawFd: Copy + From<i32>, IoError> TryFrom<Message<RawFd, IoError>>
    for CacheDaemonMessage<RawFd>
{
    type Error = MessageErr<IoError>;
    fn try_from(msg: Message<RawFd, IoError>) -> Result<CacheDaemonMessage<RawFd>, Self::Error> {
        let recv = ciborium::from_reader::<CacheDaemonBody, &[u8]>(&msg.message)
            .map_err(|_| MessageErr::BadParse)?;
        let converted = match recv {
            CacheDaemonBody::Request(key) => CacheDaemonMessage::Request(msg.fds[0], key),
            CacheDaemonBody::NewFile => CacheDaemonMessage::NewFile(msg.fds[0]),
            CacheDaemonBody::FoundFile(meta) => CacheDaemonMessage::FoundFile {
                fd: msg.fds[0],
                meta,
            },
            CacheDaemonBody::NoFile => CacheDaemonMessage::NoFile,
            CacheDaemonBody::InitSuccessful(meta) => CacheDaemonMessage::InitSuccessful { meta },
            CacheDaemonBody::InitFailed => CacheDaemonMessage::InitFailed,
        };
        Ok(converted)
    }
}

/// Message containing file descriptors to transfer via a Unix socket.
pub struct Message<RawFd, IoError> {
    message: Vec<u8>,
    fds: Vec<RawFd>,
    phantom: PhantomData<IoError>,
}

impl<RawFd: Copy + From<i32>, IoError> Message<RawFd, IoError> {
    pub fn new(message: &[u8], fds: &[RawFd]) -> Self {
        Self {
            message: message.to_owned(),
            fds: fds.to_vec(),
            phantom: PhantomData,
        }
    }

    pub fn from_owned(message: Vec<u8>, fds: Vec<RawFd>) -> Self {
        Self {
            message,
            fds,
            phantom: PhantomData,
        }
    }

    fn empty_with_fd() -> Self {
        Self {
            message: vec![0u8; Self::BUF_SIZE],
            fds: vec![0i32.into()],
            phantom: PhantomData,
        }
    }
}

impl<RawFd: Copy, IoError> SocketMsg for Message<RawFd, IoError> {
    type IoError = IoError;
    fn message(&self) -> &[u8] {
        &self.message
    }

    fn message_mut(&mut self) -> &mut [u8] {
        &mut self.message
    }

    fn set_data_len(&mut self, len: usize) {
        self.message.truncate(len);
    }

    fn control_msg_type(&self) -> c_int {
        SCM_RIGHTS
    }

    fn control_msg_data(&self) -> &[u8] {
        // TODO: is this faster?
        // unsafe { slice::from_raw_parts(self.fds.as_ptr() as *const u8, mem::size_of_val(self.fds.as_slice())) }

        let (prefix, data, suffix) = unsafe { self.fds.as_slice().align_to::<u8>() };
        assert!(prefix.is_empty());
        assert!(suffix.is_empty());
        data
    }

    fn set_control_msg_data(&mut self, buf: &[u8]) {
        let data = cmsg::data(buf, mem::size_of_val(self.fds.as_slice()));
        let (prefix, data, suffix) = unsafe { data.align_to::<RawFd>() };
        assert!(prefix.is_empty());
        assert!(suffix.is_empty());
        self.fds.copy_from_slice(data);
    }
}

#[derive(Debug)]
pub enum MessageErr<T> {
    Closed,
    UnexpectedLen,
    InvalidLength,
    BadParse,
    IoError(T),
}

impl<T> core::fmt::Display for MessageErr<T>
where
    T: core::fmt::Display,
{
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        match self {
            Self::Closed => write!(f, "Socket Closed"),
            Self::BadParse => write!(f, "Unparseable Msg"),
            Self::UnexpectedLen => write!(f, "Unexpected Message Length"),
            Self::InvalidLength => write!(f, "Invalid Length"),
            Self::IoError(io) => write!(f, "Io Error: {}", io),
        }
    }
}
/// Unix socket message with control message (also called ancillary data)
pub trait SocketMsg: Sized {
    type IoError;

    /// Size of receiving buffer for data bytes
    const BUF_SIZE: usize = 256;

    /// Get the message data
    fn message(&self) -> &[u8];

    /// Get mutable message data
    fn message_mut(&mut self) -> &mut [u8];

    /// Truncate data buffer to `len` bytes
    fn set_data_len(&mut self, len: usize);

    /// Type of control message for cmsg_type field in cmsghdr.
    fn control_msg_type(&self) -> c_int;

    /// Raw buffer data from control message
    fn control_msg_data(&self) -> &[u8];

    /// Set data for control message
    fn set_control_msg_data(&mut self, data: &[u8]);

    /// Build a control message for this message
    fn control_msg_buf(&mut self) -> Result<Vec<u8>, MessageErr<Self::IoError>> {
        let payload = self.control_msg_data();
        let len = payload.len();
        let mut header: cmsghdr = unsafe { core::mem::zeroed() };

        let cmsg_len = cmsg::len(len);
        #[cfg(target_env = "musl")]
        let cmsg_len = u32::try_from(cmsg_len).unwrap();
        header.cmsg_len = cmsg_len;
        header.cmsg_level = SOL_SOCKET;
        header.cmsg_type = self.control_msg_type();

        let mut buf = vec![0u8; cmsg::space(len)];
        unsafe {
            core::ptr::write(buf.as_mut_ptr() as *mut cmsghdr, header);
        }
        cmsg::data_mut(&mut buf, payload.len()).copy_from_slice(payload);
        Ok(buf)
    }
}

pub trait StructuredMessageConnection: CMsgConnection {
    type Message: TryFrom<Message<Self::RawFd, Self::IoError>, Error = MessageErr<Self::IoError>>
        + TryInto<Message<Self::RawFd, Self::IoError>, Error = MessageErr<Self::IoError>>;
    /// Send a file descriptor along with a message tag.
    fn receive_item(
        &mut self,
    ) -> Result<Self::Message, MessageErr<<Self as CMsgConnection>::IoError>> {
        let msg = self.receive(Message::<Self::RawFd, Self::IoError>::empty_with_fd())?;
        msg.try_into()
    }
    /// Send a file descriptor along with a message tag.
    fn send_item(&mut self, msg: Self::Message) -> Result<(), MessageErr<Self::IoError>> {
        self.send(msg.try_into()?)
    }
}

pub trait CMsgConnection {
    type IoError;
    type RawFd: Copy + From<i32>;
    fn sendmsg(&mut self, msg: &libc::msghdr, flags: libc::c_int) -> Result<usize, Self::IoError>;
    fn recvmsg(
        &mut self,
        msg: &mut libc::msghdr,
        flags: libc::c_int,
    ) -> Result<usize, Self::IoError>;

    /// Receive a control message
    fn receive<M>(&mut self, mut message: M) -> Result<M, MessageErr<Self::IoError>>
    where
        M: SocketMsg<IoError = Self::IoError>,
    {
        let mut control_buf = message.control_msg_buf()?;
        let mut iov = iovec {
            iov_base: message.message_mut().as_mut_ptr() as *mut c_void,
            iov_len: message.message().len(),
        };
        let mut msghdr: msghdr = unsafe { core::mem::zeroed() };
        msghdr.msg_name = core::ptr::null_mut();
        msghdr.msg_namelen = 0;
        msghdr.msg_iov = &mut iov;
        msghdr.msg_iovlen = 1;
        msghdr.msg_control = control_buf.as_mut_ptr() as *mut c_void;
        let control_len = control_buf.len();
        #[cfg(target_env = "musl")]
        let control_len = u32::try_from(control_len).unwrap();
        msghdr.msg_controllen = control_len;
        msghdr.msg_flags = 0;
        let len = self.recvmsg(&mut msghdr, 0).map_err(MessageErr::IoError)?;
        if len == 0 {
            Err(MessageErr::Closed)
        } else {
            message.set_control_msg_data(&control_buf);
            message.set_data_len(len);
            Ok(message)
        }
    }

    /// Send a control message
    fn send<M>(&mut self, mut message: M) -> Result<(), MessageErr<Self::IoError>>
    where
        M: SocketMsg<IoError = Self::IoError>,
    {
        let mut control_buf = message.control_msg_buf()?;
        let mut iov = iovec {
            iov_base: message.message_mut().as_mut_ptr() as *mut c_void,
            iov_len: message.message().len(),
        };
        let mut msghdr: msghdr = unsafe { core::mem::zeroed() };
        msghdr.msg_name = core::ptr::null_mut();
        msghdr.msg_namelen = 0;
        msghdr.msg_iov = &mut iov;
        msghdr.msg_iovlen = 1;
        msghdr.msg_control = control_buf.as_mut_ptr() as *mut c_void;
        let control_len = control_buf.len();
        #[cfg(target_env = "musl")]
        let control_len = u32::try_from(control_len).unwrap();
        msghdr.msg_controllen = control_len;
        msghdr.msg_flags = 0;

        self.sendmsg(&msghdr, 0).map_err(MessageErr::IoError)?;
        Ok(())
    }
}
