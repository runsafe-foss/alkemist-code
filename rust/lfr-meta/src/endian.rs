// Copyright (c) 2022 RunSafe Security Inc.

//! Convenient conversion of Different Endianness bytes to targeted types.

#[derive(Debug, Copy, Clone)]
pub enum Endian {
    Big,
    Little,
}

pub trait EndianRead {
    type Array: Default + AsRef<[u8]> + AsMut<[u8]>;
    fn from_le_bytes(bytes: Self::Array) -> Self;
    fn from_be_bytes(bytes: Self::Array) -> Self;
}
pub trait EndianWrite {
    type Array: Default + AsRef<[u8]> + AsMut<[u8]>;
    fn into_le_bytes(v: Self) -> Self::Array;
    fn into_be_bytes(v: Self) -> Self::Array;
}

macro_rules! impl_EndianRead_for_ints (( $($int:ident),* ) => {
    $(
        impl EndianRead for $int {
            type Array = [u8; core::mem::size_of::<Self>()];
            fn from_le_bytes(bytes: Self::Array) -> Self { Self::from_le_bytes(bytes) }
            fn from_be_bytes(bytes: Self::Array) -> Self { Self::from_be_bytes(bytes) }
        }
        impl EndianWrite for $int {
            type Array = [u8; core::mem::size_of::<Self>()];
            fn into_le_bytes(v: Self) -> Self::Array { Self::to_le_bytes(v) }
            fn into_be_bytes(v: Self) -> Self::Array { Self::to_be_bytes(v) }
        }
    )*
});

impl_EndianRead_for_ints!(u8, u16, u32, u64);

#[cfg(feature = "goblin-enum")]
use goblin::container::Endian as GoblinEndian;
#[cfg(feature = "goblin-enum")]
impl From<goblin::container::Endian> for Endian {
    fn from(endian: GoblinEndian) -> Endian {
        match endian {
            GoblinEndian::Big => Endian::Big,
            GoblinEndian::Little => Endian::Little,
        }
    }
}
