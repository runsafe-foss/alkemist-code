// Copyright (c) 2022 RunSafe Security Inc.

//! An MLF (Memory Layout Format) keeps track of some base addresses
//! and the set of functions that are/were randomized.
use cfg_if::cfg_if;
use serde::{Deserialize, Serialize};
#[cfg(feature = "read_mlf")]
use winnow::prelude::*;

cfg_if! {
    if #[cfg(not(feature="std"))] {
        extern crate alloc;
        use alloc::{vec, vec::Vec};
        use alloc::string::String;
    } else {
        use intervaltree::IntervalTree;
    }
}

/// A Function Representation
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct Func {
    /// Undiversified Starting Address
    pub undiv_start: u64,
    /// Diversified Starting Address
    pub div_start: u64,
    /// Size of Function
    pub size: u64,
}

impl Func {
    pub fn new(undiv_start: u64, div_start: u64, size: u64) -> Func {
        /* zero has special meaning in some cases and will cause a lot of grief */
        assert_ne!(undiv_start, 0);
        assert_ne!(div_start, 0);
        Func {
            undiv_start,
            div_start,
            size,
            //base: 0,
        }
    }
    pub fn undiv_range(&self) -> core::ops::Range<u64> {
        self.undiv_start..(self.undiv_start + self.size)
    }
    pub fn div_range(&self) -> core::ops::Range<u64> {
        self.div_start..(self.div_start + self.size)
    }
    pub fn offset(&self, offset: u64, base: Option<u64>) -> u64 {
        offset - self.undiv_start + self.div_start + base.unwrap_or(0)
    }
}

impl core::fmt::Display for Func {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        writeln!(f, "{{")?;
        writeln!(f, "undiv_range: {:08x?}", self.undiv_range())?;
        writeln!(f, "div_range: {:08x?}", self.div_range())?;
        writeln!(f, "size: {}", self.size)?;
        writeln!(f, "}}")?;
        Ok(())
    }
}
impl core::fmt::Display for Mlf {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        writeln!(f, "Version: {}", self.version)?;
        writeln!(f, "Func_Base: 0x{:08x}", self.func_base.unwrap_or(0))?;
        writeln!(f, "File_Base: 0x{:08x}", self.file_base.unwrap_or(0))?;
        writeln!(f, "Func_Size: 0x{:08x}", self.func_size)?;
        writeln!(f, "Mod Name: {}", self.mod_name)?;
        writeln!(f, "Funcs:")?;
        for func in &self.funcs {
            writeln!(f, "{}", func)?;
        }
        Ok(())
    }
}

/// MLF (Memory Layout Format).
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct Mlf {
    /// Verison of Info
    pub version: u32,
    /// Seed Value
    pub seed: Option<Vec<u32>>,
    /// The offsets within the file of the executable region
    pub file_base: Option<u64>,
    /// The offsets within memory of the executable region    
    pub func_base: Option<u64>,
    /// The size of the executable region
    pub func_size: u64,
    /// The name of the module. Empty for the main binary.
    pub mod_name: String,
    /// A list of functions contained within the given region.
    pub funcs: Vec<Func>,
}

impl Mlf {
    #[cfg(feature = "std")]
    pub fn undiv_intervals(&self) -> IntervalTree<u64, &Func> {
        self.funcs
            .iter()
            .map(|f| (f.undiv_range(), f))
            .collect::<IntervalTree<_, _>>()
    }
    #[cfg(feature = "std")]
    pub fn div_intervals(&self) -> IntervalTree<u64, &Func> {
        self.funcs
            .iter()
            .map(|f| (f.div_range(), f))
            .collect::<IntervalTree<_, _>>()
    }
    /// Allows reading of the on-disk format. Mostly deprecated.
    #[cfg(feature = "read_mlf")]
    pub fn read(input: &mut &[u8]) -> PResult<Mlf> {
        use winnow::binary::{le_u32, le_u64};
        use winnow::combinator::{repeat, repeat_till0};
        use winnow::token::take_while;
        let ptr_read = le_u64;
        let (version, seed_len) = (le_u32, le_u32).parse_next(input)?;
        let seed = repeat(0..seed_len as usize, le_u32).parse_next(input)?;
        let (file_base, func_base, func_size) = (ptr_read, ptr_read, le_u64).parse_next(input)?;

        let mod_name = take_while(0.., |c| c != 0)
            .recognize()
            .try_map(|x| core::str::from_utf8(x))
            .parse_next(input)?;
        let _null = b'\0'.parse_next(input)?;
        let (funcs, _) = repeat_till0(
            |input: &mut _| {
                let (undiv_start, div_start, size) = (le_u64, le_u64, le_u32).parse_next(input)?;
                //println!("Size: {:x?}", (undiv_start, div_start, size));
                //println!("{:?}", input);
                Ok(Func {
                    undiv_start: undiv_start - file_base,
                    div_start: div_start - file_base,
                    size: size as u64,
                })
            },
            b"\0\0\0\0\0\0\0\0",
        )
        .parse_next(input)?;
        Ok(Mlf {
            version,
            seed: Some(seed),
            file_base: Some(file_base),
            func_base: Some(func_base),
            func_size,
            funcs,
            mod_name: mod_name.to_string(),
        })
    }
}

impl AsRef<Mlf> for Mlf {
    fn as_ref(&self) -> &Mlf {
        self
    }
}

impl Default for Mlf {
    fn default() -> Self {
        Mlf {
            version: 1,
            seed: None,
            file_base: None,
            func_base: None,
            func_size: 0,
            mod_name: "".into(),
            funcs: vec![],
        }
    }
}
