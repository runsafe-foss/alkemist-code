/* Copyright (c) 2022 RunSafe Security Inc. */
use std::io::Write;
use std::path::{Path, PathBuf};

fn main() {
    println!("cargo:rerun-if-env-changed=CMAKE_INSTALL_PREFIX");
    let prefix = PathBuf::from(
        std::env::var("CMAKE_INSTALL_PREFIX")
            .ok()
            .unwrap_or_else(|| "CMAKE_INSTALL_PREFIX".to_string()),
    );
    let out_dir = PathBuf::from(std::env::var("OUT_DIR").unwrap());

    let mut f = std::fs::File::create(out_dir.join("paths.rs")).unwrap();

    let paths = [
        ("CMAKE_INSTALL_PREFIX", prefix.clone()),
        (
            "CMAKE_LFR_BIN_DIR",
            prefix.join(Path::new("bin/alkemist/lfr")),
        ),
        (
            "CMAKE_LFR_SCRIPTS_DIR",
            prefix.join(Path::new("bin/alkemist/lfr/scripts/")),
        ),
    ];
    for p in paths {
        writeln!(
            f,
            "pub const {}: &str = \"{}\";",
            p.0,
            p.1.to_str().unwrap()
        )
        .unwrap();
    }
}
