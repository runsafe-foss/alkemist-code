/* Copyright (c) 2022 RunSafe Security Inc. */
#![cfg_attr(not(feature = "std"), no_std)]
extern crate entrypoint;

pub use entrypoint::*;
use lfrgdbapi::modinfo::TargetAddr;
use lfrgdbapi::randolib;

///Define symbols for methods we don't have and don't use.
macro_rules! unused_sym {
    ($($name:ident),*) => {
        $(
            #[no_mangle]
            #[allow(non_snake_case)]
            fn $name () {
                unimplemented!("Unused symbol {} hit", stringify!($name))
            }
        )*
    }
}

unused_sym! {
    fmaxl,
    fmod,
    fmodf,
    logbl,
    scalbn,
    scalbnl,
    scalbnf
}

#[cfg(target_arch = "arm")]
unused_sym! {
    __gnu_Unwind_Find_exidx,
    __cxa_begin_cleanup,
    __cxa_call_unexpected,
    __cxa_type_match,
    raise,
    // This is almost certainly a bug. Arm should have a way of invoking
    // processes. But we don't have runtime license checking anymore. So maybe it's moot?
    vfork
}

#[no_mangle]
pub extern "C" fn RandoMain(module: &lfrgdbapi::modinfo::ModuleInfo) {
    let _ = randolib::rando_main(module);
}

#[no_mangle]
pub extern "C" fn RandoFinish(addr: TargetAddr<usize>) {
    randolib::rando_finish(addr);
}

/// # Safety
/// Expects a valid pointer to a conditional var.
#[no_mangle]
pub unsafe extern "C" fn lfr_trampoline_wait(condvar: *mut u32) {
    randolib::trampoline_wait(condvar)
}
