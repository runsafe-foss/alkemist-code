/* Copyright (c) 2022 RunSafe Security Inc. */
#![no_std]
#![feature(step_trait)]
use core::marker::PhantomData;
use core::ops::Range;
use core::ops::{Add, Sub};
use core::ptr::NonNull;

#[derive(Copy, Clone, PartialEq, Eq, Ord, PartialOrd, Default)]
#[repr(transparent)]
pub struct TargetAddr<T>(T);

impl<T> core::fmt::Display for TargetAddr<T>
where
    T: core::fmt::LowerHex,
{
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{:#x}", self.0)
    }
}

impl<T> core::fmt::Debug for TargetAddr<T>
where
    T: core::fmt::Debug + core::fmt::LowerHex,
{
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{:#x?}", self.0)
    }
}

impl<T> core::fmt::LowerHex for TargetAddr<T>
where
    T: core::fmt::LowerHex,
{
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        core::fmt::LowerHex::fmt(&self.0, f)
    }
}

#[cfg(target_pointer_width = "64")]
impl From<u64> for TargetAddr<usize> {
    fn from(val: u64) -> TargetAddr<usize> {
        TargetAddr(val.try_into().unwrap())
    }
}

#[cfg(target_pointer_width = "64")]
impl From<usize> for TargetAddr<u64> {
    fn from(val: usize) -> TargetAddr<u64> {
        TargetAddr(val.try_into().unwrap())
    }
}

impl From<TargetAddr<u64>> for u64 {
    fn from(val: TargetAddr<u64>) -> u64 {
        val.0
    }
}

#[cfg(target_pointer_width = "64")]
impl From<TargetAddr<usize>> for u64 {
    fn from(val: TargetAddr<usize>) -> u64 {
        val.0.try_into().unwrap()
    }
}

impl From<u32> for TargetAddr<usize> {
    fn from(val: u32) -> TargetAddr<usize> {
        TargetAddr(val.try_into().unwrap())
    }
}

#[cfg(target_pointer_width = "32")]
impl From<usize> for TargetAddr<u32> {
    fn from(val: usize) -> TargetAddr<u32> {
        TargetAddr(val.try_into().unwrap())
    }
}

#[cfg(target_pointer_width = "32")]
impl From<TargetAddr<usize>> for u32 {
    fn from(val: TargetAddr<usize>) -> Self {
        val.0.try_into().unwrap()
    }
}

#[cfg(target_pointer_width = "32")]
impl From<TargetAddr<usize>> for TargetAddr<u32> {
    fn from(val: TargetAddr<usize>) -> Self {
        TargetAddr(val.0.try_into().unwrap())
    }
}

#[cfg(target_pointer_width = "32")]
impl From<TargetAddr<u32>> for TargetAddr<usize> {
    fn from(val: TargetAddr<u32>) -> Self {
        TargetAddr(val.0.try_into().unwrap())
    }
}

impl From<TargetAddr<u32>> for u32 {
    fn from(val: TargetAddr<u32>) -> Self {
        val.0
    }
}

impl From<usize> for TargetAddr<usize> {
    fn from(val: usize) -> TargetAddr<usize> {
        TargetAddr(val)
    }
}

impl From<TargetAddr<usize>> for usize {
    fn from(val: TargetAddr<usize>) -> usize {
        val.0
    }
}

impl From<u32> for TargetAddr<u64> {
    fn from(val: u32) -> TargetAddr<u64> {
        TargetAddr(val.into())
    }
}

impl From<u32> for TargetAddr<u32> {
    fn from(val: u32) -> TargetAddr<u32> {
        TargetAddr(val)
    }
}

impl From<u16> for TargetAddr<u32> {
    fn from(val: u16) -> TargetAddr<u32> {
        TargetAddr(val.into())
    }
}

impl From<u64> for TargetAddr<u64> {
    fn from(val: u64) -> TargetAddr<u64> {
        TargetAddr(val)
    }
}

impl<T> TryFrom<TargetAddr<usize>> for NonNull<T> {
    type Error = ();
    fn try_from(addr: TargetAddr<usize>) -> Result<NonNull<T>, ()> {
        NonNull::new(addr.absolute_addr() as *mut T).ok_or(())
    }
}

impl<T> From<TargetAddr<usize>> for Option<NonNull<T>> {
    fn from(addr: TargetAddr<usize>) -> Option<NonNull<T>> {
        addr.try_into().ok()
    }
}

impl<T> Sub<T> for TargetAddr<T>
where
    T: num_traits::WrappingSub,
{
    type Output = Self;

    fn sub(self, other: T) -> Self {
        Self(self.0.wrapping_sub(&other))
    }
}
impl<T> Add<T> for TargetAddr<T>
where
    T: num_traits::WrappingAdd,
{
    type Output = Self;

    fn add(self, other: T) -> Self {
        Self(self.0.wrapping_add(&other))
    }
}

pub trait TargetAddrPtr:
    num_traits::WrappingAdd
    + num_traits::WrappingSub
    + core::ops::BitAnd<Output = Self>
    + core::ops::Not<Output = Self>
    + num_traits::One
    + num_traits::Zero
    + num_traits::CheckedAdd
    + num_traits::WrappingShl
    + num_traits::FromPrimitive
    + PartialOrd
    + PartialEq
    + TryFrom<Self>
    + TryInto<Self>
    + TryFrom<u64>
    + TryFrom<i64>
    + TryInto<u64>
    + TryFrom<u32>
    + TryInto<u32>
    + TryFrom<usize>
    + TryInto<usize>
    + Copy
    + core::fmt::Debug
    + core::fmt::LowerHex
// where
//     <Self as TryFrom<u64>>::Error: core::fmt::Debug,
//     <Self as TryFrom<u32>>::Error: core::fmt::Debug,
//     <Self as TryFrom<usize>>::Error: core::fmt::Debug,
{
}

impl<T> core::iter::Step for TargetAddr<T>
where
    T: core::iter::Step,
{
    fn steps_between(start: &Self, end: &Self) -> Option<usize> {
        T::steps_between(&start.0, &end.0)
    }
    fn forward_checked(start: Self, count: usize) -> Option<Self> {
        T::forward_checked(start.0, count).map(TargetAddr)
    }
    fn backward_checked(start: Self, count: usize) -> Option<Self> {
        T::backward_checked(start.0, count).map(TargetAddr)
    }
}

impl TargetAddrPtr for u64 {}
impl TargetAddrPtr for u32 {}
impl TargetAddrPtr for usize {}

impl<T> TargetAddr<T>
where
    T: TargetAddrPtr, // <T as TryFrom<u64>>::Error: core::fmt::Debug,
                      // <T as TryFrom<u32>>::Error: core::fmt::Debug,
                      // <T as TryFrom<usize>>::Error: core::fmt::Debug
{
    pub fn raw_range(r: &Range<TargetAddr<T>>) -> Range<T> {
        r.start.0..r.end.0
    }

    pub fn from_raw<R>(addr: R) -> Self
    where
        R: Into<T>,
    {
        Self(addr.into())
    }

    #[must_use]
    pub fn wrapping_add<I>(&self, addr: I) -> Self
    where
        I: Into<TargetAddr<T>>,
    {
        TargetAddr(self.0.wrapping_add(&addr.into().0))
    }
    #[must_use]
    pub fn wrapping_sub<I>(&self, offset: I) -> Self
    where
        I: Into<TargetAddr<T>>,
    {
        TargetAddr(self.0.wrapping_sub(&offset.into().0))
    }

    #[must_use]
    pub fn absolute_addr(&self) -> T {
        self.0
    }

    #[must_use]
    pub fn align_mask(&self, bit: u32) -> TargetAddr<T> {
        self.mask(T::one().wrapping_shl(bit).wrapping_sub(&T::one()))
    }

    #[must_use]
    pub fn align_val_mask(&self, align: T) -> TargetAddr<T> {
        self.mask(!align.wrapping_sub(&T::one()))
    }

    #[must_use]
    pub fn mask(&self, mask: T) -> TargetAddr<T> {
        TargetAddr(self.0 & mask)
    }

    #[must_use]
    pub fn is_null(&self) -> bool {
        self.0 == T::zero()
    }

    #[must_use]
    pub fn offset(a: &Self, b: &Self) -> usize {
        a.0.wrapping_sub(&b.0).try_into().map_err(|_| ()).unwrap()
    }
}

#[derive(Debug)]
#[repr(C)]
// ELF-specific module metadata
pub struct TrapSectionInfoTable {
    pub exec: CRange<TargetAddr<usize>>,
    pub trap: CRange<TargetAddr<usize>>,
}

#[derive(Debug)]
#[repr(C)]
pub struct CSlice<'a, T> {
    start: *const T,
    len: usize,
    phantom: PhantomData<&'a ()>,
}

impl<'a, T> CSlice<'a, T> {
    pub fn len(&self) -> usize {
        self.len
    }
    pub fn is_empty(&self) -> bool {
        self.len == 0
    }
}
impl<'a, T> Copy for CSlice<'a, T> {}

impl<'a, T> Clone for CSlice<'a, T> {
    fn clone(&self) -> Self {
        *self
    }
}
impl<'a, T> From<CSlice<'a, T>> for &'a [T] {
    fn from(cslice: CSlice<'a, T>) -> &'a [T] {
        unsafe { core::slice::from_raw_parts(cslice.start, cslice.len) }
    }
}

impl<'a, T> From<&'a [T]> for CSlice<'a, T> {
    fn from(slice: &'a [T]) -> CSlice<'a, T> {
        CSlice {
            start: slice.as_ptr(),
            len: slice.len(),
            phantom: PhantomData,
        }
    }
}

#[derive(Copy, Clone, Debug)]
#[repr(C)]
pub struct CRange<T> {
    pub start: T,
    pub end: T,
}

impl<T> CRange<T>
where
    T: PartialEq,
{
    pub fn is_empty(&self) -> bool {
        self.start == self.end
    }
}

impl<T> From<CRange<T>> for Range<T> {
    fn from(f: CRange<T>) -> Range<T> {
        f.start..f.end
    }
}

impl<T> From<Range<T>> for CRange<T> {
    fn from(f: Range<T>) -> CRange<T> {
        CRange {
            start: f.start,
            end: f.end,
        }
    }
}

#[derive(Debug)]
#[repr(usize)]
pub enum LfrAddr {
    TextBegin = 0usize,
    TextEnd,
    TextRampBegin,
    TextRampSize,
    TxtRpBegin,
    TxtRpEnd,
    Num,
}

#[derive(Debug)]
#[repr(C)]
pub struct ModuleInfo<'a> {
    // Start of process arguments on the stack
    // This is set by lfr_entry, so
    // it will be NULL for shared libraries
    pub args: *const libc::uintptr_t,

    pub orig_dt_init: TargetAddr<usize>,
    pub orig_entry: TargetAddr<usize>,

    // Locations of entry trampolines to relocate
    pub lfr_preinit: TargetAddr<usize>,
    pub lfr_init: TargetAddr<usize>,
    pub lfr_entry: TargetAddr<usize>,
    pub lfr_remove_call: TargetAddr<usize>,
    pub lfr_return: TargetAddr<usize>,

    // Location of export trampoline table
    pub xptramp: CRange<TargetAddr<usize>>,

    // Location of GOT
    pub got_start: TargetAddr<usize>,

    // Location of dynamic table
    pub dynamic: TargetAddr<usize>,

    // End of .txtrp pages
    pub trap_end_page: TargetAddr<usize>,

    // Difference between absolute addresses in TRaP info on disk (post-linking)
    // and in memory at load time
    pub trap_load_bias: usize,

    pub trampoline_counter_ptr: *mut u32,
    pub trampoline_condvar_ptr: *mut u32,

    // Location of linker stubs
    pub linker_stubs: TargetAddr<usize>,

    // Location of .text section
    // FIXME: for now, assume that there is only a fixed
    // number of sections and they contain all the code
    // Custom linker scripts may break this
    // We still put in a num_sections field, for future use
    // Also, we use num_sections to mark whether
    // we've added the sections to the table or not
    pub sections: CSlice<'a, TrapSectionInfoTable>,
}
