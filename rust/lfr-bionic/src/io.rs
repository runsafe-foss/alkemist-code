// Copyright (c) 2022 RunSafe Security Inc.

extern crate alloc;

use alloc::string::String;
use alloc::vec::Vec;
use core::fmt::Arguments;

pub type RawFd = i32;

#[derive(Debug)]
pub struct OwnedFd {
    fd: RawFd,
}

impl OwnedFd {
    /// # Safety
    /// You must have exclusive ownership of the RawFd.
    pub unsafe fn from_raw_fd(fd: RawFd) -> Self {
        Self { fd }
    }
}

impl Drop for OwnedFd {
    fn drop(&mut self) {
        unsafe { bionic_sys::close(self.fd) };
    }
}

#[derive(Copy, Clone, Debug)]
pub struct BorrowedFd<'a> {
    fd: &'a OwnedFd,
}

pub trait AsFd {
    fn as_fd(&self) -> BorrowedFd<'_>;
}

pub trait AsRawFd {
    fn as_raw_fd(&self) -> RawFd;
}

impl<'a> AsFd for BorrowedFd<'a> {
    fn as_fd(&self) -> BorrowedFd<'a> {
        *self
    }
}

impl AsFd for OwnedFd {
    fn as_fd(&self) -> BorrowedFd<'_> {
        BorrowedFd { fd: self }
    }
}

pub trait IntoRawFd {
    fn into_raw_fd(self) -> RawFd;
}

pub trait FromRawFd {
    /// # Safety
    /// You msut have exclusive ownership of the RawFd;
    unsafe fn from_raw_fd(fd: RawFd) -> Self;
}

impl IntoRawFd for OwnedFd {
    fn into_raw_fd(self) -> RawFd {
        let fd = self.fd;
        core::mem::forget(self);
        fd
    }
}

impl FromRawFd for OwnedFd {
    unsafe fn from_raw_fd(fd: RawFd) -> OwnedFd {
        Self { fd }
    }
}
impl AsRawFd for OwnedFd {
    fn as_raw_fd(&self) -> RawFd {
        self.fd
    }
}

impl AsRawFd for BorrowedFd<'_> {
    fn as_raw_fd(&self) -> RawFd {
        self.fd.as_raw_fd()
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
#[non_exhaustive]
pub enum Error {
    InvalidFile,
    InvalidString,
    Interrupted,
    UnexpectedEof,
    PathAccess,
    BadFileDescriptor,
    DoesNotExist,
    Again,
    Timeout,
    BadCString,
    FormatError,
    Unknown,
    InputContainsNull,
    InvalidData,
    Code(i64),
}

pub type Result<T> = core::result::Result<T, Error>;

pub trait Write {
    fn write(&mut self, buf: &[u8]) -> Result<usize>;
    fn flush(&mut self) -> Result<()>;
    fn write_all(&mut self, mut buf: &[u8]) -> Result<()> {
        while !buf.is_empty() {
            match self.write(buf) {
                Ok(n) => buf = &buf[n..],
                Err(Error::Interrupted | Error::Again) => continue,
                Err(e) => return Err(e),
            }
        }

        Ok(())
    }
    fn write_fmt(&mut self, fmt: Arguments<'_>) -> Result<()> {
        //Borrowed from rust std lib.
        use alloc::fmt;
        // Create a shim which translates a Write to a fmt::Write and saves
        // off I/O errors. instead of discarding them
        struct Adapter<'a, T: ?Sized + 'a> {
            inner: &'a mut T,
            error: Result<()>,
        }

        impl<T: Write + ?Sized> fmt::Write for Adapter<'_, T> {
            fn write_str(&mut self, s: &str) -> fmt::Result {
                match self.inner.write_all(s.as_bytes()) {
                    Ok(()) => Ok(()),
                    Err(e) => {
                        self.error = Err(e);
                        Err(fmt::Error)
                    }
                }
            }
        }

        let mut output = Adapter {
            inner: self,
            error: Ok(()),
        };
        match fmt::write(&mut output, fmt) {
            Ok(()) => Ok(()),
            Err(..) => {
                // check if the error came from the underlying `Write` or not
                if output.error.is_err() {
                    output.error
                } else {
                    Err(Error::FormatError)
                }
            }
        }
    }
    fn by_ref(&mut self) -> &mut Self
    where
        Self: Sized,
    {
        self
    }
}

pub trait Read {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize>;
    fn read_to_end(&mut self, buf: &mut Vec<u8>) -> Result<usize> {
        let mut tmp = [0; 1024];
        loop {
            match self.read(&mut tmp) {
                Ok(0) => break,
                Ok(n) => buf.extend(&tmp[..n]),
                Err(Error::Interrupted | Error::Again) => continue,
                Err(e) => return Err(e),
            }
        }

        Ok(buf.len())
    }
    fn read_to_string(&mut self, buf: &mut String) -> Result<usize> {
        // This could be more efficient, using the target string memory instead.
        let mut v = Vec::new();
        self.read_to_end(&mut v)?;
        *buf = String::from_utf8(v).map_err(|_| Error::InvalidString)?;
        Ok(buf.len())
    }
    fn read_exact(&mut self, mut buf: &mut [u8]) -> Result<()> {
        while !buf.is_empty() {
            match self.read(buf) {
                Ok(0) => break,
                Ok(n) => buf = &mut buf[n..],
                Err(Error::Interrupted | Error::Again) => continue,
                Err(e) => return Err(e),
            }
        }

        if buf.is_empty() {
            Ok(())
        } else {
            Err(Error::UnexpectedEof)
        }
    }
    fn by_ref(&mut self) -> &mut Self
    where
        Self: Sized,
    {
        self
    }
}

impl Write for Vec<u8> {
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        self.extend(buf);
        Ok(buf.len())
    }
    fn flush(&mut self) -> Result<()> {
        Ok(())
    }
}

pub struct BufReader<I> {
    inner: I,
    buf: [u8; 1024],
    written: core::ops::Range<usize>,
}

impl<I> BufReader<I> {
    pub fn new(inner: I) -> Self {
        Self {
            inner,
            buf: [0u8; 1024],
            written: 0..0,
        }
    }
}

pub trait BufRead {
    fn fill_buf(&mut self) -> Result<&[u8]>;
    fn consume(&mut self, amt: usize);
    fn read_until(&mut self, byte: u8, buf: &mut Vec<u8>) -> Result<usize> {
        loop {
            let local = self.fill_buf()?;
            if local.is_empty() {
                return Ok(0);
            } else if let Some(p) = local.iter().position(|&v| v == byte) {
                buf.extend(&local[..=p]);
                self.consume(p + 1);
                return Ok(p + 1);
            } else {
                buf.extend(local);
                let len = local.len();
                self.consume(len);
            }
        }
    }
}

impl<I> BufRead for BufReader<I>
where
    I: Read,
{
    fn fill_buf(&mut self) -> Result<&[u8]> {
        if self.written.is_empty() {
            let count = self.inner.read(&mut self.buf[..])?;
            self.written = 0..count;
        }
        Ok(&self.buf[self.written.clone()])
    }
    fn consume(&mut self, amt: usize) {
        self.written.start += amt;
    }
}

// None of these stdio, stderr, stdlock use locks to protect against threading errors.
// LFR does not use not use threads and can (for now) ignore this concern.

pub struct Stdin;
pub struct Stdout;
pub struct Stderr;

impl Write for Stdout {
    fn flush(&mut self) -> Result<()> {
        Ok(())
    }
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        let mut stdout = unsafe { crate::fs::File::from_raw_fd(1) };
        let res = stdout.write(buf);
        stdout.into_raw_fd();
        res
    }
}

impl Write for Stderr {
    fn flush(&mut self) -> Result<()> {
        Ok(())
    }
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        let mut stderr = unsafe { crate::fs::File::from_raw_fd(2) };
        let res = stderr.write(buf);
        stderr.into_raw_fd();
        res
    }
}

impl Read for Stdin {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        let mut stdin = unsafe { crate::fs::File::from_raw_fd(0) };
        let res = stdin.read(buf);
        stdin.into_raw_fd();
        res
    }
}

impl Read for &[u8] {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        let count = core::cmp::min(buf.len(), self.len());
        let (copy, remain) = self.split_at(count);
        buf[..count].copy_from_slice(copy);
        *self = remain;
        Ok(count)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn bufreader() {
        let v = [0, 1, 2, 3, 4, 5, 6];
        let mut buf = BufReader::new(&v[..]);
        let mut tmp = vec![];
        buf.read_until(3, &mut tmp).expect("Ok");
        assert_eq!(tmp, &[0, 1, 2, 3]);
        tmp.clear();

        buf.read_until(5, &mut tmp).expect("Ok");
        assert_eq!(tmp, &[4, 5]);
        tmp.clear();

        buf.read_until(5, &mut tmp).expect("Ok");
        assert_eq!(tmp, &[6]);
        tmp.clear();

        buf.read_until(5, &mut tmp).expect("Ok");
        assert_eq!(tmp, &[]);
        tmp.clear()
    }

    #[test]
    fn bufreader2() {
        let v: Vec<_> = (0..=255).cycle().take(10000).collect();
        let mut buf = BufReader::new(&v[..]);

        let mut old_len = 0;
        loop {
            let res = buf.fill_buf().expect("Ok");
            assert_eq!(res, &v[old_len..old_len + res.len()]);
            old_len += res.len();
            let len = res.len();
            if res.is_empty() {
                break;
            }
            buf.consume(len);
        }
    }
}
