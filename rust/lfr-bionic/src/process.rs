/* Copyright (c) 2022 RunSafe Security Inc. */
use crate::ffi::{OsStr, OsStrExt, OsString};
use crate::prelude::*;
extern crate alloc;
use crate::posix::{Posix, OS};
use alloc::borrow::ToOwned;
use alloc::collections::BTreeMap;
use alloc::ffi::CString;

#[derive(Debug)]
pub struct Command {
    process: OsString,
    clear_env: bool,
    args: Vec<OsString>,
    envs: BTreeMap<OsString, Option<OsString>>,
}

impl Command {
    pub fn new<S>(process: S) -> Self
    where
        S: AsRef<OsStr>,
    {
        Self {
            process: process.as_ref().to_os_str(),
            clear_env: false,
            args: vec![process.as_ref().to_os_str()],
            envs: BTreeMap::new(),
        }
    }
    pub fn arg<S1>(&mut self, val: S1) -> &mut Self
    where
        S1: AsRef<OsStr>,
    {
        self.args.push(val.as_ref().to_owned());
        self
    }
    pub fn args<I, S>(&mut self, val: I) -> &mut Self
    where
        I: IntoIterator<Item = S>,
        S: AsRef<OsStr>,
    {
        self.args
            .extend(val.into_iter().map(|s| s.as_ref().to_owned()));
        self
    }

    pub fn env<S1, S2>(&mut self, name: S1, val: S2) -> &mut Self
    where
        S1: AsRef<OsStr>,
        S2: AsRef<OsStr>,
    {
        let name = name.as_ref().to_owned();
        self.envs.insert(name, Some(val.as_ref().to_owned()));
        self
    }

    pub fn envs<I, S, S2>(&mut self, val: I) -> &mut Self
    where
        I: IntoIterator<Item = (S, S2)>,
        S: AsRef<OsStr>,
        S2: AsRef<OsStr>,
    {
        self.envs.extend(
            val.into_iter()
                .map(|(s, s2)| (s.as_ref().to_owned(), Some(s2.as_ref().to_owned()))),
        );
        self
    }

    pub fn env_remove<S1>(&mut self, name: S1) -> &mut Self
    where
        S1: AsRef<OsStr>,
    {
        let name = name.as_ref().to_owned();
        self.envs.insert(name, None);
        self
    }

    pub fn env_clear(&mut self) -> &mut Self {
        self.clear_env = true;
        self
    }

    pub fn spawn(&mut self) -> crate::io::Result<Child> {
        fn no_null<'a, S>(s: &'a S) -> crate::io::Result<&'a OsStr>
        where
            S: AsRef<OsStr> + 'a,
        {
            if s.as_ref().as_bytes().iter().any(|&x| x == b'\0') {
                return Err(crate::io::Error::InputContainsNull);
            }
            Ok(s.as_ref())
        }
        fn cstr_array(len: usize, arr: &[u8]) -> Vec<*const u8> {
            core::iter::once(0)
                .chain(
                    arr.iter()
                        .enumerate()
                        .filter(|(_, &c)| c == b'\0')
                        .map(|(p, _)| p + 1),
                )
                .take(len)
                .flat_map(|i| Some(arr[i..].as_ptr()))
                .chain(core::iter::once(core::ptr::null()))
                .collect()
        }
        use crate::ffi::OsStringExt;
        use crate::io::Write;
        let process = CString::new(self.process.as_bytes()).unwrap();
        let mut args_buf = Vec::new();
        for a in &self.args {
            args_buf.write_all(no_null(a)?.as_bytes())?;
            args_buf.write_all(&[b'\0'])?;
        }
        let args_ptrs = cstr_array(self.args.len(), &args_buf);
        use alloc::borrow::Cow;
        let new_env = if self.clear_env {
            Cow::Borrowed(&self.envs)
        } else {
            Cow::Owned(
                #[allow(deprecated)]
                crate::env::vars_os()
                    .map(|(n, v)| (n, Some(v)))
                    .chain(self.envs.clone())
                    .collect(),
            )
        };
        let mut env_buf = Vec::new();
        let mut env_len = 0;

        for (n, v) in new_env.iter().flat_map(|(n, v)| v.as_ref().map(|v| (n, v))) {
            env_buf.write_all(no_null(n)?.as_bytes())?;
            env_buf.write_all(&[b'='])?;
            env_buf.write_all(no_null(v)?.as_bytes())?;
            env_buf.write_all(&[b'\0'])?;
            env_len += 1;
        }
        let env_ptrs = cstr_array(env_len, &env_buf);
        let ret = Posix::retval_is_err(unsafe {
            bionic_sys::libc_vfork_and_exec(
                process.as_ptr() as _,
                args_ptrs.as_ptr() as _,
                env_ptrs.as_ptr() as _,
            )
        });
        match ret {
            Ok(0) => {
                unreachable!("Only child forked process should enter here");
            }
            Ok(pid) => Ok(Child { pid }),
            Err(_) => Err(crate::io::Error::Unknown),
        }
    }
}

pub struct ExitStatus {
    status: i32,
}

impl ExitStatus {
    pub fn code(&self) -> Option<i32> {
        Some(self.status)
    }
    pub fn success(&self) -> bool {
        self.status == 0
    }
}

pub struct Child {
    pid: bionic_sys::pid_t,
}

impl Child {
    pub fn wait(&mut self) -> crate::io::Result<ExitStatus> {
        let mut status = core::mem::MaybeUninit::uninit();
        let options = 0;
        loop {
            let ret = Posix::retval_is_err(unsafe {
                bionic_sys::wait4(
                    self.pid,
                    status.as_mut_ptr(),
                    options,
                    core::ptr::null_mut(),
                )
            });
            match ret {
                Ok(_) => {
                    let status = unsafe { status.assume_init() };
                    assert!(libc::WIFEXITED(status));
                    return Ok(ExitStatus {
                        status: libc::WEXITSTATUS(status),
                    });
                }
                Err(libc::EINTR) => { /*still waiting */ }
                Err(ret) => {
                    return Err(crate::io::Error::Code(ret.into()));
                }
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn spawn_mvp() {
        let envs = [("FOO", "BAR")];

        Command::new("/usr/bin/env")
            .env_clear()
            .envs(envs)
            .spawn()
            .unwrap();
    }
    #[test]
    #[should_panic]
    fn spawn_fail() {
        let envs = [("FOO\x00", "BAR")];
        Command::new("/usr/bin/env")
            .env_clear()
            .envs(envs)
            .spawn()
            .unwrap();
    }
    #[test]
    #[should_panic]
    fn spawn_fail2() {
        let envs = [("FOO", "BAR\x00")];

        Command::new("/usr/bin/env")
            .env_clear()
            .envs(envs)
            .spawn()
            .unwrap();
    }
    #[test]
    #[should_panic]
    fn spawn_fail3() {
        let args = ["FOO", "BAR\x00"];

        Command::new("/usr/bin/env")
            .env_clear()
            .args(args)
            .spawn()
            .unwrap();
    }
}
