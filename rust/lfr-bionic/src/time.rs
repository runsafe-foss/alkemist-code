/* Copyright (c) 2022 RunSafe Security Inc. */
use core::mem::MaybeUninit;
use core::time::Duration;

#[derive(Copy, Clone)]
pub struct Instant {
    time: bionic_sys::timespec,
}

pub const UNIX_EPOCH: Instant = Instant {
    time: bionic_sys::timespec {
        tv_sec: 0,
        tv_nsec: 0,
    },
};

impl Instant {
    pub fn now() -> Self {
        let mut ts = MaybeUninit::uninit();
        let res = unsafe { bionic_sys::libc_clock_gettime(ts.as_mut_ptr()) };
        if res == -1 {
            panic!("Unable to get clock value");
        }
        let ts = unsafe { ts.assume_init() };

        Instant { time: ts }
    }
    pub fn elapsed(&self) -> Duration {
        let now = Instant::now();
        now - *self
    }
}

impl core::ops::Sub<Instant> for Instant {
    type Output = Duration;
    fn sub(self, other: Instant) -> Duration {
        // We do saturating subs and will not panic if the `other` instant is newer.
        let end = Duration::from_secs(self.time.tv_sec.try_into().unwrap())
            + Duration::from_nanos(self.time.tv_nsec.try_into().unwrap());
        let start = Duration::from_secs(other.time.tv_sec.try_into().unwrap())
            + Duration::from_nanos(other.time.tv_nsec.try_into().unwrap());

        end.saturating_sub(start)
    }
}
