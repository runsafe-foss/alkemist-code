// Copyright (c) 2022 RunSafe Security Inc.

use anyhow::{bail, Result};
use core::ops::{Deref, DerefMut, Drop};

extern crate alloc;
use crate::alloc::MmapAllocator;
use alloc::vec::Vec;

/// The wrapper is only constructable when the memory is opened by [`HiddenMem`]
/// and will be hidden again when this object is dropped.
pub struct VisibleMem<'a, T> {
    wrapped: &'a mut HiddenMem<T>,
}

pub struct VisibleMemMut<'a, T> {
    wrapped: &'a mut HiddenMem<T>,
}

/// Hides memory
impl<'a, T> Drop for VisibleMem<'a, T> {
    fn drop(&mut self) {
        unsafe { self.wrapped.protect_mem() };
    }
}

impl<'a, T> Drop for VisibleMemMut<'a, T> {
    fn drop(&mut self) {
        unsafe { self.wrapped.protect_mem() };
    }
}

impl<'a, T> Deref for VisibleMemMut<'a, T> {
    type Target = Vec<T, MmapAllocator>;
    fn deref(&self) -> &Self::Target {
        &self.wrapped.mem
    }
}

impl<'a, T> Deref for VisibleMem<'a, T> {
    type Target = Vec<T, MmapAllocator>;
    fn deref(&self) -> &Self::Target {
        &self.wrapped.mem
    }
}

impl<'a, T> DerefMut for VisibleMemMut<'a, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.wrapped.mem
    }
}

pub struct HiddenMem<T> {
    mem: alloc::vec::Vec<T, MmapAllocator>,
}

impl<T> Default for HiddenMem<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T> HiddenMem<T> {
    pub fn new() -> Self {
        Self {
            mem: Vec::new_in(MmapAllocator),
        }
    }
    unsafe fn unprotect_mem(&mut self) -> Result<()> {
        if self.mem.capacity() == 0 {
            return Ok(());
        }
        let prot = bionic_sys::PROT_READ | bionic_sys::PROT_WRITE;
        if bionic_sys::mprotect(
            self.mem.as_mut_ptr() as *mut _,
            self.mem.capacity(),
            prot.try_into().unwrap(),
        ) != 0
        {
            bail!("unable to view hidden memory");
        }
        Ok(())
    }

    unsafe fn protect_mem(&mut self) {
        if self.mem.capacity() == 0 {
            return;
        }
        let prot = bionic_sys::PROT_NONE;
        if bionic_sys::mprotect(
            self.mem.as_mut_ptr() as *mut _,
            self.mem.capacity(),
            prot.try_into().unwrap(),
        ) != 0
        {
            panic!("unable to protect mem")
        }
    }

    /// Gives an object the keeps the visibility of the memory only open until it is dropped.
    pub fn view(&mut self) -> Result<VisibleMem<'_, T>> {
        unsafe { self.unprotect_mem()? };
        Ok(VisibleMem { wrapped: self })
    }
    pub fn view_mut(&mut self) -> Result<VisibleMemMut<'_, T>> {
        unsafe { self.unprotect_mem()? };
        Ok(VisibleMemMut { wrapped: self })
    }

    pub fn mem_ptr(&self) -> (*mut T, usize) {
        let ptr = if self.mem.capacity() == 0 {
            core::ptr::null_mut()
        } else {
            self.mem.as_ptr() as *mut _
        };

        (ptr, self.mem.len())
    }
}
