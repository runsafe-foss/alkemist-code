/* Copyright (c) 2022 RunSafe Security Inc. */
// Functions relating to safe wrapper around `dl_iterate_phdr` and `auxv`
use core::ffi::CStr;
/// `dl_iterate_phdr` built based on the example found at [Rust Closures in FFI](https://adventures.michaelfbryan.com/posts/rust-closures-in-ffi/)
use core::ops::Range;
use core::ptr::NonNull;

type DlIteratePhdrCallback =
    unsafe extern "C" fn(*mut libc::dl_phdr_info, libc::size_t, *mut libc::c_void) -> libc::c_int;

type DlIteratePhdr = unsafe extern "C" fn(
    callback: *mut DlIteratePhdrCallback,
    data: *mut libc::c_void,
) -> libc::c_int;

cfg_if::cfg_if! {
    if #[cfg(target_os="android")] {
        // This is due to the different way that glibc and bionic handle dl_iterate_phdr
        // See LFR!222 for more detailed information.
        #[link(name = "dl")]
        extern "C" {
            #[link_name = "dl_iterate_phdr"]
            fn dl_iterate_phdr_extern (
                callback: *mut DlIteratePhdrCallback,
                data: *mut libc::c_void,
            ) -> libc::c_int;
        }
        fn get_dl_iterate_phdr() -> Option<DlIteratePhdr> {
            Some(dl_iterate_phdr_extern)
        }
    } else {
        extern "C" {
            #[linkage = "extern_weak"]
            #[link_name = "dl_iterate_phdr"]
            static dl_iterate_phdr_ptr: *mut DlIteratePhdr;
        }
        fn get_dl_iterate_phdr() -> Option<DlIteratePhdr> {
            unsafe { core::mem::transmute(dl_iterate_phdr_ptr) }
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum AuxvEntry {
    Phdr(usize),
    PhEnt(usize),
    PhNum(usize),
    Other(usize),
}
#[repr(C)]
#[derive(Debug)]
struct AuxvC {
    typ: usize,
    val: usize,
}
impl AuxvEntry {
    /// Converts [`AuxvC`] entries to rust friendly ones.
    fn new(typ: usize, val: usize) -> AuxvEntry {
        match typ {
            3 => Self::Phdr(val),
            4 => Self::PhEnt(val),
            5 => Self::PhNum(val),
            _ => Self::Other(typ),
        }
    }
}
/// A useful abstraction over the Auxiliary Vector
/// Use it to iterate over the entries.
pub struct Auxv {
    start: *const AuxvC,
}

impl Iterator for Auxv {
    type Item = AuxvEntry;
    fn next(&mut self) -> Option<Self::Item> {
        if unsafe { (*self.start).typ } == 0 {
            None
        } else {
            let entry = AuxvEntry::new(unsafe { (*self.start).typ }, unsafe { (*self.start).val });
            self.start = unsafe { self.start.add(1) };
            Some(entry)
        }
    }
}

impl Auxv {
    pub fn from_args(args: NonNull<libc::uintptr_t>) -> Option<Self> {
        Self::find(args)
    }
    /// Finds the Auxiliary vector in the same fashion as RandoLib.
    /// ```text
    /// [argc, args_1... args_n, env_1 ... env_n, NULL, auxv ... auxv_n]
    /// ```
    /// Has some useful information in the event that we are unable to access the
    /// dynamic library phdr information. Additionally we need to know if the
    /// binary is static, so we don't inadvertently use the `dl_iterate_phdr`
    fn find(args: NonNull<libc::uintptr_t>) -> Option<Self> {
        let args = args.as_ptr();
        let auxv = unsafe {
            let argc = *args;
            let mut env = args
                .add(1) //argc
                .add(argc)
                .add(1) //null
                ;
            while *env != 0 {
                env = env.add(1);
            }
            env.add(1)
        };
        Some(Self {
            start: auxv as *const _,
        })
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum SegmentType {
    Null,
    Load,
    Dynamic,
    Interp,
    Note,
    ShLib,
    Phdr,
    EhFrame,
    GnuRelRo,
    ArmExIdx,
    Other(u64),
}

impl From<u64> for SegmentType {
    fn from(st: u64) -> Self {
        match st {
            0 => SegmentType::Null,
            1 => SegmentType::Load,
            2 => SegmentType::Dynamic,
            3 => SegmentType::Interp,
            4 => SegmentType::Note,
            5 => SegmentType::ShLib,
            6 => SegmentType::Phdr,
            0x6474e550 => SegmentType::EhFrame,
            0x6474E552 => SegmentType::GnuRelRo,
            0x70000001 => SegmentType::ArmExIdx,
            e => SegmentType::Other(e),
        }
    }
}

impl From<u32> for SegmentType {
    fn from(st: u32) -> Self {
        SegmentType::from(u64::from(st))
    }
}

/// The Trampoline that will be provided as a callback to `dl_iterate_phdr`.
/// It takes the closure that was provided as user data and converts it back
/// into a rust closure and executes it.
unsafe extern "C" fn trampoline<'a, F>(
    info: *mut libc::dl_phdr_info,
    _size: libc::size_t,
    data: *mut libc::c_void,
) -> libc::c_int
where
    F: FnMut(&PhdrInfo<'a>) -> bool,
{
    let user_data = &mut *(data as *mut F);
    info.as_ref()
        .map(|info| user_data(&info.into()))
        .unwrap_or(false)
        .into()
}

/// Gets a trampoline with a given type to ensure type safety.
fn get_trampoline<'a, F>(_closure: &F) -> DlIteratePhdrCallback
where
    F: FnMut(&PhdrInfo<'a>) -> bool,
{
    trampoline::<'a, F>
}

/// Call `dl_iterate_phdr` safely with a given closure.
pub fn dl_iterate_phdr<'a, F>(mut callback: F)
where
    F: FnMut(&PhdrInfo<'a>) -> bool,
{
    let trampoline = get_trampoline(&callback);
    if let Some(dl_iterate_phdr) = get_dl_iterate_phdr() {
        unsafe {
            dl_iterate_phdr(
                trampoline as *mut _,
                &mut callback as *mut _ as *mut libc::c_void,
            );
        }
    }
}

#[cfg(target_pointer_width = "32")]
pub type ElfPhdr = libc::Elf32_Phdr;
#[cfg(target_pointer_width = "64")]
pub type ElfPhdr = libc::Elf64_Phdr;

/// Safe wrapper for a few fields from [`libc::dl_phdr_info`]
#[derive(Clone)]
pub struct PhdrInfo<'a> {
    pub name: &'a CStr,
    pub base: usize,
    pub hdrs: &'a [ElfPhdr],
}

impl core::fmt::Debug for PhdrInfo<'_> {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> Result<(), core::fmt::Error> {
        f.debug_struct("PhdrInfo")
            .field("name", &self.name)
            .field("base", &self.base)
            .field("hdrs_count", &(self.hdrs.len()))
            .finish()
    }
}

impl<'a> PhdrInfo<'a> {
    pub fn iter(&self) -> PhdrIter<'a> {
        PhdrIter { hdrs: self.hdrs }
    }
}

/// Safe wrapper for [`libc::Elf32_Phdr`] or [`libc::Elf64_Phdr`]
#[derive(Debug, Copy, Clone)]
pub struct Phdr {
    pub segment_type: SegmentType,
    pub offset: usize,
    pub vaddr: usize,
    pub memsz: usize,
    pub filesz: usize,
    pub flags: u32,
}
#[derive(Copy, Clone)]
pub struct Phdrs<'a> {
    hdrs: &'a [ElfPhdr],
}

impl<'a> Phdrs<'a> {
    pub fn new(hdrs: &'a [ElfPhdr]) -> Self {
        Self { hdrs }
    }
    pub fn start(&self) -> usize {
        self.hdrs.as_ptr() as usize
    }
    pub fn iter(&self) -> PhdrIter<'_> {
        PhdrIter { hdrs: self.hdrs }
    }
    pub fn to_info(&self, name: &'a CStr, base: usize) -> PhdrInfo<'a> {
        PhdrInfo {
            name,
            base,
            hdrs: self.hdrs,
        }
    }
}

/// Iterator for the `Phdrs` of the `Phdr_info` which will convert
/// 32 or 64 to a shared representation.
#[derive(Clone)]
pub struct PhdrIter<'a> {
    hdrs: &'a [ElfPhdr],
}

impl<'a> PhdrIter<'a> {}

trait PhdrData {
    fn vaddr(&self) -> usize;
    fn memsz(&self) -> usize;
    fn filesz(&self) -> usize;
    fn segment_type(&self) -> SegmentType;
    fn flags(&self) -> u32;
    fn offset(&self) -> usize;
}

impl PhdrData for libc::Elf64_Phdr {
    fn vaddr(&self) -> usize {
        self.p_vaddr as usize
    }
    fn memsz(&self) -> usize {
        self.p_memsz as usize
    }
    fn filesz(&self) -> usize {
        self.p_filesz as usize
    }
    fn segment_type(&self) -> SegmentType {
        self.p_type.into()
    }
    fn flags(&self) -> u32 {
        self.p_flags
    }

    fn offset(&self) -> usize {
        self.p_offset as usize
    }
}

impl PhdrData for libc::Elf32_Phdr {
    fn vaddr(&self) -> usize {
        self.p_vaddr as usize
    }
    fn memsz(&self) -> usize {
        self.p_memsz as usize
    }
    fn filesz(&self) -> usize {
        self.p_filesz as usize
    }
    fn segment_type(&self) -> SegmentType {
        self.p_type.into()
    }
    fn flags(&self) -> u32 {
        self.p_flags
    }
    fn offset(&self) -> usize {
        self.p_offset as usize
    }
}

impl<'a> Iterator for PhdrIter<'a> {
    type Item = Phdr;

    fn next(&mut self) -> Option<Self::Item> {
        let first = self.hdrs.first()?;
        self.hdrs = &self.hdrs[1..];
        Some(Self::Item {
            vaddr: first.vaddr(),
            offset: first.offset(),
            memsz: first.memsz(),
            segment_type: first.segment_type(),
            flags: first.flags(),
            filesz: first.filesz(),
        })
    }
}

impl From<&libc::dl_phdr_info> for PhdrInfo<'_> {
    fn from(info: &libc::dl_phdr_info) -> Self {
        let name = unsafe { CStr::from_ptr(info.dlpi_name) };
        #[allow(clippy::useless_conversion)] // into varies based on 32/64bit
        let base = info.dlpi_addr as usize;
        let hdrs = unsafe { core::slice::from_raw_parts(info.dlpi_phdr, info.dlpi_phnum.into()) };
        Self { name, base, hdrs }
    }
}

#[repr(C)]
pub struct RustModuleInfo {
    pub args: Option<NonNull<libc::uintptr_t>>,
    pub dynamic: Option<NonNull<libc::uintptr_t>>,
}

impl RustModuleInfo {
    fn get_phdrs(&self) -> Option<PhdrInfo<'_>> {
        let auxv = Auxv::find(self.args?)?;
        let mut phdr = None;
        let mut phnum = None;
        auxv.for_each(|e| match e {
            AuxvEntry::Phdr(phdr_in) => phdr = Some(phdr_in),
            AuxvEntry::PhNum(phnum_in) => phnum = Some(phnum_in),
            _ => {}
        });
        let phdr_start = phdr?;
        let phnum = phnum?;

        let phdrs = PhdrInfo {
            name: unsafe { CStr::from_ptr("\0".as_ptr().cast()) },
            base: 0,
            hdrs: unsafe { core::slice::from_raw_parts(phdr_start as *const _, phnum) },
        };
        Some(phdrs)
    }
    fn is_static(&self) -> bool {
        if let Some(phdrs) = self.get_phdrs() {
            !phdrs
                .iter()
                .any(|phdr| phdr.segment_type == SegmentType::Dynamic)
        } else {
            false
        }
    }

    /// Looks through the `dl_iterate_phdr` info to find a given target address
    /// and returns the entire section that it was found in.
    /// This is useful because many rust methods require a known fixed size range
    /// of values to work on, so we need ot make sure that when reading a section
    /// we have a valid region of memory.
    fn find_segment(&self, start: usize) -> Option<Range<usize>> {
        let phdrs = self.get_phdrs()?;

        let image_base = phdrs
            .iter()
            .find_map(|phdr| match phdr.segment_type {
                SegmentType::Phdr => Some(phdrs.hdrs.as_ptr() as usize - phdr.vaddr),
                SegmentType::Dynamic => {
                    Some(self.dynamic.map(|p| p.as_ptr() as usize).unwrap_or(0) - phdr.vaddr)
                }
                _ => None,
            })
            .unwrap_or(0);

        phdrs
            .iter()
            .map(|phdr| {
                let base = image_base + phdr.vaddr;
                (phdr, base..(base + phdr.memsz))
            })
            .find(|(_phdr, range)| range.contains(&start))
            .map(|(_phdr, range)| range)
    }

    /// Finds a given section and reduces the range to match the target
    /// starting address.
    fn find_segment_trim(&self, start: usize) -> Option<Range<usize>> {
        self.find_segment(start).map(|range| start..range.end)
    }
}

pub fn find_segment_trim(
    module_info: Option<&RustModuleInfo>,
    start: usize,
) -> Option<Range<usize>> {
    let found = module_info.and_then(|mod_info| {
        mod_info
            .find_segment_trim(start)
            .map(|range| start..range.end)
    });
    if found.is_some() {
        return found;
    }
    let is_static = module_info
        .map(|mod_info| mod_info.is_static())
        .unwrap_or(false);
    if !is_static {
        let mut target_size = None;

        dl_iterate_phdr(|info: &PhdrInfo| {
            let image_base = if cfg!(target_os = "android") && info.base == 0 {
                // https://android-review.googlesource.com/c/toolchain/gcc/+/711193
                info.iter()
                    .filter(|phdr| phdr.segment_type == SegmentType::Load)
                    .map(|phdr| info.hdrs.as_ptr() as usize - phdr.vaddr)
                    .next()
                    .unwrap_or(0)
            } else {
                info.base
            };
            let found = info
                .iter()
                .map(|phdr| {
                    let base = image_base + phdr.vaddr;
                    (phdr, base..(base + phdr.memsz))
                })
                .find(|(_phdr, range)| range.contains(&start))
                .map(|(_phdr, range)| target_size = Some(start..range.end));
            found.is_some()
        });
        target_size
    } else {
        None
    }
}

#[cfg(test)]
mod dl_iterate_test {
    use super::*;
    #[test]
    fn test_dl_iterate_phdr() {
        let mut count = 0;
        dl_iterate_phdr(|info| {
            println!("{:?}", info.name);
            count += 1;
            false
        });

        assert!(count > 0);
        let mut count_phdr = 0;
        dl_iterate_phdr(|info| {
            println!("Name : {:?}", info.name);
            info.iter().for_each(|phdr| {
                println!("{:x?}", phdr.vaddr);
                println!("{:x?}", phdr.memsz);
                count_phdr += 1;
            });
            false
        });

        assert!(count_phdr > 0);
    }

    #[test]
    fn auxvs() {
        #[repr(C)]
        struct Args {
            args: [libc::uintptr_t; 8],
            auxv: [AuxvC; 4],
        }
        let args: [libc::uintptr_t; 8] = [
            2,                                       // 2 args
            "hello\0".as_ptr() as *const _ as usize, // arg1
            "world\0".as_ptr() as *const _ as usize, // arg2
            0,                                       //end argv
            "env1\0".as_ptr() as *const _ as usize,  // env
            "env2\0".as_ptr() as *const _ as usize,  // env
            "env3\0".as_ptr() as *const _ as usize,  // env
            0,                                       //end env
        ];
        let auxv = [
            AuxvC { typ: 3, val: 99 },
            AuxvC { typ: 4, val: 88 },
            AuxvC { typ: 5, val: 77 },
            AuxvC { typ: 0, val: 0 },
        ];
        let args = Args { args, auxv };

        let auxv =
            Auxv::find(NonNull::new(&args as *const _ as *mut _).unwrap()).expect("expected auxv");

        let auxv_vec = auxv.collect::<Vec<_>>();
        println!("{:?}", auxv_vec);
        assert_eq!(auxv_vec[0], AuxvEntry::Phdr(99));
        assert_eq!(auxv_vec[1], AuxvEntry::PhEnt(88));
        assert_eq!(auxv_vec[2], AuxvEntry::PhNum(77));
    }
}
