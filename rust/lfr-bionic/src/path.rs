// Copyright (c) 2022 RunSafe Security Inc.

extern crate alloc;
use alloc::string::String;

use crate::ffi::{OsStr, OsStrExt};
use crate::ffi::{OsString, OsStringExt};
use alloc::vec::Vec;
use core::ops::Deref;
use serde::{Deserialize, Serialize};

#[derive(Clone, Eq, PartialEq, Deserialize, Serialize)]
#[serde(from = "String")]
pub struct PathBuf {
    path: Vec<u8>,
}

impl PathBuf {
    pub fn into_os_string(self) -> OsString {
        OsString::from_bytes(self.path)
    }
    pub fn push<P>(&mut self, path: P)
    where
        P: AsRef<Path>,
    {
        if path.as_ref().is_absolute() {
            self.path.clear();
        } else if !self.path.is_empty() && self.path.iter().last() != Some(&b'/') {
            self.path.push(b'/');
        }
        self.path.extend(path.as_ref().inner.iter());
    }
}

impl alloc::fmt::Debug for PathBuf {
    fn fmt(&self, f: &mut alloc::fmt::Formatter<'_>) -> alloc::fmt::Result {
        alloc::fmt::Debug::fmt(self.as_ref(), f)
    }
}

impl alloc::fmt::Display for PathBuf {
    fn fmt(&self, f: &mut alloc::fmt::Formatter<'_>) -> alloc::fmt::Result {
        alloc::fmt::Display::fmt(self.as_ref(), f)
    }
}
impl AsRef<Path> for String {
    fn as_ref(&self) -> &Path {
        unsafe { core::mem::transmute(self.as_bytes()) }
    }
}

impl AsRef<Path> for str {
    fn as_ref(&self) -> &Path {
        unsafe { core::mem::transmute(self) }
    }
}

impl AsRef<Path> for [u8] {
    fn as_ref(&self) -> &Path {
        unsafe { core::mem::transmute(self) }
    }
}

impl AsRef<Path> for PathBuf {
    fn as_ref(&self) -> &Path {
        unsafe { core::mem::transmute(self.path.as_slice()) }
    }
}

impl AsRef<crate::ffi::OsStr> for &Path {
    fn as_ref(&self) -> &crate::ffi::OsStr {
        unsafe { core::mem::transmute(&self.inner) }
    }
}

impl AsRef<Path> for &Path {
    fn as_ref(&self) -> &Path {
        unsafe { core::mem::transmute(&self.inner) }
    }
}

impl Deref for PathBuf {
    type Target = Path;
    fn deref(&self) -> &Self::Target {
        self.as_ref()
    }
}

#[derive(Eq, PartialEq)]
pub struct Path {
    inner: [u8],
}

impl alloc::fmt::Debug for Path {
    fn fmt(&self, f: &mut alloc::fmt::Formatter<'_>) -> alloc::fmt::Result {
        f.debug_struct("Path")
            .field(
                "inner",
                &alloc::string::String::from_utf8_lossy(&self.inner),
            )
            .finish()
    }
}

impl alloc::fmt::Display for Path {
    fn fmt(&self, f: &mut alloc::fmt::Formatter<'_>) -> alloc::fmt::Result {
        write!(
            f,
            "{}",
            &alloc::string::String::from_utf8_lossy(&self.inner)
        )
    }
}

impl Path {
    pub fn new<P>(path: &P) -> &Path
    where
        P: AsRef<crate::ffi::OsStr> + ?Sized,
    {
        unsafe { core::mem::transmute(path.as_ref()) }
    }

    pub fn as_os_str(&self) -> &OsStr {
        OsStr::from_bytes(&self.inner)
    }

    pub fn to_path_buf(&self) -> PathBuf {
        PathBuf {
            path: self.inner.to_vec(),
        }
    }

    pub fn join<S>(&self, path: S) -> PathBuf
    where
        S: AsRef<Path>,
    {
        let mut pb = self.to_path_buf();
        pb.push(path);
        pb
    }

    pub fn is_absolute(&self) -> bool {
        self.inner.first().map(|&x| x == b'/').unwrap_or(false)
    }
}

impl alloc::borrow::Borrow<Path> for PathBuf {
    fn borrow(&self) -> &Path {
        self.as_ref()
    }
}

impl alloc::borrow::ToOwned for Path {
    type Owned = PathBuf;

    // Required method
    fn to_owned(&self) -> Self::Owned {
        self.to_path_buf()
    }
}

impl From<String> for PathBuf {
    fn from(s: String) -> PathBuf {
        PathBuf {
            path: s.into_bytes(),
        }
    }
}
