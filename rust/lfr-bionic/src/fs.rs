// Copyright (c) 2022 RunSafe Security Inc.

extern crate alloc;

use crate::ffi::OsStrExt;
use crate::io::Error as BionicIoError;
use crate::io::{AsFd, AsRawFd, BorrowedFd, FromRawFd, IntoRawFd, OwnedFd, Read, Write};
use crate::path::Path;
use crate::posix::{Posix, OS};
use alloc::ffi::CString;
use alloc::string::String;
use core::mem::MaybeUninit;

pub type RawFd = libc::c_int;

pub struct File {
    fd: OwnedFd,
}

pub struct OpenOptions {
    read: bool,
    write: bool,
    create: bool,
    truncate: bool,
    append: bool,
    path: bool,
    sync: bool,
    mode: u32,
}

impl Default for OpenOptions {
    fn default() -> Self {
        Self {
            read: false,
            write: false,
            create: false,
            truncate: false,
            append: false,
            path: false,
            sync: false,
            mode: 0o660,
        }
    }
}

impl OpenOptions {
    pub fn sync(&mut self, set: bool) -> &mut Self {
        self.sync = set;
        self
    }
    pub fn path(&mut self, set: bool) -> &mut Self {
        self.path = set;
        self
    }
    pub fn create(&mut self, set: bool) -> &mut Self {
        self.create = set;
        self
    }
    pub fn write(&mut self, set: bool) -> &mut Self {
        self.write = set;
        self
    }
    pub fn read(&mut self, set: bool) -> &mut Self {
        self.read = set;
        self
    }
    pub fn append(&mut self, set: bool) -> &mut Self {
        self.append = set;
        self
    }
    pub fn truncate(&mut self, set: bool) -> &mut Self {
        self.truncate = set;
        self
    }
    pub fn mode(&mut self, set: u32) -> &mut Self {
        self.mode = set;
        self
    }

    pub fn open<P>(&mut self, path: P) -> Result<File, BionicIoError>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        let path =
            CString::new(path.as_os_str().as_bytes()).map_err(|_| BionicIoError::BadCString)?;

        let mut flags = 0;
        if self.path {
            flags |= libc::O_PATH;
        } else {
            if self.create {
                flags |= libc::O_CREAT;
            }
            if self.read && self.write {
                flags |= libc::O_RDWR;
            } else if self.read {
                flags |= libc::O_RDONLY;
            } else if self.write {
                flags |= libc::O_WRONLY;
            }
            if self.truncate {
                flags |= libc::O_TRUNC;
            }
            if self.sync {
                flags |= libc::O_SYNC;
            }
            if self.append {
                flags |= libc::O_APPEND;
            }
        }

        let res = Posix::retval_is_err(unsafe {
            bionic_sys::open(path.as_ptr() as *mut _, flags, self.mode)
        });
        match res {
            Ok(fd) => Ok(File {
                fd: unsafe { OwnedFd::from_raw_fd(fd) },
            }),
            Err(_) => Err(BionicIoError::InvalidFile),
        }
    }
}

impl FromRawFd for File {
    unsafe fn from_raw_fd(fd: RawFd) -> Self {
        Self {
            fd: OwnedFd::from_raw_fd(fd),
        }
    }
}

impl From<File> for OwnedFd {
    fn from(f: File) -> OwnedFd {
        f.fd
    }
}

impl From<OwnedFd> for File {
    fn from(fd: OwnedFd) -> File {
        File { fd }
    }
}

impl IntoRawFd for File {
    fn into_raw_fd(self) -> RawFd {
        self.fd.into_raw_fd()
    }
}
impl AsRawFd for File {
    fn as_raw_fd(&self) -> RawFd {
        self.fd.as_raw_fd()
    }
}

impl AsFd for File {
    fn as_fd(&self) -> BorrowedFd<'_> {
        self.fd.as_fd()
    }
}

impl File {
    pub fn open<P>(path: P) -> Result<Self, BionicIoError>
    where
        P: AsRef<Path>,
    {
        OpenOptions::default().read(true).open(path)
    }

    pub fn create<P>(path: P) -> Result<Self, BionicIoError>
    where
        P: AsRef<Path>,
    {
        OpenOptions::default()
            .create(true)
            .write(true)
            .truncate(true)
            .open(path)
    }
    pub fn read(&mut self, buf: &mut [u8]) -> Result<usize, BionicIoError> {
        let res = Posix::retval_is_err(unsafe {
            bionic_sys::read(self.fd.as_raw_fd(), buf.as_mut_ptr() as *mut _, buf.len())
        });
        match res {
            Ok(res) => Ok(res.try_into().unwrap()),
            Err(libc::EAGAIN) => Err(BionicIoError::Again),
            Err(libc::EINTR) => Err(BionicIoError::Interrupted),
            Err(err) => Err(BionicIoError::Code(err.into())),
        }
    }

    pub fn metadata(&self) -> Metadata {
        Metadata::from_fd(self.as_fd()).expect("Valid File Descriptor")
    }

    pub fn set_len(&self, size: u64) -> Result<(), BionicIoError> {
        match Posix::retval_is_err(unsafe {
            bionic_sys::ftruncate64(self.fd.as_raw_fd(), size as i64)
        }) {
            Ok(0) => Ok(()),
            _ => Err(BionicIoError::Unknown),
        }
    }
}

impl Write for File {
    fn flush(&mut self) -> crate::io::Result<()> {
        Ok(())
    }
    fn write(&mut self, buf: &[u8]) -> crate::io::Result<usize> {
        let res = Posix::retval_is_err(unsafe {
            bionic_sys::write(self.fd.as_raw_fd(), buf.as_ptr() as *mut _, buf.len())
        });
        match res {
            Ok(bytes) => Ok(bytes.try_into().unwrap()),
            Err(libc::EAGAIN) => Err(BionicIoError::Again),
            Err(libc::EINTR) => Err(BionicIoError::Interrupted),
            Err(err) => Err(BionicIoError::Code(err.into())),
        }
    }
}

impl Read for File {
    fn read(&mut self, buf: &mut [u8]) -> crate::io::Result<usize> {
        let res = Posix::retval_is_err(unsafe {
            bionic_sys::read(self.fd.as_raw_fd(), buf.as_mut_ptr() as *mut _, buf.len())
        });
        match res {
            Ok(bytes) => Ok(bytes.try_into().unwrap()),
            Err(libc::EAGAIN) => Err(BionicIoError::Again),
            Err(libc::EINTR) => Err(BionicIoError::Interrupted),
            Err(err) => Err(BionicIoError::Code(err.into())),
        }
    }
}

pub struct Metadata {
    stat: bionic_sys::real_stat64,
}

pub trait MetadataExt {
    fn mode(&self) -> u32;
    fn gid(&self) -> u32;
    fn uid(&self) -> u32;
}

impl MetadataExt for Metadata {
    fn mode(&self) -> u32 {
        self.stat.st_mode
    }

    fn gid(&self) -> u32 {
        self.stat.st_gid
    }

    fn uid(&self) -> u32 {
        self.stat.st_uid
    }
}

impl Metadata {
    pub fn len(&self) -> u64 {
        self.stat.st_size.try_into().unwrap()
    }
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }
    pub fn file_type(&self) -> FileType {
        if self.stat.st_mode & libc::S_IFDIR != 0 {
            FileType::Dir
        } else if self.stat.st_mode & libc::S_IFREG != 0 {
            FileType::File
        } else if self.stat.st_mode & libc::S_IFLNK != 0 {
            FileType::Symlink
        } else {
            FileType::Unknown
        }
    }
    fn from_fd(fd: BorrowedFd<'_>) -> Result<Metadata, BionicIoError> {
        let mut stat = MaybeUninit::uninit();
        let res =
            Posix::retval_is_err(unsafe { bionic_sys::fstat64(fd.as_raw_fd(), stat.as_mut_ptr()) });

        match res {
            Ok(_) => {
                let stat = unsafe { stat.assume_init() };
                Ok(Metadata { stat })
            }
            Err(libc::EACCES) => Err(BionicIoError::PathAccess),
            Err(libc::EBADF) => Err(BionicIoError::BadFileDescriptor),
            Err(libc::ENOENT) => Err(BionicIoError::DoesNotExist),
            Err(e) => Err(BionicIoError::Code(e.into())),
        }
    }
    fn from_path<P>(path: P) -> Result<Metadata, BionicIoError>
    where
        P: AsRef<Path>,
    {
        let file = OpenOptions::default().path(true).open(path)?;
        Self::from_fd(file.as_fd())
    }
}

pub fn metadata<P>(path: P) -> Result<Metadata, BionicIoError>
where
    P: AsRef<Path>,
{
    Metadata::from_path(path)
}

pub fn read_link<P: AsRef<Path>>(path: P) -> Result<crate::path::PathBuf, BionicIoError> {
    let path = CString::new(path.as_ref().as_os_str().as_bytes())
        .map_err(|_| BionicIoError::InputContainsNull)?;
    let mut buf = [0u8; 2048];
    match Posix::retval_is_err(unsafe {
        bionic_sys::readlinkat(
            0,
            path.as_ptr(),
            buf.as_mut_ptr() as *mut libc::c_char,
            buf.len(),
        )
    }) {
        Ok(b) => Ok(Path::new(&buf[..b.try_into().unwrap()]).to_path_buf()),
        Err(_) => Err(BionicIoError::Unknown),
    }
}

#[derive(PartialEq, Debug)]
pub enum FileType {
    Dir,
    File,
    Symlink,
    Unknown,
}

impl FileType {
    pub fn is_dir(&self) -> bool {
        *self == FileType::Dir
    }
    pub fn is_file(&self) -> bool {
        *self == FileType::File
    }
    pub fn is_symlink(&self) -> bool {
        *self == FileType::Symlink
    }
}

pub fn read_to_string<P: AsRef<Path>>(path: P) -> Result<String, BionicIoError> {
    let mut file = File::open(path)?;
    let mut s = String::new();
    file.read_to_string(&mut s)?;
    Ok(s)
}

pub fn read<P: AsRef<Path>>(path: P) -> Result<alloc::vec::Vec<u8>, BionicIoError> {
    let mut file = File::open(path)?;
    let mut buf = alloc::vec![];
    file.read_to_end(&mut buf)?;
    Ok(buf)
}

#[cfg(test)]
mod test {
    use super::*;
    extern crate alloc;
    use alloc::{string::String, vec};
    use tempfile::NamedTempFile;

    impl AsRef<crate::path::Path> for tempfile::TempPath {
        fn as_ref(&self) -> &crate::path::Path {
            AsRef::<std::path::Path>::as_ref(self).as_ref()
        }
    }
    impl AsRef<crate::path::Path> for std::path::Path {
        fn as_ref(&self) -> &crate::path::Path {
            unsafe { std::mem::transmute(self) }
        }
    }

    #[test]
    fn write() {
        let mut file = OpenOptions::default()
            .write(true)
            .append(true)
            .open("/dev/null")
            .expect("valid file");
        let s = "test123";
        file.write_all(s.as_bytes()).expect("wrote to /dev/null");
    }

    #[test]
    fn read() {
        let mut file = File::open("/dev/zero").expect("valid file");
        let mut buf = vec![10; 256];
        file.read_exact(&mut buf).expect("read entire buffer");
        assert_eq!(vec![0; 256], buf);
    }
    #[test]
    fn write_read() {
        let contents = "12345678910abcdefg";
        let name = NamedTempFile::new().unwrap().into_temp_path();
        {
            let mut file = File::create(&name).expect("created file");
            file.write_all(contents.as_bytes())
                .expect("wrote all contents to file");
        }
        {
            let mut file = File::open(&name).expect("opened file");
            let mut s = String::new();
            file.read_to_string(&mut s).expect("read file to string");
            assert_eq!(s, contents);
        }
    }

    #[test]
    fn macros() {
        let contents = "12345678910abcdefgALPHABET";
        let name = NamedTempFile::new().unwrap().into_temp_path();
        {
            let mut file = File::create(&name).expect("created file");
            write!(file, "{}", contents).expect("wrote all contents to file");
        }
        {
            let mut file = File::open(&name).expect("opened file");
            let mut s = String::new();
            file.read_to_string(&mut s).expect("read file to string");
            assert_eq!(s, contents);
        }
    }

    #[test]
    fn metadata() {
        let contents = "12345678910abcdefg";
        let name = NamedTempFile::new().unwrap().into_temp_path();
        {
            let mut file = File::create(&name).expect("created file");
            assert_eq!(file.metadata().len(), 0);
            write!(file, "{}", contents).expect("wrote");
            assert_eq!(file.metadata().len(), contents.len().try_into().unwrap());
        }

        let meta = super::metadata(name).expect("exists");

        assert_eq!(meta.len(), contents.len().try_into().unwrap());
        assert!(meta.file_type().is_file());

        let meta = super::metadata("/dev/").expect("exists");
        assert!(meta.file_type().is_dir());
    }
}
