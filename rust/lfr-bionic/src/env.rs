/* Copyright (c) 2022 RunSafe Security Inc. */
use crate::ffi::{OsStr, OsStrExt, OsString};
extern crate alloc;
use alloc::string::{String, ToString};
use alloc::vec::Vec;
use core::ffi::CStr;
use core::ptr::NonNull;
use once_cell::race::OnceBox;

#[derive(Debug)]
pub(crate) enum EnvironMem {
    Proc(&'static [u8]),
    Environ,
}

struct EnvEntry<'a> {
    entry: &'a CStr,
}

impl<'a> EnvEntry<'a> {
    fn split_key_value(&self) -> (&'a OsStr, &'a CStr) {
        let bytes = self.entry.to_bytes_with_nul();
        let equals = bytes.iter().position(|&b| b == b'=');
        let (key, value) = if let Some(equals) = equals {
            let (key, value) = bytes.split_at(equals);
            let (_equals_sign, value) = value.split_first().unwrap();
            (key, value)
        } else {
            bytes.split_at(bytes.len() - 1)
        };

        (
            OsStrExt::from_bytes(key),
            CStr::from_bytes_with_nul(value).unwrap(),
        )
    }
}

pub(crate) struct EnvIter<'a, 'b> {
    env: &'a EnvironMem,
    start: &'b mut usize,
}

impl<'a, 'b> EnvIter<'a, 'b> {
    pub(crate) fn new(env: &'a EnvironMem, start: &'b mut usize) -> EnvIter<'a, 'b> {
        Self { env, start }
    }
}

impl<'a, 'b> Iterator for EnvIter<'a, 'b> {
    type Item = (&'a OsStr, &'a CStr);
    fn next(&mut self) -> Option<Self::Item> {
        let entry = match self.env {
            EnvironMem::Proc(env) => {
                let null = env[*self.start..].iter().position(|&b| b == b'\0')?;
                let entry = &env[*self.start..][..=null];
                *self.start += entry.len();
                CStr::from_bytes_with_nul(entry).unwrap()
            }
            EnvironMem::Environ => {
                let environ: NonNull<*const libc::c_char> =
                    NonNull::new(unsafe { bionic_sys::libc_get_environ_raw() as *mut _ })?;

                let ptr = NonNull::new(unsafe { *environ.as_ptr().add(*self.start) as *mut _ })?;
                *self.start += 1;
                let s = unsafe { CStr::from_ptr::<'static>(ptr.as_ptr()) };
                s
            }
        };
        let entry = EnvEntry { entry };
        Some(entry.split_key_value())
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        let find_remain = || -> Option<usize> {
            match &self.env {
                EnvironMem::Proc(env) => {
                    Some(env[*self.start..].iter().filter(|&&x| x == b'\0').count())
                }
                EnvironMem::Environ => {
                    let environ: NonNull<*const libc::c_char> =
                        NonNull::new(unsafe { bionic_sys::libc_get_environ_raw() as *mut _ })?;
                    let mut cur = unsafe { environ.as_ptr().add(*self.start) };
                    let mut count = 0;
                    loop {
                        let env: *const libc::c_char = unsafe { *cur };
                        if NonNull::new(env as *mut libc::c_char).is_none() {
                            break;
                        }
                        count += 1;
                        cur = unsafe { cur.add(1) };
                    }
                    Some(count)
                }
            }
        };
        let remain = find_remain();
        (remain.unwrap_or(0), remain)
    }
}

pub struct VarsOs {
    env: EnvironMem,
    start: usize,
}

impl VarsOs {
    pub(crate) fn new() -> Self {
        let environ = unsafe { bionic_sys::libc_get_environ_raw() };
        let env = if environ.is_null() {
            static PROC: OnceBox<Vec<u8>> = OnceBox::new();
            let proc = PROC.get_or_init(|| {
                alloc::boxed::Box::new(
                    crate::fs::read("/proc/self/environ").unwrap_or_else(|_| Vec::new()),
                )
            });
            EnvironMem::Proc(proc.as_ref())
        } else {
            EnvironMem::Environ
        };
        Self { env, start: 0 }
    }
    #[cfg(test)]
    fn from_bytes(bytes: &'static [u8]) -> Self {
        Self {
            env: EnvironMem::Proc(bytes),
            start: 0,
        }
    }

    /// Given a key finds the corresponding value.
    /// This is a specialized function that avoids allocating OsStrings for each
    /// key value pair as it traverses the environment.
    fn find_key<P>(&self, mut predicate: P) -> Option<OsString>
    where
        P: FnMut(&OsStr) -> bool,
    {
        let mut start = self.start;
        EnvIter::new(&self.env, &mut start)
            .find(|(key, _value)| predicate(key))
            .map(|(_key, value)| OsStr::from_bytes(value.to_bytes()).to_os_str())
    }
}

#[deprecated(note = "var_os is deprecated in favor of using SecureEnv")]
pub fn var_os<K>(key: K) -> Option<OsString>
where
    K: AsRef<OsStr>,
{
    VarsOs::new().find_key(|v| v == key.as_ref())
}

pub enum VarError {
    NotPresent,
    NotUnicode(OsString),
}

#[deprecated(note = "var is deprecated in favor of using SecureEnv")]
pub fn var<K>(key: K) -> Result<String, VarError>
where
    K: AsRef<OsStr>,
{
    #[allow(deprecated)]
    let env = var_os(key).ok_or(VarError::NotPresent)?;
    match core::str::from_utf8(env.as_ref().as_bytes()) {
        Ok(v) => Ok(v.to_string()),
        Err(_) => Err(VarError::NotUnicode(env)),
    }
}

impl Iterator for VarsOs {
    type Item = (OsString, OsString);

    fn next(&mut self) -> Option<Self::Item> {
        EnvIter::new(&self.env, &mut self.start)
            .next()
            .map(|(key, value)| {
                (
                    key.to_os_str(),
                    OsStr::from_bytes(value.to_bytes()).to_os_str(),
                )
            })
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let mut start = self.start;
        EnvIter::new(&self.env, &mut start).size_hint()
    }
}

#[deprecated(note = "vars_os is deprecated in favor of using SecureEnv")]
pub fn vars_os() -> VarsOs {
    VarsOs::new()
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::ffi::OsStrExt;
    use std::collections::HashMap;
    use std::io::Write;

    #[test]
    fn env() {
        let vars: HashMap<OsString, OsString> = [
            ("SHELL", "/bin/bash"),
            ("HOME", "/home/foo"),
            ("EDITOR", "emacs"),
            ("FOO", "/"),
            ("FOO2", ""),
            (
                "SOMETHING",
                "emacs=sdjfolasdfdsfajslkdfjlasdf=sdsdfjlaksjdf",
            ),
        ]
        .into_iter()
        .map(|(n, v)| {
            (
                OsStr::from_bytes(n.as_bytes()).to_os_str(),
                OsStr::from_bytes(v.as_bytes()).to_os_str(),
            )
        })
        .collect();

        let mut buf = vec![];
        for (n, v) in &vars {
            buf.write_all(n.as_os_str().as_bytes()).unwrap();
            buf.write_all(&[b'=']).unwrap();
            buf.write_all(v.as_os_str().as_bytes()).unwrap();
            buf.write_all(&[b'\0']).unwrap();
        }
        let parsed: HashMap<_, _> = VarsOs::from_bytes(Box::leak(Box::new(buf))).collect();

        assert_eq!(parsed, vars);
    }
}
