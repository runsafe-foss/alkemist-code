/* Copyright (c) 2022 RunSafe Security Inc. */
extern crate alloc;

use crate::ffi::OsStrExt;
use crate::io::{AsRawFd, OwnedFd, RawFd, Result};
use crate::path::Path;
use crate::posix::{Posix, OS};
use crate::{Gid, Pid, Uid};
use alloc::ffi::CString;
use core::time::Duration;

pub struct UnixStream {
    read_timeout: Option<Duration>,
    write_timeout: Option<Duration>,
    socket: OwnedFd,
}

#[derive(Debug)]
pub struct SocketCred {
    pub uid: Uid,
    pub gid: Gid,
    pub pid: Pid,
}

impl AsRawFd for UnixStream {
    fn as_raw_fd(&self) -> RawFd {
        self.socket.as_raw_fd()
    }
}

impl UnixStream {
    pub fn connect<P>(path: P) -> Result<Self>
    where
        P: AsRef<Path>,
    {
        let mut sock_addr: libc::sockaddr_un = libc::sockaddr_un {
            sun_family: libc::AF_UNIX as u16,
            sun_path: [0; 108],
        };
        let cstr = CString::new(path.as_ref().as_os_str().as_bytes())
            .map_err(|_| crate::io::Error::InputContainsNull)?;
        let cstr = cstr.to_bytes_with_nul();
        if cstr.len() <= sock_addr.sun_path.len() {
            sock_addr.sun_path[..cstr.len()]
                .iter_mut()
                .zip(cstr.iter().copied())
                .for_each(|(w, r)| *w = r as _);
        } else {
            return Err(crate::io::Error::Unknown);
        }

        let socket = match Posix::retval_is_err(unsafe {
            bionic_sys::socket(libc::AF_UNIX, libc::SOCK_STREAM, 0)
        }) {
            Ok(v) => unsafe { OwnedFd::from_raw_fd(v) },
            Err(_) => return Err(crate::io::Error::Unknown),
        };

        match Posix::retval_is_err(unsafe {
            bionic_sys::connect(
                socket.as_raw_fd(),
                (&sock_addr) as *const _ as *mut _,
                (core::mem::size_of_val(&sock_addr.sun_family) + cstr.len()) as u32,
            )
        }) {
            Ok(0) => {}
            _ => return Err(crate::io::Error::Unknown),
        };

        Ok(Self {
            socket,
            read_timeout: None,
            write_timeout: None,
        })
    }

    pub fn set_nonblocking(&self, nonblocking: bool) -> Result<()> {
        #[cfg(target_pointer_width = "32")]
        let fcntl = bionic_sys::fcntl64;
        #[cfg(target_pointer_width = "64")]
        let fcntl = bionic_sys::fcntl;

        let mut flags =
            match Posix::retval_is_err(unsafe { fcntl(self.socket.as_raw_fd(), libc::F_GETFL, 0) })
            {
                Err(_) => return Err(crate::io::Error::Unknown),
                Ok(f) => f,
            };
        if nonblocking {
            flags |= libc::O_NONBLOCK
        } else {
            flags &= !libc::O_NONBLOCK
        }
        Posix::retval_is_err(unsafe { fcntl(self.socket.as_raw_fd(), libc::F_SETFL, flags) })
            .map_err(|_| crate::io::Error::Unknown)?;
        Ok(())
    }

    pub fn set_read_timeout(&mut self, timeout: Option<Duration>) -> Result<()> {
        self.read_timeout = timeout;
        Ok(())
    }

    pub fn set_write_timeout(&mut self, timeout: Option<Duration>) -> Result<()> {
        self.read_timeout = timeout;
        Ok(())
    }

    fn poll(&self, timeout: Option<Duration>, events: i16) -> Result<()> {
        let timeout = if let Some(timeout) = timeout {
            timeout
        } else {
            return Ok(());
        };
        let pfd = &[libc::pollfd {
            fd: self.socket.as_raw_fd(),
            events,
            revents: 0,
        }];
        let ts = libc::timespec {
            tv_sec: timeout.as_secs().try_into().unwrap(),
            tv_nsec: 0,
        };
        match Posix::retval_is_err(unsafe {
            bionic_sys::ppoll(
                pfd.as_ptr() as *const _ as *mut _,
                pfd.len().try_into().unwrap(),
                &ts as *const _ as *mut _,
                core::ptr::null_mut(),
                0,
            )
        }) {
            Err(_) => Err(crate::io::Error::Unknown),
            Ok(0) => Err(crate::io::Error::Timeout),
            Ok(_) => Ok(()),
        }
    }

    pub fn cred(&mut self) -> Result<SocketCred> {
        use libc::{c_void, socklen_t, ucred};
        use libc::{SOL_SOCKET, SO_PEERCRED};
        let mut ucred = ucred {
            pid: 0,
            uid: 0,
            gid: 0,
        };
        let mut ucred_len = core::mem::size_of::<ucred>() as socklen_t;
        let ret = Posix::retval_is_err(unsafe {
            bionic_sys::getsockopt(
                self.socket.as_raw_fd(),                    // sockfd
                SOL_SOCKET,                                 // level
                SO_PEERCRED,                                // optname
                &mut ucred as *mut _ as *mut c_void,        // optval
                &mut ucred_len as *mut _ as *mut socklen_t, // optlen
            )
        });
        match ret {
            Ok(_) => Ok(SocketCred {
                uid: Uid(ucred.uid),
                pid: Pid(ucred.pid as u32),
                gid: Gid(ucred.gid),
            }),
            Err(libc::EACCES) => Err(crate::io::Error::PathAccess),
            Err(libc::EAGAIN) => Err(crate::io::Error::Again),
            Err(libc::EBADF) => Err(crate::io::Error::BadFileDescriptor),
            Err(libc::EINVAL) => Err(crate::io::Error::InvalidData),
            Err(_) => Err(crate::io::Error::Unknown),
        }
    }
}

pub trait CMsg {
    fn sendmsg(&mut self, msg: &libc::msghdr, flags: libc::c_int) -> Result<usize>;
    fn recvmsg(&mut self, msg: &mut libc::msghdr, flags: libc::c_int) -> Result<usize>;
}

impl CMsg for UnixStream {
    fn sendmsg(&mut self, msg: &libc::msghdr, flags: libc::c_int) -> Result<usize> {
        self.poll(self.write_timeout, libc::POLLOUT)?;
        let res = Posix::retval_is_err(unsafe {
            bionic_sys::sendmsg(
                self.socket.as_raw_fd(),
                msg as *const _ as *mut _,
                flags as u32,
            )
        });
        res.map(|v| v.try_into().unwrap()).map_err(|e| match e {
            libc::EACCES => crate::io::Error::PathAccess,
            libc::EAGAIN => crate::io::Error::Again,
            libc::EBADF => crate::io::Error::BadFileDescriptor,
            libc::EINVAL => crate::io::Error::InvalidData,
            e => crate::io::Error::Code(e.into()),
        })
    }
    fn recvmsg(&mut self, msg: &mut libc::msghdr, flags: libc::c_int) -> Result<usize> {
        self.poll(self.read_timeout, libc::POLLIN)?;
        let res = Posix::retval_is_err(unsafe {
            bionic_sys::recvmsg(
                self.socket.as_raw_fd(),
                msg as *const _ as *mut _,
                flags as u32,
            )
        });
        res.map(|v| v.try_into().unwrap()).map_err(|e| match e {
            libc::EACCES => crate::io::Error::PathAccess,
            libc::EAGAIN => crate::io::Error::Again,
            libc::EBADF => crate::io::Error::BadFileDescriptor,
            libc::EINVAL => crate::io::Error::InvalidData,
            e => crate::io::Error::Code(e.into()),
        })
    }
}
impl crate::io::Read for UnixStream {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        self.poll(self.read_timeout, libc::POLLIN)?;

        match Posix::retval_is_err(unsafe {
            bionic_sys::read(self.socket.as_raw_fd(), buf.as_mut_ptr().cast(), buf.len())
        }) {
            Err(_) => Err(crate::io::Error::Unknown),
            Ok(len) => Ok(len as usize),
        }
    }
}

impl crate::io::Write for UnixStream {
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        self.poll(self.write_timeout, libc::POLLOUT)?;

        match Posix::retval_is_err(unsafe {
            bionic_sys::write(self.socket.as_raw_fd(), buf.as_ptr().cast(), buf.len())
        }) {
            Ok(len) => Ok(len as usize),
            Err(_) => Err(crate::io::Error::Unknown),
        }
    }

    fn flush(&mut self) -> Result<()> {
        Ok(())
    }
}
