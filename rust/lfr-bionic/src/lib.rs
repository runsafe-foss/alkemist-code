// Copyright (c) 2022 RunSafe Security Inc.

#![cfg_attr(not(test), no_std)]
#![feature(allocator_api)]
#![feature(alloc_layout_extra)]
#![feature(ptr_as_uninit)]
#![feature(maybe_uninit_slice)]
#![feature(slice_ptr_get)]
#![feature(linkage)]
pub extern crate alloc as sys_alloc;

pub use bionic_sys;
use serde::{Deserialize, Serialize};

#[cfg(not(any(feature = "std", test)))]
#[allow(unused_macros)]
#[macro_export]
macro_rules! println {
    ($($arg:tt)*) => ({
        use $crate::sys_alloc::format;
        use core::convert::TryInto;
        let s = format!($($arg)*) + "\n";
        let stdout = 1;
        unsafe { $crate::bionic_sys::write(stdout, s.as_bytes().as_ptr() as *mut _, s.len().try_into().unwrap());}
    })
}

#[cfg(not(any(feature = "std", test)))]
#[allow(unused_macros)]
#[macro_export]
macro_rules! eprintln {
    ($($arg:tt)*) => ({
        use $crate::sys_alloc::format;
        use core::convert::TryInto;
        let s = format!($($arg)*) + "\n";
        let stderr = 2;
        unsafe { $crate::bionic_sys::write(stderr, s.as_bytes().as_ptr() as *mut _, s.len().try_into().unwrap());}
    })
}

pub mod alloc;
pub mod env;
pub mod ffi;
pub mod fs;
pub mod hidden;
pub mod io;
pub mod mem;
pub mod net;
pub mod path;
pub mod phdr;
pub mod posix;
pub mod process;
pub mod time;

pub mod prelude {
    pub use crate::fs::File;
    pub use core::write;
    pub use sys_alloc::format;
    pub use sys_alloc::string::{String, ToString};
    pub use sys_alloc::{vec, vec::Vec};
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(transparent)]
pub struct Uid(pub u32);
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(transparent)]
pub struct Gid(pub u32);
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(transparent)]
pub struct Pid(pub u32);

pub fn getpid() -> Pid {
    Pid(unsafe { bionic_sys::getpid() } as u32)
}

pub fn getuid() -> Uid {
    Uid(unsafe { bionic_sys::getuid() } as u32)
}

pub fn futex_wait(futex: &core::sync::atomic::AtomicU32) {
    unsafe {
        bionic_sys::futex(
            futex.as_ptr(),
            libc::FUTEX_WAIT | libc::FUTEX_PRIVATE_FLAG,
            0,
            core::ptr::null_mut(),
            core::ptr::null_mut(),
            0,
        );
    }
}

pub fn futex_wake(futex: &core::sync::atomic::AtomicU32) {
    unsafe {
        bionic_sys::futex(
            futex.as_ptr(),
            libc::FUTEX_WAKE | libc::FUTEX_PRIVATE_FLAG,
            !0,
            core::ptr::null_mut(),
            core::ptr::null_mut(),
            0,
        );
    }
}

#[cfg(target_arch = "arm")]
pub fn cache_flush(start: usize, end: usize, flags: i32) -> Result<(), i32> {
    use crate::posix::OS;
    crate::posix::Posix::retval_is_err(unsafe {
        bionic_sys::cacheflush(start as i32, end as i32, flags)
    })
    .map(|_| ())
}
