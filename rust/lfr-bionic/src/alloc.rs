// Copyright (c) 2022 RunSafe Security Inc.

use crate::posix::{Posix, OS};
use core::alloc::GlobalAlloc;
use core::alloc::{AllocError, Layout};
use core::ptr::NonNull;

/// A custom allocator that uses [`bionic_sys::mmap`] and [`bionic_sys::munmap`] to manage
/// memory in a `![no_std]` context. This mimics the work done in `liblfr`.
pub struct MmapAllocator;
unsafe impl GlobalAlloc for MmapAllocator {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        if layout.size() == 0 {
            return core::ptr::null_mut();
        }
        match Posix::retval_is_err(
            bionic_sys::mmap(
                core::ptr::null_mut(),
                layout.size(),
                (bionic_sys::PROT_READ | bionic_sys::PROT_WRITE)
                    .try_into()
                    .unwrap(),
                (bionic_sys::MAP_PRIVATE | bionic_sys::MAP_ANONYMOUS)
                    .try_into()
                    .unwrap(),
                -1,
                0,
            )
            .cast(),
        ) {
            Err(_) => core::ptr::null_mut(),
            Ok(ret) => ret,
        }
    }
    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        if ptr.is_null() || layout.size() == 0 {
            return;
        }
        bionic_sys::munmap(ptr as *mut _, layout.size());
    }

    unsafe fn realloc(&self, ptr: *mut u8, layout: Layout, new_size: usize) -> *mut u8 {
        let flags = bionic_sys::MREMAP_MAYMOVE;
        #[cfg(target_pointer_width = "64")]
        let flags = u64::from(flags);
        let addr = bionic_sys::mremap(ptr as *mut _, layout.size(), new_size, flags);
        match Posix::retval_is_err(addr.cast()) {
            Ok(addr) => addr,
            Err(_) => core::ptr::null_mut(),
        }
    }
}

unsafe impl core::alloc::Allocator for MmapAllocator {
    fn allocate(&self, layout: Layout) -> Result<NonNull<[u8]>, AllocError> {
        let ptr = if layout.size() > 0 {
            NonNull::new(unsafe { self.alloc(layout) }).ok_or(AllocError)?
        } else {
            layout.dangling()
        };

        Ok(NonNull::slice_from_raw_parts(ptr, layout.size()))
    }
    fn allocate_zeroed(&self, layout: Layout) -> Result<NonNull<[u8]>, AllocError> {
        // mmap is guaranteed to return zeroed anonymous pages.
        self.allocate(layout)
    }
    unsafe fn deallocate(&self, ptr: NonNull<u8>, layout: Layout) {
        if layout.size() > 0 {
            self.dealloc(ptr.as_ptr(), layout);
        }
    }

    unsafe fn shrink(
        &self,
        ptr: NonNull<u8>,
        old_layout: Layout,
        new_layout: Layout,
    ) -> Result<NonNull<[u8]>, AllocError> {
        match new_layout.size() {
            0 => {
                self.deallocate(ptr, old_layout);
                self.allocate(new_layout)
            }
            _ => {
                let new_ptr =
                    NonNull::new(self.realloc(ptr.as_ptr(), old_layout, new_layout.size()))
                        .ok_or(AllocError)?;
                Ok(NonNull::slice_from_raw_parts(new_ptr, new_layout.size()))
            }
        }
    }

    unsafe fn grow(
        &self,
        ptr: NonNull<u8>,
        old_layout: Layout,
        new_layout: Layout,
    ) -> Result<NonNull<[u8]>, AllocError> {
        match old_layout.size() {
            0 => {
                self.deallocate(ptr, old_layout);
                self.allocate(new_layout)
            }
            _ => {
                let new_ptr =
                    NonNull::new(self.realloc(ptr.as_ptr(), old_layout, new_layout.size()))
                        .ok_or(AllocError)?;

                Ok(NonNull::slice_from_raw_parts(new_ptr, new_layout.size()))
            }
        }
    }

    unsafe fn grow_zeroed(
        &self,
        ptr: NonNull<u8>,
        old_layout: Layout,
        new_layout: Layout,
    ) -> Result<NonNull<[u8]>, AllocError> {
        let grown = self.grow(ptr, old_layout, new_layout)?;
        grown.as_uninit_slice_mut()[old_layout.size()..]
            .iter_mut()
            .for_each(|u| {
                u.write(0);
            });
        Ok(grown)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use core::mem::MaybeUninit;
    #[test]
    fn test_alloc() {
        let mmap = MmapAllocator {};
        let mem = unsafe { mmap.alloc(Layout::from_size_align(20, 16).unwrap()) };
        assert!(!mem.is_null());
        assert!(mem != 1 as *mut _);

        let mem = unsafe { mmap.alloc(Layout::from_size_align(0, 16).unwrap()) };
        assert!(mem.is_null());
    }

    #[test]
    fn test_allocate() {
        use core::alloc::Allocator;
        let mmap = MmapAllocator {};
        let layout = Layout::from_size_align(4, 16).unwrap();
        let ptr = mmap.allocate_zeroed(layout).expect("allocated");

        {
            let slice = unsafe { ptr.as_uninit_slice_mut() };
            let slice = unsafe { MaybeUninit::slice_assume_init_mut(slice) };

            assert_eq!(slice, &[0, 0, 0, 0]);
            slice.fill(0xff);
            assert_eq!(slice, &[0xff, 0xff, 0xff, 0xff]);
        }
        let new_layout = Layout::from_size_align(2, 16).unwrap();
        let ptr =
            unsafe { mmap.shrink(ptr.as_non_null_ptr(), layout, new_layout) }.expect("allocated");

        {
            let slice = unsafe { ptr.as_uninit_slice_mut() };
            let slice = unsafe { MaybeUninit::slice_assume_init_mut(slice) };

            assert_eq!(slice, &[0xff, 0xff]);
        }
        let layout = new_layout;
        let new_layout = Layout::from_size_align(4, 16).unwrap();
        let ptr = unsafe { mmap.grow_zeroed(ptr.as_non_null_ptr(), layout, new_layout) }
            .expect("allocated");

        {
            let slice = unsafe { ptr.as_uninit_slice_mut() };
            let slice = unsafe { MaybeUninit::slice_assume_init_mut(slice) };

            assert_eq!(slice, &[0xff, 0xff, 0, 0]);
        }
    }

    #[test]
    fn test_vec() {
        let mut v: Vec<u8, _> = Vec::new_in(MmapAllocator);
        assert_eq!(v.capacity(), 0);
        let mut grow = 0;
        let mut prev_cap = v.capacity();
        println!("Growing");
        #[allow(clippy::same_item_push)]
        for _ in 0..0x10000 {
            v.push(1);
            if prev_cap != v.capacity() {
                println!("Grew");
                prev_cap = v.capacity();
                grow += 1;
            }
        }
        assert!(grow >= 3);
        assert!(v.capacity() >= 0x10000);
        println!("Shrink");
        v.resize(0x2000, 0xff);
        v.shrink_to(0x2000);
        assert_eq!(v.capacity(), 0x2000);
        println!("Shrank");
        v.resize(0x1000, 0xff);
        v.shrink_to(0x1000);
        assert_eq!(v.capacity(), 0x1000);
        println!("Shrunk");
        v.clear();
        v.shrink_to(0);
        assert_eq!(v.capacity(), 0);
        println!("Regrow");
        v.resize(10, 0xff);
        assert!(v.capacity() >= 10);
    }
    // This check isn't actually pointless if your memory allocator is broken. Which this is testing.
    #[allow(useless_ptr_null_checks)]
    #[test]
    fn test_allocate_vec() {
        let v: Vec<u8, _> = Vec::with_capacity_in(30, MmapAllocator);
        assert!(!v.as_ptr().is_null());
        assert!(v.as_ptr() != 0x1 as *const u8);
    }
}
