// Copyright (c) 2022 RunSafe Security Inc.
extern crate alloc;
use alloc::borrow::ToOwned;
use alloc::string::String;
use alloc::vec::Vec;
use core::borrow::Borrow;

pub trait OsStringExt {
    fn from_bytes(slice: Vec<u8>) -> Self;
    fn as_bytes(&self) -> Vec<u8>;
}

pub trait OsStrExt {
    fn from_bytes(slice: &[u8]) -> &Self;
    fn as_bytes(&self) -> &[u8];
}

#[derive(Clone, PartialEq, Eq, Hash, Debug, PartialOrd, Ord)]
pub struct OsString {
    inner: Vec<u8>,
}

impl OsString {
    pub fn as_os_str(&self) -> &OsStr {
        self.as_ref()
    }
}
impl OsStrExt for OsStr {
    fn from_bytes(slice: &[u8]) -> &Self {
        unsafe { core::mem::transmute(slice) }
    }
    fn as_bytes(&self) -> &[u8] {
        &self.inner
    }
}
impl OsStringExt for OsString {
    fn from_bytes(slice: Vec<u8>) -> Self {
        OsString { inner: slice }
    }
    fn as_bytes(&self) -> Vec<u8> {
        self.inner.clone()
    }
}
impl AsRef<OsStr> for String {
    fn as_ref(&self) -> &OsStr {
        unsafe { core::mem::transmute(self.as_bytes()) }
    }
}

impl AsRef<OsStr> for str {
    fn as_ref(&self) -> &OsStr {
        unsafe { core::mem::transmute(self) }
    }
}

impl AsRef<OsStr> for [u8] {
    fn as_ref(&self) -> &OsStr {
        unsafe { core::mem::transmute(self) }
    }
}

impl AsRef<OsStr> for OsStr {
    fn as_ref(&self) -> &OsStr {
        unsafe { core::mem::transmute(self) }
    }
}

impl AsRef<OsStr> for OsString {
    fn as_ref(&self) -> &OsStr {
        unsafe { core::mem::transmute(self.inner.as_slice()) }
    }
}

impl core::ops::Deref for OsString {
    type Target = OsStr;
    fn deref(&self) -> &OsStr {
        self.as_ref()
    }
}

#[derive(PartialEq, Eq, Hash, Ord, PartialOrd)]
pub struct OsStr {
    inner: [u8],
}

impl OsStr {
    pub fn to_os_str(&self) -> OsString {
        OsString {
            inner: self.inner.to_vec(),
        }
    }
    pub fn to_str(&self) -> Option<&str> {
        alloc::str::from_utf8(&self.inner).ok()
    }
}

impl Borrow<OsStr> for OsString {
    fn borrow(&self) -> &OsStr {
        self.as_ref()
    }
}

impl ToOwned for OsStr {
    type Owned = OsString;
    fn to_owned(&self) -> Self::Owned {
        OsString {
            inner: self.as_bytes().to_vec(),
        }
    }
}
