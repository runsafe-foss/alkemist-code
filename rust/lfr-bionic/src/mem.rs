/* Copyright (c) 2022 RunSafe Security Inc. */
extern crate alloc;
use crate::io::{AsFd, AsRawFd, BorrowedFd};
use crate::posix::{Posix, OS};
use core::ops::{BitOr, BitOrAssign};
use core::ops::{Deref, DerefMut};
use core::ptr::NonNull;

#[derive(PartialEq, Debug, Copy, Clone)]
pub enum MemPerm {
    R = 1 << 0,
    W = 1 << 1,
    X = 1 << 2,
}

#[derive(PartialEq, Debug, Copy, Clone, Default)]
pub struct MemPerms(u32);

impl MemPerms {
    pub fn read(&self) -> bool {
        self.0 & MemPerm::R as u32 != 0
    }
    pub fn write(&self) -> bool {
        self.0 & MemPerm::W as u32 != 0
    }
    pub fn execute(&self) -> bool {
        self.0 & MemPerm::X as u32 != 0
    }
    fn bionic_perms(&self) -> u32 {
        let mut perms = 0;
        if self.read() {
            perms |= bionic_sys::PROT_READ
        };
        if self.write() {
            perms |= bionic_sys::PROT_WRITE
        }
        if self.execute() {
            perms |= bionic_sys::PROT_EXEC
        }

        perms
    }
    pub fn set(&self, perm: MemPerm) -> MemPerms {
        *self | perm
    }

    pub fn clear(&self, perm: MemPerm) -> MemPerms {
        MemPerms(self.0 & !(perm as u32))
    }

    pub fn from_phdr_flags(flags: u32) -> MemPerms {
        use goblin::elf::program_header as PH;
        let mut perms = MemPerms::default();
        if flags & PH::PF_R != 0 {
            perms |= MemPerm::R;
        }
        if flags & PH::PF_W != 0 {
            perms |= MemPerm::W;
        }
        if flags & PH::PF_X != 0 {
            perms |= MemPerm::X;
        }

        perms
    }
}

impl core::fmt::Display for MemPerms {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> Result<(), core::fmt::Error> {
        if self.read() {
            write!(f, "R")?;
        } else {
            write!(f, "-")?;
        }
        if self.write() {
            write!(f, "W")?;
        } else {
            write!(f, "-")?;
        }
        if self.execute() {
            write!(f, "X")?;
        } else {
            write!(f, "-")?;
        }
        Ok(())
    }
}
impl BitOr<MemPerms> for MemPerms {
    type Output = MemPerms;

    fn bitor(self, rhs: MemPerms) -> Self::Output {
        MemPerms(self.0 | rhs.0)
    }
}

impl BitOrAssign<MemPerms> for MemPerms {
    fn bitor_assign(&mut self, rhs: MemPerms) {
        *self = *self | rhs;
    }
}

impl BitOr<MemPerm> for MemPerms {
    type Output = MemPerms;

    fn bitor(self, rhs: MemPerm) -> Self::Output {
        MemPerms(self.0 | rhs as u32)
    }
}

impl BitOrAssign<MemPerm> for MemPerms {
    fn bitor_assign(&mut self, rhs: MemPerm) {
        *self = *self | rhs;
    }
}

impl BitOr<MemPerm> for MemPerm {
    type Output = MemPerms;

    fn bitor(self, rhs: MemPerm) -> Self::Output {
        MemPerms(self as u32 | rhs as u32)
    }
}

impl From<MemPerm> for MemPerms {
    fn from(mp: MemPerm) -> MemPerms {
        MemPerms(mp as u32)
    }
}

#[derive(Debug)]
pub struct MemPermErr;

#[derive(Debug)]
struct MemInner {
    ptr: NonNull<u8>,
    len: usize,
    perms: MemPerms,
}

#[derive(Debug)]
pub struct Mem {
    inner: MemInner,
}

#[derive(Debug)]
pub struct MemMut {
    inner: MemInner,
}

#[derive(Debug)]
pub struct UnmapErr;

macro_rules! mem_impl {
    ($name:ident) => {
        impl $name {
            /// # Safety
            /// You need to have exlusive ownership fo the specified memory. No other references
            /// are allowed to exist.
            pub unsafe fn from_raw(ptr: NonNull<u8>, len: usize, perms: MemPerms) -> Self {
                Self {
                    inner: MemInner { ptr, len, perms },
                }
            }

            pub fn len(&self) -> usize {
                self.inner.len
            }
            pub fn is_empty(&self) -> bool {
                self.inner.len == 0
            }
            pub fn addr(&self) -> NonNull<u8> {
                self.inner.ptr
            }
            pub fn perms(&self) -> MemPerms {
                self.inner.perms
            }
            /// # Safety
            /// It is unsafe to change permissions because you can set memory you are
            /// currently executing or reading to unreadable which will cause a crash.
            pub unsafe fn change_perms(&mut self, new: MemPerms) -> Result<(), MemPermErr> {
                if self.inner.perms == new {
                    return Ok(());
                }
                self.force_change_perms(new)
            }
            /// # Safety
            /// It is unsafe to change permissions because you can set memory you are
            /// currently executing or reading to unreadable which will cause a crash.
            pub unsafe fn force_change_perms(&mut self, new: MemPerms) -> Result<(), MemPermErr> {
                let (addr, sz) = self.page_aligned();
                let res = Posix::retval_is_err(unsafe {
                    bionic_sys::mprotect(
                        addr as *mut _,
                        sz.try_into().unwrap(),
                        new.bionic_perms().try_into().unwrap(),
                    )
                });
                match res {
                    Ok(0) => {
                        self.inner.perms = new;
                        Ok(())
                    }
                    _ => Err(MemPermErr),
                }
            }
            pub fn slice(&self) -> &[u8] {
                assert!(self.inner.perms.read());
                unsafe { core::slice::from_raw_parts(self.inner.ptr.as_ptr(), self.inner.len) }
            }
            pub fn range(&self) -> core::ops::Range<usize> {
                self.inner.ptr.as_ptr() as usize..self.inner.ptr.as_ptr() as usize + self.inner.len
            }

            fn page_aligned(&self) -> (*mut u8, usize) {
                let paged_addr = Posix::page_align(self.inner.ptr.as_ptr() as usize);
                let paged_size = (self.inner.ptr.as_ptr() as usize + self.inner.len) - paged_addr;

                (paged_addr as *mut _, paged_size)
            }

            pub fn unmap(self) -> Result<(), UnmapErr> {
                match Posix::retval_is_err(unsafe {
                    bionic_sys::munmap(self.inner.ptr.as_ptr() as *mut _, self.inner.len)
                }) {
                    Ok(0) => Ok(()),
                    _ => Err(UnmapErr),
                }
            }

            pub fn drop_unmap(self) -> MemUnmap<Self> {
                MemUnmap { mem: Some(self) }
            }
        }

        impl Deref for $name {
            type Target = [u8];
            fn deref(&self) -> &Self::Target {
                self.slice()
            }
        }

        impl UnmappableMemory for $name {
            fn unmap(self) -> Result<(), UnmapErr> {
                $name::unmap(self)
            }
        }

        impl Memory for $name {
            fn addr(&self) -> NonNull<u8> {
                $name::addr(self)
            }
            fn range(&self) -> core::ops::Range<usize> {
                $name::range(self)
            }

            unsafe fn force_change_perms(&mut self, new: MemPerms) -> Result<(), MemPermErr> {
                $name::force_change_perms(self, new)
            }
        }
    };
}

mem_impl! { Mem }
mem_impl! { MemMut }

impl MemMut {
    pub fn slice_mut(&mut self) -> &mut [u8] {
        assert!(self.inner.perms.read() && self.inner.perms.write());
        unsafe { core::slice::from_raw_parts_mut(self.inner.ptr.as_ptr(), self.inner.len) }
    }
}

impl DerefMut for MemMut {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.slice_mut()
    }
}

pub trait Memory {
    fn addr(&self) -> NonNull<u8>;
    fn range(&self) -> core::ops::Range<usize>;
    /// # Safety
    /// It is unsafe to change perms on actively used memory as you may make it
    /// inaccessible to yourself. Additionally, adding executable permissions is
    /// always dangerous.
    unsafe fn force_change_perms(&mut self, new: MemPerms) -> Result<(), MemPermErr>;
}

pub trait UnmappableMemory {
    fn unmap(self) -> Result<(), UnmapErr>;
}

pub struct MemUnmap<T>
where
    T: UnmappableMemory,
{
    mem: Option<T>,
}

impl<T> MemUnmap<T>
where
    T: UnmappableMemory,
{
    pub fn persist(mut self) -> T {
        self.mem.take().unwrap()
    }
}

impl<T> Drop for MemUnmap<T>
where
    T: UnmappableMemory,
{
    fn drop(&mut self) {
        if let Some(m) = self.mem.take() {
            m.unmap().unwrap();
        }
    }
}

impl<T> Deref for MemUnmap<T>
where
    T: UnmappableMemory,
{
    type Target = T;
    fn deref(&self) -> &Self::Target {
        self.mem.as_ref().unwrap()
    }
}

impl<T> DerefMut for MemUnmap<T>
where
    T: UnmappableMemory,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.mem.as_mut().unwrap()
    }
}

#[derive(Debug)]
pub struct MemBuilder<'a> {
    hint: Option<(NonNull<u8>, bool)>,
    file: Option<BorrowedFd<'a>>,
    offset: Option<usize>,
    flags: u32,
    private: bool,
    exec: bool,
}

impl Default for MemBuilder<'_> {
    fn default() -> Self {
        Self {
            hint: None,
            file: None,
            offset: None,
            private: true,
            exec: false,
            flags: 0,
        }
    }
}

#[derive(Debug)]
pub struct MemBuildErr;

impl<'a> MemBuilder<'a> {
    /// # Safety
    /// It is unsafe to specify an exact address because it may allow
    /// you to alias existing memory.
    pub unsafe fn with_addr(&mut self, hint: NonNull<u8>) -> &mut Self {
        self.hint = Some((hint, true));
        self
    }
    pub fn with_hint(&mut self, hint: NonNull<u8>) -> &mut Self {
        self.hint = Some((hint, false));
        self
    }
    /// # Safety
    /// It is unsafe to reference a file as the contents of the file may change or allow
    /// multiple aliased mmap sections of the same file.
    pub unsafe fn with_fd<Fd: AsFd>(&mut self, file: &'a Fd, offset: usize) -> &mut Self {
        self.file = Some(file.as_fd());
        self.offset = Some(offset);
        self
    }
    pub fn with_shared(&mut self, shared: bool) -> &mut Self {
        self.private = !shared;
        self
    }
    pub fn with_exec(&mut self, exec: bool) -> &mut Self {
        self.exec = exec;
        self
    }
    fn build_inner(&self, mut perms: MemPerms, len: usize) -> Result<(MemPerms, *mut u8), i32> {
        let (hint, fixed) = self
            .hint
            .map(|(hint, b)| (hint.as_ptr(), b))
            .unwrap_or((core::ptr::null_mut(), false));

        if self.exec {
            perms |= MemPerm::X;
        }
        let mut flags = self.flags;
        if self.private {
            flags |= bionic_sys::MAP_PRIVATE
        } else {
            flags |= bionic_sys::MAP_SHARED
        }
        if self.file.is_none() {
            flags |= bionic_sys::MAP_ANONYMOUS
        }
        if fixed {
            flags |= bionic_sys::MAP_FIXED
        }
        let res = unsafe {
            bionic_sys::mmap(
                hint.cast(),
                len,
                perms.bionic_perms() as i32,
                flags as i32,
                self.file.map(|x| x.as_raw_fd()).unwrap_or(-1),
                self.offset.unwrap_or(0).try_into().unwrap(),
            )
        };
        Posix::retval_is_err(res as *mut _).map(|r| (perms, r))
    }
    pub fn build(&self, len: usize) -> Result<MemUnmap<Mem>, MemBuildErr> {
        match self.build_inner(MemPerm::R.into(), len) {
            Ok((perms, res)) => {
                let new_mem =
                    unsafe { Mem::from_raw(NonNull::new(res as *mut _).unwrap(), len, perms) };
                Ok(new_mem.drop_unmap())
            }
            Err(_) => Err(MemBuildErr),
        }
    }
    pub fn build_mut(&self, len: usize) -> Result<MemUnmap<MemMut>, MemBuildErr> {
        match self.build_inner(MemPerm::W | MemPerm::R, len) {
            Ok((perms, res)) => {
                let new_mem =
                    unsafe { MemMut::from_raw(NonNull::new(res as *mut _).unwrap(), len, perms) };
                Ok(new_mem.drop_unmap())
            }
            Err(_) => Err(MemBuildErr),
        }
    }
}
