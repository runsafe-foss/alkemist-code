/* Copyright (c) 2022 RunSafe Security Inc. */
pub trait RetVal {
    fn to_val(&self) -> i64;
}

impl<T> RetVal for *mut T {
    fn to_val(&self) -> i64 {
        *self as i64
    }
}

impl RetVal for usize {
    fn to_val(&self) -> i64 {
        *self as i64
    }
}

impl RetVal for isize {
    fn to_val(&self) -> i64 {
        *self as i64
    }
}

impl RetVal for i64 {
    fn to_val(&self) -> i64 {
        *self
    }
}

impl RetVal for i32 {
    fn to_val(&self) -> i64 {
        (*self).into()
    }
}

/// A catch-all OS specific trait that keeps most of the global values and helpful functions
/// together.
pub trait OS {
    /// Default alignment for functions.
    const FUNCTION_P2ALIGN: u32;
    /// The number of bits to shift a value to be page aligned.
    const PAGE_SHIFT: u32;
    /// The size of a page in bytes (usually `1 << Self::PAGE_SHIFT`).
    const PAGE_SIZE: usize;
    /// Whether the platform needs the function offsets preserved.
    const PRESERVE_FUNCTION_OFFSET: bool;

    /// Aligns a given value to the page for this platform.
    fn page_align(addr: usize) -> usize {
        addr & !(Self::PAGE_SIZE - 1)
    }

    /// Aligns a given value to the page for this platform.
    fn next_page_align(addr: usize) -> usize {
        Self::page_align(addr) + Self::PAGE_SIZE
    }

    /// Gives last byte address in page for address (inclusive).,
    fn page_end(addr: usize) -> usize {
        Self::page_align(addr) + Self::PAGE_SIZE - 1
    }

    /// Convert a return value into a more structured error type.
    fn retval_is_err<R: RetVal>(ret: R) -> Result<R, i32>;
}
pub struct Posix;

impl OS for Posix {
    const FUNCTION_P2ALIGN: u32 = 2;
    const PRESERVE_FUNCTION_OFFSET: bool = true;
    const PAGE_SHIFT: u32 = 12;
    const PAGE_SIZE: usize = 1 << Self::PAGE_SHIFT;
    fn retval_is_err<R: RetVal>(ret: R) -> Result<R, i32> {
        match ret.to_val() {
            v @ -4096..=-1 => Err(v.abs() as i32),
            _ => Ok(ret),
        }
    }
}
