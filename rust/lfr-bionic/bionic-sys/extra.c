/* Copyright (c) 2022 RunSafe Security Inc. */
#include "extra.h"

#pragma GCC visibility push(hidden)
void __aeabi_unwind_cpp_pr0(void) __attribute__((weak));
void __aeabi_unwind_cpp_pr0(void) {}
void __aeabi_unwind_cpp_pr1(void) __attribute__((weak));
void __aeabi_unwind_cpp_pr1(void) {}
#pragma GCC visibility pop

extern pid_t vfork(void);
extern int SYSCALL(execve)(const char *process, char *const args[], char *const env[]);
__attribute__((noreturn)) extern void SYSCALL(__exit)(int);

pid_t LIBC(vfork_and_exec)(char *process, char *const args[], char *const env[]) {
    pid_t pid = vfork();
    if (pid == 0) {
        SYSCALL(execve)(process, args, env);
        // The exit status of 127 is the value set by the shell when a command
        // is not found, and POSIX recommends that applications should do the
        // same
        SYSCALL(__exit)(127);
    } else {
        return pid;
    }
}

char **LIBC(get_environ_raw)(void) {
    extern char **environ __attribute__((weak));
    if (&environ != 0 && environ != 0) {
        return environ;
    }
    return 0;
}
