/* Copyright (c) 2022 RunSafe Security Inc. */

__attribute__((visibility("hidden")))
long __set_errno_internal(__attribute__((unused)) int i) {
    return -1;
}
