// Copyright (c) 2022 RunSafe Security Inc.

extern crate bindgen;

use lfr_target::{
    build::{depend, env, env_os},
    Arch, BuildTarget,
};
use regex::Regex;
use std::ffi::OsStr;
use std::os::unix::ffi::OsStrExt;
use std::path::{Path, PathBuf};

fn cc_builder(sysroot: Option<&str>) -> cc::Build {
    let mut build = cc::Build::new_target(false);

    if let Some(sysroot) = sysroot {
        build.flag("--sysroot").flag(sysroot);
    }
    build
}

fn build_bionic(out_path: &Path, sr_arch: Arch, toolchain: Option<&str>) -> PathBuf {
    let tmp_dir = out_path.join("bionic");
    let bionic_dir = Path::new("bionic-src/")
        .canonicalize()
        .expect("failed to canonicalize bionic path");
    if tmp_dir.exists() {
        std::fs::remove_dir_all(&tmp_dir).expect("Couldn't clear old tmp directory");
    }
    std::fs::create_dir_all(&tmp_dir).expect("Can't create generated dir");

    let val = std::process::Command::new("python3")
        .arg(depend(bionic_dir.join("gensyscalls.py")))
        .arg(&tmp_dir)
        .current_dir(&bionic_dir)
        .output()
        .expect("couldn't generate bionic (maybe missing python3?)");

    if !val.status.success() {
        println!("{:?}", std::str::from_utf8(&val.stderr).unwrap());
    }
    assert!(val.status.success(), "Failed to generate bionic");

    let parsed_files = regex::bytes::RegexBuilder::new(r"parse_file: (\S+)")
        .multi_line(true)
        .build()
        .expect("bad regular expression");

    let generated_files = regex::bytes::RegexBuilder::new(r"generating (\S+)")
        .multi_line(true)
        .build()
        .expect("bad regular expression");

    let mut build = cc_builder(toolchain);

    build.include(&bionic_dir);

    let arch_files = match sr_arch {
        Arch::X86_64 => ["vfork.S"].as_slice(),
        Arch::X86 => ["mmap.cpp", "vfork.S"].as_slice(),
        Arch::Arm => ["mmap.cpp"].as_slice(),
        Arch::Arm64 => ["vfork.S"].as_slice(),
    };

    let arch_dir = bionic_dir.join(format!("arch-{}", sr_arch));
    build.include(&arch_dir);
    let arch_files: Vec<_> = arch_files.iter().map(|f| arch_dir.join(f)).collect();

    let default_files: Vec<_> = [
        "libc/open.cpp",
        "stubs/set_errno_internal.c",
        "upstream-netbsd/rand_r.c",
        "upstream-openbsd/time.c",
    ]
    .into_iter()
    .map(|f| bionic_dir.join(f))
    .collect();

    parsed_files.captures_iter(&val.stderr).for_each(|c| {
        let path = Path::new(OsStr::from_bytes(&c[1]));
        depend(bionic_dir.join(path));
    });

    let arch_dir = format!("arch-{}", sr_arch);
    let generated: Vec<_> = generated_files
        .captures_iter(&val.stderr)
        .filter_map(|c| {
            let path = Path::new(OsStr::from_bytes(&c[1]));
            if path.starts_with(&arch_dir) {
                Some(path.to_path_buf())
            } else {
                None
            }
        })
        .map(|p| tmp_dir.join(p))
        .collect();

    for f in generated
        .iter()
        .chain(default_files.iter())
        .chain(arch_files.iter())
    {
        build.file(f);
        if !f.starts_with(&tmp_dir) {
            depend(f);
        }
    }
    build.file(depend("extra.c"));

    build.compile("bionic");
    tmp_dir.join(format!("syscalls-{}.h", sr_arch))
}

fn main() {
    let out_path = PathBuf::from(env_os("OUT_DIR").unwrap());
    let sr_arch = Arch::build_script();

    let toolchain = env("TOOLCHAIN").ok();

    let header = build_bionic(&out_path, sr_arch, toolchain.as_deref());

    println!("Using header {}", header.display());
    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    let mut builder = bindgen::Builder::default()
        // The input header we would like to generate
        // bindings for.
        //.header("wrapper.h")
        .header(header.to_str().unwrap())
        .header(depend("extra.h"))
        .allowlist_type("real_stat64")
        .allowlist_function("_TRaP_syscall_.*")
        .allowlist_function("_TRaP_libc_.*")
        .allowlist_var("PROT_.*")
        .allowlist_var("MAP_.*")
        .allowlist_var("MREMAP_.*")
        .ctypes_prefix("libc")
        .use_core()
        // Tell cargo to invalidate the built crate whenever any of the
        // included header files changed.
        .parse_callbacks(Box::new(bindgen::CargoCallbacks::new()));

    for a in cc::Build::new().get_compiler().args() {
        builder = builder.clang_arg(a.to_str().unwrap());
    }
    builder = builder.detect_include_paths(true);

    let bindings = builder
        // Finish the builder and generate the bindings.
        .generate()
        // Unwrap the Result and panic on failure.
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let bindings = bindings.to_string();

    let re = Regex::new(r"pub fn (?P<link>_TRaP_(syscall|libc)_+(?P<name>[a-zA-Z_0-9]+))").unwrap();
    let bindings = re.replace_all(&bindings, |cap: &regex::Captures| {
        let name = if cap[1].contains("libc") {
            format!("libc_{}", &cap[3])
        } else {
            cap[3].to_string()
        };
        format!(
            "#[link_name=\"{link}\"]\npub fn {name}",
            link = &cap[1],
            name = name,
        )
    });
    std::fs::write(out_path.join("bindings.rs"), bindings.as_ref())
        .expect("Couldn't write bindings");
}
