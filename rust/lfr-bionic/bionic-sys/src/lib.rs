// Copyright (c) 2022 RunSafe Security Inc.

#![no_std]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(deref_nullptr)]

pub const MAP_FAILED: *mut libc::c_void = usize::MAX as *mut libc::c_void;

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
extern "C" {
    pub fn memcpy(dst: *mut i8, src: *mut i8, count: usize);
}
