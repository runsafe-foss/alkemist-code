/* Copyright (c) 2022 RunSafe Security Inc. */
#include <sys/types.h>

#define SYSCALL(name) _TRaP_syscall_##name
#define LIBC(name)    _TRaP_libc_##name

extern pid_t __attribute__((noinline))
LIBC(vfork_and_exec)(char *process, char *const args[], char *const env[]);
extern char **LIBC(get_environ_raw)(void);
