#!/usr/bin/env perl

### THIS IS A HUGE HACK ###
# This script is for ARM compiles on platforms where EABI flags are set. It swaps
# the synthetic `symbols.o` with the next object that it finds. This allows the linker
# to "latch on" to the correct target architecture and build the desired output.
# It also calls the `lfr-real-linker` after swapping. If no `symbols.o` is found, no swap occurs.
#
# The synthetic objects were introduced here:
# https://github.com/rust-lang/rust/commit/773f533eae25129cea7241b74e54f26ce5eebb62
# We beleive the fix would look like the MR: https://github.com/rust-lang/rust/pull/113497
#
# Example Error Messages:
# ```
#           /arm-toolchain/bin/../lib/gcc/arm-openwrt-linux-muslgnueabi/11.2.0/../../../../arm-openwrt-linux-muslgnueabi/bin/ld: error: source object /tmp/rustcNQByMV/libbionic_sys-c74266ecdf7163a0.rlib(6ebc6c04cb82e846-__gettimeofday.o) has EABI version 5, but target /alkemist-code/build/debian/arm-build/src/RandoLib/posix/armv7-unknown-linux-gnueabihf/release/deps/liblfr_shared.so has EABI version 0
#          /arm-toolchain/bin/../lib/gcc/arm-openwrt-linux-muslgnueabi/11.2.0/../../../../arm-openwrt-linux-muslgnueabi/bin/ld: failed to merge target specific data of file /tmp/rustcNQByMV/libbionic_sys-c74266ecdf7163a0.rlib(6ebc6c04cb82e846-__gettimeofday.o)
# ```

use strict;
use warnings;

my @new_args = @ARGV;

my ($symbols_index) = grep { $new_args[$_] =~ m@/symbols\.o$@ } (0 .. @new_args-1);

if (defined $symbols_index) {
    my ($next_obj_index) = grep { $new_args[$_] =~ m@\.o$@ } ($symbols_index +1 .. @new_args-1);
    if ( defined $next_obj_index) {
        my $swap = $new_args[$symbols_index];
        $new_args[$symbols_index] = $new_args[$next_obj_index];
        $new_args[$next_obj_index] = $swap;
    }
}

my ($real_linker_index) = grep { $new_args[$_] =~ m/^(?:-Wl,)?--lfr-real-linker=/ } (0 .. @new_args-1);

my ($real_linker) = ($new_args[$real_linker_index] =~ m/^(?:-Wl,)?--lfr-real-linker=(.+?)$/ig);

splice @new_args, $real_linker_index, 1;
unshift @new_args, $real_linker;

system(@new_args) == 0 || die("Linker failed");
exit 0;
