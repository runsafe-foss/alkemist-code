#!/usr/bin/perl
use strict;
use warnings;
use File::Temp qw/ tempfile tempdir /;


my $txtrp = 0;
my @pos;
foreach $a (@ARGV) {
    if ($a eq "--txtrp") {
        $txtrp = 1;
    } else {
        push(@pos, $a);
    }
}

my ($input, $output) = @pos;

if (not defined $input) {
    die("Need to specify input");
}
my $temp_output = 0;
if (not defined $output || $output eq $input) {
    $output = "$input.tmp";
    $temp_output = 1;
}

if (not defined $ENV{OBJDUMP}) {
    die("Need to set OBJDUMP ")
}

if (not defined $ENV{NM}) {
    die("Need to set NM")
}

if (not defined $ENV{OBJCOPY}) {
    die("Need to set OBJCOPY")
}

my @remove = ();
my %rename = ();
my %redefine = ();

my $objcopy_cmd = "$ENV{OBJCOPY}  $input $output --prefix-symbols=_TRaP_ ";
if (! $txtrp) {
    $objcopy_cmd .= " --prefix-alloc-sections=.lfr ";
}

`$objcopy_cmd`;
if ($?) {
    die("Couldn't prefix sections with .lfr");
}
my @objdump = `$ENV{OBJDUMP} -h $output`;
if ($?) {
    die("Couldn't read sections");
}

my @read_symbols = `$ENV{NM} -P $output`;
if ($?) {
    die("Couldn't read symbols");
}
my @symbols = map { $_ =~ s/^(\S+).*$/$1/; chomp $_; $_ } @read_symbols;

my @sections = grep { 
    m/^\./
} map {
    my $line = $_;
    $line =~ s/^\s*//i;
    my @fields = split(/\s+/, $line);
    $fields[1] or ""
} @objdump;

foreach my $s ( @sections ) {
    if ( $s =~ m/^$/) {
        # ignore empty
    } elsif ($txtrp && $s =~ /^\.(text|rodata)/) {
        $rename{$s} = ".txtrp"
    } elsif ($s =~ m/^(\.lfr)?(\.ARM\.exidx|\.ARM\.extab|^\.eh_frame)/ig) {
        push(@remove, $s);
    } elsif ($s =~ m/^(\.lfr)?\.data\.rel\.ro.*/) {
        my $prefix = $1 ? $1 : "";
        $rename{$s} = "$prefix.data.rel.ro";
    } elsif ($s =~ m/^\.lfr(\.lfr)?\.([^\.]+).*/ig) {
        my $new_sym = $2;
        $new_sym =~ s/^text/entry/ig;
        $rename{$s} = ".lfr.$new_sym"
    } elsif ($s =~ m/^(\.lfr)?\.(llvmbc|llvmcmd)/) {
        push(@remove, $s);
    } else {
        #print("Ignored $s\n");
    }
}

#$rename{".lfr.eh_frame"} = ".eh_frame";
$rename{".lfr.textramp"} = ".textramp";

my @non_trap_syms = (
    "_GLOBAL_OFFSET_TABLE_",
    "_DYNAMIC",
    "dl_iterate_phdr",
    "__android_log_write",
    '$a',
    '$d',
    '$t',
    '$x',
    "__aeabi_unwind_cpp_pr0",
    "__aeabi_unwind_cpp_pr1",
    "__aeabi_uidivmod",
    "__aeabi_idivmod",
    "__aeabi_uidiv",
    "__aeabi_idiv",
    "environ",
    );


my @weaken;

for my $sym (@symbols) {
    if ($sym =~ m/^_TRaP_(\$.+)$/) {
        $redefine{$sym} = $1;
    } elsif (grep { my $non = $_; $sym eq "_TRaP_$non" || $sym eq "_TRaP_rust_extern_with_linkage_$non" } @non_trap_syms) {
        my $new_sym = $sym;
        $new_sym =~ s/^_TRaP_(rust_extern_with_linkage_)?//;
        $redefine{$sym} = $new_sym;
    }
    
    if ($sym =~ /^(_TRaP_)?(__u?div|__aeabi).*$/) {
        if (defined $redefine{$sym}) {
            push(@weaken, $redefine{$sym});
        } else {
            push(@weaken, $sym);
        }
    }
}


my ($fh, $tempname) = tempfile();

for my $k (keys %redefine) {
    print $fh "--redefine-sym $k=$redefine{$k}\n";
}

for my $k (@weaken) {
    print $fh "--weaken-symbol $k\n";
}

for my $k (keys %rename) {
    print $fh "--rename-section $k=$rename{$k}\n";
}

for my $r (@remove) {
    print $fh "--remove-section $r\n";
}

`$ENV{OBJCOPY} \@$tempname $output`;
if ($?) {
    die("objcopy failed to rewrite section information");
}
if ($temp_output) {
    `mv $output $input`;
    if ($?) {
        die("failed to move tmp output to input");
    }

}

exit 0;
